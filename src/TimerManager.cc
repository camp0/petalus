/*
 * Petalus a crypto micro service wallet database.
 *
 * Copyright (C) 2018-2022  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */

#include "TimerManager.h"

namespace petalus {

#if defined(PYTHON_BINDING)

void TimerManager::start_timer(const SharedPointer<Timer> t) {

        t->timer->expires_from_now(boost::posix_time::seconds(t->seconds));
        t->timer->async_wait(boost::bind(&TimerManager::scheduler_handler, this,
                boost::asio::placeholders::error, WeakPointer<Timer>(t)));
}

void TimerManager::stop_timer(const SharedPointer<Timer> t) {

        t->timer->cancel();
}

void TimerManager::scheduler_handler(boost::system::error_code error, const WeakPointer<Timer> wt) {

        if (wt.expired()) {
#if DEBUG
                std::cout << __FILE__ << ":" << __func__ << ":Timer have been free\n";
#endif
                return;
        }

        SharedPointer<Timer> t = wt.lock();

        // Check if the timer have been cancel
        if (error ==  boost::asio::error::operation_aborted) {
#if DEBUG
                std::cout << __FILE__ << ":" << __func__ << ":Timer have been cancel (" << t->seconds << ")\n";
#endif
                timers_.erase(t->seconds);
                return;
        }

        t->executeCallback();

        start_timer(t);

        return;
}

void TimerManager::addTimer(PyObject *callback, int seconds) {

        // The user wants to remove the callback
        if (callback == Py_None) {
                // Find any Timer for that seconds
                if (auto it = timers_.find(seconds); it != timers_.end()) {
                        SharedPointer<Timer> t = (*it).second;

                        stop_timer(t);
                        timers_.erase(it);
                }
        } else {
                // Verify that the object/callback or whatever can be called
                if (PyCallable_Check(callback)) {
                        // Find any Timer for that seconds
                        SharedPointer<Timer> t;
                        auto it = timers_.find(seconds);
                        if (it != timers_.end()) {
                                // The timer exists, reuse
                                t = (*it).second;
                        } else {
                                // New timer
                                t = SharedPointer<Timer>(new Timer(io_));
                                timers_.emplace(seconds, t);
                        }
                        t->setCallbackWithNoArgs(callback);
                        t->seconds = seconds;
                        start_timer(t);
                }
        }
}

std::ostream& operator<< (std::ostream &out, const TimerManager &tm) {

        for (auto &item: tm.timers_) {
                auto t = item.second;

                out << "\t" << "Timer:" << t->getName() << " expires every " << item.first << " secs\n";
        }
        return out;
}

void TimerManager::statistics(std::basic_ostream<char> &out) const {

        out << *this;
}

#endif

} // namespace petalus
