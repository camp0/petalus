/*
 * Petalus a crypto micro service wallet database.
 *
 * Copyright (C) 2018-2022  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */

#include "HTTPUserSession.h"
#include "System.h"

namespace petalus::http {

void HTTPUserSession::handle_get_message(bhttp::response<bhttp::dynamic_body> &response) {

	std::string uri(request.target());

	if (auto it = request.find(boost::beast::string_view(http_session_header_value)); it != request.end()) {
		std::string session(it->value());

		// The session should be the same as the one given on the open operation
		if (sessionid.compare(session) == 0) {
			if (PetalusUriBlock puri(uri); puri.wallet.length() > 0) {
				PetalusBlock block;

				SessionContext ctx(puri.wallet, session);

				if (int32_t bytes = swm->read(ctx, block, puri.folder, puri.block_number); bytes > 0) {

					if (block.type() == BlockType::USR_JSON_STRING)
						response.set(bhttp::field::content_type, "application/json");
					else if (block.type() == BlockType::USR_BINARY_FILE)
						response.set(bhttp::field::content_type, "application/octet-stream");
					else if (block.type() == BlockType::USR_SOURCE_CRYPT) 
						response.set(bhttp::field::content_type, "application/pgp-encrypted");
					else
						response.set(bhttp::field::content_type, "text/plain");

            				boost::beast::ostream(response.body()).write(block.data(), block.length());
					response.content_length(block.length()); 
					response.result(bhttp::status::ok);
					++ gstats->total_blocks_reads;
					++ stats.total_blocks_reads;
				} else {
					std::ostringstream out;
					++ gstats->total_blocks_not_found;
					++ stats.total_blocks_not_found;

					response.result(bhttp::status::not_found);
            				response.set(bhttp::field::content_type, "text/plain");
					out << "Block " << puri.block_number << " not found";
            				boost::beast::ostream(response.body()) << out.str();
					response.content_length(response.payload_size());
				}
			} else {
				++ gstats->total_wrong_wallet_uri;
				++ stats.total_wrong_wallet_uri;

				response.result(bhttp::status::bad_request);
            			response.set(bhttp::field::content_type, "text/plain");
				boost::beast::ostream(response.body()) << "Wallet not valid";
				response.content_length(response.payload_size());
			}
        	} else {
			++ gstats->total_sessions_not_found;
			++ stats.total_sessions_not_found;

			response.result(bhttp::status::not_found);
            		response.set(bhttp::field::content_type, "text/plain");
			boost::beast::ostream(response.body()) << "Session ID not valid for read";
			response.content_length(response.payload_size());
		}
	} else {
		// Check if the uri is status or wallets

		++ gstats->total_sessions_not_found;
		++ stats.total_sessions_not_found;

		response.result(bhttp::status::not_found);
            	response.set(bhttp::field::content_type, "text/plain");
		boost::beast::ostream(response.body()) << "Session ID not valid";
		response.content_length(response.payload_size());
	}

	PINFO << "[" << ipsrc << ":" << portsrc << "] " 
		<< "GET:" << uri
		<< " " << response[bhttp::field::content_type] << " " << response.result_int();	
}


/* The wallet can be open by the user */

void HTTPUserSession::handle_user_open_wallet(bhttp::response<bhttp::dynamic_body> &response) {
	
	boost::beast::string_view content_type = request[bhttp::field::content_type];

	if (content_type.compare("application/json") == 0) {
		std::ostringstream bodydata;
        	bodydata << boost::beast::buffers_to_string(request.body().data());

                nlohmann::json j = nlohmann::json::parse(bodydata.str(), nullptr, false);
                if (j.is_discarded()) {
                        response.set(bhttp::field::content_type, "text/html");
                        response.result(bhttp::status::bad_request);
                        boost::beast::ostream(response.body()) << "Not valid json content";
                        response.content_length(response.payload_size());
                        return;
                }

        	if (j.find("wallet") != j.end()) {
        		// Both parameters should be strings
                	if (j["wallet"].is_string()) {
                		std::string wallet = j["wallet"];
				std::string password = "";

				if ((j.find("password") != j.end())and(j["password"].is_string()))	
                        		password = j["password"];

				// The session is open by the user with password 
				if (sessionid = swm->openSession(wallet, password); sessionid.length() > 0) {
                        		// send the session id
                               		response.set(http_session_header_value, sessionid);
                               		response.result(bhttp::status::ok);
                        	} else {
					WalletOpenCode code = swm->getOpenStatusCode();
					std::ostringstream out;
                               		response.set(bhttp::field::content_type, "text/html");
					if (code == WalletOpenCode::WALLET_WRONG_CREDENTIALS) {
						out << "Wrong credentials";

                               			response.result(bhttp::status::unauthorized);
					} else if (code == WalletOpenCode::WALLET_WRONG_FORMAT) {
						out << "Wrong wallet format";

                               			response.result(bhttp::status::bad_request);
                               		} else if (code == WalletOpenCode::WALLET_NO_EXISTS) {
                                                out << "Wallet no exists";

                                                response.result(bhttp::status::not_found);
					} else {
						out << "Can not open more sessions";

                                		response.result(bhttp::status::internal_server_error);
                        		}
					out << " [error code:" << (int)code << "]";
                               		boost::beast::ostream(response.body()) << out.str();
					response.content_length(out.str().length());
					PWARN << "[" << ipsrc << ":" << portsrc << "] "
						<< out.str();
				}
			} else {
				const constexpr char *message = "Incorrect parameter types";

				PWARN << "[" << ipsrc << ":" << portsrc << "] "
					<< message;

                		response.set(bhttp::field::content_type, "text/html");
                        	response.result(bhttp::status::bad_request);
                        	boost::beast::ostream(response.body()) << message;
				response.content_length(response.payload_size());
                	}
		} else {
			const constexpr char *message = "Parameters not found";

			PWARN << "[" << ipsrc << ":" << portsrc << "] "
				<< message;

        		response.set(bhttp::field::content_type, "text/html");
                	response.result(bhttp::status::bad_request);
                	boost::beast::ostream(response.body()) << message;
			response.content_length(response.payload_size());
        	}
	} else {
		const constexpr char *message = "Incorrect content type for opening the wallet";
		constexpr std::size_t len = std::strlen(message);

		PWARN << "[" << ipsrc << ":" << portsrc << "] "
			<< message;

        	response.set(bhttp::field::content_type, "text/html");
               	response.result(bhttp::status::unsupported_media_type);
               	boost::beast::ostream(response.body()) << message;
		response.content_length(len);
	}
}

void HTTPUserSession::handle_user_write_wallet(bhttp::response<bhttp::dynamic_body> &response, const SessionContext &ctx) {

	std::string uri(request.target());

	if (swm->isReadOnly()) {
                response.set(bhttp::field::content_type, "text/html");
                response.result(bhttp::status::service_unavailable);
                boost::beast::ostream(response.body()) << "System in read only mode";
                response.content_length(response.payload_size());
	} else {
		if (ctx.getWallet().length() > 0) {
			std::ostringstream data;
			boost::beast::string_view content_type = request[bhttp::field::content_type];
			[[maybe_unused]] boost::beast::string_view body = request[bhttp::field::body];
			data << boost::beast::buffers_to_string(request.body().data());

			if (data.str().length() > 0) {
				PetalusUriBlock puri(uri);
				PetalusBlock block(data.str());
				std::string decoded_signature = "";
				BlockType type = BlockType::USR_PLAIN;

				// Check if the write has been signed with a base64 signature
				auto base64sign = request.find(boost::beast::string_view(http_user_signature_header_value));
				if (base64sign != request.end()) {
					if (base64sign->value().length() > 0) {
						std::string signature = std::string(base64sign->value());
						CryptoPP::StringSource(signature, true, new CryptoPP::Base64Decoder(new CryptoPP::StringSink(decoded_signature)));
						block.signature(decoded_signature);
					}
				}

				if (content_type.compare("application/json") == 0)
					type = BlockType::USR_JSON_STRING;
				else if (content_type.compare("application/octet-stream") == 0)
					type = BlockType::USR_BINARY_FILE;
				else if (content_type.compare("application/pgp-encrypted") == 0)
					type = BlockType::USR_SOURCE_CRYPT;

				block.type(type);

				if (int32_t bytes = swm->write(ctx, block, puri.folder); bytes >= (int32_t)block.length()) {
					response.result(bhttp::status::ok);
					response.set(bhttp::field::content_type, "text/plain");
					std::stringstream out;
					int32_t block_number = swm->getLastBlockNumberWritten(ctx.getSession());

					out << "Block " << block_number << " added";	
					if (puri.folder.length() > 0)
						out << " to folder " << puri.folder;
						
					boost::beast::ostream(response.body()) << out.str();
					response.content_length(out.str().length());

					++ gstats->total_blocks_writes;
					++ stats.total_blocks_writes;
				} else {
					++ gstats->total_blocks_write_fails;
					++ stats.total_blocks_write_fails;

					// NOT sure of this response.keep_alive(false); // Should close the connection
					response.result(bhttp::status::internal_server_error);
					response.set(bhttp::field::content_type, "text/plain");
					boost::beast::ostream(response.body()) << "The data can not be written";
					response.content_length(response.payload_size());
				}
			} else {
				++ gstats->total_blocks_write_fails;
				++ stats.total_blocks_write_fails;

				response.result(bhttp::status::bad_request);
				response.set(bhttp::field::content_type, "text/plain");
				boost::beast::ostream(response.body()) << "Data not found";
				response.content_length(response.payload_size());
			}
		} else {
			++ gstats->total_wrong_wallet_uri;
			++ stats.total_wrong_wallet_uri;

			response.result(bhttp::status::bad_request);
			response.set(bhttp::field::content_type, "text/plain");
			boost::beast::ostream(response.body()) << "Wallet not valid";
			response.content_length(response.payload_size());
		}
	}
}

void HTTPUserSession::handle_user_close_wallet(bhttp::response<bhttp::dynamic_body> &response, const std::string &session) {

	if (swm->close(session)) {
        	response.result(bhttp::status::ok);
	} else {
                response.result(bhttp::status::not_found);
                response.set(bhttp::field::content_type, "text/plain");
                boost::beast::ostream(response.body()) << "Session ID not valid for close";
                response.content_length(response.payload_size());
	}
}

// This function manage the users open, close and writes information on the wallet
void HTTPUserSession::handle_post_message(bhttp::response<bhttp::dynamic_body> &response) {

	std::string uri(request.target());

	if (auto it = request.find(boost::beast::string_view(http_session_header_value)); it != request.end()) {
		std::string session(it->value());

		// The generated session id should be the same as the one generated on the 
		// open operation
		if (sessionid.compare(session) == 0) {
			// Check if is a close message
			if (uri.compare(0, PetalusUris::close_wallet.length(), PetalusUris::close_wallet.data()) == 0) {
				handle_user_close_wallet(response, session);
			} else {
				if (PetalusUriBlock puri(uri); puri.wallet.length() > 0) {
					SessionContext ctx(puri.wallet, session);
					handle_user_write_wallet(response, ctx);
				} else {
                                	++ gstats->total_wrong_wallet_uri;
                                	++ stats.total_wrong_wallet_uri;

                                	response.result(bhttp::status::bad_request);
                                	response.set(bhttp::field::content_type, "text/plain");
                                	boost::beast::ostream(response.body()) << "Wallet not valid";
                                	response.content_length(response.payload_size());
				}
			}
		} else {
			++ gstats->total_sessions_not_found;
			++ stats.total_sessions_not_found;

                        response.result(bhttp::status::bad_request);
                        response.set(bhttp::field::content_type, "text/plain");
                        boost::beast::ostream(response.body()) << "Session ID not valid for write";
			response.content_length(response.payload_size());
		}
	} else { 
		// Check if the user wants to open the wallet
		if (uri.compare(0, PetalusUris::open_wallet.length(), PetalusUris::open_wallet.data()) == 0) {
			handle_user_open_wallet(response);
		} else {	
			++ gstats->total_wrong_wallet_uri;
			++ stats.total_wrong_wallet_uri;
			
                       	response.result(bhttp::status::not_found);
                       	response.set(bhttp::field::content_type, "text/plain");
                       	boost::beast::ostream(response.body()) << "Not found";
                       	response.content_length(response.payload_size());
		}
        }

	PINFO << "[" << ipsrc << ":" << portsrc << "] "
		<< "POST:" << uri
		<< " " << request[bhttp::field::content_type] << " " << response.result_int();	
}

void HTTPUserSession::process_request(bhttp::request<bhttp::dynamic_body>&& req) {

	bhttp::response<bhttp::dynamic_body> response;
	response.version(req.version());
        response.keep_alive(req.keep_alive());
        response.set(bhttp::field::server, "Petalus " VERSION);
	response.content_length(0);

	if (proxy_support_) {
		// Find the Forwarded header
		if (boost::beast::string_view forwarded = request[bhttp::field::forwarded]; forwarded.length() > 0) {
       			struct sockaddr_in sa;
			std::string ip(forwarded);

       			if (inet_pton(AF_INET, ip.c_str(), &(sa.sin_addr)))
				ipsrc = std::move(ip);
       		}
	}

	if (swm == nullptr) {
		const constexpr char *message = "No Session Manager available";

		PWARN << "[" << ipsrc << ":" << portsrc << "] "
			<< message;

		response.keep_alive(false);
		response.set(bhttp::field::content_type, "text/html");
		response.result(bhttp::status::service_unavailable);
		boost::beast::ostream(response.body()) << message;
		response.content_length(response.payload_size());

		return send_response(std::move(response));
	}

        switch(req.method()) {
        	case bhttp::verb::get:
	    		handle_get_message(response);
			++ gstats->total_http_method_gets;
			++ stats.total_http_method_gets;
            		break;
        	case bhttp::verb::post:
	    		handle_post_message(response);
			++ gstats->total_http_method_posts;
			++ stats.total_http_method_posts;
            		break;
        	default:
			++ gstats->total_http_method_unknown;
			++ stats.total_http_method_unknown;
            		response.result(bhttp::status::bad_request);
            		response.set(bhttp::field::content_type, "text/plain");
            		boost::beast::ostream(response.body())
                		<< "Invalid request-method '"
                		<< request.method_string().to_string()
                		<< "'";
            		break;
        }

#if defined(PYTHON_BINDING)
	if (security_callback_)
		if (security_callback_->haveCallback()) {
                        try {
                                PythonGilContext gil_lock();

                                boost::python::call<bool>(security_callback_->getCallback(), 
					boost::python::ptr(this));
                        } catch (std::exception &e) {
                                PERROR << e.what();
                        }
                }
#endif
	return send_response(std::move(response));
}

#if defined(PYTHON_BINDING)

void HTTPUserSession::close_connection() {

	PINFO << "[" << ipsrc << ":" << portsrc << "] "
		<< "The TCP connection " << getSocketIPAddress() << ":" << portsrc << " has been closed"; 

	close_session();
}

#endif

} // namespace petalus::http
