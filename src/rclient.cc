/*
 * Petalus a crypto micro service wallet database.
 *
 * Copyright (C) 2018-2022  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <execinfo.h>
#include <unistd.h>
#include <fstream>
#include <iostream>
#include <functional>
#include <cctype>
#include <csignal>
#include <boost/program_options.hpp>
#include <fstream>
#include <boost/date_time/posix_time/posix_time_io.hpp>
#include <termio.h>
#include "HTTPReplicationClient.h"

using namespace petalus;
using namespace petalus::replication;

std::string ipaddress = "";
int port;
bool use_ssl = false;
std::string auth_token = "";

void show_package_banner() {

	std::cout << "\tGCC version:" << __GNUG__ << "." << __GNUC_MINOR__ << "." << __GNUC_PATCHLEVEL__ << std::endl;
	std::cout << "\tBoost version:" << BOOST_VERSION / 100000 << "." << BOOST_VERSION / 100 % 1000 << std::endl;
        std::cout << std::endl;
}

int main(int argc, char* argv[]) {

	namespace po = boost::program_options;
	po::variables_map var_map;

	po::options_description mandatory_ops("Mandatory arguments");
	mandatory_ops.add_options()
		("ip,i",   po::value<std::string>(&ipaddress)->default_value("0.0.0.0"),
			"Listening ip address.")
		("port,p",   po::value<int>(&port),
			"Listening port.")
        	;

        po::options_description optional_ops("Optional arguments");
        optional_ops.add_options()
                ("token,t",    po::value<std::string>(&auth_token),
                        "Sets the authorization token for connect to another peer.")
                ;

	mandatory_ops.add(optional_ops);

	try {
		if (isatty(fileno(stdin))) 
        		po::store(po::parse_command_line(argc, argv, mandatory_ops), var_map);
		else 
        		po::store(po::parse_config_file(std::cin, mandatory_ops), var_map);

        	if (var_map.count("help")) {
            		std::cout << PACKAGE " " VERSION << std::endl;
            		std::cout << mandatory_ops << std::endl;
			std::cout << std::endl;
			//show_gpl_banner();
            		exit(0);
        	}
        	if (var_map.count("version")) {
            		std::cout << PACKAGE " " VERSION << std::endl;
			std::cout << std::endl;
			// show_gpl_banner();
			std::cout << std::endl;
			show_package_banner();
            		exit(0);
        	}
		if ((var_map.count("ip") == 0)or(var_map.count("port") == 0)) {
            		std::cout << PACKAGE " " VERSION << std::endl;
            		std::cout << mandatory_ops << std::endl;
			exit(-1);
		}

		if (var_map.count("ssl")) use_ssl = true;

        	po::notify(var_map);
    	
	} catch(boost::program_options::required_option& e) {
            	std::cout << PACKAGE " " VERSION << std::endl;
        	std::cerr << "Error: " << e.what() << std::endl;
		std::cout << mandatory_ops << std::endl;
        	exit(-1);
	} catch(std::exception& e) {
            	std::cout << PACKAGE " " VERSION << std::endl;
        	std::cerr << "Unsupported option." << e.what() << std::endl;
		std::cout << mandatory_ops << std::endl;
        	exit(-2);
    	}

	boost::asio::io_context io{1};
	boost::asio::ssl::context ssl_ctx {boost::asio::ssl::context::sslv23};

	auto c = SharedPointer<HTTPReplicationClient>(new HTTPReplicationClient(io, ssl_ctx, ipaddress, port)); 
	auto rm = SharedPointer<ReplicationManager>(new ReplicationManager());

	rm->setDistributedKeyToken(auth_token);
	c->enableTLSSupport(true);
	c->setReplicationManager(rm);
        try {
		c->start();
                io.run();

        } catch (const std::exception &e) {
                std::ostringstream msg;
                msg << "ERROR:" << e.what() << std::endl;
                // msg << boost::diagnostic_information(e);
                // std::cout << "Errno:" << errno << " enfile:" << ENFILE  << std::endl;
                //PFATAL << msg.str();
                //error_message(msg.str());
                //void *array[10];
                //size_t size;

                //size = backtrace(array, 10);

                //backtrace_symbols_fd(array, size, STDERR_FILENO);
        }


	return 0;
}

