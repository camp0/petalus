/*
 * Petalus a crypto micro service wallet database.
 *
 * Copyright (C) 2018-2022  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */

#ifndef SRC_PYTHON_HELP_H_
#define SRC_PYTHON_HELP_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

const char *help_swm_data_directory =           "Gets/Sets the directory where all the data will be stored.";
const char *help_swm_password =                 "Gets/Sets the password for encrypt the user blocks.";
const char *help_swm_hash_type =                "Gets/Sets the hash method for the blockchain, sha256, blake2s and\n"
                                                " sha3-256 are supported.";
const char *help_swm_timeout =                  "Gets/Sets the maximum time in seconds the wallet can be open without use it";
const char *help_swm_max_blockfile_size =       "Sets/Gets the maximum file size of the BlockFiles, if this value is \n"
                                                "zero means that the generated files will have no limit, if is non \n"
                                                "zero will have as maximum that size.";
const char *help_swm_show =                     "Shows statistics of the SessionWalletManager.";
const char *help_swm_show_sessions =            "Shows the active Sessions (The open wallets).";
const char *help_swm_show_session =             "Shows specific detail of the active Sessions (The wallets).";
const char *help_swm_close_session =            "Close a specific active Session.";
const char *help_swm_show_triggers =            "Shows the active triggers associated to the Wallets.";
const char *help_swm_inc_memory =               "Increase the number of allocated memory Wallets.";
const char *help_swm_dec_memory =               "Decrease the number of allocated memory Wallets.";
const char *help_swm_add_write_trigger =        "Adds a new write function trigger to a specific Wallet.";
const char *help_swm_add_read_trigger =         "Adds a new read function trigger to a specific Wallet.";
const char *help_swm_close_sessions =           "Close all the active Sessions.";
const char *help_swm_read_only =                "Enable the read only functionality so Wallets just can read information.";
const char *help_swm_rotation_key = 		"Re-encrypt all the data with the new encryption key.";
const char *help_swm_master_blockfile = 	"Sets a master block file for tracking wallet creation.";

const char *help_wallet_name =			"Returns the name of the wallet (The sha256 of the initial data).";
const char *help_wallet_filename = 		"Returns the path where the wallet is stored.";
const char *help_wallet_total_bytes =		"Returns the total number of bytes of the wallet.";
const char *help_wallet_total_blocks =		"Returns the total number of blocks of the wallet.";
const char *help_wallet_block =			"Returns the current block processsed (in memory) by the wallet.";
const char *help_wallet_signed =                "Returns if the block has been signed by the user.";

const char *help_server_address = 		"Gets/Sets the Listen IP address of the server.";
const char *help_server_port = 			"Gets/Sets the TCP port of the server.";
const char *help_server_adm_address = 		"Gets/Sets the Listen administration IP address of the server.";
const char *help_server_adm_port = 		"Gets/Sets the administration TCP port of the server.";
const char *help_server_swm =			"Gets/Sets the SessionWalletManager that will be user on the server.";
const char *help_server_shell =			"Enables or disables the python shell.";
const char *help_server_start = 		"Starts the server to listen TCP connections.";
const char *help_server_run =			"Runs the server.";
const char *help_server_stop = 			"Stops the services of the server.";
const char *help_server_show =			"Shows statistics of the server.";
const char *help_server_show_conns =		"Shows the active TCP connections.";
const char *help_server_add_timer = 		"Add a timer function.";
const char *help_server_priv_key = 		"Sets the SSL private key.";
const char *help_server_private_key_pass =	"Sets the encryption password of the private key.";
const char *help_server_cert = 			"Sets the SSL Certificate.";
const char *help_server_ca = 			"Sets the SSL CA Certificate.";
const char *help_server_mtls = 			"Sets Mutual TLS support.";
const char *help_server_sec_callback =          "Gets/Sets the security callback for all the HTTP requests.";
const char *help_server_show_conn =		"Shows the status of a TCP connection.";
const char *help_server_close_conn =		"Close a TCP connection.";
const char *help_server_proxy_support = 	"Process the Forwarded HTTP header if present on the requests.";
const char *help_server_adm_ip_addresses =      "Sets a list of IP addresss that are allowed to connect to the administration port.";

const char *help_wallet_length =		"Returns the length of the block.";
const char *help_wallet_data =			"Gets/Sets the data of the block.";
const char *help_wallet_metadata =		"Gets/Sets the metadata of a JSON block.";

const char *help_httpcon_ip_address =		"Returns the IP address of the connection.";
const char *help_httpcon_http_errors =		"Total number of HTTP errors of the connection.";
const char *help_httpcon_http_gets =		"Total number of HTTP GETs of the connection.";
const char *help_httpcon_http_posts =		"Total number of HTTP POSTs of the connection.";
const char *help_httpcon_http_unknowns =	"Total number of Unknowns HTTP methods of the connection.";
const char *help_httpcon_no_sessions =		"Total number of HTTP requests without a valid session.";
const char *help_httpcon_wrong_uri_wallet =	"Total number of invalid wallet URIs.";
const char *help_httpcon_body_exceeds =		"Total number of HTTP body limit exceeds.";
const char *help_httpcon_close =		"Close the current HTTP connection.";


#endif // SRC_PYTHON_HELP_H_
