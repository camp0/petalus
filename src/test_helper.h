/*
 * Petalus a crypto micro service wallet database.
 *
 * Copyright (C) 2018-2022  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */

#pragma once
#ifndef SRC_TEST_HELPER_H_
#define SRC_TEST_HELPER_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>
#include <string>
#include <boost/utility/string_ref.hpp>
#include "CryptoOperations.h"
#include "BlockTypes.h"

namespace petalus {
namespace blockchain {

std::string generateRandomString(int length = 128);
int generateRandomInt(int max = 1024 * 1024);
std::string generateTestFile(const std::string wallet, int folder = 0);
std::string generateHexBlockFilename(const std::string &tag = "");
std::string generateBlockFileName(const std::string &tag = "");
std::string generateTestPath(const std::string &wallet);
std::string generateAESPassword(const std::string &pass);
std::string generateBlowfishPassword(const std::string &pass);
std::string generateSHA256(const std::string &pass);
void setDataDirectory(const std::string &path); 
const char *getDataDirectory();
std::string generateBase64PublicKey(PrivateKey &priv, PublicKey &pub, CryptoPP::OID curve = CryptoPP::ASN1::secp256k1());
std::string generateBase64ED25519PublicKey(ED25519PrivateKey &priv, ED25519PublicKey &pub);

} // namespace blockchain
} // namespace petalus

#endif  // SRC_TEST_HELPER_H_

