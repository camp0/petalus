/*
 * Petalus a crypto micro service wallet database.
 *
 * Copyright (C) 2018-2022  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */

#ifndef SRC_HTTPADMINISTRATIONSESSION_H_
#define SRC_HTTPADMINISTRATIONSESSION_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <functional>
#include <iostream>
#include <memory>
#include <set>
#include "Pointer.h"
#include "SessionWalletManager.h"
#include "json.hpp"
#include "HTTPStatistics.h"
#include "HTTPSession.h"
#include "Logger.h"

namespace ip = boost::asio::ip;         // from <boost/asio.hpp>
using tcp = boost::asio::ip::tcp;       // from <boost/asio.hpp>
namespace bhttp = boost::beast::http;    // from <boost/beast/http.hpp>

using namespace petalus::blockchain;

namespace petalus::http {

class HTTPAdministrationSession : public HTTPSession
{
public:
	explicit HTTPAdministrationSession(tcp::socket socket):
		HTTPSession(std::move(socket)) {
		super_::setProcessHandler(std::bind(&HTTPAdministrationSession::process_request, this, std::placeholders::_1));
	}

	explicit HTTPAdministrationSession(tcp::socket socket, boost::asio::ssl::context& ctx):
		HTTPSession(std::move(socket), ctx) {
		super_::setProcessHandler(std::bind(&HTTPAdministrationSession::process_request, this, std::placeholders::_1));
	}

	virtual ~HTTPAdministrationSession() {}

	void setAuthorizationTokenKey(const CryptoPP::SecByteBlock &token) { authorization_token_key_ = token; }
	void setLicenseMaxFoldersPerWallet(int value) { max_folders_per_wallet_ = value; }

	const char *getIP() const { return ipsrc.c_str(); }

private:
	typedef HTTPSession super_;

    	// Asynchronously receive a complete request message.
    	void process_request(bhttp::request<bhttp::dynamic_body>&& req);
	void handle_get_message(bhttp::response<bhttp::dynamic_body> &response);
	void handle_post_message(bhttp::response<bhttp::dynamic_body> &response);
	void handle_create_wallet(bhttp::response<bhttp::dynamic_body> &response);
	void handle_user_open_wallet(bhttp::response<bhttp::dynamic_body> &response);
	void handle_show_status(bhttp::response<bhttp::dynamic_body> &response);
	void handle_show_sessions(bhttp::response<bhttp::dynamic_body> &response);
	void handle_show_session(bhttp::response<bhttp::dynamic_body> &response);
	void handle_show_statistics(bhttp::response<bhttp::dynamic_body> &response);

	bool tls_support_ = false;

	CryptoPP::SecByteBlock authorization_token_key_ {};
	int max_folders_per_wallet_ = 0;
};

} // namespace petalus::http

#endif // SRC_HTTPADMINISTRATIONSESSION_H_
