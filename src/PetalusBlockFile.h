/*
 * Petalus a crypto micro service wallet database.
 *
 * Copyright (C) 2018-2022  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */

#ifndef SRC_PETALUSBLOCKFILE_H_
#define SRC_PETALUSBLOCKFILE_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "CryptoOperations.h"
#include "BlockHashTypes.h"

namespace petalus::blockchain {

// 0123456789ABCDEF
// PETA=FE7A

struct __attribute__((packed)) petalus_header_file {
	static const uint32_t magic_token = 0x1EF17AFE; // FE7AF11E Petalus File
	uint32_t magic = magic_token;
	int16_t version = 1;
	int32_t blocks = 0;
	int32_t last_block_size = 0;
}; 

struct __attribute__((packed)) petalus_block_file_v1 {

	petalus_block_file_v1() {}
	petalus_block_file_v1 (int32_t len, uint8_t btype): petalus_block_file_v1(len, btype, static_cast<uint8_t>(BlockHashTypes::SHA256)) {}
	petalus_block_file_v1 (int32_t len, uint8_t btype, uint8_t htype): length(len), type(btype), hash_type(htype) {}

	static const uint32_t magic_token = 0x0CB17AFE; // FE7AB10C Petalus Block
	uint32_t magic = magic_token;
	int32_t length = 0;
	time_t  timestamp = 0;
	uint8_t type = 0x00;
	uint8_t hash_type;
	uint8_t signature_length = 0x00;
	uint8_t prev_hash[HASH_BLOCK_SIZE] = { 0x00 };
	uint8_t hash[HASH_BLOCK_SIZE] = { 0x00 };
	uint8_t data[0];

	friend std::ostream& operator<< (std::ostream &out, const petalus_block_file_v1 &block) {

		std::string prev_hash, hash;
		std::time_t t = block.timestamp;
		std::tm tm = *std::localtime(&t);

        	out << " Len:" << block.length;
        	out << " Timestamp:" << std::put_time(&tm, "%H:%M:%S %Y/%m/%d");
        	out << " Type:" << (int)block.type;
		out << " SLen:" << (int)block.signature_length;
        	out << " Hash:";
        	if (block.hash_type < static_cast<uint8_t>(BlockHashTypes::MAX_BLOCK_HASH_TYPES))
                	out << hash_handlers[block.hash_type].name << "\n";
        	else
                	out << "WARNING: unknown\n";

		CryptoPP::HexEncoder prev_encoder;
		prev_encoder.Attach( new CryptoPP::StringSink(prev_hash) );
		prev_encoder.Put( block.prev_hash, sizeof(block.prev_hash) );
		prev_encoder.MessageEnd();

		CryptoPP::HexEncoder encoder;
		encoder.Attach( new CryptoPP::StringSink(hash) );
		encoder.Put( block.hash, sizeof(block.hash) );
		encoder.MessageEnd();

        	out << "\t" << "PrevHash:" << prev_hash << "\n";
        	out << "\t" << "Hash:    " << hash << std::endl;
	
		return out;
	}
};

} // namespace petalus::blockchain

#endif  // SRC_PETALUSBLOCKFILE_H_
