/*
 * Petalus a crypto micro service wallet database.
 *
 * Copyright (C) 2018-2022  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */

#include "EncryptionKey.h"

namespace petalus::blockchain {

EncryptionKey::EncryptionKey(CryptoPP::SecByteBlock &key) {

	assign(key.data(), key.size());
}

void EncryptionKey::assign(const uint8_t *data, int len) {

        key_.CleanNew(len);
        key_.Assign(data, len);
#if defined(STAND_ALONE_TEST) || defined(TESTING)
       	decrypted_password_.assign((const char*)data, len);
#endif
}

bool EncryptionKey::password(const std::string &password) {

	std::string source;
	EncryptionKeyType type = EncryptionKeyType::AES;

        if (password.compare(0, 5, "{AES}") == 0) {
                source = password.substr(5, password.length() - 5);
	} else if (password.compare(0, 10, "{BLOWFISH}") == 0) {
                source = password.substr(10, password.length() - 10);
		type = EncryptionKeyType::BLOWFISH;
	}

        std::string destination;
        CryptoPP::StringSource ss(source, true, new CryptoPP::HexDecoder(new CryptoPP::StringSink(destination)));

        PetalusBlock block(destination);
        block.type(BlockType::USR_CRYPT_PLAIN);

        CryptoPP::SecByteBlock key { 32 };
        updateKey(key);

	EncryptionKey ekey(key);

        block.decrypt(ekey);

	if (type == EncryptionKeyType::AES) {
        	if ((block.length() == 16)or(block.length() == 24)or(block.length() == 32)) {
        		// AES passwords can only be 16 byte, 24 byte or 32 length
                	assign((const uint8_t*)block.data(), block.length());
			type_ = type;
			return true;
		}
        } else if (type == EncryptionKeyType::BLOWFISH) {
		if ((block.length() >= CryptoPP::Blowfish::MIN_KEYLENGTH)and(block.length() <= CryptoPP::Blowfish::MAX_KEYLENGTH)) {
        		// BLOWFISH passwords can be on a range 
                	assign((const uint8_t*)block.data(), block.length());
			type_ = type;
			return true;
		}
	}
	return false;
}

bool EncryptionKey::operator == (const EncryptionKey &ekey) {

	return ((type_ == ekey.type())and(key_ == ekey.key()));
}

bool EncryptionKey::operator != (const EncryptionKey &ekey) {

	return ((type_ != ekey.type())or(key_ != ekey.key()));
}

} // namespace petalus::crypto
