/*
 * Petalus a crypto micro service wallet database.
 *
 * Copyright (C) 2018-2022  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */

#ifndef SRC_BLOCKCHAIN_BLOCKHASHTYPES_H_
#define SRC_BLOCKCHAIN_BLOCKHASHTYPES_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>

namespace petalus::blockchain { 

// Supported types of hash blocks
enum class BlockHashTypes : uint8_t {
	SHA256 = 		0x00,
	BLAKE2S = 		0x01,
	SHA3_256 = 		0x02,
	MAX_BLOCK_HASH_TYPES =	0x03
};

} // namespace petalus::blockchain 

#endif  // SRC_BLOCKCHAIN_BLOCKTYPES_H_
