/*
 * Petalus a crypto micro service wallet database.
 *
 * Copyright (C) 2018-2022  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */

#include "Wallet.h"

namespace petalus::blockchain {

void Wallet::setBlockHashType(BlockHashTypes type) {

	hash_func_type_ = type;
}

const char *Wallet::getBlockHashType() const {

	return hash_handlers[static_cast<uint8_t>(hash_func_type_)].name; 
}

void Wallet::set_initial_blockfile(const std::filesystem::path &path, int32_t bytes) {

	// Create a new instance of BlockFile
        if (bfile_ = SharedPointer<BlockFile>(new BlockFile(bytes)); bfile_) {
        	bfile_->open(path);
                bfile_->setBlockHashType(hash_func_type_); // Set the hash model
		bfile_->setEncryptionKey(key_);
                if (bfile_->is_open()) {
                	total_bytes_ += bfile_->getTotalDataBytes();
                        total_blocks_ += bfile_->getTotalBlocks();
                }
	}
}


bool Wallet::is_open() const {

	if (bfile_)
		return bfile_->is_open();

	return false;
}

void Wallet::open(const std::filesystem::path &path, bool create) {
	uint8_t seed[HASH_BLOCK_SIZE] = { 0x00 };

	open(path, create, seed);
}

void Wallet::open(const std::filesystem::path &path, bool create, uint8_t seed[]) {

#ifdef MYDEBUG
	std::cout << __FILE__ << ":" << __func__ << ":path:" << path << " this:" << this << std::endl;
#endif

	std::string filename = wallet_name_ + std::string(".mblk0000");
	path_ = path;
	std::filesystem::path newpath(path);

	if (!std::filesystem::is_directory(newpath))
		newpath.remove_filename();
	
	newpath /= filename;

	// Check if the wallet is new 
	if (!std::filesystem::exists(newpath)and(create == true)) {
		int32_t new_alloc_size = max_blockfile_size_;
		if (max_blockfile_size_ == 0)
                	new_alloc_size = /* 16 MBs */ (BlockFile::BufferBlockSize * boost::iostreams::mapped_file::alignment());

		set_initial_blockfile(newpath, new_alloc_size);
	} else {
		for (int i = 0; i <= 1000; ++i) {
			std::ostringstream out;

			out << wallet_name_ << ".mblk" << std::setfill('0') << std::setw(4) << i;

			newpath.remove_filename();
			newpath /= out.str();

			if (std::filesystem::exists(newpath)) {
				int32_t new_alloc_size = 0; 
				boost::uintmax_t filesize = 0;
				std::error_code ec;

				// Take the size of the file
				filesize = std::filesystem::file_size(newpath, ec);
				if (ec) {
					std::cout << "ERROR on path:" << newpath << " ec:" << ec << std::endl;
					return;
				}

				new_alloc_size = filesize;
				if (max_blockfile_size_ == 0) 
					new_alloc_size += (BlockFile::BufferBlockSize * boost::iostreams::mapped_file::alignment());
				else 
					if (max_blockfile_size_ >= (int32_t)filesize) 
						new_alloc_size = max_blockfile_size_;

				set_initial_blockfile(newpath, new_alloc_size);
			} else {
				break;
			}
		}
	}
	return;
}	

void Wallet::open(const std::filesystem::path &path) {

	// Just open the wallet 
	open(path, false);
}

BlockFileWriteCode Wallet::getWriteErrorCode() const {

	if (bfile_)
		return bfile_->getWriteErrorCode();
	else
		return BlockFileWriteCode::BLOCKFILE_WRITE_FILE_NOT_OPEN;
}

bool Wallet::isLock() const {

	if (bfile_) 
		return bfile_->is_lock();
	else
		return false;
}

void Wallet::writePassword(const std::string &password, const std::string &public_key) {

	std::string public_key_type = "ec_public_key";

	write_sha256_password(password, public_key, public_key_type);
}

void Wallet::writePassword(const std::string &password, const std::string &public_key, const std::string &public_key_type) {

	write_sha256_password(password, public_key, public_key_type);
}

void Wallet::write_sha256_password(const std::string &password, const std::string &public_key, const std::string &public_key_type) {

	if ((bfile_)and(bfile_->is_open())) {
        	// Write the password credential
		nlohmann::json j;
		std::ostringstream out;
		std::string salt;

		generateRandomBytes(salt, 8);

		std::string buffer(salt + password);

		// We store the sha256 of the password + salt
        	CryptoPP::SHA256 hash;
        	uint8_t digest[CryptoPP::SHA256::DIGESTSIZE];

        	hash.CalculateDigest(digest, (const uint8_t*)buffer.c_str(), buffer.length());

        	CryptoPP::HexEncoder encoder;
        	std::string hash_buffer;
        	encoder.Attach(new CryptoPP::StringSink(hash_buffer));
        	encoder.Put(digest, sizeof(digest));
        	encoder.MessageEnd();

        	j["password_sha256"] = hash_buffer;
        	j["salt"] = salt;

		if (public_key.length() > 0) {
			// The format of the public key is on base64 
			j[public_key_type] = public_key;
		}

		out << j;

		PetalusBlock block(out.str(), BlockType::SYS_JSON_BLOCKCHAIN_ACCESS);

        	bfile_->write(block);
	}
}

void Wallet::write_md5_password(const std::string &password) {

        if ((bfile_)and(bfile_->is_open())) {
                // Write the password credential
                nlohmann::json j;
                std::ostringstream out;
		std::string salt;

		generateRandomBytes(salt, 8);

		std::string buffer(salt + password);

                // We store the md5 of the password
                CryptoPP::Weak::MD5 hash;
                uint8_t digest[CryptoPP::Weak::MD5::DIGESTSIZE];

                hash.CalculateDigest(digest, (const uint8_t*)buffer.c_str(), buffer.length());

                CryptoPP::HexEncoder encoder;
                std::string hash_buffer;
                encoder.Attach(new CryptoPP::StringSink(hash_buffer));
                encoder.Put(digest, sizeof(digest));
                encoder.MessageEnd();

                j["password_md5"] = hash_buffer;
                j["salt"] = salt;

                out << j;

                PetalusBlock block(out.str(), BlockType::SYS_JSON_BLOCKCHAIN_ACCESS);

                bfile_->write(block);
        }
}

bool Wallet::valid_sha256_password(const std::string &password) const {

        if ((bfile_)and(password.length() > 0)) {
                // Compare the sha256 with the store password
                CryptoPP::SHA256 hash;
                uint8_t digest[CryptoPP::SHA256::DIGESTSIZE];

		std::string buffer(std::string(bfile_->getSalt()) + password);

                hash.CalculateDigest(digest, (const uint8_t*)buffer.c_str(), buffer.length());

                CryptoPP::HexEncoder encoder;
                std::string hash_buffer;
                encoder.Attach(new CryptoPP::StringSink(hash_buffer));
                encoder.Put(digest, sizeof(digest));
                encoder.MessageEnd();

                if (hash_buffer.compare(bfile_->getAccessPassword()) == 0)
                        return true;
        }
	return false;
}

bool Wallet::valid_md5_password(const std::string &password) const {

        if ((bfile_)and(password.length() > 0)) {
                // Compare the md5 with the store password
                CryptoPP::Weak::MD5 hash;
                uint8_t digest[CryptoPP::Weak::MD5::DIGESTSIZE];

		std::string buffer(std::string(bfile_->getSalt()) + password);

                hash.CalculateDigest(digest, (const uint8_t*)buffer.c_str(), buffer.length());

                CryptoPP::HexEncoder encoder;
                std::string hash_buffer;
                encoder.Attach(new CryptoPP::StringSink(hash_buffer));
                encoder.Put(digest, sizeof(digest));
                encoder.MessageEnd(); 

                if (hash_buffer.compare(bfile_->getAccessPassword()) == 0)
                        return true;
        }
        return false;
}


int32_t Wallet::write(PetalusBlock &block) {

	int32_t bytes = 0;

	if (bfile_) {
		bytes = write_data_on_blockfile(bfile_, block);
		last_block_number_written_ = bfile_->getTotalBlocks();
		last_block_number_ = last_block_number_written_;
	}
	return bytes;
}


int32_t Wallet::write_data_on_blockfile(SharedPointer<BlockFile> &bfile, PetalusBlock &block) {

	int32_t bytes = 0;

        if (!bfile->haveSpaceOnFile(block.length())) { // There is no space on the file
		if (max_blockfile_size_ == 0) { // All the blocks on one file
			int32_t new_alloc_size = bfile->getMaxFileSize() + /* 16 MBs */ (BlockFile::BufferBlockSize * boost::iostreams::mapped_file::alignment());
			
			std::filesystem::path path(bfile->getPath());
			bfile->close();
			bfile.reset();
		
			// reopen the same file with 16 Mb more	
			if (bfile = SharedPointer<BlockFile>(new BlockFile(new_alloc_size)); bfile) {
				bfile->open(path);
				bfile->setBlockHashType(hash_func_type_); // Set the hash model for the new file
				bfile->setEncryptionKey(key_);
			}
		} else {	
			// Needs to close the other file and open a new one and linked
			uint8_t digest[HASH_BLOCK_SIZE];

			bfile->readBlockHash(bfile->getTotalBlocks(), digest);

			std::filesystem::path path(bfile->getPath());
			bfile->close();

			bfile.reset();
			if (bfile = SharedPointer<BlockFile>(new BlockFile(max_blockfile_size_)); bfile) {
				std::string extension(path.extension().c_str());
				std::string strval = extension.substr(extension.length() - 4);
				int value = std::atoi(strval.c_str());
				std::ostringstream out;

				out << ".mblk" << std::setfill('0') << std::setw(4) << (value + 1);

				path.replace_extension(out.str());

				// Link the files
                        	bfile->setSeed(digest);
				bfile->setBlockHashType(hash_func_type_); // Set the hash model for the new file
				bfile->setEncryptionKey(key_);
                        	bfile->open(path);
                	}
		}
        }
	if (bfile->is_open()) {
        	bytes = bfile->write(block);
		if (bytes > 0) {
        		total_bytes_ += bytes;
			++ total_blocks_;
		}
	}
	return bytes;
}

void Wallet::closeFolders() {

	// Close the folders
	for (auto &item: folders_) {
		item.second->close();
	}
	folders_.clear();
}

void Wallet::close() {

	if (bfile_) {
		bfile_->close();
		bfile_.reset();
	}
	closeFolders();
}

void Wallet::reset() {

	wallet_name_ = "";	
	session_id_ = "";
	path_ = "/";
	total_bytes_ = 0; // total user bytes without the file headers;
	total_blocks_ = 0;
	last_operation_ = 0;
	last_block_number_written_ = 0;
	last_block_number_ = 0;
	hash_func_type_ = BlockHashTypes::SHA256;
        max_blockfile_size_ = 0;
	key_.reset();
#if defined(PYTHON_BINDING)
        current_block_ = nullptr;
#endif
	close();
}

int32_t Wallet::read(int32_t number, PetalusBlock &block) {

	int32_t bytes = 0;

	if (bfile_) {
		bytes = bfile_->read(number, block);
		if (bytes > 0)
			last_block_number_ = number;
	}
	return bytes;
}

void Wallet::readBlockDataHash(int32_t number, uint8_t digest[]) const {

	if (bfile_)
		bfile_->readBlockDataHash(number, digest);
}

void Wallet::readBlockDataHashFromFolder(const std::string &folder, int32_t number, uint8_t digest[]) const {

        if (auto item = folders_.find(folder); item != folders_.end()) {
                auto bfile = item->second;

                bfile->readBlockDataHash(number, digest);
        } else {
                if (bfile_) {
                        if (auto bfile = bfile_->getFolder(folder); bfile) {
                		bfile->readBlockDataHash(number, digest);
                        }
                }
        }
}

void Wallet::create_folder(const std::string &name) {

	if (bfile_) {
		// Find the folder
		if (auto item = folders_.find(name); item == folders_.end()) {
			// Need to create a new file and a folder block
			std::filesystem::path path(path_);
			std::ostringstream folder_name;
        		nlohmann::json j;
        		std::ostringstream out;

			folder_name << wallet_name_ << "." << bfile_->getTotalBlocks() + 1 << ".fblk0000";

			j["folder_name"] = name;
			j["block_filename"] = folder_name.str();		
      
			out << j;

			PetalusBlock block(out.str(), BlockType::SYS_FOLDER_DEFINITION);
 
			// write the folder definition block
			if (bfile_->write(block) > 0) {
                        	uint8_t digest[HASH_BLOCK_SIZE];

				// Read the last block hash of the main file
                        	bfile_->readBlockHash(bfile_->getTotalBlocks(), digest);

				int32_t new_alloc_size = max_blockfile_size_;
				if (max_blockfile_size_ == 0) 
                			new_alloc_size = /* 16 MBs */ (BlockFile::BufferBlockSize * boost::iostreams::mapped_file::alignment());
				
                        	if (auto folder_file = SharedPointer<BlockFile>(new BlockFile(new_alloc_size)); folder_file) {
					std::ostringstream fout;
					nlohmann::json jp;

					if (std::filesystem::is_regular_file(path)) 
						path.remove_filename();

					path /= folder_name.str();

					// If the parent have public key we need to propagate to the folder file also
					if (bfile_->havePublicKey()) {
						if (bfile_->isECPublicKey()) 
							folder_file->setPublicKey(bfile_->getECPublicKey());
						else
							folder_file->setPublicKey(bfile_->getED25519PublicKey());
					}
                                	// Link the files
                                	folder_file->setSeed(digest);
                                	folder_file->open(path);

					// Probably is better to take this value from the bfile TODO
					folder_file->setBlockHashType(hash_func_type_); // Set the hash model for the new file
					folder_file->setEncryptionKey(key_);
#ifdef MYDEBUG
					std::cout << __FILE__ << ":" << __func__ << ":name:" << folder_name.str() << std::endl;
#endif

					jp["parent_block"] = bfile_->getPath().filename().c_str();

					fout << jp;

					PetalusBlock pblock(fout.str(), BlockType::SYS_FOLDER_NAME);

					folders_.insert(std::make_pair(name, folder_file));
					// write the blocks
					folder_file->write(pblock);	 // log the error
				}
			}
		}
	}
}

int32_t Wallet::readFromFolder(const std::string &folder, int32_t number, PetalusBlock &block) {

        int32_t bytes = 0;

        if (auto item = folders_.find(folder); item != folders_.end()) {
                auto bfile = item->second;

                bytes = bfile->read(number, block);
        } else {
                if (bfile_) {
                        if (auto bfile = bfile_->getFolder(folder); bfile) {
                                folders_.insert(std::make_pair(folder, bfile));
                                bytes = bfile->read(number, block);
                        }
                }
        }
        return bytes;
}

int32_t Wallet::writeOnFolder(const std::string &folder, PetalusBlock &block) {

        int32_t bytes = 0;

        if (auto item = folders_.find(folder); item != folders_.end()) {
		auto bfile = item->second;

		bytes = write_data_on_blockfile(bfile, block);
		last_block_number_written_ = bfile->getTotalBlocks();
	} else {
		if (bfile_) {
			if (auto bfile = bfile_->getFolder(folder); bfile) {
				folders_.insert(std::make_pair(folder, bfile));
				bytes = write_data_on_blockfile(bfile, block);
				last_block_number_written_ = bfile->getTotalBlocks();
			}
		}
	}
	return bytes;
}

bool Wallet::validPassword(const std::string &password) {

	return valid_sha256_password(password);
}

std::ostream& operator<< (std::ostream &out, const Wallet &wa) {

	out << "Wallet (" << wa.wallet_name_ << ")\n";
        out << "\t" << "Path:" << wa.path_ << "\n";
	out << "\t" << "HashType:" << std::right << std::setfill(' ') << std::setw(25) << wa.getBlockHashType() << "\n";
        out << "\t" << "Total bytes:            " << std::setw(10) << wa.total_bytes_ << "\n";
        out << "\t" << "Total blocks:           " << std::setw(10) << wa.total_blocks_ << "\n";
        out << "\t" << "Total folders:          " << std::setw(10) << wa.folders_.size() << "\n";
#if defined(PYTHON_BINDING)
	if (wa.callback_read.haveCallback())
		out << "\t" << "Read callback:" << wa.callback_read.getName() << "\n";
	if (wa.callback_write.haveCallback())
		out << "\t" << "Write callback:" << wa.callback_write.getName() << "\n";
#endif
	if (wa.folders_.size() > 0) {
        	const char *header = "%-20s %-14s %-14s";
        	const char *format = "%-20s %-14d %-14d";
        	out << "\t\t" << boost::format(header) % "Folder" % "Bytes" % "Blocks";
		out << "\n";
		for (auto &item: wa.folders_) {
			std::string name(item.first);
			int32_t bytes = item.second->getTotalBytes();
			int32_t blocks = item.second->getTotalBlocks();
			out << "\t\t" << boost::format(format) % name  % bytes % blocks;
                	out << "\n";
		}
	}
	return out;
}

nlohmann::json& operator<< (nlohmann::json &out, const Wallet &wa) {

	out["name"] = wa.wallet_name_;
	out["path"] = wa.path_.string();
	out["hash_type"] = wa.getBlockHashType();
	out["bytes"] = wa.total_bytes_;
	out["blocks"] = wa.total_blocks_;
#if defined(PYTHON_BINDING)
	if (wa.callback_read.haveCallback())
		out["read_callback"] = wa.callback_read.getName();
	if (wa.callback_write.haveCallback())
		out["write_callback"] = wa.callback_write.getName();
#endif
	if (wa.folders_.size() > 0) {
		for (auto &item: wa.folders_) {
			nlohmann::json obj;

			obj["name"] = item.first;
			obj["bytes"] = item.second->getTotalBytes();
			obj["blocks"] = item.second->getTotalBlocks();

			out["folders"].push_back(obj);	
		}
	}
	return out;
}

void Wallet::showLastBlock(std::basic_ostream<char> &out) const {

	if (bfile_)
		bfile_->showBlock(out, last_block_number_);
}

void Wallet::showBlock(std::basic_ostream<char> &out, int32_t number, const std::string &folder) {

	SharedPointer<BlockFile> bf = bfile_;

	if (folder.length() > 0) {
        	if (auto item = folders_.find(folder); item != folders_.end()) {
                	bf = item->second;
		} else {
                	if (bfile_) {
                        	if (bf = bfile_->getFolder(folder); bf) {
                                	folders_.insert(std::make_pair(folder, bf));
				}
			}
		}
	}	
	
	if (bf) {
		int32_t block_number = bf->getTotalBlocks();

		if (number > 0) block_number = number;

		bf->showBlock(out, block_number);
        }
}

void Wallet::showBlock(nlohmann::json &out, int32_t number, const std::string &folder) {

        SharedPointer<BlockFile> bf = bfile_;

        if (folder.length() > 0) {
                if (auto item = folders_.find(folder); item != folders_.end()) {
                        bf = item->second;
                } else {
                        if (bfile_) {
                                if (bf = bfile_->getFolder(folder); bf) {
                                        folders_.insert(std::make_pair(folder, bf));
                                }
                        }
                }
        }

        if (bf) {
                int32_t block_number = bf->getTotalBlocks();

                if (number > 0) block_number = number;

                bf->showBlock(out, block_number);
        }
}

} // namespace petalus::blockchain
