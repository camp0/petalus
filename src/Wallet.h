/*
 * Petalus a crypto micro service wallet database.
 *
 * Copyright (C) 2018-2022  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */

#pragma once
#ifndef SRC_WALLET_H_
#define SRC_WALLET_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>
#include <string>
#include <boost/utility/string_ref.hpp>
#include <boost/format.hpp>
#include "Pointer.h"
#include "BlockFile.h"
#include "PetalusBlock.h"
#include "Callback.h"

namespace petalus::blockchain {

class Wallet
{
public:
	explicit Wallet() {}
#if defined(STAND_ALONE_TEST) || defined(TESTING)
	explicit Wallet(const std::string &name): wallet_name_(name) {}
#endif
	virtual ~Wallet() { close(); }

	void open(const std::filesystem::path &path);
	void open(const std::filesystem::path &path, bool create);
	void open(const std::filesystem::path &path, bool create, uint8_t seed[]);

	const std::filesystem::path &getPath() const { return path_; } 
	const char *getFilename() const { return path_.string().c_str(); }

	void setName(const std::string &name) { wallet_name_ = name; }
	const char *getName() const { return wallet_name_.c_str(); }

	bool is_open() const; 

	void reset();

        int32_t writeOnFolder(const std::string &name, PetalusBlock &block);        
	int32_t write(PetalusBlock &block);

	BlockFileWriteCode getWriteErrorCode() const; 

	int64_t getTotalBytes() const { return total_bytes_; }
	int32_t getTotalBlocks() const { return total_blocks_; }

	void writePassword(const std::string &password) { writePassword(password, ""); }
	void writePassword(const std::string &password, const std::string &public_key);
	void writePassword(const std::string &password, const std::string &public_key, const std::string &public_key_type);

	// Sets the encryption of the wallet, all user blocks will be encrypted
	void setEncryptionKey(const EncryptionKey &key) { key_ = key; }

	int32_t readFromFolder(const std::string &folder, int32_t number, PetalusBlock &block);
	int32_t read(int32_t number, PetalusBlock &block);

	void readBlockDataHash(int32_t number, uint8_t digest[]) const;
	void readBlockDataHashFromFolder(const std::string &folder, int32_t number, uint8_t digest[]) const;

	void close();
	void closeFolders();

	int32_t getLastBlockNumberWritten() const { return last_block_number_written_; } 

	int getLastTimeOperation() const { return (int)last_operation_; }
	void setLastTimeOperation(std::time_t t) { last_operation_ = t; }
	
        // For update the Wallet time 
        struct updateTime {
                explicit updateTime(time_t t): t_(t) {}
                void operator()(const SharedPointer<Wallet>& w) {
                        w->setLastTimeOperation(t_);
                }
                private:
                        time_t t_;
        };

#if defined(STAND_ALONE_TEST) || defined(TESTING)
	int getTotalFolders() const { return folders_.size(); }

	const SharedPointer<BlockFile> &getBlockFile() const { return bfile_; }

	void writeMD5Password(const std::string &password) { write_md5_password(password); }
	void writeSHA256Password(const std::string &password) { write_sha256_password(password, "", ""); }

	bool validSHA256Password(const std::string &password) const { return valid_sha256_password(password); }
	bool validMD5Password(const std::string &password) const { return valid_md5_password(password); }
	
	const std::map<std::string, SharedPointer<BlockFile>> &getFolders() const { return folders_; }

	EncryptionKey &getEncryptionKey() { return key_; } 

#endif
	friend std::ostream& operator<< (std::ostream &out, const Wallet &wa);
	friend nlohmann::json& operator<< (nlohmann::json &out, const Wallet &wa);

	void createFolder(const std::string &name) { create_folder(name); }

	void setSessionId(const std::string &id) { session_id_ = id; }
	std::string getSessionId() const { return session_id_; }

	bool isLock() const;
	bool validPassword(const std::string &password);

	void showLastBlock(std::basic_ostream<char> &out) const;
	void showBlock(std::basic_ostream<char> &out, int32_t number, const std::string &folder = "");
	void showBlock(nlohmann::json &out, int32_t number, const std::string &folder = "");

#if defined(PYTHON_BINDING)
	Callback callback_read;
	Callback callback_write;

	PetalusBlock *getCurrentBlock() const { return current_block_; }
	void setCurrentBlock(PetalusBlock *block) { current_block_ = block; }
#endif

	// Sets and gets the values of the blockfiles
	void setBlockHashType(BlockHashTypes type);
        const char *getBlockHashType() const;

	void setMaxBlockFileSize(int32_t max_size) { max_blockfile_size_ = max_size; }

private:
	void create_folder(const std::string &name);
	void write_sha256_password(const std::string &password, const std::string &public_key, const std::string &public_key_type);
	void write_md5_password(const std::string &password);
	bool valid_sha256_password(const std::string &password) const;
	bool valid_md5_password(const std::string &password) const;

	void set_initial_blockfile(const std::filesystem::path &path, int32_t bytes);
	int32_t write_data_on_blockfile(SharedPointer<BlockFile> &bfile, PetalusBlock &block);

	std::string wallet_name_ = "";
	std::string session_id_ = "";
	std::filesystem::path path_ {};
	int64_t total_bytes_ = 0; // total user bytes without the file headers;
	int32_t total_blocks_ = 0;
	int32_t last_block_number_written_ = 0; // on the main file or on the folders
	int32_t last_block_number_ = 0; // Last read/write block
	int32_t max_blockfile_size_ = 0;
	std::time_t last_operation_ = std::time(nullptr);
	SharedPointer<BlockFile> bfile_ = nullptr;
	std::map<std::string, SharedPointer<BlockFile>> folders_ {};
	BlockHashTypes hash_func_type_ = BlockHashTypes::SHA256; // For all the blockfiles
	EncryptionKey key_ {};
#if defined(PYTHON_BINDING)
	PetalusBlock *current_block_ = nullptr;
#endif

};

} // namespace petalus::blockchain

#endif  // SRC_WALLET_H_

