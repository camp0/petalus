/*
 * Petalus a crypto micro service wallet database.
 *
 * Copyright (C) 2018-2022  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */

#ifndef SRC_PETALUSURIS_H_
#define SRC_PETALUSURIS_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

namespace petalus {

class PetalusUris {
public:
	static constexpr std::string_view create_wallet = "/v1/create";
	static constexpr std::string_view open_wallet = "/v1/open";
	static constexpr std::string_view close_wallet = "/v1/close";
	static constexpr std::string_view rw_block_wallet = "/v1/block/";

        static constexpr std::string_view status = "/v1/status";
        static constexpr std::string_view sessions = "/v1/sessions";
        static constexpr std::string_view session = "/v1/session/";
        static constexpr std::string_view statistics = "/v1/statistics";
};

} // namespace petalus

#endif  // SRC_PETALUSURIS_H_
