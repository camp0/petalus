#!/usr/bin/env python
#
# Petalus automatic functional tests
#
# This file contains multiple functional tests that simulate the behavior
# from the client through the server.
# Basically every class defined on the file contains a set of tests depending
# on the configuration of the server. Each of these clases fork a minimal
# instance of the server and then simulate HTTP requests from the client.
#
# Copyright (C) 2018-2022  Luis Campo Giralte
#
# Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
#
""" Unit tests for petalus python wrapper """
import os
import sys
import datetime
import unittest
import json
import socket
import time
from random import *
from io import BytesIO
import hashlib
import pycurl
try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO
import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
import binascii
import shutil
import netifaces
from multiprocessing import Process
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.primitives.asymmetric import ec
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization
from cryptography.x509.oid import NameOID
from cryptography.hazmat.primitives import hashes
from cryptography import x509
from cryptography.hazmat.primitives.asymmetric import ed25519
import urllib3
import ecdsa
import base64

urllib3.disable_warnings()
py_major = sys.version_info.major
py_minor = sys.version_info.minor

verify_ssl = False
ip = "127.0.0.1"
user_port1 = 5555
user_port2 = 5556
admin_port = 5557

""" The token is aaaaaaaaaaaaaaaa 
    and on base64 is YWFhYWFhYWFhYWFhYWFhYQ==
"""
b64_auth_token = "YWFhYWFhYWFhYWFhYWFhYQ=="
authorization_token = "{AES}C4F65AAC358F3CF2BB9C9C3FE2A4566E2D4B21C415C34B7F065A817A29F6940E"
creation_headers = {
    "Authorization": f"Basic {b64_auth_token}",
    "Connection": "close"
}

total_http_posts = 0
total_http_gets = 0
server_starttime = 1

""" From https://stackoverflow.com/questions/287871/print-in-terminal-with-colors """
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def generate_data_wallet(name="Craig", surname="Kelly", password="p", folders=[]):
    """Creates a valid random wallet for the tests"""
    data = {
        "name": name,
        "surname": surname,
        "token": randint(100, 1000000),
        "age": randint(18, 100),
        "date": str(datetime.datetime.now()).split(" ")[0],
        "password": hashlib.sha256(password.encode('utf-8')).hexdigest().upper(),
        "email": "%s.%s@coldmail.net" % (name.lower(), surname.lower())
    }

    cad = json.dumps(data).encode("utf-8")
    wallet = hashlib.sha256(cad).hexdigest().upper()
    data["wallet"] = wallet

    if (len(folders) > 0):
        data["folders"] = folders

    return wallet, data

def generate_ca():
    root_key = rsa.generate_private_key(
        public_exponent=65537,
        key_size=2048,
        backend=default_backend()
    )
    subject = issuer = x509.Name([
        x509.NameAttribute(NameOID.COUNTRY_NAME, u"ES"),
        x509.NameAttribute(NameOID.STATE_OR_PROVINCE_NAME, u"Madrid"),
        x509.NameAttribute(NameOID.LOCALITY_NAME, u"Madrid"),
        x509.NameAttribute(NameOID.ORGANIZATION_NAME, u"My Company"),
        x509.NameAttribute(NameOID.COMMON_NAME, u"My CA"),
    ])

    root_cert = x509.CertificateBuilder().subject_name(
        subject
    ).issuer_name(
        issuer
    ).public_key(
        root_key.public_key()
    ).serial_number(
        x509.random_serial_number()
    ).not_valid_before(
        datetime.datetime.utcnow()
    ).not_valid_after(
        datetime.datetime.utcnow() + datetime.timedelta(days=3650)
    ).add_extension(
        x509.BasicConstraints(ca=True, path_length=None), critical=True
    ).sign(root_key, hashes.SHA256(), default_backend())

    # Write our certificate out to disk.

    return root_key, root_cert

def generate_certificate(type, domain=u"mydomain.com", root_cert=None, root_key=None):
    """We generate a RSA/EC private key"""

    if "rsa" in type:
        pkey = rsa.generate_private_key(
            public_exponent=65537,
            key_size=2048,
            backend=default_backend())
    elif "ec" in type:
        pkey = ec.generate_private_key(ec.SECP384R1(), default_backend())
    else:
        pkey = None

    private_key  = pkey.private_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PrivateFormat.TraditionalOpenSSL,
        encryption_algorithm=serialization.NoEncryption())

    one_day = datetime.timedelta(1, 0, 0)

    builder = x509.CertificateBuilder()
    if root_cert:
        builder = builder.issuer_name(root_cert.issuer)
    else:
        builder = builder.issuer_name(x509.Name([
            x509.NameAttribute(NameOID.COMMON_NAME, domain),
        ]))
    builder = builder.subject_name(x509.Name([
        x509.NameAttribute(NameOID.COMMON_NAME, domain),
    ]))
    builder = builder.not_valid_before(datetime.datetime.today() - one_day)
    builder = builder.not_valid_after(datetime.datetime.today() + (one_day * 30))
    builder = builder.serial_number(x509.random_serial_number())
    builder = builder.public_key(pkey.public_key())
    builder = builder.add_extension(
        x509.SubjectAlternativeName(
            [x509.DNSName(u"localhost")]
        ),
        critical=False)

    if root_key:
        cert = builder.sign(
            private_key=root_key, algorithm=hashes.SHA256(),
            backend=default_backend())
    else:
        cert = builder.sign(
            private_key=pkey, algorithm=hashes.SHA256(),
            backend=default_backend())

    certificate = cert.public_bytes(serialization.Encoding.PEM)

    return private_key, certificate

def do_get(con, uri, headers={}):
    global total_http_gets 
    total_http_gets += 1
    
    res = con.get(uri, headers=headers, verify=verify_ssl) 
    return res

def do_post(con, uri, data=None, json=None, headers={}, cert=None, verify=False):
    global total_http_posts 
    total_http_posts += 1

    if json is not None:
        res = con.post(uri, json=json, headers=headers, cert=cert, verify=verify)
    else:
        res = con.post(uri, data=data, headers=headers, cert=cert, verify=verify)

    return res

def get_sha256_from_file(filename):
    hash_sha256 = hashlib.sha256()
    with open(filename, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_sha256.update(chunk)
    return hash_sha256.hexdigest()

def curl_request(con, url, headers, data=None):
    """ This method will generate HTTP traffic from the readonly_device
    interface, allowing us to test the functionality of systems
    that want to read wallets"""

    global total_http_posts
    global total_http_gets
    buffer = BytesIO()
    rheaders = BytesIO()
    con.setopt(pycurl.URL, url)
    if (data != None):
        con.setopt(pycurl.POST, True)
        con.setopt(pycurl.POSTFIELDS, data)
        total_http_posts += 1
    else:
        con.setopt(pycurl.POST, False)
        total_http_gets += 1

    h = list()
    headers["Content-Type"] = "application/json"
    for k, v in headers.items():
        h.append("%s: %s" % (k, v))

    con.setopt(pycurl.HEADERFUNCTION, rheaders.write)
    con.setopt(pycurl.HTTPHEADER, h)
    con.setopt(pycurl.TIMEOUT, 10)
    con.setopt(pycurl.WRITEFUNCTION, buffer.write)
    con.setopt(pycurl.INTERFACE, readonly_device)
    # con.setopt(pycurl.VERBOSE, 1)
    con.perform()

    sid = ""
    h = str(rheaders.getvalue())
    if "Petalus-SessionId" in h:
        """ Horrible parsing due to issues with BytesIO with py2.7 and 3.6 """
        id = h.split("Petalus-SessionId:")[1].replace('\r', "").replace('\n', "")
        sid = id.replace("'", "").replace(" ", "").replace("\\r", "").replace("\\n", "")

    # Json response
    resp = buffer.getvalue().decode('UTF-8')
    code = con.getinfo(pycurl.HTTP_CODE)

    #  Check response is a JSON if not there was an error
    try:
        resp = json.loads(resp)
    except ValueError:
        pass

    buffer.close()
    return code, resp, sid

def get_new_port(from_port, to_port):
    port = 0 
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    for p in range(from_port, to_port):
        try:
            s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            s.bind((ip, p))
            port = p
            break
        except socket.error as e:
            pass

    s.close()
    return port

def defined(value):
    with open("../config.h") as f:
        for l in f.readlines():
           if (l.startswith("#define %s" % value)):
               return True
    return False

class PetalusTestSuite01 (unittest.TestCase):
    """The aim of this test suite is to verify the minimum functionality
    of the server, no encryption and no ECDSA and HTTP"""

    @classmethod
    def setUpClass(self):
        """Delete the previous data runs"""
        self.data_directory = "./%s_data" % self.__name__.lower()
        shutil.rmtree(self.data_directory, ignore_errors=True)
        self.layer = "http"

    def setUp(self):
        self.p = Process(target=self.__micro_service)
        self.p.daemon = True
        self.p.start()
        time.sleep(server_starttime)
        return

    def tearDown(self):
        """Stop and delete the server"""
        self.p.terminate()
        return

    def __micro_service(self):
        """Micro service :)"""
        import petalus
        sw = petalus.SessionWalletManager()
        s = petalus.Server()
        sw.data_directory = self.data_directory
        sw.increase_allocated_memory(2)
        s.session_wallet_manager = sw
        s.address = ip
        s.administration_address = ip
        s.port = user_port_1
        s.administration_port = admin_port
        s.administration_ip_addresses = [ip]
        s.authorization_token = authorization_token

        s.start()
        s.run()
        s.stop()

    def __create_wallet(self, password):
        """Creates a valid random wallet for the tests"""
        wallet, data = generate_data_wallet(name="Nicolas", surname="Muller", password=password)

        uri = f"{self.layer}://{admin_address}/v1/create"
        res = do_post(requests, uri, json=data, headers=creation_headers)
        self.assertEqual(res.status_code, 201)
        return wallet

    def test01(self):
        """Creates a valid wallet"""

        wallet = self.__create_wallet("AAAAA")
        self.assertGreater(len(wallet), 16)

    def test02(self):
        """Tries to Creates a wallet but the format is wrong"""
       
        uri = f"{self.layer}://{admin_address}/v1/create"
        wallet, data = generate_data_wallet(name="Nicolas", surname="Muller", password="password")

        wallet = "nothing valid"
        data["wallet"] = wallet

        res = do_post(requests, uri, json=data, headers=creation_headers)
        self.assertEqual(res.status_code ,403)
        self.assertEqual(res.content, b"Can not create wallet [error code:1]")

    def test03(self):
        """Creates a wallet and open it"""

        password = "AAAAAAA"
        wallet = self.__create_wallet(password)

        uri = f"{self.layer}://{address}/v1/open"
        data = {
            "wallet": wallet,
            "password": hashlib.sha256(password.encode('utf-8')).hexdigest().upper()
        }

        res = do_post(requests, uri, json=data)
        self.assertEqual(res.status_code ,200)
        self.assertIn("Petalus-SessionId", res.headers)

    def test04(self):
        """Creates a wallet and open, write , read and close"""

        password = "user password"
        wallet = self.__create_wallet(password)

        uri = f"{self.layer}://{address}/v1/open"
        data = {
            "wallet": wallet,
            "password": hashlib.sha256(password.encode('utf-8')).hexdigest().upper()
        }

        with requests.Session() as httpsession:
            res = do_post(httpsession, uri, json=data)
            self.assertEqual(res.status_code ,200)
            self.assertIn("Petalus-SessionId", res.headers)
            id = res.headers["Petalus-SessionId"]

            uri = f"{self.layer}://{address}/v1/block/{wallet}"
            headers = {
                "Petalus-SessionId": id,
                "Content-Type": "text/plain"
            }
            data = "This is a message that will go on a block"
            res = do_post(httpsession, uri, data, headers=headers)
            self.assertEqual(res.status_code ,200)
            self.assertEqual(res.text, "Block 3 added")

            uri = f"{self.layer}://{address}/v1/block/{wallet}"
            res = do_post(httpsession, uri, "", headers=headers)
            self.assertEqual(res.status_code ,400)
            self.assertEqual(res.content, b"Data not found")

            uri = f"{self.layer}://{address}/v1/block/this_wallet_is_wrong"
            res = do_post(httpsession, uri, data, headers=headers)
            self.assertEqual(res.status_code ,500)
            self.assertEqual(res.content, b"The data can not be written")
 
            uri = f"{self.layer}://{address}/v1/block/{wallet}/3"
            res = do_get(httpsession, uri, headers=headers)
            self.assertEqual(res.status_code ,200)
            self.assertEqual(res.text, data)

            uri = f"{self.layer}://{address}/v1/close"
            res = do_post(httpsession, uri, "", headers=headers)
            self.assertEqual(res.status_code ,200)

    def test05(self):
        """Creates a wallet and use a bad password for open"""

        password = "user password"
        wallet = self.__create_wallet(password)

        password = "bad pass"
        for i in range(0, 8):
            uri = f"{self.layer}://{address}/v1/open"
            data = {
                "wallet": wallet,
                "password": hashlib.sha256(password.encode('utf-8')).hexdigest().upper()
            }

            res = do_post(requests, uri, json=data)
            self.assertEqual(res.status_code ,401)
            self.assertNotIn("Petalus-SessionId", res.headers)

    def test06(self):
        """Creates a wallet and tries to open a different one"""

        password1 = "AAAAAAAAAAAAA"
        wallet1 = self.__create_wallet(password1)

        password2 = "BBBBBBBBBBBB"
        wallet2 = self.__create_wallet(password2)

        self.assertNotEqual(wallet1, wallet2)

        uri = f"{self.layer}://{address}/v1/open"
        data = { 
            "wallet": wallet1, 
            "password": hashlib.sha256(password2.encode('utf-8')).hexdigest().upper()
        }

        res = do_post(requests, uri, json=data)
        self.assertEqual(res.status_code ,401)
        self.assertNotIn("Petalus-SessionId", res.headers)
        self.assertEqual(res.content, b"Wrong credentials [error code:4]")

        data = { 
            "wallet": wallet2, 
            "password": hashlib.sha256(password1.encode('utf-8')).hexdigest().upper() 
        }

        res = do_post(requests, uri, json=data)
        self.assertEqual(res.status_code ,401)
        self.assertEqual(res.content, b"Wrong credentials [error code:4]")

        data = { 
            "wallet": wallet1, 
            "password": hashlib.sha256(password1.encode('utf-8')).hexdigest().upper() 
        }

        res = do_post(requests, uri, json=data)
        self.assertEqual(res.status_code ,200)

    def test07(self):
        """Creates a wallet and tries to open a wrong wallet parameter"""
        password = "AAAAAAAAAAAAA"
        wallet = self.__create_wallet(password)

        uri = f"{self.layer}://{address}/v1/open"
        data = { 
            "wallet": "this can not be open", 
            "password": password 
        }

        res = do_post(requests, uri, json=data)
        self.assertEqual(res.status_code ,400)
        self.assertNotIn("Petalus-SessionId", res.headers)
        self.assertEqual(res.content, b"Wrong wallet format [error code:2]")

        uri = f"{self.layer}://{address}/v1/open"
        data = { 
            "wallet": wallet, 
            "password": hashlib.sha256(password.encode('utf-8')).hexdigest().upper()
        }

        res = do_post(requests, uri, json=data)
        self.assertEqual(res.status_code ,200)

    def test08(self):
        """Creates a wallet and write and get some blocks"""
        msg1 = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
        msg2 = "BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB"
        password = "AAAAAAAAAAAAAtest08"
        wallet = self.__create_wallet(password)

        uri = f"{self.layer}://{address}/v1/open"
        data = { 
            "wallet": wallet, 
            "password": hashlib.sha256(password.encode('utf-8')).hexdigest().upper()
        }

        session = requests.Session()

        res = do_post(session, uri, json=data)
        self.assertEqual(res.status_code ,200)
        self.assertIn("Petalus-SessionId", res.headers)
        id = res.headers["Petalus-SessionId"]

        uri = f"{self.layer}://{address}/v1/block/{wallet}"
        headers = { 
            "Petalus-SessionId" : id,
            "Content-Type" : "text/plain"
        }
        res = do_post(session, uri, msg1, headers=headers)
        self.assertEqual(res.status_code ,200)
        res = do_post(session, uri, msg2, headers=headers)
        self.assertEqual(res.status_code ,200)

        # now blocks 3 and 4 exists on the wallet
        uri = f"{self.layer}://{address}/v1/block/{wallet}/5"
        res = do_get(session, uri, headers=headers)
        self.assertEqual(res.status_code ,404)
        self.assertEqual(res.content, b"Block 5 not found")

        uri = f"{self.layer}://{address}/v1/block/{wallet}/4"
        res = do_get(session, uri, headers=headers)
        self.assertEqual(res.status_code ,200)
        self.assertEqual(res.text, msg2)

        uri = f"{self.layer}://{address}/v1/block/{wallet}/3"
        res = do_get(session, uri, headers=headers)
        self.assertEqual(res.status_code ,200)
        self.assertEqual(res.text, msg1)

        uri = f"{self.layer}://{address}/v1/block/{wallet}/3"
        res = do_get(session, uri)
        self.assertEqual(res.status_code ,404)
        self.assertEqual(res.content, b"Session ID not valid")

        uri = f"{self.layer}://{address}/v1/block/{wallet}/3"
        # Generate other session id
        headers = {"Petalus-SessionId": binascii.b2a_hex(os.urandom(16)).upper()}
        res = do_get(session, uri, headers=headers)
        self.assertEqual(res.status_code ,404)
        self.assertEqual(res.content, b"Session ID not valid for read")

        session.close()

    def test09(self):
        """Creates a wallet but the content type of the open operation is wrong"""
        password = "AAAAAAAAAAAAAtest09"
        wallet = self.__create_wallet(password)

        uri = f"{self.layer}://{address}/v1/open"
        data = { 
            "wallet": wallet, 
            "password": hashlib.sha256(password.encode('utf-8')).hexdigest().upper()
        }

        # The HTTP content-type is not json
        res = do_post(requests, uri, data)
        self.assertEqual(res.status_code ,415) # unsupported_media_type
        self.assertNotIn("Petalus-SessionId", res.headers)

    def test10(self):
        """Tries to open a wallet that is not created yet"""

        password = "123456789"
        data = { 
            "name": "Craig",
            "surname": "Kelly",
            "token": randint(100, 100000),
            "age": randint(18, 90),
            "date": str(datetime.datetime.now()),
            "password": hashlib.sha256(password.encode('utf-8')).hexdigest().upper() 
        }

        cad = json.dumps(data).encode("utf-8")
        wallet = hashlib.sha256(cad).hexdigest().upper()

        data["wallet"] = wallet
        # The data of the wallet is ready but not written on the wallet

        # Tries to open the wallet
        uri = f"{self.layer}://{address}/v1/open"
        open_data = { 
            "wallet": wallet, 
            "password": hashlib.sha256(password.encode('utf-8')).hexdigest().upper()
        }

        res = do_post(requests, uri, json=open_data)
        self.assertEqual(res.status_code ,404) # not_found
        self.assertNotIn("Petalus-SessionId", res.headers)

        # Now create the wallet
        uri = f"{self.layer}://{admin_address}/v1/create"
        res = do_post(requests, uri, json=data, headers=creation_headers)
        self.assertEqual(res.status_code, 201)

        # Reopen again
        uri = f"{self.layer}://{address}/v1/open"
        open_data = { 
            "wallet": wallet, 
            "password": hashlib.sha256(password.encode('utf-8')).hexdigest().upper()
        }
        res = do_post(requests, uri, json=open_data)
        self.assertEqual(res.status_code ,200)

    def test11(self):
        """Tries to read a block from another wallet"""

        password1 = "AAAAAAAAAAAAAtest10"
        password2 = "XXXXXXXtest10"
        wallet1 = self.__create_wallet(password1)
        wallet2 = self.__create_wallet(password2)

        with requests.Session() as session1, requests.Session() as session2:
            # Open both wallets
            uri = f"{self.layer}://{address}/v1/open"
            open_data = { 
                "wallet": wallet1, 
                "password": hashlib.sha256(password1.encode('utf-8')).hexdigest().upper() 
            }
            res = do_post(session1, uri, json=open_data)
            self.assertEqual(res.status_code ,200) 
            self.assertIn("Petalus-SessionId", res.headers)
            id1 = res.headers["Petalus-SessionId"]

            open_data = { 
                "wallet": wallet2, 
                "password": hashlib.sha256(password2.encode('utf-8')).hexdigest().upper()
            }
            res = do_post(session2, uri, json=open_data)
            self.assertEqual(res.status_code ,200) 
            self.assertIn("Petalus-SessionId", res.headers)
            id2 = res.headers["Petalus-SessionId"]

            self.assertNotEqual(id1, id2)

            # Now both wallets writes a block
            msg1 = "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ"
            msg2 = "OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO"

            uri = f"{self.layer}://{address}/v1/block/{wallet1}"
            headers = { 
                "Petalus-SessionId": id1,
                "Content-Type": "text/plain"
            }
            res = do_post(session1, uri, data=msg1, headers=headers)
            self.assertEqual(res.status_code ,200)

            uri = f"{self.layer}://{address}/v1/block/{wallet2}"
            headers = { 
                "Petalus-SessionId": id2,
                "Content-Type": "text/plain"
            }
            res = do_post(session2, uri, msg2, headers=headers)
            self.assertEqual(res.status_code ,200)

            # Now the uris are changed by mistake or intentionally

            uri = f"{self.layer}://{address}/v1/block/{wallet2}/3"
            headers = { 
                "Petalus-SessionId": id1,
                "Content-Type": "text/plain"
            }
            res = do_get(session1, uri, headers=headers)
            self.assertEqual(res.status_code ,404)

            uri = f"{self.layer}://{address}/v1/block/{wallet1}/3"
            headers = {"Petalus-SessionId" : id2}
            res = do_get(session2, uri, headers=headers)
            self.assertEqual(res.status_code ,404)

            # Now put the correct values and read again the blocks
            uri = f"{self.layer}://{address}/v1/block/{wallet1}/3"
            headers = {"Petalus-SessionId" : id1}
            res = do_get(session1, uri, headers=headers)
            self.assertEqual(res.status_code ,200)
            self.assertEqual(res.text, msg1)

            uri = f"{self.layer}://{address}/v1/block/{wallet2}/3"
            headers = {"Petalus-SessionId" : id2}
            res = do_get(session2, uri, headers=headers)
            self.assertEqual(res.status_code ,200)
            self.assertEqual(res.text, msg2)

    def test12(self):
        """Simulate a password attack knowing the wallet that is not open"""
        real_password = "AAAAAAAAAAAAAtest10"
        wallet = self.__create_wallet(real_password)

        passwords = ["AAAA", "BBBB", "CCCC", "DDDD", "EEEE", "FFFF"]

        for p in passwords:
            uri = f"{self.layer}://{address}/v1/open"
            open_data = { 
                "wallet": wallet, 
                "password": hashlib.sha256(p.encode('utf-8')).hexdigest().upper() 
            }
            res = do_post(requests, uri, json=open_data)
            self.assertEqual(res.status_code ,401) # unauthorized
            self.assertNotIn("Petalus-SessionId", res.headers)

        # Now open with the real password
        uri = f"{self.layer}://{address}/v1/open"
        open_data = { 
            "wallet": wallet, 
            "password": hashlib.sha256(real_password.encode('utf-8')).hexdigest().upper()
        }
        res = do_post(requests, uri, json=open_data)
        self.assertEqual(res.status_code ,200)
        self.assertIn("Petalus-SessionId", res.headers)

    def test13(self):
        """Simulate a password attack knowing the wallet is open"""
        real_password = "AAAAAAAAAAAAAtest10"
        wallet = self.__create_wallet(real_password)

        # Open the wallet
        uri = f"{self.layer}://{address}/v1/open"
        open_data = { 
            "wallet": wallet, 
            "password": hashlib.sha256(real_password.encode("utf-8")).hexdigest().upper()
        }
        res = do_post(requests, uri, json=open_data)
        self.assertEqual(res.status_code ,200)
        self.assertIn("Petalus-SessionId", res.headers)

        passwords = ["AAAA", "BBBB", "CCCC", "DDDD", "EEEE", "FFFF"]

        for p in passwords:
            uri = f"{self.layer}://{address}/v1/open"
            open_data = { 
                "wallet": wallet, 
                "password": hashlib.sha256(p.encode("utf-8")).hexdigest().upper()
            }
            res = do_post(requests, uri, json=open_data)
            self.assertEqual(res.status_code ,401) # unauthorized
            self.assertNotIn("Petalus-SessionId", res.headers)

    def test14(self):
        """Simulate a session attack for get information knowing the wallet is open"""
        real_password = "AAAAAAAAAAAAAtest10"
        wallet = self.__create_wallet(real_password)

        session = requests.Session()
        # Open the wallet
        uri = f"{self.layer}://{address}/v1/open"
        open_data = { 
            "wallet": wallet, 
            "password": hashlib.sha256(real_password.encode("utf-8")).hexdigest().upper()
        }
        res = do_post(session, uri, json=open_data)
        self.assertEqual(res.status_code ,200)
        self.assertIn("Petalus-SessionId", res.headers)
        good_session = res.headers["Petalus-SessionId"]

        # Makes 1000 requests to try if one works
        for i in range(0, 1000):
            uri = f"{self.layer}://{address}/v1/block/{wallet}/1"
            headers = {"Petalus-SessionId": binascii.b2a_hex(os.urandom(16)).upper()}
            res = do_get(session, uri, headers=headers)
            self.assertEqual(res.status_code ,404) # not found

        uri = f"{self.layer}://{address}/v1/block/{wallet}/1"
        headers = {"Petalus-SessionId": good_session}
        res = do_get(session, uri, headers=headers)
        self.assertEqual(res.status_code ,200) # found

        session.close()

    def test15(self):
        """Simulate a session attack for polute information knowing the wallet is open"""
        real_password = "AAAAAAAAAAAAAtest10"
        wallet = self.__create_wallet(real_password)

        with requests.Session() as session:
            # Open the wallet
            uri = f"{self.layer}://{address}/v1/open"
            open_data = { 
                "wallet": wallet, 
                "password": hashlib.sha256(real_password.encode("utf-8")).hexdigest().upper()
            }
            res = do_post(session, uri, json=open_data)
            self.assertEqual(res.status_code ,200)
            self.assertIn("Petalus-SessionId", res.headers)
            good_session = res.headers["Petalus-SessionId"]

            # Makes 1000 requests to try if one writes on the wallet
            for i in range(0, 1000):
                uri = f"{self.layer}://{address}/v1/block/{wallet}"
                headers = { 
                    "Petalus-SessionId": binascii.b2a_hex(os.urandom(16)).upper(),
                    "Content-Type" : "text/plain"
                }
                res = do_post(session, uri, "This is very very bad my friend :D", headers=headers)
                self.assertEqual(res.status_code ,400) 
                self.assertEqual(res.content, b"Session ID not valid for write")

    def test16(self):
        """Tries to close a wallet that is not open by a session attack"""
        real_password = "AAAAAAAAAAAAAtest10"
        wallet = self.__create_wallet(real_password)

        uri = f"{self.layer}://{address}/v1/close"
        headers = {"Content-Type" : "text/plain"}
        res = do_post(requests, uri, "AAAAAA", headers=headers)
        self.assertEqual(res.status_code ,404)
        self.assertEqual(res.text, "Not found")

        session = requests.Session()
        # Open the wallet
        uri = f"{self.layer}://{address}/v1/open"
        open_data = { 
            "wallet": wallet, 
            "password": hashlib.sha256(real_password.encode('utf-8')).hexdigest().upper()
        }
        res = do_post(session, uri, json=open_data)
        self.assertEqual(res.status_code ,200)
        self.assertIn("Petalus-SessionId", res.headers)
        good_session = res.headers["Petalus-SessionId"]

        # Makes 1000 requests to try if one writes on the wallet
        for i in range(0, 1000):
            uri = f"{self.layer}://{address}/v1/close"
            headers = { 
                "Petalus-SessionId": binascii.b2a_hex(os.urandom(16)).upper(),
                "Content-Type" : "text/plain"
            }
            res = do_post(session, uri, "", headers=headers)
            self.assertEqual(res.status_code ,400) # Not found

        # Now the real user writes, reads and close the wallet

        data = "Some where over the rainbow" 
        uri = f"{self.layer}://{address}/v1/block/{wallet}"
        headers = { 
            "Petalus-SessionId": good_session,
            "Content-Type": "text/plain"
        }
        res = do_post(session, uri, data, headers=headers)
        self.assertEqual(res.status_code ,200)

        uri = f"{self.layer}://{address}/v1/block/{wallet}/3"
        res = do_get(session, uri, headers=headers)
        self.assertEqual(res.status_code ,200)
        self.assertEqual(res.text, data) 

        uri = f"{self.layer}://{address}/v1/close"
        res = do_post(session, uri, "", headers=headers)
        self.assertEqual(res.status_code ,200)

        session.close()

    def test17(self):
        """Opens a wallet and generate some bad URIS"""
        real_password = "AAAAAAAAAAAAAtest10"
        wallet = self.__create_wallet(real_password)

        session = requests.Session()
        # Open the wallet
        uri = f"{self.layer}://{address}/v1/open"
        open_data = { 
            "wallet": wallet, 
            "password": hashlib.sha256(real_password.encode('utf-8')).hexdigest().upper()
        }
        res = do_post(session, uri, json=open_data)
        self.assertEqual(res.status_code ,200)
        self.assertIn("Petalus-SessionId", res.headers)
        good_session = res.headers["Petalus-SessionId"]

        data = "Some where over the rainbow......"
        uri = f"{self.layer}://{address}/v1/block/{wallet}"
        headers = { 
            "Petalus-SessionId": good_session,
            "Content-Type": "text/plain"
        }
        res = do_post(session, uri, data, headers=headers)
        self.assertEqual(res.status_code ,200)

        uri = f"{self.layer}://{address}/v1/block/{wallet}/4"
        res = do_get(session, uri, headers=headers)
        self.assertEqual(res.status_code ,404)
        self.assertEqual(res.content, b"Block 4 not found")

        uri = f"{self.layer}://{address}/v1/block/{wallet}/ThisDontWork"
        res = do_get(session, uri, headers=headers)
        self.assertEqual(res.status_code ,404)

        uri = f"{self.layer}://{address}/v1/block/{wallet}/\\xAA\\xAA\\xAA\\xAA\\xAA"
        res = do_get(session, uri, headers=headers)
        self.assertEqual(res.status_code ,404)

        uri = f"{self.layer}://{address}/v1/block/{wallet}"\
               "/?q=human+read+some+booksl&t=canonical&ia=web"
        res = do_get(session, uri, headers=headers)
        self.assertEqual(res.status_code ,404)

        uri = f"{self.layer}://{address}/v1/block/{wallet}/3"
        res = do_get(session, uri, headers=headers)
        self.assertEqual(res.status_code ,200)

        uri = f"{self.layer}://{address}/v1/block/{wallet}/"
        uri += "resources/wiki/start/topics/examples/"
        uri += "resources/wiki/start/topics/exales/"
        uri += "resources/wiki/start/topics/examles/"
        uri += "resources/wiki/end/topics/examples/"
        res = do_get(session, uri, headers=headers)
        self.assertEqual(res.status_code ,404)

        uri = f"{self.layer}://{address}/v1/close"
        res = do_post(session, uri, "", headers=headers)
        self.assertEqual(res.status_code ,200)

        session.close()

    def test18(self):
        """Open a session and sent a HEAD, PUT and DELETE HTTP request"""
        password = "AAAAAAA"
        wallet = self.__create_wallet(password)

        uri = f"{self.layer}://{address}/v1/open"
        data = { 
            "wallet": wallet, 
            "password": hashlib.sha256(password.encode('utf-8')).hexdigest().upper() 
        }

        httpsession = requests.Session()
        res = do_post(httpsession, uri, json = data)
        self.assertEqual(res.status_code ,200)
        self.assertIn("Petalus-SessionId", res.headers)
        good_session = res.headers["Petalus-SessionId"]

        headers = {"Petalus-SessionId": good_session }
        res = httpsession.head(uri, headers=headers)
        self.assertEqual(res.status_code ,400)

        res = httpsession.put(uri, headers=headers)
        self.assertEqual(res.status_code ,400)

        res = httpsession.delete(uri, headers=headers)
        self.assertEqual(res.status_code ,400)
        
        res = httpsession.options(uri, headers=headers)
        self.assertEqual(res.status_code ,400)

        httpsession.close()

    def test19(self):
        """ Tries to open a wallet with https instead http """
        password = "AAAAAAA"
        wallet = self.__create_wallet(password)

        uri = f"https://{address}/v1/open"
        data = { 
            "wallet": wallet, 
            "password": hashlib.sha256(password.encode('utf-8')).hexdigest().upper()
        }

        httpsession = requests.Session()
        # The next request will be close by the server
        # and generates an exception
        try:
            _ = do_post(httpsession, uri, data)
            self.assertTrue(False)
        except requests.exceptions.ConnectionError as e:
            self.assertTrue(True)

        httpsession.close()

    def test20(self):
        """Tries to open a wallet with a wrong content-type"""
        password = "AAAAAAA"
        wallet = self.__create_wallet(password)

        uri = f"{self.layer}://{address}/v1/open"
        data = { 
            "wallet": wallet, 
            "password": hashlib.sha256(password.encode('utf-8')).hexdigest().upper() 
        }

        headers = {"Content-Type": "text/plain" }
        httpsession = requests.Session()
        res = do_post(httpsession, uri, data)
        self.assertEqual(res.status_code, 415)
        self.assertEqual(res.content, b"Incorrect content type for opening the wallet")

        httpsession.close()

    def test21(self):
        """Tries to create a wallet with no definition on it"""
        wallet, data = generate_data_wallet(password="something")

        # Just remove the wallet entrie
        del data["wallet"]

        uri = f"{self.layer}://{admin_address}/v1/create"
        res = do_post(requests, uri, json = data, headers = creation_headers)
        self.assertEqual(res.status_code, 400)
        self.assertEqual(res.content, b"No wallet string on wallet definition")

    def test22(self):
        """Tries to create a wallet with wrong password parameters"""
        wallet, data = generate_data_wallet(password="something")

        # Just remove the wallet entrie
        del data["password"]

        uri = f"{self.layer}://{admin_address}/v1/create"
        res = do_post(requests, uri, json=data, headers=creation_headers)
        self.assertEqual(res.status_code, 400)
        self.assertEqual(res.content, b"No password provided on the creation of the wallet")

        data["password"] = 100

        res = do_post(requests, uri, json=data, headers=creation_headers)
        self.assertEqual(res.status_code, 400)
        self.assertEqual(res.content, b"Wrong password type")

    def test23(self):
        """Tries to open the wallet with no correct parameters"""
        password = "AAAAAAA"
        wallet = self.__create_wallet(password)

        with requests.Session() as httpsession:
            uri = f"{self.layer}://{address}/v1/open"
            data = {}
            res = do_post(httpsession, uri, json=data, headers={"Connection": "close"})
            self.assertEqual(res.status_code, 400)
            self.assertEqual(res.content, b"Parameters not found")

            data = {"wallet": 12, "password": [ 1,2,3]}
            res = do_post(httpsession, uri, json=data, headers={"Connection": "close"})
            self.assertEqual(res.status_code, 400)
            self.assertEqual(res.content, b"Incorrect parameter types")

    def test24(self):
        """Tries to Creates a wallet with nothing on the definition"""

        uri = f"{self.layer}://{admin_address}/v1/create"
        wallet, data = generate_data_wallet(password="password")

        data = {}
        data["wallet"] = wallet

        res = do_post(requests, uri, json=data, headers=creation_headers)
        self.assertEqual(res.status_code ,400)
        self.assertEqual(res.content, b"No wallet definition")
 
        data["wallet"] = wallet
        data["something"] = 12

        res = do_post(requests, uri, json=data, headers=creation_headers)
        self.assertEqual(res.status_code ,400)
        self.assertEqual(res.content, b"No password provided on the creation of the wallet") 

        res = do_post(requests, uri, json=data, headers={})
        self.assertEqual(res.status_code ,401)
        self.assertEqual(res.content, b"No Authorization token found") 

    def test25(self):
        """Tries to open a wallet with no json content on the HTTP request"""

        password = "AAAAAAA"
        wallet = self.__create_wallet(password)

        uri = f"{self.layer}://{address}/v1/open"
        headers = {"Content-Type": "application/json"}
        res = do_post(requests, uri, data="This is not json", headers=headers)
        self.assertEqual(res.status_code ,400)
        self.assertEqual(res.text, "Not valid json content")

    def test26(self):
        """Creates a valid wallet but with a incorrect HTTP payload"""

        headers = creation_headers

        headers["Content-Type"] = "application/json"
        uri = f"{self.layer}://{admin_address}/v1/create"
        res = do_post(requests, uri, data="This can not worrrkkk  {}}}}};;;[][", headers=headers)
        self.assertEqual(res.status_code, 400)
        self.assertEqual(res.text, "Not valid json content")

    def test27(self):
        """Open the wallet from two different connections."""

        password = "AAAAAAAAAAAAAtest10"
        wallet = self.__create_wallet(password)

        with requests.Session() as session1, requests.Session() as session2:
            # Open wallet
            uri = f"{self.layer}://{address}/v1/open"
            open_data = {
                "wallet": wallet,
                "password": hashlib.sha256(password.encode('utf-8')).hexdigest().upper()
            }
            res = do_post(session1, uri, json=open_data)
            self.assertEqual(res.status_code ,200)
            self.assertIn("Petalus-SessionId", res.headers)
            id1 = res.headers["Petalus-SessionId"]

            open_data = {
                "wallet": wallet,
                "password": hashlib.sha256(password.encode('utf-8')).hexdigest().upper()
            }
            res = do_post(session2, uri, json=open_data)
            self.assertEqual(res.status_code ,200)
            self.assertIn("Petalus-SessionId", res.headers)
            id2 = res.headers["Petalus-SessionId"]

            # The sessionid is the same for both connections
            self.assertEqual(id1, id2)

            data = b"Some where over the rainbow......"
            uri = f"{self.layer}://{address}/v1/block/{wallet}"
            headers = {
                "Petalus-SessionId": id1,
                "Content-Type": "text/plain"
            }
            res = do_post(session1, uri, data, headers=headers)
            self.assertEqual(res.status_code ,200)

            uri = f"{self.layer}://{address}/v1/block/{wallet}/3"
            res = do_get(session2, uri, headers=headers)
            self.assertEqual(res.status_code ,200)
            self.assertEqual(res.content, data)

            # LUIS


class PetalusTestSuite02 (unittest.TestCase):
    """The aim of this test suite is to verify the minimum functionality
    of the server with TLS RSA encryption on the TCP communications."""

    @classmethod
    def setUpClass(self):
        """Now we generate a private/public key with a self signed certificate"""
        self.private_key, self.certificate = generate_certificate("rsa") 
        self.layer = "https"       

        # We write the private key on a file LUIS
        with open("server_key.pem", "w") as fd:
            fd.write(self.private_key.decode("utf-8"))
 
        # Delete the previous data runs
        self.data_directory = "./%s_data" % self.__name__.lower()
        shutil.rmtree(self.data_directory, ignore_errors=True)

    def setUp(self):
        self.p = Process(target = self.__micro_service)
        self.p.daemon = True
        self.p.start()
        time.sleep(server_starttime)
        return

    def tearDown(self):
        # Stop and delete the server
        self.p.terminate()
        return

    def __micro_service(self):
        """Micro service :)"""
        import petalus
        sw = petalus.SessionWalletManager()
        s = petalus.Server()
        sw.data_directory = self.data_directory
        sw.increase_allocated_memory(5)
        s.session_wallet_manager = sw
        s.address = ip
        s.port = user_port_1
        s.administration_address = ip
        s.administration_port = admin_port
        s.administration_ip_addresses = [ip]
        s.authorization_token = authorization_token
        s.private_key = "server_key.pem"
        s.certificate = self.certificate
        s.mtls = False 
        s.start()
        s.run()
        s.stop()

    def __create_wallet(self, password):
        """Creates a valid random wallet for the tests"""
        wallet, data = generate_data_wallet(password = password)
        uri = f"{self.layer}://{admin_address}/v1/create"

        res = do_post(requests, uri, json=data, headers=creation_headers)
        self.assertEqual(res.status_code, 201)
        return wallet

    def test01(self):
        """Create a basic wallet"""

        wallet = self.__create_wallet("AAAAA")
        self.assertGreater(len(wallet), 16)

    def test02(self):
        """Do basic operations, open read, write and close"""

        password = "Good morning1234"
        wallet = self.__create_wallet(password)
        self.assertGreater(len(wallet), 16)

        session = requests.Session()

        uri = f"{self.layer}://{address}/v1/open"
        open_data = { 
            "wallet": wallet, 
            "password": hashlib.sha256(password.encode('utf-8')).hexdigest().upper()
        }
        res = do_post(session, uri, json=open_data)
        self.assertEqual(res.status_code ,200)
        self.assertIn("Petalus-SessionId", res.headers)
        good_session = res.headers["Petalus-SessionId"]

        data1 = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
        data2 = "........................................................."
        data3 = "*********************************************************"

        uri = f"{self.layer}://{address}/v1/block/{wallet}"
        headers = { 
            "Petalus-SessionId": good_session,
            "Content-Type": "text/plain"
        }
        res = do_post(session, uri, data=data1, headers=headers)
        self.assertEqual(res.status_code ,200)
        res = do_post(session, uri, data=data2, headers=headers)
        self.assertEqual(res.status_code ,200)
        res = do_post(session, uri, data=data3, headers=headers)
        self.assertEqual(res.status_code ,200)

        uri = f"{self.layer}://{address}/v1/block/{wallet}/5"
        res = do_get(session, uri, headers=headers)
        self.assertEqual(res.status_code ,200)
        self.assertEqual(res.text, data3)

        uri = f"{self.layer}://{address}/v1/block/{wallet}/4"
        res = do_get(session, uri, headers=headers)
        self.assertEqual(res.status_code ,200)
        self.assertEqual(res.text, data2)

        uri = f"{self.layer}://{address}/v1/block/{wallet}/3"
        res = do_get(session, uri, headers=headers)
        self.assertEqual(res.status_code ,200)
        self.assertEqual(res.text, data1)

        uri = f"{self.layer}://{address}/v1/close"
        res = do_post(session, uri, headers=headers)
        self.assertEqual(res.status_code ,200)
 
        session.close()

class PetalusTestSuite03 (unittest.TestCase):
    """ The aim of this test suite is to verify the functionality 
    associated to the security of the service (security-callback)."""

    @classmethod
    def setUpClass(self):
        """Now we generate a EC private/public key with a self signed certificate"""
        self.private_key, self.certificate = generate_certificate("ec") 
        self.layer = "https"

        # We write the private key on a file LUIS
        with open("server_cert.pem", "w") as fd:
            fd.write(self.certificate.decode("utf-8"))
 
        # Delete the previous data runs
        self.data_directory = "./%s_data" % self.__name__.lower()
        shutil.rmtree(self.data_directory, ignore_errors=True)

    def setUp(self):
        self.p = Process(target = self.__micro_service)
        self.p.daemon = True
        self.p.start()
        time.sleep(server_starttime)
        return

    def tearDown(self):
        # Stop and delete the server
        self.p.terminate()
        return

    def __micro_service(self):
        """Example with a security callback"""
        import petalus
        sw = petalus.SessionWalletManager()
        s = petalus.Server()
        sw.data_directory = self.data_directory
        sw.increase_allocated_memory(5)
        s.session_wallet_manager = sw
        s.address = ip 
        s.port = user_port_1
        s.administration_port = admin_port
        s.administration_ip_addresses = [ ip ]
        s.authorization_token = authorization_token
        s.private_key = self.private_key
        s.certificate = "server_cert.pem"
       
        def security_callback(stats):
            #print(stats.http_no_sessions, stats.http_errors, stats.http_wrong_wallet_uris,
            #    stats.http_posts, stats.http_gets, stats.http_unknowns)
            if (stats.http_posts > 10):
                stats.close()
            elif (stats.http_gets > 10):
                stats.close()
            elif (stats.http_wrong_wallet_uris > 5):
                stats.close()
            elif (stats.http_no_sessions > 5):
                stats.close()

        s.security_callback = security_callback

        s.start()
        s.run()
        s.stop()

    def __create_wallet(self, password):
        """Creates a valid random wallet for the tests"""

        uri = f"{self.layer}://{admin_address}/v1/create"
        wallet, data = generate_data_wallet(password=password)

        res = do_post(requests, uri, json=data, headers=creation_headers)
        self.assertEqual(res.status_code, 201)
        return wallet

    def test01(self):
        """Example for limit the number of posts per connection"""

        password = "Good morning1234"
        wallet = self.__create_wallet(password)
        self.assertGreater(len(wallet), 16)

        httpsession = requests.Session()
        uri = f"{self.layer}://{address}/v1/open"
        open_data = { 
            "wallet": wallet, 
            "password": hashlib.sha256(password.encode('utf-8')).hexdigest().upper()
        }
        res = do_post(httpsession, uri, json=open_data)
        self.assertEqual(res.status_code ,200)
        self.assertIn("Petalus-SessionId", res.headers)
        good_session = res.headers["Petalus-SessionId"]

        data1 = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
        data2 = "........................................................."
        data3 = "*********************************************************"

        uri = f"{self.layer}://{address}/v1/block/{wallet}"

        headers = { 
            "Petalus-SessionId": good_session,
            "Content-Type": "text/plain"
        }
        for i in range(0, 9):
            res = do_post(httpsession, uri, data=data1, headers=headers)
            self.assertEqual(res.status_code ,200)

        # The next request will be close by the server
        try:
            res = do_post(httpsession, uri, data=data1, headers=headers)
            self.assertTrue(False)
        except requests.exceptions.ConnectionError as e:
            self.assertTrue(True)

        httpsession.close()

    def test02(self):
        """Example for limit the number of gets per connection"""

        password = "Good morning1234"
        wallet = self.__create_wallet(password)
        self.assertGreater(len(wallet), 16)

        httpsession = requests.Session()
        uri = f"{self.layer}://{address}/v1/open"
        open_data = { 
            "wallet": wallet, 
            "password": hashlib.sha256(password.encode('utf-8')).hexdigest().upper()
        }
        res = do_post(httpsession, uri, json=open_data)
        self.assertEqual(res.status_code ,200)
        self.assertIn("Petalus-SessionId", res.headers)
        good_session = res.headers["Petalus-SessionId"]

        data = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
        data += "........................................................."
        data += "*********************************************************"

        uri = f"{self.layer}://{address}/v1/block/{wallet}"
        headers = { 
            "Petalus-SessionId": good_session,
            "Content-Type": "text/plain"
        }
        res = do_post(httpsession, uri, data=data, headers=headers)
        self.assertEqual(res.status_code ,200)

        uri = f"{self.layer}://{address}/v1/block/{wallet}/3"
        # Make to many requests
        for i in range(0, 10):
            res = do_get(httpsession, uri, headers=headers)
            self.assertEqual(res.status_code ,200)

        # The next request will be close by the server
        try:
            _ = do_get(httpsession, uri, headers=headers)
            self.assertTrue(False)
        except requests.exceptions.ConnectionError as e:
            self.assertTrue(True)

        httpsession.close()

    def test03(self):
        """Simulate a open session attack"""

        password = "Good morning1234"
        wallet = self.__create_wallet(password)
        self.assertGreater(len(wallet), 16)

        httpsession = requests.Session()

        # Makes 5 open requests to try open the wallet
        for i in range(0, 5):
            uri = f"{self.layer}://{address}/v1/open"
            headers = { 
                "Petalus-SessionId": binascii.b2a_hex(os.urandom(16)).upper(),
                "Content-Type" : "text/pepe" 
            }
            res = do_post(httpsession, uri, headers=headers)
            self.assertEqual(res.status_code ,400) # bad_request

        # The next request will be close by the server
        try:
            _ = do_get(httpsession, uri, headers=headers)
            self.assertTrue(False)
        except requests.exceptions.ConnectionError as e:
            self.assertTrue(True)

        httpsession.close()

    def test04(self):
        """Simulate a polution session attack with two wallets"""

        password1 = "Good morning1234"
        password2 = "morning1234"
        wallet1 = self.__create_wallet(password1)
        wallet2 = self.__create_wallet(password2)
        self.assertGreater(len(wallet1), 16)
        self.assertGreater(len(wallet2), 16)

        httpsession1 = requests.Session()
        httpsession2 = requests.Session()

        uri = f"{self.layer}://{address}/v1/open"
        open_data = { 
            "wallet": wallet1, 
            "password": hashlib.sha256(password1.encode("utf-8")).hexdigest().upper()
        }
        res = do_post(httpsession1, uri, json=open_data)
        self.assertEqual(res.status_code ,200)
        self.assertIn("Petalus-SessionId", res.headers)
        good_session = res.headers["Petalus-SessionId"]

        open_data = { 
            "wallet": wallet2, 
            "password": hashlib.sha256(password2.encode("utf-8")).hexdigest().upper()
        }
        res = do_post(httpsession2, uri, json=open_data)
        self.assertEqual(res.status_code ,200)
        self.assertIn("Petalus-SessionId", res.headers)
        bad_session = res.headers["Petalus-SessionId"]

        self.assertGreater(len(good_session), 12)
        self.assertGreater(len(bad_session), 12)
        self.assertNotEqual(good_session, bad_session)

        data = "Some where over the rainbow......"
        uri = f"{self.layer}://{address}/v1/block/{wallet1}"
        headers = { 
            "Petalus-SessionId": bad_session, 
            "Content-Type": "text/plain"
        }

        # The user of the wallet2 tries to write on wallet1
        for i in range(0, 6):
            res = do_post(httpsession2, uri, data=data, headers=headers)
            self.assertEqual(res.status_code ,500)

        # The user1 writes in his own wallet
        headers = { 
            "Petalus-SessionId": good_session, 
            "Content-Type": "text/plain"
        }
        res = do_post(httpsession1, uri, data="Yes we can", headers=headers)
        self.assertEqual(res.status_code ,200)

        httpsession1.close()
        httpsession2.close()

class PetalusTestSuite04 (unittest.TestCase):
    """The aim of this test suite is to verify the functionality
    that signs messages from the client as well as full encryption
    of the data. """

    @classmethod
    def setUpClass(self):
        """Now we generate a EC private/public key with a self signed certificate"""

        self.private_key, self.certificate = generate_certificate("ec")
        self.layer = "https"

        # Delete the previous data runs
        self.data_directory = "./%s_data" % self.__name__.lower()
        shutil.rmtree(self.data_directory, ignore_errors=True)

    def setUp(self):
        self.p = Process(target=self.__micro_service)
        self.p.daemon = True
        self.p.start()
        time.sleep(server_starttime)
        return

    def tearDown(self):
        # Stop and delete the server
        self.p.terminate()
        return

    def __micro_service(self):
        """Micro service"""
        import petalus
        sw = petalus.SessionWalletManager()
        s = petalus.Server()
        sw.data_directory = self.data_directory
        sw.increase_allocated_memory(5)
        sw.hash_type = "blake2s"

        # AAAAAAAAAAAAAAAA 
        sw.encryption_password = "{AES}E4D67A8C15AF1CD29BBCBC1FC284764" \
                                 "E41B8B852FE1AD9FBC6FE2C8F30F61B20"
        s.session_wallet_manager = sw
        s.address = ip 
        s.port = user_port_1
        s.administration_address = ip
        s.administration_port = admin_port
        s.administration_ip_addresses = [ ip ]
        s.authorization_token = authorization_token
        s.private_key = self.private_key
        s.certificate = self.certificate

        s.start()
        s.run()
        s.stop()

    def __create_wallet(self, password, folders=[], curve=ecdsa.SECP256k1):
        """Creates a valid random wallet for the tests"""

        uri = f"{self.layer}://{admin_address}/v1/create"

        wallet, data = generate_data_wallet(password=password)
        sk = ecdsa.SigningKey.generate(curve=curve)
        vk = sk.get_verifying_key()
        pubkey = vk.to_der()

        data["ec_public_key"] = base64.b64encode(pubkey).decode("utf-8")
        data["wallet"] = wallet

        if (len(folders) > 0):
            data["folders"] = folders

        res = do_post(requests, uri, json=data, headers=creation_headers)
        self.assertEqual(res.status_code, 201)
        return wallet, sk

    def __basic_use_case(self, wallet, user_password, sk):
        """Helper for the tests that just change the EC curve type"""
        session = requests.Session()

        uri = f"{self.layer}://{address}/v1/open"
        open_data = { 
            "wallet": wallet, 
            "password": hashlib.sha256(user_password.encode("utf-8")).hexdigest().upper()
        }
        res = do_post(session, uri, json=open_data)
        self.assertEqual(res.status_code ,200)
        self.assertIn("Petalus-SessionId", res.headers)
        good_session = res.headers["Petalus-SessionId"]

        self.assertGreater(len(good_session), 12)

        # Now sign a block of data
        data1 = "Some where over the rainbow ......"

        # sha256 the data received
        a = data1.encode("utf-8")
        hdata = hashlib.sha256(a).digest();

        # sign and base64 the generated signature
        sb = base64.b64encode(sk.sign(hdata, hashfunc=hashlib.sha256))

        headers = { 
            "Petalus-SessionId" : good_session,
            "Petalus-User-Signature" : sb 
        }
        uri = f"{self.layer}://{address}/v1/block/{wallet}"
        res = do_post(session, uri, data=data1, headers=headers)
        self.assertEqual(res.status_code ,200)

        data2 = "This information is not signed by the user"
        headers = {"Petalus-SessionId" : good_session}
        uri = f"{self.layer}://{address}/v1/block/{wallet}"
        res = do_post(session, uri, data=data2, headers=headers)
        self.assertEqual(res.status_code ,200)

        # Now read the written blocks
        uri = f"{self.layer}://{address}/v1/block/{wallet}/3"
        headers = {"Petalus-SessionId" : good_session}
        res = do_get(session, uri, headers=headers)
        self.assertEqual(res.status_code ,200)
        self.assertEqual(res.text, data1)

        uri = f"{self.layer}://{address}/v1/block/{wallet}/4"
        headers = {"Petalus-SessionId" : good_session}
        res = do_get(session, uri, headers=headers)
        self.assertEqual(res.status_code ,200)
        self.assertEqual(res.text, data2)

        session.close()

    def test01(self):
        """Creates a valid wallet with a public key on it"""
        wallet, sk = self.__create_wallet("AAAAA")
        self.assertGreater(len(wallet), 16)

    def test02(self):
        """Creates a valid wallet with a public key on it
        and read and write some blocks"""

        user_password = "Some password"
        wallet, sk = self.__create_wallet(user_password)
        self.assertGreater(len(wallet), 16)

        session = requests.Session()

        uri = f"{self.layer}://{address}/v1/open"
        open_data = { 
            "wallet": wallet, 
            "password": hashlib.sha256(user_password.encode("utf-8")).hexdigest().upper()
        }
        res = do_post(session, uri, json=open_data)
        self.assertEqual(res.status_code ,200)
        self.assertIn("Petalus-SessionId", res.headers)
        good_session = res.headers["Petalus-SessionId"]

        self.assertGreater(len(good_session), 12)

        # Now sign a block of data
        data1 = "Some where over the rainbow......"

        # sha256 the data received
        hdata = hashlib.sha256(data1.encode("utf-8")).digest();

        # sign and base64 the generated signature
        sb = base64.b64encode(sk.sign(hdata, hashfunc=hashlib.sha256))

        headers = { 
            "Petalus-SessionId" : good_session, 
            "Petalus-User-Signature" : sb 
        }
        uri = f"{self.layer}://{address}/v1/block/{wallet}"
        res = do_post(session, uri, data=data1, headers=headers)
        self.assertEqual(res.status_code ,200)

        data2 = "This information is not signed by the user"
        headers = {"Petalus-SessionId" : good_session} 
        uri = f"{self.layer}://{address}/v1/block/{wallet}"
        res = do_post(session, uri, data=data2, headers=headers)
        self.assertEqual(res.status_code ,200)

        # Now read the written blocks
        uri = f"{self.layer}://{address}/v1/block/{wallet}/3"
        headers = {"Petalus-SessionId" : good_session}
        res = do_get(session, uri, headers=headers)
        self.assertEqual(res.status_code ,200)
        self.assertEqual(res.text, data1)
        
        uri = f"{self.layer}://{address}/v1/block/{wallet}/4"
        headers = {"Petalus-SessionId" : good_session}
        res = do_get(session, uri, headers=headers)
        self.assertEqual(res.status_code ,200)
        self.assertEqual(res.status_code ,200)
        self.assertEqual(res.text, data2)

        session.close()

    def test03(self):
        """Creates a valid wallet with a public key and 
        tries to polute with invalid signatures"""

        user_password = "Some password"
        wallet, sk = self.__create_wallet(user_password)
        self.assertGreater(len(wallet), 16)

        with requests.Session() as session:
            uri = f"{self.layer}://{address}/v1/open"
            open_data = { 
                "wallet": wallet, 
                "password": hashlib.sha256(user_password.encode("utf-8")).hexdigest().upper()
            }
            res = do_post(session, uri, json=open_data)
            self.assertEqual(res.status_code ,200)
            self.assertIn("Petalus-SessionId", res.headers)
            good_session = res.headers["Petalus-SessionId"]

            self.assertGreater(len(good_session), 12)

            # Makes 50 requests to try if one writes on the wallet with random signatures
            uri = f"{self.layer}://{address}/v1/block/{wallet}"
            for i in range(0, 50):
                headers = { 
                    "Petalus-SessionId": good_session, 
                    "Petalus-User-Signature": base64.b64encode(binascii.b2a_hex(os.urandom(32)).upper()) 
                }
                res = do_post(session, uri, data="Please dont write this!",  headers=headers)
                self.assertEqual(res.status_code ,500) 

            # The same but with big length on the signature
            for i in range(0, 50):
                signature = base64.b64encode(binascii.b2a_hex(os.urandom(512)).upper()) 
                headers = { 
                    "Petalus-SessionId": good_session, 
                    "Petalus-User-Signature": signature 
                }
                res = do_post(session, uri, data="Please dont write this!",  headers=headers)
                self.assertEqual(res.status_code ,500) 

    def test04(self):
        """Creates a valid wallet with a public key on it
        and read and write some blocks on folders """

        user_password = "Some password"
        wallet, sk = self.__create_wallet(user_password,
                                          folders=["bills", "notes", "claims", "debt"])
        self.assertGreater(len(wallet), 16)

        session = requests.Session()

        uri = f"{self.layer}://{address}/v1/open"
        open_data = { 
            "wallet": wallet, 
            "password": hashlib.sha256(user_password.encode("utf-8")).hexdigest().upper() 
        }
        res = do_post(session, uri, json=open_data)
        self.assertEqual(res.status_code ,200)
        self.assertIn("Petalus-SessionId", res.headers)
        good_session = res.headers["Petalus-SessionId"]

        self.assertGreater(len(good_session), 12)

        data = {}
        data["bills"] = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
        data["notes"] = "XXXXXXXXXXXXXX"
        data["claims"] = "****************************************"
        data["debt"] = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"

        for key, value in data.items():
            hdata = hashlib.sha256(value.encode("utf-8")).digest();
            sb = base64.b64encode(sk.sign(hdata, hashfunc=hashlib.sha256))
            headers = { 
                "Petalus-SessionId" : good_session,
                "Petalus-User-Signature" : sb 
            }
            uri = f"{self.layer}://{address}/v1/block/{wallet}/{key}"
            res = do_post(session, uri, data=value, headers=headers)
            self.assertEqual(res.status_code ,200)

        # Now read the written blocks
        for key, value in data.items():
            uri = f"{self.layer}://{address}/v1/block/{wallet}/{key}/2"
            headers = { "Petalus-SessionId" : good_session }
            res = do_get(session, uri, headers=headers)
            self.assertEqual(res.status_code ,200)
            self.assertEqual(res.text, value)

        # Now read blocks that dont exists
        for key, value in data.items():
            uri = f"{self.layer}://{address}/v1/block/{wallet}/{key}/3"
            headers = {"Petalus-SessionId" : good_session}
            res = do_get(session, uri, headers=headers)
            self.assertEqual(res.status_code ,404)
            self.assertNotEqual(res.text, value)

        session.close()

    def test05(self):
        """Send encrypted text from the user.""" 
        user_password = "Some password"

        wallet, sk = self.__create_wallet(user_password, folders=["debt"])
        self.assertGreater(len(wallet), 16)

        session = requests.Session()

        uri = f"{self.layer}://{address}/v1/open"
        open_data = { 
            "wallet": wallet, 
            "password": hashlib.sha256(user_password.encode("utf-8")).hexdigest().upper() 
        }
        res = do_post(session, uri, json=open_data)
        self.assertEqual(res.status_code ,200)
        self.assertIn("Petalus-SessionId", res.headers)
        good_session = res.headers["Petalus-SessionId"]

        encryption_key = os.urandom(32)    
        nonce = os.urandom(16)
        algorithm = algorithms.ChaCha20(encryption_key, nonce)
        cipher = Cipher(algorithm, mode=None, backend=default_backend())
        encryptor = cipher.encryptor()
        secret = b"This is a secret message that only the user can see :)"
        data = encryptor.update(secret)
       
        # Write the block two times
        hdata = hashlib.sha256(data).digest();
        sb = base64.b64encode(sk.sign(hdata, hashfunc=hashlib.sha256))
        headers = { 
            "Petalus-SessionId": good_session,
            "Petalus-User-Signature": sb,
            "Content-Type": "application/pgp-encrypted" 
        }
        uri = f"{self.layer}://{address}/v1/block/{wallet}" 
        res = do_post(session, uri, data=data, headers=headers)
        self.assertEqual(res.status_code ,200)

        # Now without the signature
        headers = { 
            "Petalus-SessionId": good_session,
            "Content-Type": "application/pgp-encrypted" 
        }
        uri = f"{self.layer}://{address}/v1/block/{wallet}" 
        res = do_post(session, uri, data=data, headers=headers)
        self.assertEqual(res.status_code ,200)

        # Now read the blocks
        uri = f"{self.layer}://{address}/v1/block/{wallet}/4" 
        headers = {"Petalus-SessionId" : good_session}
        res = do_get(session, uri, headers=headers)
        self.assertEqual(res.status_code ,200)
        self.assertEqual(res.headers.get('content-type'), "application/pgp-encrypted")
        self.assertEqual(res.content, data)

        uri = f"{self.layer}://{address}/v1/block/{wallet}/5" 
        headers = {"Petalus-SessionId" : good_session}
        res = do_get(session, uri, headers=headers)
        self.assertEqual(res.status_code ,200)
        self.assertEqual(res.headers.get('content-type'), "application/pgp-encrypted")
        self.assertEqual(res.content, data)

        session.close()

    def test06(self):
        """Send some signed files on folder"""

        user_password = "Some password"
        wallet, sk = self.__create_wallet(user_password, folders=["debt"])
        self.assertGreater(len(wallet), 16)

        session = requests.Session()

        uri = f"{self.layer}://{address}/v1/open"
        open_data = { 
            "wallet": wallet, 
            "password": hashlib.sha256(user_password.encode("utf-8")).hexdigest().upper() 
        }
        res = do_post(session, uri, json=open_data)
        self.assertEqual(res.status_code ,200)
        self.assertIn("Petalus-SessionId", res.headers)
        good_session = res.headers["Petalus-SessionId"]

        data1 = os.urandom(1024 * 1024) # 1MB
        data2 = os.urandom(4096)

        hdata = hashlib.sha256(data1).digest();
        sb = base64.b64encode(sk.sign(hdata, hashfunc=hashlib.sha256))
        headers = { 
            "Petalus-SessionId": good_session,
            "Petalus-User-Signature": sb,
            "Content-Type": "application/octet-stream" 
        }
        uri = f"{self.layer}://{address}/v1/block/{wallet}"
        res = do_post(session, uri, data=data1, headers=headers)
        self.assertEqual(res.status_code ,200)

        hdata = hashlib.sha256(data2).digest();
        sb = base64.b64encode(sk.sign(hdata, hashfunc=hashlib.sha256))
        headers = { 
            "Petalus-SessionId": good_session,
            "Petalus-User-Signature": sb,
            "Content-Type": "application/octet-stream" 
        }
        uri = f"{self.layer}://{address}/v1/block/{wallet}/debt"
        res = do_post(session, uri, data=data2, headers=headers)
        self.assertEqual(res.status_code ,200)

        # Now read the generated blocks
        uri = f"{self.layer}://{address}/v1/block/{wallet}/4"
        headers = {"Petalus-SessionId" : good_session}
        res = do_get(session, uri, headers=headers)
        self.assertEqual(res.status_code ,200)
        self.assertEqual(res.headers.get('content-type'), "application/octet-stream")
        self.assertEqual(res.content, data1)

        uri = f"{self.layer}://{address}/v1/block/{wallet}/debt/2"
        headers = {"Petalus-SessionId" : good_session}
        res = do_get(session, uri, headers=headers)
        self.assertEqual(res.status_code ,200)
        self.assertEqual(res.headers.get('content-type'), "application/octet-stream")
        self.assertEqual(res.content, data2)

        # Do a big post that will generate an HTTP body exceeds
        data3 = os.urandom(1024 * 1024 * 16) # 16 MBytes
        headers = { 
            "Petalus-SessionId" : good_session,
            "Content-Type" : "application/octet-stream" 
        }
        uri = f"{self.layer}://{address}/v1/block/{wallet}"
        try:
            res = do_post(session, uri, data=data3, headers=headers)
            self.assertEqual(res.status_code ,200)
        except requests.exceptions.ConnectionError as e:
            self.assertTrue(True)

        # The connection has been closed on the previous request
        uri = f"{self.layer}://{address}/v1/close"
        headers = {"Petalus-SessionId": good_session}
        res = do_post(session, uri, headers=headers)
        self.assertEqual(res.status_code ,400) 

        session.close()

    def test07(self):
        """Reopen the wallet some times"""

        user_password = "Some password"
        wallet, sk = self.__create_wallet(user_password, folders=["debt"])
        self.assertGreater(len(wallet), 16)

        session = requests.Session()

        uri = f"{self.layer}://{address}/v1/open"
        open_data = { 
            "wallet": wallet, 
            "password": hashlib.sha256(user_password.encode("utf-8")).hexdigest().upper()
        }
        res = do_post(session, uri, json=open_data)
        self.assertEqual(res.status_code ,200)
        self.assertIn("Petalus-SessionId", res.headers)
        good_session = res.headers["Petalus-SessionId"]
        for i in range(0, 4):
            res = do_post(session, uri, json=open_data)
            self.assertEqual(res.status_code ,200)
            self.assertIn("Petalus-SessionId", res.headers)
            self.assertEqual(res.headers["Petalus-SessionId"], good_session)

        # Now the password for open is wrong
        open_data = { 
            "wallet": wallet, 
            "password": "bugggggyyyyy" 
        }
        res = do_post(session, uri, json=open_data)
        self.assertEqual(res.status_code ,401)
        self.assertNotIn("Petalus-SessionId", res.headers)

        # The session is close
        headers = { 
            "Petalus-SessionId": good_session,
            "Content-Type": "application/octet-stream" 
        }
        uri = f"{self.layer}://{address}/v1/block/{wallet}/debt"
        res = do_post(session, uri, data=os.urandom(128), headers=headers)
        self.assertEqual(res.status_code ,400)

        # Tries to read ....
        uri = f"{self.layer}://{address}/v1/block/{wallet}/1"
        headers = {"Petalus-SessionId" : good_session}
        res = do_get(session, uri, headers=headers)
        self.assertEqual(res.status_code ,404)

        session.close()

    def test08(self):
        """Send some signed json data"""

        user_password = "Some password"
        wallet, sk = self.__create_wallet(user_password, folders=["debt"])
        self.assertGreater(len(wallet), 16)

        session = requests.Session()

        uri = f"{self.layer}://{address}/v1/open"
        open_data = { 
            "wallet": wallet, 
            "password": hashlib.sha256(user_password.encode("utf-8")).hexdigest().upper()
        }
        res = do_post(session, uri, json=open_data)
        self.assertEqual(res.status_code ,200)
        self.assertIn("Petalus-SessionId", res.headers)
        good_session = res.headers["Petalus-SessionId"]

        data1 = { 
            "key1" : "value1",
            "key2" : "value2",
            "key3" : base64.b64encode(os.urandom(128)).decode("utf-8"),
            "key4" : [ 1, 2, 3, 4, 5, 6, 7, 8, 9],
            "key5" : {
                "subkey1": "other value",
                "subkey2": "other value a bit longer"
            }
        }

        # Sign the json data
        a = json.dumps(data1).encode("utf-8")
        hdata = hashlib.sha256(a).digest();
        sb = base64.b64encode(sk.sign(hdata, hashfunc=hashlib.sha256))
        headers = { 
            "Petalus-SessionId" : good_session,
            "Petalus-User-Signature" : sb 
        }
        uri = f"{self.layer}://{address}/v1/block/{wallet}"
        res = do_post(session, uri, json=data1, headers=headers)
        self.assertEqual(res.status_code ,200)

        # Now read the generated block
        uri = f"{self.layer}://{address}/v1/block/{wallet}/4"
        headers = {"Petalus-SessionId": good_session}
        res = do_get(session, uri, headers=headers)
        self.assertEqual(res.status_code ,200)
        self.assertEqual(res.headers.get('content-type'), "application/json")
        ret = res.json()
        self.assertEqual(ret, data1)

        session.close()

    def test09(self):
        """Send a encrypted user block that is upload as application/octect-stream 
        So there will be two encryptions maded on client side and server"""

        user_password = "Some password"
        wallet, sk = self.__create_wallet(user_password)
        self.assertGreater(len(wallet), 16)

        session = requests.Session()

        uri = f"{self.layer}://{address}/v1/open"
        open_data = { 
            "wallet": wallet, 
            "password": hashlib.sha256(user_password.encode("utf-8")).hexdigest().upper()
        }
        res = do_post(session, uri, json=open_data)
        self.assertEqual(res.status_code ,200)
        self.assertIn("Petalus-SessionId", res.headers)
        good_session = res.headers["Petalus-SessionId"]

        encryption_key = os.urandom(32)
        nonce = os.urandom(16)
        algorithm = algorithms.ChaCha20(encryption_key, nonce)
        cipher = Cipher(algorithm, mode=None, backend=default_backend())
        encryptor = cipher.encryptor()
        secret = b"This is a secret message that only the user can see :) and is double encrypted"
        data = encryptor.update(secret)

        # Sign the block
        hdata = hashlib.sha256(data).digest();
        sb = base64.b64encode(sk.sign(hdata, hashfunc=hashlib.sha256))
        headers = { 
            "Petalus-SessionId" : good_session,
            "Petalus-User-Signature" : sb,
            "Content-Type" : "application/octet-stream" 
        }
        uri = f"{self.layer}://{address}/v1/block/{wallet}"
        res = do_post(session, uri, data=data, headers=headers)
        self.assertEqual(res.status_code ,200)

        # Now we retrieve the block again
        uri = f"{self.layer}://{address}/v1/block/{wallet}/3"
        headers = {"Petalus-SessionId" : good_session}
        res = do_get(session, uri, headers=headers)
        self.assertEqual(res.status_code ,200)
        self.assertEqual(res.headers.get('content-type'), "application/octet-stream")

        decryptor = cipher.decryptor()
        ret = decryptor.update(res.content)
        self.assertEqual(ret, secret)

        session.close()

    def test10(self):
        """Creates a valid wallet with a public key
        NIST521p on it and read and write some blocks"""

        user_password = "Some password"
        wallet, sk = self.__create_wallet(user_password, curve=ecdsa.NIST521p)
        self.assertGreater(len(wallet), 16)

        self.__basic_use_case(wallet, user_password, sk)

    def test11(self):
        """Creates a valid wallet with a public key
        NIST384p on it and read and write some blocks"""

        user_password = "XXXX password"
        wallet, sk = self.__create_wallet(user_password, curve=ecdsa.NIST384p)
        self.assertGreater(len(wallet), 16)

        self.__basic_use_case(wallet, user_password, sk)

    def test12(self):
        """Creates a valid wallet with a public key
        NIST256p on it and read and write some blocks"""

        user_password = "YYYY password"
        wallet, sk = self.__create_wallet(user_password, curve=ecdsa.NIST256p)
        self.assertGreater(len(wallet), 16)

        self.__basic_use_case(wallet, user_password, sk)

    def test13(self):
        """Generate a RSA public key and tries to create a wallet with it"""

        uri = f"{self.layer}://{admin_address}/v1/create"

        wallet, data = generate_data_wallet(name="Terje", surname="Hakonsen", password="ABCDEFGHIJ")

        private_key = rsa.generate_private_key(
            public_exponent=65537,
            key_size=2048,
            backend=default_backend())
        
        public_key = private_key.public_key()
        pubkey = private_key.private_bytes(
            encoding=serialization.Encoding.DER,
            format=serialization.PrivateFormat.TraditionalOpenSSL,
            encryption_algorithm=serialization.NoEncryption())

        data["ec_public_key"] = base64.b64encode(pubkey).decode("utf-8")
        data["wallet"] = wallet

        res = do_post(requests, uri, json=data, headers=creation_headers)
        self.assertEqual(res.status_code, 403)
        self.assertEqual(res.content, b"Can not create wallet [error code:5]") 

        # Check also the logs
        with open("petalus-00000.log") as fd:
            last = fd.readlines()[-2]
            self.assertIn("Can not create wallet [error code:5]", str(last))

    def test14(self):
        """Get the documentation file of petalus and upload signed"""

        user_password = "user_password"
        wallet, sk = self.__create_wallet(user_password, folders=["policies"], curve=ecdsa.NIST384p)
        self.assertGreater(len(wallet), 16)

        session = requests.Session()

        uri = f"{self.layer}://{address}/v1/open"
        open_data = {
            "wallet": wallet,
            "password": hashlib.sha256(user_password.encode("utf-8")).hexdigest().upper()
        }
        res = do_post(session, uri, json=open_data)
        self.assertEqual(res.status_code ,200)
        self.assertIn("Petalus-SessionId", res.headers)
        good_session = res.headers["Petalus-SessionId"]

        hash_sha256 = hashlib.sha256()
        with open("../docs/petalus.rst", "rb") as f:
            for chunk in iter(lambda: f.read(4096), b""):
                hash_sha256.update(chunk)
    
        f = open("../docs/petalus.rst", "r")
        filedata = f.read()
        f.close()

        sb = base64.b64encode(sk.sign(hash_sha256.digest(), hashfunc=hashlib.sha256))
        headers = {
            "Petalus-SessionId": good_session,
            "Petalus-User-Signature": sb,
            "Content-Type": "application/octet-stream"
        }
        uri = f"{self.layer}://{address}/v1/block/{wallet}/policies"
        res = do_post(session, uri, data=filedata, headers=headers)
        self.assertEqual(res.status_code ,200)
        self.assertEqual(res.text, "Block 2 added to folder policies")
        
        # Now read the file again
        uri = f"{self.layer}://{address}/v1/block/{wallet}/policies/2"
        headers = {"Petalus-SessionId" : good_session}
        res = do_get(session, uri, headers=headers)
        self.assertEqual(res.status_code ,200)
        readhash = hashlib.sha256(res.content).hexdigest()

        # The content must be the same
        self.assertEqual(hash_sha256.hexdigest(), readhash)

        session.close()

    def test15(self):
        """We use a different module for generate
        the public/privake keys with EC"""

        uri = f"{self.layer}://{admin_address}/v1/create"

        wallet, data = generate_data_wallet(name="Terje", surname="Hakonsen", password="12345qwert")

        private_key = ec.generate_private_key(ec.SECP521R1(), default_backend())
        public_key = private_key.public_key()
        pubkey = public_key.public_bytes(
            encoding=serialization.Encoding.DER,
            format=serialization.PublicFormat.SubjectPublicKeyInfo)

        data["ec_public_key"] = base64.b64encode(pubkey).decode("utf-8")
        data["wallet"] = wallet

        res = do_post(requests, uri, json=data, headers=creation_headers)
        self.assertEqual(res.status_code, 201)

class PetalusTestSuite05 (unittest.TestCase):
    """ The aim of this test suite is to verify the functionality
        of the triggers with JSON data

        Also shows how the wallet.block.accept property can be used
        for accept or reject the block depending on the logic of the trigger.
    """

    @classmethod
    def setUpClass(self):
        """Generate two wallets for all the suite"""

        self.layer = "https"
        self.data_directory = "./%s_data" % self.__name__.lower()
        shutil.rmtree(self.data_directory, ignore_errors=True)

        self.user_password = "password"
        self.wallet1, self.data1 = generate_data_wallet(name="Travis",
                                                        surname="Rice",
                                                        password=self.user_password)
        self.wallet2, self.data2 = generate_data_wallet(name="Torstein",
                                                        surname="Horgmo",
                                                        password=self.user_password)

        self.wallet1_created = False 
        self.wallet2_created = False

        # Just a public key on the second wallet
        self.sk = ecdsa.SigningKey.generate(curve=ecdsa.SECP256k1)
        vk = self.sk.get_verifying_key()
        pubkey = vk.to_der()

        self.data2["ec_public_key"] = base64.b64encode(pubkey).decode("utf-8")

        # We generate a RSA private key
        self.private_key, self.certificate = generate_certificate("rsa")

    def setUp(self):
        self.p = Process(target=self.__micro_service)
        self.p.daemon = True
        self.p.start()
        time.sleep(server_starttime)
        return

    def tearDown(self):
        # Stop and delete the server
        self.p.terminate()
        return

    def __micro_service(self):
        """Micro service"""
        import petalus
        sw = petalus.SessionWalletManager()
        s = petalus.Server()
        sw.data_directory = self.data_directory
        sw.increase_allocated_memory(5)
        sw.hash_type = "sha3-256"
        sw.encryption_password = ""
        s.session_wallet_manager = sw
        s.port = user_port_1
        s.administration_port = admin_port
        s.administration_ip_addresses = [ ip ]
        s.authorization_token = authorization_token
        s.private_key = self.private_key
        s.certificate = self.certificate

        def read_callback(wallet):
            a = json.loads(wallet.block.data)
            if "read_reject" in a:
                wallet.block.accept = False
            else: 
                wallet.block.accept = True

        def write_callback(wallet):
            a = json.loads(wallet.block.data)
            if "write_reject" in a:
                wallet.block.accept = False
            else: 
                wallet.block.accept = True

        # Add the triggers
        sw.add_read_trigger(self.wallet1, read_callback)
        sw.add_write_trigger(self.wallet1, write_callback)
        sw.add_read_trigger(self.wallet2, read_callback)
        sw.add_write_trigger(self.wallet2, write_callback)

        s.start()
        s.run()
        s.stop()
        sys.exit(0)

    def test01(self):
        """TODO"""
 
        uri = f"{self.layer}://{admin_address}/v1/create"
        res = do_post(requests, uri, json=self.data1, headers=creation_headers)
        self.assertEqual(res.status_code, 201)
        type(self).wallet1_created = True

        session = requests.Session()

        uri = f"{self.layer}://{address}/v1/open"
        open_data = { 
            "wallet": self.wallet1, 
            "password": hashlib.sha256(self.user_password.encode("utf-8")).hexdigest().upper()
        }
        res = do_post(session, uri, json=open_data)
        self.assertEqual(res.status_code ,200)
        self.assertIn("Petalus-SessionId", res.headers)
        good_session = res.headers["Petalus-SessionId"]

        data = {"key": "value"} 

        headers = {"Petalus-SessionId" : good_session}
        uri = f"{self.layer}://{address}/v1/block/{self.wallet1}"
        res = do_post(session, uri, json=data, headers=headers)
        self.assertEqual(res.status_code ,200)

        uri = f"{self.layer}://{address}/v1/block/{self.wallet1}/3"
        headers = {"Petalus-SessionId": good_session}
        res = do_get(session, uri, headers=headers)
        self.assertEqual(res.status_code ,200)
        self.assertEqual(res.headers.get('content-type'), "application/json")
        ret = res.json()
        self.assertIn("key", ret)

        session.close()

    def test02(self):
        """Write a block that later will be rejected by the callback"""

        if (self.wallet1_created == False):
            uri = f"{self.layer}://{admin_address}/v1/create"
            res = do_post(requests, uri, json=self.data1, headers=creation_headers)
            self.assertEqual(res.status_code, 201)
            type(self).wallet1_created = True

        session = requests.Session()

        uri = f"{self.layer}://{address}/v1/open"
        open_data = { 
            "wallet": self.wallet1, 
            "password": hashlib.sha256(self.user_password.encode("utf-8")).hexdigest().upper()
        }
        res = do_post(session, uri, json=open_data)
        self.assertEqual(res.status_code ,200)
        self.assertIn("Petalus-SessionId", res.headers)
        good_session = res.headers["Petalus-SessionId"]

        data = { 
            "key1" : "value", 
            "key2" : "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", 
            "key3" : base64.b64encode(os.urandom(200)).decode("utf-8"),
            "key4" : [ "im an item on a list" ],
            "read_reject" : True 
        }

        headers = {"Petalus-SessionId" : good_session}
        uri = f"{self.layer}://{address}/v1/block/{self.wallet1}"
        res = do_post(session, uri, json=data, headers=headers)
        self.assertEqual(res.status_code ,200)

        # The read block as been rejected
        uri = f"{self.layer}://{address}/v1/block/{self.wallet1}/2"
        headers = {"Petalus-SessionId" : good_session}
        res = do_get(session, uri, headers=headers)
        self.assertEqual(res.status_code ,200)

        data = { 
            "key1" : "value",
            "key2" : "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" 
        }

        headers = {"Petalus-SessionId" : good_session}
        uri = f"{self.layer}://{address}/v1/block/{self.wallet1}"
        res = do_post(session, uri, json=data, headers=headers)
        self.assertEqual(res.status_code ,200)

        session.close()

    def test03(self):
        """Reject Write blocks"""

        if (self.wallet1_created == False):
            uri = f"{self.layer}://{admin_address}/v1/create"
            res = do_post(requests, uri, json=self.data1, headers=creation_headers)
            self.assertEqual(res.status_code, 201)
            type(self).wallet1_created = True

        session = requests.Session()

        uri = f"{self.layer}://{address}/v1/open"
        open_data = { 
            "wallet": self.wallet1, 
            "password": hashlib.sha256(self.user_password.encode("utf-8")).hexdigest().upper()
        }
        res = do_post(session, uri, json=open_data)
        self.assertEqual(res.status_code ,200)
        self.assertIn("Petalus-SessionId", res.headers)
        good_session = res.headers["Petalus-SessionId"]

        data = {
            "key1" : "value",
            "key2" : "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
            "key3" : base64.b64encode(os.urandom(200)).decode("utf-8"),
            "key4" : [ "im an item on a list"],
            "write_reject" : True
        }

        headers = {"Petalus-SessionId" : good_session}
        uri = f"{self.layer}://{address}/v1/block/{self.wallet1}"
        res = do_post(session, uri, json=data, headers=headers)
        self.assertEqual(res.status_code ,500)

        del data["write_reject"] 

        headers = {"Petalus-SessionId" : good_session}
        uri = f"{self.layer}://{address}/v1/block/{self.wallet1}"
        res = do_post(session, uri, json=data, headers=headers)
        self.assertEqual(res.status_code ,200)
        session.close()

    def test04(self):
        """Using the second wallet with public key fo write"""

        uri = f"{self.layer}://{admin_address}/v1/create"

        res = do_post(requests, uri, json=self.data2, headers=creation_headers)
        self.assertEqual(res.status_code, 201)

        session = requests.Session()

        uri = f"{self.layer}://{address}/v1/open"
        open_data = { 
            "wallet": self.wallet2, 
            "password": hashlib.sha256(self.user_password.encode("utf-8")).hexdigest().upper()
        }
        res = do_post(session, uri, json=open_data)
        self.assertEqual(res.status_code ,200)
        self.assertIn("Petalus-SessionId", res.headers)
        good_session = res.headers["Petalus-SessionId"]

        data = {
            "key1" : "value",
            "key2" : "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
            "key3" : base64.b64encode(os.urandom(200)).decode("utf-8"),
            "key4" : [ "im an item on a list"],
            "write_reject" : True
        } 

        # sign the data
        a = json.dumps(data).encode("utf-8")        
        hdata = hashlib.sha256(a).digest();
        sb = base64.b64encode(self.sk.sign(hdata, hashfunc=hashlib.sha256))
        headers = { 
            "Petalus-SessionId" : good_session,
            "Petalus-User-Signature" : sb 
        }

        uri = f"{self.layer}://{address}/v1/block/{self.wallet2}"
        res = do_post(session, uri, json=data, headers=headers)
        self.assertEqual(res.status_code ,500)

        # Remove the flag and resign the  data
        del data["write_reject"]

        # sign the data
        a = json.dumps(data).encode("utf-8")        
        hdata = hashlib.sha256(a).digest();
        sb = base64.b64encode(self.sk.sign(hdata, hashfunc=hashlib.sha256))
        headers = { 
            "Petalus-SessionId": good_session,
            "Petalus-User-Signature": sb 
        }

        uri = f"{self.layer}://{address}/v1/block/{self.wallet2}"
        res = do_post(session, uri, json=data, headers=headers)
        self.assertEqual(res.status_code ,200)

        # Read the block
        uri = f"{self.layer}://{address}/v1/block/{self.wallet2}/3"
        headers = {"Petalus-SessionId" : good_session}
        res = do_get(session, uri, headers=headers)
        self.assertEqual(res.status_code ,200)
        self.assertEqual(json.dumps(data), res.text) 

        session.close()

class PetalusTestSuite06 (unittest.TestCase):
    """ The aim of this test suite is to verify the case
        where there is no SessionWalletManager plugged on the server

    """
    @classmethod
    def setUpClass(self):
        """ Delete the previous data runs """
        self.data_directory = "./%s_data" % self.__name__.lower()
        shutil.rmtree(self.data_directory, ignore_errors=True)
        
        # We generate a RSA private key
        self.private_key, self.certificate = generate_certificate("rsa")
        self.layer = "https"

    def setUp(self):
        self.p = Process(target = self.__micro_service)
        self.p.daemon = True
        self.p.start()
        time.sleep(server_starttime)
        return

    def tearDown(self):
        # Stop and delete the server
        self.p.terminate()
        return

    def __micro_service(self):
        """ Micro service :) """
        import petalus
        sw = petalus.SessionWalletManager()
        s = petalus.Server()
        sw.data_directory = self.data_directory
        sw.increase_allocated_memory(1)
        sw.hash_type = "blake2s"
        s.address = ip
        s.port = user_port_1
        s.administration_port = admin_port
        s.authorization_token = authorization_token
        s.private_key = self.private_key
        s.certificate = self.certificate

        s.start()
        s.run()
        s.stop()
        sys.exit(0)

    def test01(self):
        """ Tries to create a wallet but no Session Manager is pluged to the server """
        wallet, data = generate_data_wallet(name="Gigi", surname="Ruf", password="XXXXXXXXXXX")

        uri = f"{self.layer}://{admin_address}/v1/create"
        try:
            _ = do_post(requests, uri, json = data, headers = creation_headers)
            self.assertTrue(False)
        except requests.exceptions.ConnectionError as e:
            self.assertTrue(True)

    def test02(self):
        """Verifies that can not access to the status and wallets"""

        uri = f"{self.layer}://{admin_address}/petalus/status"
        try:
            _ = do_get(requests, uri)
            self.assertTrue(False)
        except requests.exceptions.ConnectionError as e:
            self.assertTrue(True)

        # Verifies that can not access to the sessions output
        uri = f"{self.layer}://{admin_address}/petalus/sessions"
        try:
            _ = do_get(requests, uri)
            self.assertTrue(False)
        except requests.exceptions.ConnectionError as e:
            self.assertTrue(True)


class PetalusTestSuite07 (unittest.TestCase):
    """ The aim of this test suite is to verify the cases
        where there is no memory allocated on the SessionWalletManager
    """

    @classmethod
    def setUpClass(self):
        """ Delete the previous data runs """
        self.data_directory = "./%s_data" % self.__name__.lower()
        shutil.rmtree(self.data_directory, ignore_errors=True)

        # We generate a RSA private key
        self.private_key, self.certificate = generate_certificate("ec")
        self.layer = "https"

    def setUp(self):
        self.p = Process(target = self.__micro_service)
        self.p.daemon = True
        self.p.start()
        time.sleep(server_starttime)
        return

    def tearDown(self):
        # Stop and delete the server
        self.p.terminate()
        return

    def __micro_service(self):
        """ Micro service :) """
        import petalus
        sw = petalus.SessionWalletManager()
        s = petalus.Server()
        sw.data_directory = self.data_directory
        """ Just one wallet on memory allow """
        sw.increase_allocated_memory(1)
        sw.hash_type = "blake2s"
        s.address = ip
        s.port = user_port_1
        s.administration_port = admin_port 
        s.administration_ip_addresses = [ ip ]
        s.authorization_token = authorization_token
        s.session_wallet_manager = sw

        s.private_key = self.private_key
        s.certificate = self.certificate

        s.start()
        s.run()
        s.stop()
        sys.exit(0)

    def test01(self):
        """Create two wallets and only one is able open"""

        password1 = "asdf1342"
        password2 = "asdf1342"
        wallet1, data1 = generate_data_wallet(name="Bert",
                                              surname="LaMar",
                                              password=password1)
        wallet2, data2 = generate_data_wallet(name="John Paul",
                                              surname="Walker",
                                              password=password2)

        # Create two wallets on the same connection
        session = requests.Session()
        uri = f"{self.layer}://{admin_address}/v1/create"
        res = do_post(session, uri, json=data1, headers=creation_headers)
        self.assertEqual(res.status_code, 201)
        res = do_post(session, uri, json=data2, headers=creation_headers)
        self.assertEqual(res.status_code, 201)
        session.close()

        session1 = requests.Session()
        session2 = requests.Session()

        # User1 opens the wallet
        uri = f"{self.layer}://{address}/v1/open"
        open_data = { 
            "wallet": wallet1, 
            "password": hashlib.sha256(password1.encode("utf-8")).hexdigest().upper()
        }
        res = do_post(session1, uri, json=open_data)
        self.assertEqual(res.status_code ,200)
        self.assertIn("Petalus-SessionId", res.headers)
        good_session = res.headers["Petalus-SessionId"]

        # User2 can not open the wallet
        uri = f"{self.layer}://{address}/v1/open"
        open_data = { 
            "wallet": wallet2, 
            "password": hashlib.sha256(password2.encode("utf-8")).hexdigest().upper()
        }
        res = do_post(session2, uri, json=open_data)
        self.assertEqual(res.status_code ,500)
        self.assertEqual(res.content, b"Can not open more sessions [error code:1]")
        self.assertNotIn("Petalus-SessionId", res.headers)

        # User2 writes and reads a block
        headers = {"Petalus-SessionId": good_session}
        uri = f"{self.layer}://{address}/v1/block/{wallet1}"
        res = do_post(session1, uri, data="X", headers=headers)
        self.assertEqual(res.status_code ,200)

        uri = f"{self.layer}://{address}/v1/block/{wallet1}/3"
        res = do_get(session1, uri, headers=headers)
        self.assertEqual(res.status_code ,200)
        self.assertEqual(res.content, b"X")

        # User2 stil can not open the wallet
        uri = f"{self.layer}://{address}/v1/open"
        open_data = { 
            "wallet": wallet2, 
            "password": hashlib.sha256(password2.encode("utf-8")).hexdigest().upper()
        }
        res = do_post(session2, uri, json=open_data)
        self.assertEqual(res.status_code ,500)
        self.assertEqual(res.content, b"Can not open more sessions [error code:1]")
        self.assertNotIn("Petalus-SessionId", res.headers)

        # User1 close the wallet
        uri = f"{self.layer}://{address}/v1/close"
        headers = {"Petalus-SessionId": good_session}
        res = do_post(session1, uri, headers=headers)
        self.assertEqual(res.status_code ,200)

        # Now user2 can open the wallet
        uri = f"{self.layer}://{address}/v1/open"
        open_data = { 
            "wallet": wallet2, 
            "password": hashlib.sha256(password2.encode("utf-8")).hexdigest().upper()
        }
        res = do_post(session2, uri, json=open_data)
        self.assertEqual(res.status_code ,200)
        self.assertIn("Petalus-SessionId", res.headers)

        session1.close()
        session2.close()

class PetalusTestSuite08 (unittest.TestCase):
    """ The aim of this test suite is to verify the case
        where there is no administration IP address so no wallets
        can be created 

    """
    @classmethod
    def setUpClass(self):
        """ Delete the previous data runs """
        self.data_directory = "./%s_data" % self.__name__.lower()
        shutil.rmtree(self.data_directory, ignore_errors=True)

        self.layer = "http"

    def setUp(self):
        self.p = Process(target = self.__micro_service)
        self.p.daemon = True
        self.p.start()
        time.sleep(server_starttime)
        return

    def tearDown(self):
        # Stop and delete the server
        self.p.terminate()
        return

    def __micro_service(self):
        """ Micro service :) """
        import petalus
        sw = petalus.SessionWalletManager()
        s = petalus.Server()
        sw.data_directory = self.data_directory
        sw.increase_allocated_memory(5)
        sw.hash_type = "blake2s"
        s.address = ip 
        s.port = user_port_1
        s.administration_port = admin_port
        s.session_wallet_manager = sw

        s.start()
        s.run()
        s.stop()
        sys.exit(0)

    def test01(self):
        """ Tries to Create a wallet """
        password = "asdf1342"
        wallet, data = generate_data_wallet(name = "Bert", surname = "LaMar", password = password)

        uri = f"{self.layer}://{admin_address}/v1/create"
        try:
            _ = do_post(requests, uri, json=data, headers=creation_headers)
            self.assertTrue(False)
        except requests.exceptions.ConnectionError as e:
            self.assertTrue(True)

        uri = f"{self.layer}://{admin_address}/petalus/status"
        try:
            _ = do_post(requests, uri, json=[1,2,3,4,5,6], headers={'Connection':'close'})
            self.assertTrue(False)
        except requests.exceptions.ConnectionError as e:
            self.assertTrue(True)

class PetalusTestSuite09 (unittest.TestCase):
    """ The aim of this test suite is to verify the case
        where the petalus instance is behind a proxy and
        process the Forwarded header 

    """
    @classmethod
    def setUpClass(self):
        """ Delete the previous data runs """
        self.data_directory = "./%s_data" % self.__name__.lower()
        shutil.rmtree(self.data_directory, ignore_errors=True)
        self.layer = "http"

    def setUp(self):
        self.p = Process(target = self.__micro_service)
        self.p.daemon = True
        self.p.start()
        time.sleep(server_starttime)
        return

    def tearDown(self):
        # Stop and delete the server
        self.p.terminate()
        return

    def __micro_service(self):
        """ Micro service :) """
        import petalus
        sw = petalus.SessionWalletManager()
        s = petalus.Server()
        sw.data_directory = self.data_directory
        sw.increase_allocated_memory(5)
        sw.hash_type = "blake2s"
        s.address = ip
        s.port = user_port_1
        s.administration_port = admin_port
        s.session_wallet_manager = sw
        s.administration_ip_addresses = [ ip ]
        s.authorization_token = authorization_token 
        s.proxy_support = True

        def security_callback(stats):
            if (stats.http_posts > 2):
                stats.close()

        s.security_callback = security_callback

        s.start()
        s.run()
        s.stop()
        sys.exit(0)

    def test01(self):
        """Create one wallet and verify the IPs"""
        password = "asdf1342"
        wallet, data = generate_data_wallet(name="Bert", surname="LaMar", password=password)

        uri = f"{self.layer}://{admin_address}/v1/create"
        res = do_post(requests, uri, json=data, headers=creation_headers)
        self.assertEqual(res.status_code, 201)

        session = requests.Session()

        # User1 opens the wallet
        real_ip = "192.168.1.%d" % randint(1, 255)

        headers = {"Forwarded" : real_ip}
        uri = f"{self.layer}://{address}/v1/open"
        open_data = { 
            "wallet": wallet, 
            "password": hashlib.sha256(password.encode('utf-8')).hexdigest().upper()
        }
        res = do_post(session, uri, json=open_data, headers=headers) 
        self.assertEqual(res.status_code ,200)
        self.assertIn("Petalus-SessionId", res.headers)
        good_session = res.headers["Petalus-SessionId"]

        # Open the log file and verify that the IP has changed
        with open("petalus-00000.log") as fd:
            last = fd.readlines()[-1]
            self.assertIn(real_ip, last)

        headers = { 
            "Petalus-SessionId" : good_session, 
            "Forwarded" : real_ip 
        }
        uri = f"{self.layer}://{address}/v1/block/{wallet}"
        res = do_post(session, uri, data="X", headers=headers) 
        self.assertEqual(res.status_code ,200)

        # Open the log file and verify that the IP has changed
        with open("petalus-00000.log") as fd:
            last = fd.readlines()[-1]
            self.assertIn(real_ip, last)

        uri = f"{self.layer}://{address}/v1/block/{wallet}"
        try:
            res = do_post(session, uri, data = "This is closed", headers=headers)
            self.assertTrue(False)
            self.assertEqual(res.status_code ,200)
        except requests.exceptions.ConnectionError as e:
            self.assertTrue(True)

        session.close()

    def test02(self):
        """Simulate a SessionID hijack from another IP."""
        password1 = "asdf1342"
        wallet1, data1 = generate_data_wallet(name="Bert", surname="LaMar", password=password1)

        password2 = "asdf1342AAAAAA"
        wallet2, data2 = generate_data_wallet(name="Victor", surname="De la rue", password=password2)

        uri = f"{self.layer}://{admin_address}/v1/create"
        res = do_post(requests, uri, json=data1, headers=creation_headers)
        self.assertEqual(res.status_code, 201)
        res = do_post(requests, uri, json=data2, headers=creation_headers)
        self.assertEqual(res.status_code, 201)

        with requests.Session() as session, requests.Session() as bad_session:

            good_ip = "192.168.1.8"
            bad_ip = "192.168.1.188"

            headers = {"Forwarded" : good_ip}
            uri = f"{self.layer}://{address}/v1/open"
            open_data = {
                "wallet": wallet1,
                "password": hashlib.sha256(password1.encode('utf-8')).hexdigest().upper()
            }
            res = do_post(session, uri, json=open_data, headers=headers)
            self.assertEqual(res.status_code ,200)
            self.assertIn("Petalus-SessionId", res.headers)
            good_session = res.headers["Petalus-SessionId"]

            headers = {"Forwarded" : bad_ip}
            uri = f"{self.layer}://{address}/v1/open"
            open_data = {
                "wallet": wallet2,
                "password": hashlib.sha256(password2.encode('utf-8')).hexdigest().upper()
            }
            res = do_post(session, uri, json=open_data, headers=headers)
            self.assertEqual(res.status_code ,200)
            self.assertIn("Petalus-SessionId", res.headers)
            bad_id = res.headers["Petalus-SessionId"]

            headers = {
                "Petalus-SessionId" : good_session,
                "Forwarded" : bad_ip
            }
            uri = f"{self.layer}://{address}/v1/block/{wallet1}"
            res = do_post(bad_session, uri, data="You are been hacked", headers=headers)
            self.assertEqual(res.status_code ,400)


class PetalusTestSuite10 (unittest.TestCase):
    """ The aim of this test suite is to verify the read_only
        property of the SessionWalletManager.
        Be aware that there is a dependency between the tests cases 

    """
    @classmethod
    def setUpClass(self):
        """ Delete the previous data runs """
        self.data_directory = "./%s_data" % self.__name__.lower()
        shutil.rmtree(self.data_directory, ignore_errors=True)

        self.layer = "http"
        self.user_password = "password"
        # Generate two wallets for all the suite
        self.wallet1, self.data1 = generate_data_wallet(name="Travis",
                                                        surname="Rice",
                                                        password=self.user_password)
        self.wallet2, self.data2 = generate_data_wallet(name="Torstein",
                                                        surname="Horgmo",
                                                        password=self.user_password)

        self.block_data1 = binascii.b2a_hex(os.urandom(randint(256, 500))).upper()
        self.block_data2 = binascii.b2a_hex(os.urandom(randint(128, 1024))).upper()
        self.wallets_created = False

    def setUp(self):
        self.p = Process(target = self.__micro_service)
        self.p.daemon = True
        self.p.start()
        time.sleep(server_starttime)
        return

    def tearDown(self):
        # Stop and delete the server
        self.p.terminate()
        return

    def __micro_service(self):
        """ Micro service :) """
        import petalus
        sw = petalus.SessionWalletManager()
        s = petalus.Server()
        sw.data_directory = self.data_directory
        sw.increase_allocated_memory(5)
        sw.hash_type = "blake2s"
        s.address = ip
        s.port = user_port_1
        s.administration_port = admin_port
        s.session_wallet_manager = sw
        s.administration_ip_addresses = [ ip ]
        s.authorization_token = authorization_token
        if (self.wallets_created == True):
            sw.read_only = True

        s.start()
        s.run()
        s.stop()
        sys.exit(0)

    def test01(self):
        """Create two wallets and write two blocks on them"""

        uri = f"{self.layer}://{admin_address}/v1/create"
        res = do_post(requests, uri, json=self.data1, headers=creation_headers)
        self.assertEqual(res.status_code, 201)
        res = do_post(requests, uri, json=self.data2, headers=creation_headers)
        self.assertEqual(res.status_code, 201)
        type(self).wallets_created = True

        session1 = requests.Session()
        session2 = requests.Session()

        # User1 opens the wallet
        uri = f"{self.layer}://{address}/v1/open"
        open_data = { 
            "wallet": self.wallet1, 
            "password": hashlib.sha256(self.user_password.encode("utf-8")).hexdigest().upper()
        }
        res = do_post(session1, uri, json=open_data)
        self.assertEqual(res.status_code ,200)
        self.assertIn("Petalus-SessionId", res.headers)
        good_session1 = res.headers["Petalus-SessionId"]

        # User2 open the wallet
        uri = f"{self.layer}://{address}/v1/open"
        open_data = { 
            "wallet": self.wallet2, 
            "password": hashlib.sha256(self.user_password.encode("utf-8")).hexdigest().upper()
        }
        res = do_post(session2, uri, json=open_data)
        self.assertEqual(res.status_code ,200)
        self.assertIn("Petalus-SessionId", res.headers)
        good_session2 = res.headers["Petalus-SessionId"]

        # User1 writes a block
        headers = {"Petalus-SessionId": good_session1}
        uri = f"{self.layer}://{address}/v1/block/{self.wallet1}"
        res = do_post(session1, uri, data=self.block_data1, headers=headers)
        self.assertEqual(res.status_code ,200)

        # User2 writes a block
        headers = {"Petalus-SessionId": good_session2}
        uri = f"{self.layer}://{address}/v1/block/{self.wallet2}"
        res = do_post(session2, uri, data=self.block_data2, headers=headers)
        self.assertEqual(res.status_code ,200)

        # User1 close the wallet
        uri = f"{self.layer}://{address}/v1/close"
        headers = {"Petalus-SessionId": good_session1}
        res = do_post(session1, uri, headers=headers)
        self.assertEqual(res.status_code ,200)

        # User2 close the wallet
        uri = f"{self.layer}://{address}/v1/close"
        headers = {"Petalus-SessionId": good_session2}
        res = do_post(session2, uri, headers=headers)
        self.assertEqual(res.status_code ,200)



class PetalusTestSuite10 (unittest.TestCase):
    """ The aim of this test suite is to verify the read_only
        property of the SessionWalletManager.
        Be aware that there is a dependency between the tests cases 

    """
    @classmethod
    def setUpClass(self):
        """ Delete the previous data runs """
        self.data_directory = "./%s_data" % self.__name__.lower()
        shutil.rmtree(self.data_directory, ignore_errors=True)

        self.layer = "http"
        self.user_password = "password"
        # Generate two wallets for all the suite
        self.wallet1, self.data1 = generate_data_wallet(name="Travis",
                                                        surname="Rice",
                                                        password=self.user_password)
        self.wallet2, self.data2 = generate_data_wallet(name="Torstein",
                                                        surname="Horgmo",
                                                        password=self.user_password)

        self.block_data1 = binascii.b2a_hex(os.urandom(randint(256, 500))).upper()
        self.block_data2 = binascii.b2a_hex(os.urandom(randint(128, 1024))).upper()
        self.wallets_created = False

    def setUp(self):
        self.p = Process(target = self.__micro_service)
        self.p.daemon = True
        self.p.start()
        time.sleep(server_starttime)
        return

    def tearDown(self):
        # Stop and delete the server
        self.p.terminate()
        return

    def __micro_service(self):
        """ Micro service :) """
        import petalus
        sw = petalus.SessionWalletManager()
        s = petalus.Server()
        sw.data_directory = self.data_directory
        sw.increase_allocated_memory(5)
        sw.hash_type = "blake2s"
        s.address = ip
        s.port = user_port_1
        s.administration_port = admin_port
        s.session_wallet_manager = sw
        s.administration_ip_addresses = [ ip ]
        s.authorization_token = authorization_token
        if (self.wallets_created == True):
            sw.read_only = True

        s.start()
        s.run()
        s.stop()
        sys.exit(0)

    def test01(self):
        """Create two wallets and write two blocks on them"""

        uri = f"{self.layer}://{admin_address}/v1/create"
        res = do_post(requests, uri, json=self.data1, headers=creation_headers)
        self.assertEqual(res.status_code, 201)
        res = do_post(requests, uri, json=self.data2, headers=creation_headers)
        self.assertEqual(res.status_code, 201)
        type(self).wallets_created = True

        session1 = requests.Session()
        session2 = requests.Session()

        # User1 opens the wallet
        uri = f"{self.layer}://{address}/v1/open"
        open_data = { 
            "wallet": self.wallet1, 
            "password": hashlib.sha256(self.user_password.encode("utf-8")).hexdigest().upper()
        }
        res = do_post(session1, uri, json=open_data)
        self.assertEqual(res.status_code ,200)
        self.assertIn("Petalus-SessionId", res.headers)
        good_session1 = res.headers["Petalus-SessionId"]

        # User2 open the wallet
        uri = f"{self.layer}://{address}/v1/open"
        open_data = { 
            "wallet": self.wallet2, 
            "password": hashlib.sha256(self.user_password.encode("utf-8")).hexdigest().upper()
        }
        res = do_post(session2, uri, json=open_data)
        self.assertEqual(res.status_code ,200)
        self.assertIn("Petalus-SessionId", res.headers)
        good_session2 = res.headers["Petalus-SessionId"]

        # User1 writes a block
        headers = {"Petalus-SessionId": good_session1}
        uri = f"{self.layer}://{address}/v1/block/{self.wallet1}"
        res = do_post(session1, uri, data=self.block_data1, headers=headers)
        self.assertEqual(res.status_code ,200)

        # User2 writes a block
        headers = {"Petalus-SessionId": good_session2}
        uri = f"{self.layer}://{address}/v1/block/{self.wallet2}"
        res = do_post(session2, uri, data=self.block_data2, headers=headers)
        self.assertEqual(res.status_code ,200)

        # User1 close the wallet
        uri = f"{self.layer}://{address}/v1/close"
        headers = {"Petalus-SessionId": good_session1}
        res = do_post(session1, uri, headers=headers)
        self.assertEqual(res.status_code ,200)

        # User2 close the wallet
        uri = f"{self.layer}://{address}/v1/close"
        headers = {"Petalus-SessionId": good_session2}
        res = do_post(session2, uri, headers=headers)
        self.assertEqual(res.status_code ,200)

        session1.close()
        session2.close()

    def test02(self):

        if (self.wallets_created == False):
            print("This tests have dependency with other tests of the suite")
            self.assertEqual(True, False)

        session1 = requests.Session()
        session2 = requests.Session()

        # User1 opens the wallet
        uri = f"{self.layer}://{address}/v1/open"
        open_data = { 
            "wallet": self.wallet1, 
            "password": hashlib.sha256(self.user_password.encode("utf-8")).hexdigest().upper()
        }
        res = do_post(session1, uri, json=open_data)
        self.assertEqual(res.status_code ,200)
        self.assertIn("Petalus-SessionId", res.headers)
        good_session1 = res.headers["Petalus-SessionId"]

        # User2 open the wallet
        uri = f"{self.layer}://{address}/v1/open"
        open_data = { 
            "wallet": self.wallet2, 
            "password": hashlib.sha256(self.user_password.encode("utf-8")).hexdigest().upper()
        }
        res = do_post(session2, uri, json=open_data)
        self.assertEqual(res.status_code ,200)
        self.assertIn("Petalus-SessionId", res.headers)
        good_session2 = res.headers["Petalus-SessionId"]

        # They can read the information
        uri = f"{self.layer}://{address}/v1/block/{self.wallet1}/3"
        headers = {"Petalus-SessionId": good_session1}
        res = do_get(session1, uri, headers=headers)
        self.assertEqual(res.status_code ,200)
        self.assertEqual(res.content, self.block_data1)

        uri = f"{self.layer}://{address}/v1/block/{self.wallet2}/3"
        headers = {"Petalus-SessionId": good_session2} 
        res = do_get(session2, uri, headers=headers)
        self.assertEqual(res.status_code ,200)
        self.assertEqual(res.content, self.block_data2)

        # But they can not write
        headers = {"Petalus-SessionId": good_session1}
        uri = f"{self.layer}://{address}/v1/block/{self.wallet1}"
        res = do_post(session1, uri, data="This can not be written", headers=headers)
        self.assertEqual(res.status_code ,503)
        self.assertEqual(res.content, b"System in read only mode")

        uri = f"{self.layer}://{address}/v1/block/{self.wallet1}/4"
        headers = {"Petalus-SessionId": good_session1}
        res = do_get(session1, uri, headers=headers)
        self.assertEqual(res.status_code ,404)

        headers = {"Petalus-SessionId": good_session2}
        uri = f"{self.layer}://{address}/v1/block/{self.wallet2}"
        res = do_post(session2, uri, data="This can not be written", headers=headers)
        self.assertEqual(res.status_code ,503)
        self.assertEqual(res.content, b"System in read only mode")

        uri = f"{self.layer}://{address}/v1/block/{self.wallet2}/4"
        headers = {"Petalus-SessionId": good_session2}
        res = do_get(session2, uri, headers=headers)
        self.assertEqual(res.status_code ,404)

        # But can close the wallet
        uri = f"{self.layer}://{address}/v1/close"
        headers = {"Petalus-SessionId": good_session1}
        res = do_post(session1, uri, headers=headers)
        self.assertEqual(res.status_code ,200)

        headers = {"Petalus-SessionId": good_session2}
        res = do_post(session2, uri, headers=headers)
        self.assertEqual(res.status_code ,200)

        session1.close()
        session2.close()

class PetalusTestSuite11 (unittest.TestCase):
    """ The aim of this test suite is to verify that external
        IP address are allow to read from the wallet with no issues 

    """
    @classmethod
    def setUpClass(self):
        """Delete the previous data runs"""
        self.data_directory = "./%s_data" % self.__name__.lower()
        shutil.rmtree(self.data_directory, ignore_errors=True)

        self.layer = "http"
        self.token = "YWFhYWFhYWFhYWFhYWFhYQ=="

    def setUp(self):
        self.p = Process(target=self.__micro_service)
        self.p.daemon = True
        self.p.start()
        time.sleep(server_starttime)
        return

    def tearDown(self):
        # Stop and delete the server
        self.p.terminate()
        return

    def __micro_service(self):
        """Example of micro service"""
        import petalus
        sw = petalus.SessionWalletManager()
        s = petalus.Server()
        sw.data_directory = self.data_directory
        sw.increase_allocated_memory(5)
        sw.hash_type = "blake2s"
        s.address = ip
        s.port = user_port_1
        s.administration_address = "0.0.0.0" 
        s.administration_port = admin_port
        s.session_wallet_manager = sw
        s.administration_ip_addresses = [readonly_ip, ip]
        s.authorization_token = authorization_token

        s.start()
        s.run()
        s.stop()
        sys.exit(0)

    def test01(self):
        """Create one wallet and verify the IPs can not read
        because dont have the Authorization header"""

        password = "asdf1342"
        wallet, data = generate_data_wallet(name="Bert", surname="LaMar", password=password)

        uri = f"{self.layer}://{admin_address}/v1/create"
        res = do_post(requests, uri, json=data, headers=creation_headers)
        self.assertEqual(res.status_code, 201)

        session = requests.Session()
        read_session = pycurl.Curl()

        # User opens the wallet
        uri = f"{self.layer}://{address}/v1/open"
        open_data = {
            "wallet": wallet,
            "password": hashlib.sha256(password.encode('utf-8')).hexdigest().upper()
        }

        res = do_post(session, uri, json=open_data)
        self.assertEqual(res.status_code ,200)
        self.assertIn("Petalus-SessionId", res.headers)
        good_session1 = res.headers["Petalus-SessionId"]

        uri = f"{self.layer}://{address}/v1/block/{wallet}/1"
        headers = { 
            "Petalus-SessionId" : good_session1,
            "Content-Type" : "text/plain"
        }

        res = do_get(session, uri, headers=headers)
        self.assertEqual(res.status_code ,200)

        # Now the read only system tries to opens the wallet of the user
        open_readonly_data = {"wallet": wallet}
        uri = f"{self.layer}://{admin_address}/v1/open"
        headers = {"Content-Type": "application/json"}

        code, res, id = curl_request(read_session, uri,
                                     headers, data=json.dumps(open_readonly_data))
        self.assertEqual(code, 401)
        self.assertEqual(id ,"")
        self.assertEqual(res, "No Authorization token found")    

        # Now we put a bad authorization header
        headers["Authorization"] = "Not" 
        code, res, id = curl_request(read_session, uri,
                                     headers, data=json.dumps(open_readonly_data))
        self.assertEqual(code, 401)
        self.assertEqual(id ,"")
        self.assertEqual(res, "Wrong Authorization parameters")    

        # Now we put a bad authorization header
        headers["Authorization"] = "Not correct" 
        code, res, id = curl_request(read_session, uri,
                                     headers, data=json.dumps(open_readonly_data))
        self.assertEqual(code, 401)
        self.assertEqual(id ,"")
        self.assertEqual(res, "Only Authorization Basic supported")    

        # Another a bad authorization header
        headers["Authorization"] = "Basic correct" 
        code, res, id = curl_request(read_session, uri,
                                     headers, data=json.dumps(open_readonly_data))
        self.assertEqual(code, 401)
        self.assertEqual(id ,"")
        self.assertEqual(res, "Authorization token failed")

        read_session.close()
        session.close()

    def test02(self):
        """Create one wallet and verify the IPs"""

        password = "asdf1342"
        wallet, data = generate_data_wallet(name="Bert", surname="LaMar", password=password)

        uri = f"{self.layer}://{admin_address}/v1/create"
        res = do_post(requests, uri, json=data, headers=creation_headers)
        self.assertEqual(res.status_code, 201)

        read_session = pycurl.Curl()
        session = requests.Session()

        # User opens the wallet
        uri = f"{self.layer}://{address}/v1/open"
        open_data = {
            "wallet": wallet,
            "password": hashlib.sha256(password.encode('utf-8')).hexdigest().upper()
        }
        res = do_post(session, uri, json=open_data)
        self.assertEqual(res.status_code ,200)
        self.assertIn("Petalus-SessionId", res.headers)
        good_session1 = res.headers["Petalus-SessionId"]

        uri = f"{self.layer}://{address}/v1/block/{wallet}/1"
        headers = {"Petalus-SessionId" : good_session1} 
        res = do_get(session, uri, headers=headers)
        self.assertEqual(res.status_code ,200)

        # Now the read only system opens the wallet of the user
        open_readonly_data = {"wallet": wallet}
        uri = f"{self.layer}://{admin_address}/v1/open"
        headers = {"Authorization" : f"Basic {self.token}"}
        code, res, id = curl_request(read_session, uri, headers)
        self.assertEqual(code ,404)

        # Now the wallet is set on the POST
        code, res, id = curl_request(read_session, uri,
                                     headers, data=json.dumps(open_readonly_data))
        self.assertEqual(code ,200)

        # The readonly system use the session id of the current open wallet
        self.assertEqual(good_session1, id)

        uri = f"{self.layer}://{admin_address}/v1/block/{wallet}/1"
        rheaders = { 
            "Petalus-SessionId": good_session1,
            "Authorization": f"Basic {self.token}"
        } 
        code, res, id = curl_request(read_session, uri, rheaders)
        self.assertEqual(code, 200)
        self.assertIn("password_sha256", res)
        self.assertIn("salt", res)

        # The readonly system can not write on the wallet
        uri = f"{self.layer}://{admin_address}/v1/block/{wallet}"
        rheaders["Content-Type"] = "text/plain"
        code, res, id = curl_request(read_session, uri, rheaders, data="This can not be written")
        self.assertEqual(code, 404)
        self.assertEqual(res, "Not found")

        # But the real owner can
        headers = { 
            "Petalus-SessionId": good_session1,
            "Content-Type": "text/plain"
        } 
        data = base64.b64encode(os.urandom(512)).decode("utf-8")
        uri = f"{self.layer}://{address}/v1/block/{wallet}"
        res = do_post(session, uri, data=data, headers=headers)
        self.assertEqual(res.status_code ,200)

        # However the readonly can read the last block written :)
        uri = f"{self.layer}://{admin_address}/v1/block/{wallet}/3"
        code, res, id = curl_request(read_session, uri, rheaders)
        self.assertEqual(data, res)
        self.assertEqual(code, 200)

        read_session.close()
        session.close()

    def test03(self):
        """Create 3 wallets and verify the readonly system can read them if open of course"""

        password1 = "asdf1342"
        password2 = "asdf1342aaaa"
        password3 = "asdf1342xxxx"
        wallet1, data1 = generate_data_wallet(name="Bert",
                                              surname="LaMar", password=password1)
        wallet2, data2 = generate_data_wallet(name="Max",
                                              surname="Parrot", password=password2)
        wallet3, data3 = generate_data_wallet(name="Mark",
                                              surname="McMorris", password=password3)

        wallets = [[wallet1, data1, password1, requests.Session()],
            [wallet2, data2, password2, requests.Session()],
            [wallet3, data3, password3, requests.Session()]]

        uri = f"{self.layer}://{admin_address}/v1/create"
        for w in wallets:
            res = do_post(requests, uri, json=w[1], headers=creation_headers)
            self.assertEqual(res.status_code, 201)

        # Users opens the wallets
        uri = f"{self.layer}://{address}/v1/open"
        for w in wallets:
            open_data = {
                "wallet": w[0],
                "password": hashlib.sha256(w[2].encode("utf-8")).hexdigest().upper() 
            }

            res = do_post(w[3], uri, json=open_data)
            self.assertEqual(res.status_code ,200)
            self.assertIn("Petalus-SessionId", res.headers)
            good_session = res.headers["Petalus-SessionId"]
            w.append(good_session)

        """ Write on the wallets """
        for w in wallets:
            headers = { "Petalus-SessionId" : w[4] }
            data = base64.b64encode(os.urandom(512)).decode("utf-8")
            uri = f"{self.layer}://{address}/v1/block/{w[0]}"
            res = do_post(w[3], uri, data=data, headers=headers)
            self.assertEqual(res.status_code ,200)
            w.append(data)

        read_session = pycurl.Curl() 
        # The read only system reads the wallet content
        # The session wil be label just for read that wallet
        w = wallets[0]

        uri = f"{self.layer}://{address}/v1/block/{w[0]}/3"
        headers = { 
            "Petalus-SessionId" : w[4],
            "Authorization": f"Basic {self.token}"
        } 
        # This will fail because the read system should open the wallet also
        code, res, id = curl_request(read_session, uri, headers)
        self.assertEqual(code, 404)
        self.assertEqual(res, "Session ID not valid for read")
        self.assertEqual(len(id), 0)

        # The read system needs to open the wallet also
        open_data = {"wallet": w[0]}
        headers = {"Authorization" : f"Basic {self.token}"}
        uri = f"{self.layer}://{admin_address}/v1/open"
        code, res, id = curl_request(read_session, uri, headers, data=json.dumps(open_data))
        self.assertEqual(code, 200)
        self.assertEqual(id, w[4])

        # Now the read system can read the wallet
        uri = f"{self.layer}://{admin_address}/v1/block/{w[0]}/3"
        headers = {
            "Petalus-SessionId": id,
            "Authorization": f"Basic {self.token}"
        }
        # This will fail because the read system should open the wallet also
        code, res, id = curl_request(read_session, uri, headers)
        self.assertEqual(code, 200)
        self.assertEqual(w[5], res)

        # Add a Accept header with the json format
        headers = {
            "Accept": "application/json",
            "Authorization": f"Basic {self.token}"
        }
        uri = f"{self.layer}://{admin_address}/v1/sessions"
        code, res, id = curl_request(read_session, uri, headers)
        self.assertEqual(code, 200)
        self.assertEqual(len(res), 3)

        # Users close their wallets
        uri = f"{self.layer}://{address}/v1/close"
        for w in wallets:
            headers = {"Petalus-SessionId": w[4]}
            res = do_post(w[3], uri, headers=headers)
            self.assertEqual(res.status_code ,200)

        # The read only system can not read anything, all the wallets are close
        uri = f"{self.layer}://{admin_address}/v1/block/{w[0]}/3"
        headers = { 
            "Petalus-SessionId" : w[4],
            "Authorization": f"Basic {self.token}"
        } 
        code, res, id = curl_request(read_session, uri, headers)
        self.assertEqual(code, 404)
        self.assertEqual("Session ID not valid for read", res)

        read_session.close()

        for w in wallets:
            w[3].close()

    def test04(self):
        """Create one wallet and open by the readonly system"""

        password = "asdf1342"
        wallet, data = generate_data_wallet(name="Bert", surname="LaMar", password=password)

        uri = f"{self.layer}://{admin_address}/v1/create"
        res = do_post(requests, uri, json=data, headers=creation_headers)
        self.assertEqual(res.status_code, 201)

        # Tries to open with no password
        uri = f"{self.layer}://{address}/v1/open"
        open_data = {"wallet": wallet}

        res = do_post(requests, uri, json=open_data)
        self.assertEqual(res.status_code ,401)
        self.assertEqual(res.text, "Wrong credentials [error code:4]")

        # Now the read only system opens the wallet
        read_session = pycurl.Curl() 
        uri = f"{self.layer}://{admin_address}/v1/open"
        headers = {"Authorization": "Basic %s" % self.token}
        code, res, id = curl_request(read_session, uri, headers, data = json.dumps(data))
        self.assertEqual(code, 200)

        # Now the real owner opens his wallet
        uri = f"{self.layer}://{address}/v1/open"
        open_data = {
            "wallet": wallet,
            "password": hashlib.sha256(password.encode('utf-8')).hexdigest().upper()
        }
        session = requests.Session()
        res = do_post(session, uri, json=open_data)
        self.assertEqual(res.status_code ,200)
        self.assertIn("Petalus-SessionId", res.headers)
        good_session = res.headers["Petalus-SessionId"]

        # write
        headers = { 
            "Petalus-SessionId": good_session,
            "Content-Type": "text/plain"
        }
        uri = f"{self.layer}://{address}/v1/block/{wallet}"
        data = base64.b64encode(os.urandom(1024)).decode("utf-8")
        res = do_post(session, uri, data=data, headers=headers)
        self.assertEqual(res.status_code ,200)

        # read the information
        uri = f"{self.layer}://{address}/v1/block/{wallet}/3"
        headers = { "Petalus-SessionId": good_session }
        res = do_get(session, uri, headers=headers)
        self.assertEqual(res.status_code ,200)
        self.assertEqual(res.text, data)

        # Read the info from the readonly system
        rheaders = {
            "Petalus-SessionId": id,
            "Authorization": f"Basic {self.token}",
            "Content-Type": "text/plain"
        }
        uri = f"{self.layer}://{admin_address}/v1/block/{wallet}/3"
        code, res, id = curl_request(read_session, uri, rheaders)
        self.assertEqual(code, 200)
        self.assertEqual(res, data)

        # Tries to write by error
        code, res, id = curl_request(read_session, uri, rheaders, data = "AAAA")
        self.assertEqual(code, 404)
        self.assertEqual(res, "Not found")

        read_session.close()
        session.close()

    def test05(self):
        """Verifies that can access to the status page with the corresponding token"""

        uri = f"{self.layer}://{admin_address}/v1/status"
        rheaders = {
            "Authorization": "Basic %s" % self.token
        }

        read_session = pycurl.Curl() 
        code, res, id = curl_request(read_session, uri, rheaders)
        self.assertEqual(code, 200)
        self.assertIn("PetalusEngine", res)
        
	# Add a Accept header with the json format
        rheaders["Accept"] = "application/json"
        code, res, id = curl_request(read_session, uri, rheaders)
        self.assertEqual(code, 200)
        self.assertIn("PetalusEngine", res["engine"])
        self.assertEqual(res["reads"], 0)
        self.assertEqual(res["writes"], 0)

        del rheaders["Accept"]

        # Verifies that can access to the sessions output
        uri = f"{self.layer}://{admin_address}/v1/sessions"
        code, res, id = curl_request(read_session, uri, rheaders)
        self.assertEqual(code, 200)
        self.assertIn("Sessions statistics summary", res)

        # Add a Accept header with the json format
        rheaders["Accept"] = "application/json"

        code, res, id = curl_request(read_session, uri, rheaders)
        self.assertEqual(code, 200)
        self.assertEqual(res, None)

        rheaders = {
            "Authorization" : "Basic AAAA=="
        }

        uri = f"{self.layer}://{admin_address}/petalus/sessions"
        code, res, id = curl_request(read_session, uri, rheaders)
        self.assertEqual(code, 401)

        read_session.close()

    def test06(self):
        """ Create one wallet and the user writes information on wallets and
        the readonly system should read them"""

        password = "bubulovesyou"
        wallet, data = generate_data_wallet(name="Danny", surname="Kass",
                                            password=password, folders=["bills", "rewards"])

        uri = f"{self.layer}://{admin_address}/v1/create"
        res = do_post(requests, uri, json=data, headers=creation_headers)
        self.assertEqual(res.status_code, 201)

        uri = f"{self.layer}://{address}/v1/open"
        open_data = {
            "wallet": wallet,
            "password": hashlib.sha256(password.encode('utf-8')).hexdigest().upper()
        }
        session = requests.Session()
        res = do_post(session, uri, json=open_data)
        self.assertEqual(res.status_code ,200)
        self.assertIn("Petalus-SessionId", res.headers)
        good_session = res.headers["Petalus-SessionId"]

        headers = { 
            "Petalus-SessionId": good_session, 
            "Content-Type": "text/plain" 
        }
        uri = f"{self.layer}://{address}/v1/block/{wallet}"
        res = do_post(session, uri, data="This goes to the main blockfile", headers=headers)
        self.assertEqual(res.status_code ,200)

        uri = f"{self.layer}://{address}/v1/block/{wallet}/bills"
        res = do_post(session, uri, data="This is a lovely bill", headers=headers)
        self.assertEqual(res.status_code ,200)

        uri = f"{self.layer}://{address}/v1/block/{wallet}/rewards"
        res = do_post(session, uri, data="This is 200$", headers=headers)
        self.assertEqual(res.status_code ,200)

        # Now the read only system opens the wallet
        read_session = pycurl.Curl() 
        uri = f"{self.layer}://{admin_address}/v1/open"
        open_data = {"wallet": wallet}

        userid = good_session
        headers = { 
            "Authorization": f"Basic {self.token}",
            "Content-Type": "application/json" 
        }
        code, res, id = curl_request(read_session, uri, headers, data = json.dumps(open_data))
        self.assertEqual(code, 200)

        # Read the generated blocks by the user simulating a browser for example
        rheaders = {"Authorization": f"Basic {self.token}"}
        uri = f"{self.layer}://{admin_address}/v1/session/{userid}/5"
        code, res, id = curl_request(read_session, uri, rheaders)
        self.assertEqual(code, 200)

        uri = f"{self.layer}://{admin_address}/v1/session/{userid}/bills/2"
        code, res, id = curl_request(read_session, uri, rheaders)
        self.assertEqual(code, 200)

        # check the response of the /v1/sessions/ in json format
        rheaders["Accept"] = "text/html,application/xhtml+xml,application/json" \
                             ",application/xml;q=0.9,image/webp,*/*;q=0.8"

        uri = f"{self.layer}://{admin_address}/v1/session/{userid}/rewards/2"
        code, res, id = curl_request(read_session, uri, rheaders)
        self.assertEqual(code, 200)
        self.assertEqual(res["name"], wallet)
        self.assertEqual(res["blocks"], 7)
        self.assertIn("folders", res)
        self.assertEqual(res["folders"][0]["name"], "bills")
        self.assertEqual(res["folders"][1]["name"], "rewards")
        self.assertEqual(res["block"], 2)

        uri = f"{self.layer}://{admin_address}/v1/sessions"
        code, res, id = curl_request(read_session, uri, rheaders)
        self.assertEqual(code, 200)
        s = res[0] # There is only one session
        self.assertEqual(s["sid"], userid)
        self.assertEqual(s["wallet"], wallet) 

        # Check the access to the uri /v1/statistics
        rheaders["Accept"] = "application/json"
        uri = f"{self.layer}://{admin_address}/v1/statistics"
        code, res, id = curl_request(read_session, uri, rheaders)
        self.assertEqual(code, 200)
        self.assertEqual(res["http"]["errors"], 0)
        self.assertEqual(res["http"]["auths"], 7)
        self.assertEqual(res["http"]["gets"], 4)
        self.assertEqual(res["http"]["posts"], 6)
        
        del rheaders["Accept"]

        # Verifies that can access to the statistics from a browser
        uri = f"{self.layer}://{admin_address}/v1/statistics"
        code, res, id = curl_request(read_session, uri, rheaders)
        self.assertEqual(code, 200)
        self.assertIn("Statistics summary", res)
 
        # The authorization as been revoked 
        rheaders = {"Authorization": "Basic BBBBBB=="}
        uri = f"{self.layer}://{admin_address}/v1/session/{userid}/5"
        code, res, id = curl_request(read_session, uri, rheaders)
        self.assertEqual(code, 401)

        uri = f"{self.layer}://{admin_address}/v1/session/{userid}/bills/2"
        code, res, id = curl_request(read_session, uri, rheaders)
        self.assertEqual(code, 401)

        uri = f"{self.layer}://{admin_address}/v1/session/{userid}/rewards/2"
        code, res, id = curl_request(read_session, uri, rheaders)
        self.assertEqual(code, 401)

        read_session.close()
        session.close()

class PetalusTestSuite12 (unittest.TestCase):
    """ The aim of this test suite is to verify that the reencrypting
        of the data works as expected 
    """
    @classmethod
    def setUpClass(self):
        """Delete the previous data runs"""
        self.data_directory = "./%s_data" % self.__name__.lower()
        shutil.rmtree(self.data_directory, ignore_errors=True)

        # Generate random data that should be exactly the same in all the runs
        self.maindata = list()
        self.folderdata1 = list()
        self.folderdata2 = list()

        for i in range(0, 512):
            v = randint(256, 2048)
            self.maindata.append(base64.b64encode(os.urandom(v)).decode("utf-8"))

        for i in range(0, 512):
            v = randint(1024, 2048)
            self.folderdata1.append(base64.b64encode(os.urandom(v)).decode("utf-8"))
            
        for i in range(0, 512):
            v = randint(256, 1024)
            self.folderdata2.append(base64.b64encode(os.urandom(v)).decode("utf-8"))

        self.layer = "http"

    def setUp(self):
        return

    def tearDown(self):
        return

    def __micro_service(self, key, set_callback, newkey):
        """Example of service"""
        import petalus
        sw = petalus.SessionWalletManager()
        s = petalus.Server()
        sw.data_directory = self.data_directory
        sw.increase_allocated_memory(5)
        sw.hash_type = "sha3-256"
        sw.encryption_password = key
        s.address = ip
        s.port = user_port_1
        s.administration_port = admin_port
        s.session_wallet_manager = sw
        s.administration_ip_addresses = [ ip ]
        s.authorization_token = authorization_token

        if (set_callback == True):
            def timer1():
                """Process for make a rotation of the
                key while processing traffic"""

                s.session_wallet_manager = None
                sw.rotate_encryption_key(newkey)
                s.session_wallet_manager = sw
                sw.encryption_password = newkey 
                s.add_timer(None, 2)

            s.add_timer(timer1, 2)

        s.start()
        s.run()
        s.stop()
        sys.exit(0)

    def __populate_wallet(self, wallet, session, id, privatekey=None):
        headers = {
            "Petalus-SessionId": id,
            "Content-Type": "text/plain"
        }
        uri = f"{self.layer}://{address}/v1/block/{wallet}"
        for data in self.maindata:
            headers.pop("Petalus-User-Signature", None)
            if (privatekey)and(randint(0, 4) == 4):
                hdata = hashlib.sha256(data.encode("utf-8")).digest();
                sb = base64.b64encode(privatekey.sign(hdata, hashfunc=hashlib.sha256))
                headers["Petalus-User-Signature"] = sb
                
            res = do_post(session, uri, data=data, headers=headers)
            self.assertEqual(res.status_code ,200)

        uri = f"{self.layer}://{address}/v1/block/{wallet}/bills"
        for data in self.folderdata1:
            headers.pop("Petalus-User-Signature", None)
            if (privatekey)and(randint(0, 4) == 4):
                hdata = hashlib.sha256(data.encode("utf-8")).digest();
                sb = base64.b64encode(privatekey.sign(hdata, hashfunc=hashlib.sha256))
                headers["Petalus-User-Signature"] = sb

            res = do_post(session, uri, data=data, headers=headers)
            self.assertEqual(res.status_code ,200)

        uri = f"{self.layer}://{address}/v1/block/{wallet}/rewards"
        for data in self.folderdata2:
            headers.pop("Petalus-User-Signature", None)
            if (privatekey)and(randint(0, 5) == 5):
                hdata = hashlib.sha256(data.encode("utf-8")).digest();
                sb = base64.b64encode(privatekey.sign(hdata, hashfunc=hashlib.sha256))
                headers["Petalus-User-Signature"] = sb

            res = do_post(session, uri, data=data, headers=headers)
            self.assertEqual(res.status_code ,200)

    def __verify_wallet(self, wallet, password, session=requests.Session()):
        """Verify that all the information is correct from the user perspective"""

        uri = f"{self.layer}://{address}/v1/open"
        open_data = {
            "wallet": wallet,
            "password": hashlib.sha256(password.encode('utf-8')).hexdigest().upper()
        }

        res = do_post(session, uri, json=open_data) 
        self.assertEqual(res.status_code ,200)
        self.assertIn("Petalus-SessionId", res.headers)
        good_session = res.headers["Petalus-SessionId"]

        headers = {
            "Petalus-SessionId": good_session,
            "Content-Type": "text/plain"
        }

        for i in range(5, len(self.maindata) + 5):
            uri = f"{self.layer}://{address}/v1/block/{wallet}/{i}"
            res = do_get(session, uri, headers=headers)
            self.assertEqual(res.status_code ,200)
            self.assertEqual(res.text, self.maindata[i - 5])

        for i in range(2, len(self.folderdata1) + 2):
            uri = f"{self.layer}://{address}/v1/block/{wallet}/bills/{i}"
            res = do_get(session, uri, headers=headers)
            self.assertEqual(res.status_code ,200)
            self.assertEqual(res.text, self.folderdata1[i - 2])

        for i in range(2, len(self.folderdata2) + 2):
            uri = f"{self.layer}://{address}/v1/block/{wallet}/rewards/{i}"
            res = do_get(session, uri, headers=headers)
            self.assertEqual(res.status_code ,200)
            self.assertEqual(res.text, self.folderdata2[i - 2])

        session.close()

    def test01(self):
        """Create a wallet and makes 3 rotation keys of the data sequentially"""

        key = "{AES}C4F65AAC358F3CF2BB9C9C3FE2A4566E2D4B21C415C34B7F065A817A29F6940E" 
        p = Process(target = self.__micro_service, args=(key, False, "" ))
        p.daemon = True
        p.start()
        time.sleep(server_starttime) 

        # Create one wallet
        password = "bubulovesyou"
        wallet, data = generate_data_wallet(name="Danny", surname="Kass",
                                            password=password, folders=["bills", "rewards"])

        sk = ecdsa.SigningKey.generate(curve = ecdsa.SECP256k1)
        vk = sk.get_verifying_key()
        pubkey = vk.to_der()

        data["ec_public_key"] = base64.b64encode(pubkey).decode("utf-8")

        uri = f"{self.layer}://{admin_address}/v1/create"
        res = do_post(requests, uri, json=data, headers=creation_headers)
        self.assertEqual(res.status_code, 201)

        uri = f"{self.layer}://{address}/v1/open"
        open_data = {
            "wallet": wallet,
            "password": hashlib.sha256(password.encode('utf-8')).hexdigest().upper()
        }
        session = requests.Session()
        res = do_post(session, uri, json=open_data)
        self.assertEqual(res.status_code ,200)
        self.assertIn("Petalus-SessionId", res.headers)
        good_session = res.headers["Petalus-SessionId"]

        self.__populate_wallet(wallet, session, good_session, privatekey=sk)

        session.close()
        p.terminate()

        filename = self.data_directory + "/" + wallet[0] + "/" + wallet[1] + "/"
        filename += wallet + ".mblk0000"
        hashfile1 = get_sha256_from_file(filename)
 
        import petalus
        sw = petalus.SessionWalletManager()
        sw.data_directory = self.data_directory
        sw.hash_type = "sha3-256"
        sw.encryption_password = key

        newkey = "{AES}C7F559AF368C3FF1B89F9F3CE1A7556DE2C53DAC9C28D51C027C9DEE22E24872"
        sw.rotate_encryption_key(newkey)

        hashfile2 = get_sha256_from_file(filename)
        self.assertNotEqual(hashfile1, hashfile2)
       
        p = Process(target = self.__micro_service, args=(newkey, False, "" ))
        p.daemon = True
        p.start()
        time.sleep(server_starttime)

        # Verify that all the information is correct from the user perspective
        self.__verify_wallet(wallet, password)

        p.terminate()

        # reencrypt with another key 24 * c
        oldkey = newkey
        newkey = "{AES}C6F458AE378D3EF0B99E9E3DE0A6546C5D4A" \
                 "090D9CDA248CC7A1204481B69FAEC9820C4B592E8965"

        sw.encryption_password = oldkey
        sw.rotate_encryption_key(newkey)

        hashfile3 = get_sha256_from_file(filename)
        self.assertNotEqual(hashfile1, hashfile3)
        self.assertNotEqual(hashfile2, hashfile3)

        p = Process(target = self.__micro_service, args=(newkey, False, "", ))
        p.daemon = True
        p.start()
        time.sleep(server_starttime)

        # Verify that all the information is correct from the user perspective
        self.__verify_wallet(wallet, password)

        p.terminate()

        # reencrypt with another key 32 * d
        oldkey = newkey
        newkey = "{AES}C1F35FA9308A39F7BE99993AE7A1536B5A4D0E0A9BDD2" \
                 "38BBB5C7446C48E8596FEDBEF328A40A259CA99E7AE77E3F115"

        sw.encryption_password = oldkey
        sw.rotate_encryption_key(newkey)

        hashfile4 = get_sha256_from_file(filename)
        self.assertNotEqual(hashfile1, hashfile4)
        self.assertNotEqual(hashfile2, hashfile4)
        self.assertNotEqual(hashfile3, hashfile4)

        p = Process(target = self.__micro_service, args=(newkey, False, "", ))
        p.daemon = True
        p.start()
        time.sleep(server_starttime)

        # Verify that all the information is correct from the user perspective
        self.__verify_wallet(wallet, password)

        p.terminate()

    def test02(self):
        """Create a wallet and makes one rotation keys with no stop of the server"""

        staticdata = base64.b64encode(os.urandom(randint(10, 20))).decode("utf-8") 

        # We use the key from the previous test in case the test has been executed before
        key = "{AES}C1F35FA9308A39F7BE99993AE7A1536B5A4D0E0A9" \
              "BDD238BBB5C7446C48E8596FEDBEF328A40A259CA99E7AE77E3F115"
        p = Process(target = self.__micro_service, args=(key, False, "", ))
        p.daemon = True
        p.start()
        time.sleep(server_starttime)

        # Create one wallet
        password = "ThisIsThePASSWORD"
        wallet, data = generate_data_wallet(name="Stefanie", surname="Kass", password=password)

        uri = f"{self.layer}://{admin_address}/v1/create"
        res = do_post(requests, uri, json=data, headers=creation_headers)
        self.assertEqual(res.status_code, 201)

        uri = f"{self.layer}://{address}/v1/open"
        open_data = {
            "wallet": wallet,
            "password": hashlib.sha256(password.encode('utf-8')).hexdigest().upper()
        }
        session = requests.Session()
        res = do_post(session, uri, json=open_data)
        self.assertEqual(res.status_code ,200)
        self.assertIn("Petalus-SessionId", res.headers)
        good_session1 = res.headers["Petalus-SessionId"]

        headers = {
            "Petalus-SessionId": good_session1,
            "Content-Type": "text/plain"
        }
        uri = f"{self.layer}://{address}/v1/block/{wallet}"
        res = do_post(session, uri, data=staticdata, headers=headers)
        self.assertEqual(res.status_code, 200)       

        uri = f"{self.layer}://{address}/v1/block/{wallet}/3" 
        res = do_get(session, uri, headers=headers)
        self.assertEqual(res.status_code ,200)
        self.assertEqual(res.text, staticdata)

        p.terminate()
        session.close()

        # Get the hash of the file generated
        filename = self.data_directory + "/" + wallet[0] + "/" + wallet[1] + "/"
        filename += wallet + ".mblk0000"
        hashfile1 = get_sha256_from_file(filename)

        newkey = "{BLOWFISH}4861EEF2D9A64293C6CB8FC1" # hola
        p = Process(target = self.__micro_service, args=(key, True, newkey, ))
        p.daemon = True
        p.start()
        time.sleep(server_starttime)

        uri = f"{self.layer}://{address}/v1/open"
        open_data = {
            "wallet": wallet,
            "password": hashlib.sha256(password.encode('utf-8')).hexdigest().upper()
        }
        session = requests.Session()
        res = do_post(session, uri, json=open_data)
        self.assertEqual(res.status_code ,200)
        self.assertIn("Petalus-SessionId", res.headers)
        good_session2 = res.headers["Petalus-SessionId"]

        self.assertNotEqual(good_session1, good_session2)

        headers = {
            "Petalus-SessionId": good_session2,
            "Content-Type": "text/plain"
        }

        maintenance_mode = False
        correct_mode = False
        for i in range(0, 10):
            uri = f"{self.layer}://{address}/v1/block/{wallet}/3"
            res = do_get(session, uri, headers=headers)
            time.sleep(0.25)
            if (res.status_code == 200):
                self.assertEqual(res.text, staticdata)
                correct_mode = True
            if (res.status_code == 503)or(res.status_code == 404):
                # The server is in maintenance mode making the rotation of the key
                maintenance_mode = True
                # Give one second to the server for re-encrypt
                time.sleep(1)
                break

        self.assertEqual(maintenance_mode, True)
        self.assertEqual(correct_mode, True)

        session.close()
        session = requests.Session()

        # The user needs to reopen the session again
        uri = f"{self.layer}://{address}/v1/open"
        open_data = {
            "wallet": wallet,
            "password": hashlib.sha256(password.encode('utf-8')).hexdigest().upper()
        }

        res = do_post(session, uri, json=open_data)
        self.assertEqual(res.status_code ,200)
        good_session3 = res.headers["Petalus-SessionId"]
        self.assertNotEqual(good_session2, good_session3)
        self.assertNotEqual(good_session1, good_session3)

        headers = {
            "Petalus-SessionId": good_session3,
            "Content-Type": "text/plain"
        }

        uri = f"{self.layer}://{address}/v1/block/{wallet}/3"
        res = do_get(session, uri, headers=headers)
        self.assertEqual(res.status_code, 200)
        self.assertEqual(res.text, staticdata)

        p.terminate()
        session.close()

        hashfile2 = get_sha256_from_file(filename)
        self.assertNotEqual(hashfile1, hashfile2)

    def test03(self):
        """Create a wallet and makes one rotation keys with no stop of the server"""

        # We use the key from the previous test in case the test has been executed before
        key = "{BLOWFISH}4861EEF2D9A64293C6CB8FC1"
        p = Process(target = self.__micro_service, args=(key, False, "", ))
        p.daemon = True
        p.start()
        time.sleep(server_starttime)

        # Create one wallet
        password = "Hax45--Y6u8"
        wallet, data = generate_data_wallet(name="Stefanie", surname="Kass",
                                            password=password, folders=["bills", "rewards"])

        uri = f"{self.layer}://{admin_address}/v1/create"
        res = do_post(requests, uri, json=data, headers=creation_headers)
        self.assertEqual(res.status_code, 201)

        uri = f"{self.layer}://{address}/v1/open"
        open_data = {
            "wallet": wallet,
            "password": hashlib.sha256(password.encode('utf-8')).hexdigest().upper()
        }
        session = requests.Session()
        res = do_post(session, uri, json=open_data)
        self.assertEqual(res.status_code ,200)
        self.assertIn("Petalus-SessionId", res.headers)
        good_session1 = res.headers["Petalus-SessionId"]

        self.__populate_wallet(wallet, session, good_session1)

        session.close()
        p.terminate()

        newkey = "{AES}C6F458AE378D3EF0B99E9E3DE0A6546C5D4A09" \
                 "0D9CDA248CC7A1204481B69FAEC9820C4B592E8965"
        p = Process(target = self.__micro_service, args=(key, True, newkey, ))
        p.daemon = True
        p.start()
        time.sleep(server_starttime)

        uri = f"{self.layer}://{address}/v1/open"
        open_data = {
            "wallet": wallet,
            "password": hashlib.sha256(password.encode('utf-8')).hexdigest().upper()
        }
        session = requests.Session()
        res = do_post(session, uri, json=open_data)
        self.assertEqual(res.status_code ,200)
        self.assertIn("Petalus-SessionId", res.headers)
        good_session2 = res.headers["Petalus-SessionId"]

        self.assertNotEqual(good_session1, good_session2)

        headers = {
            "Petalus-SessionId": good_session2,
            "Content-Type": "text/plain"
        }

        maintenance_mode = False
        correct_mode = False
        for i in range(0, 10):
            uri = f"{self.layer}://{address}/v1/block/{wallet}/rewards/2" 
            res = do_get(session, uri, headers=headers)
            time.sleep(0.5)
            if (res.status_code == 200):
                correct_mode = True
            if (res.status_code == 503)or(res.status_code == 404):
                # The server is in maintenance mode making the rotation of the key
                maintenance_mode = True
                # Give one second to the server for re-encrypt
                time.sleep(1)
                break

        self.assertEqual(maintenance_mode, True)
        self.assertEqual(correct_mode, True)

        # The user needs to reopen the session again
        uri = f"{self.layer}://{address}/v1/open"
        open_data = {
            "wallet": wallet,
            "password": hashlib.sha256(password.encode('utf-8')).hexdigest().upper()
        }

        res = do_post(session, uri, json=open_data)
        self.assertEqual(res.status_code ,200)
        good_session3 = res.headers["Petalus-SessionId"]
        
        # The sessions has been closed on the rotation key process
        self.assertNotEqual(good_session3, good_session2)

        # Verify that all the data should be the same
        self.__verify_wallet(wallet, password, session = session)
  
        p.terminate()

    def test04(self):
        """ Create a wallet and makes one rotation keys with no stop of the server 
        but the new key is blowfish schema"""

        # We use the key from the previous test in case the test has been executed before
        key = "{AES}C6F458AE378D3EF0B99E9E3DE0A6546C5D" \
              "4A090D9CDA248CC7A1204481B69FAEC9820C4B592E8965"
        p = Process(target = self.__micro_service, args=(key, False, "", ))
        p.daemon = True
        p.start()
        time.sleep(server_starttime)

        # Create one wallet
        password = "Hax45--Y6u8"
        wallet, data = generate_data_wallet(name="Mark",
                                            surname="McMorris",
                                            password=password,
                                            folders=["bills", "rewards"])

        uri = f"{self.layer}://{admin_address}/v1/create"
        res = do_post(requests, uri, json=data, headers=creation_headers)
        self.assertEqual(res.status_code, 201)

        uri = f"{self.layer}://{address}/v1/open"
        open_data = {
            "wallet": wallet,
            "password": hashlib.sha256(password.encode('utf-8')).hexdigest().upper()
        }
        session = requests.Session()
        res = do_post(session, uri, json=open_data)
        self.assertEqual(res.status_code ,200)
        self.assertIn("Petalus-SessionId", res.headers)
        good_session1 = res.headers["Petalus-SessionId"]

        self.__populate_wallet(wallet, session, good_session1)

        session.close()
        p.terminate()

        # Now we change to the blowfish password schema
        newkey = "{BLOWFISH}25D49C27970336850812AB1B2F37F402" \
                 "9E63548F3912C03C3424A970962CC106BC57B4FC1020B0E8"
        p = Process(target = self.__micro_service, args=(key, True, newkey, ))
        p.daemon = True
        p.start()
        time.sleep(server_starttime)

        uri = f"{self.layer}://{address}/v1/open"
        open_data = {
            "wallet": wallet,
            "password": hashlib.sha256(password.encode('utf-8')).hexdigest().upper()
        }
        session = requests.Session()
        res = do_post(session, uri, json=open_data)
        self.assertEqual(res.status_code ,200)
        self.assertIn("Petalus-SessionId", res.headers)
        good_session2 = res.headers["Petalus-SessionId"]

        self.assertNotEqual(good_session1, good_session2)

        headers = {
            "Petalus-SessionId": good_session2,
            "Content-Type": "text/plain"
        }

        maintenance_mode = False
        correct_mode = False
        for i in range(0, 10):
            uri = f"{self.layer}://{address}/v1/block/{wallet}/rewards/2"
            res = do_get(session, uri, headers=headers)
            time.sleep(0.25)
            if (res.status_code == 200):
                correct_mode = True
            if (res.status_code == 503)or(res.status_code == 404):
                # The server is in maintenance mode making the rotation of the key
                maintenance_mode = True
                # Give one second to the server for re-encrypt
                time.sleep(1)
                break

        self.assertEqual(maintenance_mode, True)
        self.assertEqual(correct_mode, True)

        # The user needs to reopen the session again
        uri = f"{self.layer}://{address}/v1/open"
        open_data = {
            "wallet": wallet,
            "password": hashlib.sha256(password.encode('utf-8')).hexdigest().upper()
        }

        res = do_post(session, uri, json=open_data)
        self.assertEqual(res.status_code ,200)
        good_session3 = res.headers["Petalus-SessionId"]

        # The sessions has been closed on the rotation key process
        self.assertNotEqual(good_session3, good_session2)

        # Verify that all the data should be the same
        self.__verify_wallet(wallet, password, session = session)
 
        p.terminate()

class PetalusTestSuite13 (unittest.TestCase):
    """ The aim of this test suite is to verify that the engine
    is between a proxy and all the requests came from the same IP address
    """

    @classmethod
    def setUpClass(self):
        """Delete the previous data runs"""

        self.data_directory = "./%s_data" % self.__name__.lower()
        shutil.rmtree(self.data_directory, ignore_errors=True)

        self.layer = "http"

    def setUp(self):
        self.p = Process(target=self.__micro_service)
        self.p.daemon = True
        self.p.start()
        time.sleep(server_starttime)
        return

    def tearDown(self):
        # Stop and delete the server
        self.p.terminate()
        return

    def __micro_service(self):
        """Micro service"""
        import petalus
        sw = petalus.SessionWalletManager()
        s = petalus.Server()
        sw.data_directory = self.data_directory
        sw.increase_allocated_memory(5)
        sw.hash_type = "blake2s"
        s.address = ip
        s.port = user_port_1
        s.session_wallet_manager = sw
        s.proxy_support = True
        s.administration_port = admin_port
        s.administration_ip_addresses = [ip]
        s.authorization_token = authorization_token

        s.start()
        s.run()
        s.stop()
        sys.exit(0)

    def test01(self):
        """Create one wallet and verify the access to URIs is correct"""

        password = "asdf1342"
        wallet, data = generate_data_wallet(name="Bert", surname="LaMar", password=password)

        # Create the wallet
        uri = f"{self.layer}://{admin_address}/v1/create"
        res = do_post(requests, uri, json=data, headers=creation_headers)
        self.assertEqual(res.status_code, 201)
        self.assertIn("Connection", res.headers)
        self.assertEqual(res.headers["Connection"], "close")

        with requests.Session() as session, requests.Session() as read_session:
            # Now get the status on a new connection
            uri = f"{self.layer}://{admin_address}/v1/status"
            res = do_get(read_session, uri)
            self.assertEqual(res.status_code, 401)
            self.assertEqual(res.text, "No Authorization token found")
            self.assertIn("Connection", res.headers)
            self.assertEqual(res.headers["Connection"], "close")

            headers = {"Authorization" : f"Basic {b64_auth_token}"}

            for i in range(0, 5):
                res = do_get(read_session, uri, headers=headers)
                self.assertEqual(res.status_code, 200)
                self.assertNotIn("Connection", res.headers)

            # Now the user open his wallet
            uri = f"{self.layer}://{address}/v1/open"
            open_data = {
                "wallet": wallet,
                "password": hashlib.sha256(password.encode('utf-8')).hexdigest().upper()
            }
            headers = {"Forwarded" : "72.247.83.53"}

            res = do_post(session, uri, json=open_data, headers=headers)
            self.assertEqual(res.status_code ,200)
            self.assertIn("Petalus-SessionId", res.headers)
            good_session = res.headers["Petalus-SessionId"]
 
            # Now the user can write on his wallet
            headers = {
                "Forwarded": "72.247.83.53",
                "Petalus-SessionId": good_session,
                "Content-Type": "text/plain"
            }
            data = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
            uri = f"{self.layer}://{address}/v1/block/{wallet}"
            res = do_post(session, uri, data=data, headers=headers)
            self.assertEqual(res.status_code ,200)
            block_number = res.text.split(" ")[1]

            # Now read the block
            uri = f"{self.layer}://{address}/v1/block/{wallet}/{block_number}"
            res = do_get(session, uri, headers=headers)
            self.assertEqual(res.status_code ,200)
            self.assertEqual(res.text, data)

            # Now we have requests for read the wallet
            headers = {
                "Authorization": f"Basic {b64_auth_token}",
                "Accept": "application/json"
            } 

            uri = f"{self.layer}://{admin_address}/v1/sessions"
            res = do_get(read_session, uri, headers=headers)
            self.assertEqual(res.status_code, 200)
            j = res.json()
            self.assertEqual(len(j), 1)
            s = j[0]
            self.assertEqual(s["sid"], good_session)

            uri = f"{self.layer}://{admin_address}/v1/session/" + s["sid"]
            res = do_get(read_session, uri)
            self.assertEqual(res.status_code, 401)
            self.assertEqual(res.text, "No Authorization token found")
    
            uri = f"{self.layer}://{admin_address}/v1/session/" + s["sid"]
            res = do_get(read_session, uri, headers=headers)
            self.assertEqual(res.status_code, 200)
            j = res.json()
            self.assertEqual(j["block"], 3)   
            self.assertEqual(j["blocks"], 3)   
            self.assertEqual(j["data"][0], 88)
   
            # Try to write on the wallet intentionally or by mistake
            uri = f"{self.layer}://{admin_address}/v1/block/{wallet}"
            res = do_post(read_session, uri, data="ERROR", headers=headers)
            self.assertEqual(res.status_code, 404)

            headers = {
                "Authorization": f"Basic {b64_auth_token}",
                "Petalus-SessionId": good_session
            } 

            uri = f"{self.layer}://{admin_address}/v1/block/{wallet}"
            res = do_post(read_session, uri, data="ERROR", headers=headers)
            self.assertEqual(res.status_code, 404)
            self.assertEqual(res.text, "Not found")

            # Now the legitimate user close the wallet
            uri = f"{self.layer}://{address}/v1/close"
            headers = {
                "Connection": "close",
                "Petalus-SessionId": good_session
            } 
            res = do_post(session, uri, "", headers=headers)
            self.assertEqual(res.status_code ,200)

    def test02(self):
        """Try to write on the wallet from the other systems 
        The user will insert the Forwarded header on the requests"""

        password = "asdf1342"
        wallet, data = generate_data_wallet(name="Bert", surname="LaMar", password=password)

        # Create the wallet
        uri = f"{self.layer}://{admin_address}/v1/create"
        res = do_post(requests, uri, json=data, headers=creation_headers)
        self.assertEqual(res.status_code, 201)
        self.assertIn("Connection", res.headers)
        self.assertEqual(res.headers["Connection"], "close")

        # Now the user opens the wallet
        uri = f"{self.layer}://{address}/v1/open"
        open_data = {
            "wallet": wallet,
            "password": hashlib.sha256(password.encode('utf-8')).hexdigest().upper()
        }
        session = requests.Session()
        headers = {"Forwarded": "31.13.90.36"}

        res = do_post(session, uri, json=open_data, headers=headers)
        self.assertEqual(res.status_code ,200)
        self.assertIn("Petalus-SessionId", res.headers)
        good_session = res.headers["Petalus-SessionId"]

        # Now another systems tries to mess but dont have the Authorization token correct
        bad_session = requests.Session()

        uri = f"{self.layer}://{admin_address}/petalus/sessions"
        headers = {}
        res = do_get(bad_session, uri, headers=headers)
        self.assertEqual(res.status_code, 401)
        self.assertEqual(res.text, "No Authorization token found")

        uri = f"{self.layer}://{admin_address}/petalus/sessions"
        headers = {"Authorization": "Basic XXXXXXXXXXXXXX"}
        res = do_get(bad_session, uri, headers=headers)
        self.assertEqual(res.status_code, 401)
        self.assertEqual(res.text, "Authorization token failed")

        uri = f"{self.layer}://{admin_address}/petalus/session/{good_session}"
        res = do_get(bad_session, uri, headers=headers)
        self.assertEqual(res.status_code, 401)
        self.assertEqual(res.text, "Authorization token failed")

        # The user can work normaly
        headers = {
            "Forwarded": "31.13.90.36",
            "Petalus-SessionId": good_session,
            "Content-Type": "text/plain"
        }
        data = "ABCDEFGHIJK"
        uri = f"{self.layer}://{address}/v1/block/{wallet}"
        res = do_post(session, uri, data=data, headers=headers)
        self.assertEqual(res.status_code, 200)
        block_number = res.text.split(" ")[1]

        # Even if the system generates a simliar request will not work
        uri = f"{self.layer}://{address}/v1/block/{wallet}"
        res = do_post(bad_session, uri, data=data, headers=headers)
        self.assertEqual(res.status_code, 400)
        self.assertEqual(res.text, "Session ID not valid for write")

        uri = f"{self.layer}://{address}/v1/block/{wallet}/{block_number}"
        res = do_get(session, uri, headers=headers)
        self.assertEqual(res.status_code, 200)
        self.assertEqual(res.text, data)

        res = do_get(bad_session, uri, headers=headers)
        self.assertEqual(res.status_code, 404)
        self.assertEqual(res.text, "Session ID not valid for read")

        session.close()
        bad_session.close()

    def test03(self):
        """The user tries to use other URIS that should not"""

        password = "ABCCDFFEGTR$$$$$"
        wallet, data = generate_data_wallet(name="Stefanie", surname="Kass", password=password)

        # Create the wallet
        uri = f"{self.layer}://{admin_address}/v1/create"
        res = do_post(requests, uri, json=data, headers=creation_headers)
        self.assertEqual(res.status_code, 201)
        self.assertIn("Connection", res.headers)
        self.assertEqual(res.headers["Connection"], "close")

        # Now the user opens the wallet
        open_data = {
            "wallet": wallet,
            "password": hashlib.sha256(password.encode('utf-8')).hexdigest().upper()
        }
        session = requests.Session()

        headers = {"Forwarded" : "31.13.90.36"}

        uri = f"{self.layer}://{address}/v1/open"
        res = do_post(session, uri, json=open_data, headers=headers)
        self.assertEqual(res.status_code ,200)
        self.assertIn("Petalus-SessionId", res.headers)
        good_session = res.headers["Petalus-SessionId"]

        # Not the user tries to access the administration uris
        headers = {
            "Forwarded": "31.13.90.36",
            "Petalus-SessionId": good_session,
            "Content-Type": "text/plain"
        }
        uri = f"{self.layer}://{address}/petalus/status"
        res = do_get(session, uri, headers=headers)
        self.assertEqual(res.status_code, 400)
        self.assertEqual(res.text, "Wallet not valid")

        uri = f"{self.layer}://{address}/petalus/sessions"
        res = do_get(session, uri, headers=headers)
        self.assertEqual(res.status_code, 400)
        self.assertEqual(res.text, "Wallet not valid")

        # Can the user read is own session ?
        uri = f"{self.layer}://{address}/petalus/session/{good_session}"
        res = do_get(session, uri, headers=headers)
        self.assertEqual(res.status_code, 400)
        self.assertEqual(res.text, "Wallet not valid")

        uri = f"{self.layer}://{address}/v1/block/{wallet}/2"
        res = do_get(session, uri, headers=headers)
        self.assertEqual(res.status_code, 200)

        uri = f"{self.layer}://{address}/v1/close"
        res = do_post(session, uri, "", headers=headers)
        self.assertEqual(res.status_code ,200)
    
        session.close()

    def test04(self):
        """There is open a wallet by the administrator
        and after the user wants read also"""

        password = "ABCCDFFEGTR$$$$$"
        wallet, data = generate_data_wallet(name="Stefanie", surname="Kass", password=password)

        # Create the wallet
        uri = f"{self.layer}://{admin_address}/v1/create"
        res = do_post(requests, uri, json=data, headers=creation_headers)
        self.assertEqual(res.status_code, 201)
        self.assertIn("Connection", res.headers)
        self.assertEqual(res.headers["Connection"], "close")

        # Now the admin opens the wallet 
        open_data = {"wallet": wallet}

        read_session = requests.Session()

        headers = {"Authorization" : f"Basic {b64_auth_token}"}

        uri = f"{self.layer}://{admin_address}/v1/open"
        res = do_post(read_session, uri, json=open_data, headers=headers)
        self.assertEqual(res.status_code ,200)
        self.assertIn("Petalus-SessionId", res.headers)
        sid = res.headers["Petalus-SessionId"]

        headers = {
            "Authorization": f"Basic {b64_auth_token}",
            "Petalus-SessionId": sid
        }
        uri = f"{self.layer}://{admin_address}/v1/block/{wallet}/2"
        res = do_get(read_session, uri, headers=headers)
        self.assertEqual(res.status_code, 200)

        # Now the user open his wallet that is already open
        open_data = {
            "wallet": wallet,
            "password": hashlib.sha256(password.encode('utf-8')).hexdigest().upper()
        }
        session = requests.Session()

        headers = {"Forwarded": "31.13.90.36"}

        uri = f"{self.layer}://{address}/v1/open"
        res = do_post(session, uri, json=open_data, headers=headers)
        self.assertEqual(res.status_code ,200)
        self.assertIn("Petalus-SessionId", res.headers)
        good_session = res.headers["Petalus-SessionId"]
        self.assertEqual(good_session, sid)

        headers = {
            "Authorization": f"Basic {b64_auth_token}",
            "Petalus-SessionId": sid
        }

        # The block number 3 dont exist on this point
        uri = f"{self.layer}://{admin_address}/v1/block/{wallet}/3"
        res = do_get(read_session, uri, headers=headers)
        self.assertEqual(res.status_code, 404)

        headers = {
            "Forwarded": "31.13.90.36",
            "Petalus-SessionId": good_session
        }

        data = "AAAABBBBCCCCDDDDEEEFFFGGGG"
        uri = f"{self.layer}://{address}/v1/block/{wallet}"
        res = do_post(session, uri, data=data, headers=headers)
        self.assertEqual(res.status_code, 200)

        # Now the block number 4 exists 
        headers = {
            "Authorization": f"Basic {b64_auth_token}",
            "Petalus-SessionId": sid
        }
        uri = f"{self.layer}://{admin_address}/v1/block/{wallet}/3"
        res = do_get(read_session, uri, headers=headers)
        self.assertEqual(res.status_code, 200)
        self.assertEqual(data, res.text)

        session.close()
        read_session.close()

class PetalusTestSuite14 (unittest.TestCase):
    """ The aim of this test suite is to verify that the engine
    is between a proxy and all the requests came from the same IP addres
    but this time we missconfigure the engine and dont set the proxy mode
    parameter on the SessionWalletManager

    s.proxy_support = False 
    """
    @classmethod
    def setUpClass(self):
        """Delete the previous data runs"""
        self.data_directory = "./%s_data" % self.__name__.lower()
        shutil.rmtree(self.data_directory, ignore_errors=True)

        self.layer = "http"

    def setUp(self):
        self.p = Process(target=self.__micro_service)
        self.p.daemon = True
        self.p.start()
        time.sleep(server_starttime)
        return

    def tearDown(self):
        # Stop and delete the server
        self.p.terminate()
        return

    def __micro_service(self):
        """Micro service"""
        import petalus
        sw = petalus.SessionWalletManager()
        s = petalus.Server()
        sw.data_directory = self.data_directory
        sw.increase_allocated_memory(5)
        sw.hash_type = "blake2s"
        s.address = ip
        s.port = user_port_1
        s.administration_port = admin_port
        s.session_wallet_manager = sw
        s.administration_ip_addresses = [ip]
        s.authorization_token = authorization_token

        s.start()
        s.run()
        s.stop()
        sys.exit(0)

    def test01(self):
        """Verify that a readonly system can not access
        the admin uris without the token """

        # Now get the status on a new connection
        with requests.Session() as read_session:
            uri = f"{self.layer}://{admin_address}/v1/status"
            res = do_get(read_session, uri)
            self.assertEqual(res.status_code, 401)
            self.assertEqual(res.text, "No Authorization token found")
            self.assertIn("Connection", res.headers)
            self.assertEqual(res.headers["Connection"], "close")

            uri = f"{self.layer}://{admin_address}/v1/sessions"
            res = do_get(read_session, uri)
            self.assertEqual(res.status_code, 401)
            self.assertEqual(res.text, "No Authorization token found")
            self.assertIn("Connection", res.headers)
            self.assertEqual(res.headers["Connection"], "close")

            headers = {"Authorization": f"Basic {b64_auth_token}"}
            uri = f"{self.layer}://{admin_address}/v1/status"
            res = do_get(read_session, uri, headers=headers)
            self.assertEqual(res.status_code, 200)

            uri = f"{self.layer}://{admin_address}/v1/sessions"
            res = do_get(read_session, uri, headers=headers)
            self.assertEqual(res.status_code, 200)


class PetalusTestSuite15 (unittest.TestCase):
    """The aim of this test suite is to verify the functionality
    that signs messages with ED25519 from the client as well as
    full encryption of the data. """

    @classmethod
    def setUpClass(self):
        """Now we generate a EC private/public key with a self signed certificate"""

        self.private_key, self.certificate = generate_certificate("ec")
        self.layer = "https"

        # Delete the previous data runs
        self.data_directory = "./%s_data" % self.__name__.lower()
        shutil.rmtree(self.data_directory, ignore_errors=True)

    def setUp(self):
        self.p = Process(target=self.__micro_service)
        self.p.daemon = True
        self.p.start()
        time.sleep(server_starttime)
        return

    def tearDown(self):
        """Stop and delete the server"""
        self.p.terminate()
        return

    def __micro_service(self):
        """Micro service"""
        import petalus
        sw = petalus.SessionWalletManager()
        s = petalus.Server()
        sw.data_directory = self.data_directory
        sw.increase_allocated_memory(5)
        sw.hash_type = "blake2s"

        # AAAAAAAAAAAAAAAA
        # sw.encryption_password = "{AES}E4D67A8C15AF1CD29BBCBC1FC28"\
        #                          "4764E41B8B852FE1AD9FBC6FE2C8F30F61B20"
        s.session_wallet_manager = sw
        s.address = ip
        s.port = user_port_1
        s.administration_address = ip
        s.administration_port = admin_port
        s.administration_ip_addresses = [ip]
        s.authorization_token = authorization_token
        s.private_key = self.private_key
        s.certificate = self.certificate

        s.start()
        s.run()
        s.stop()

    def __create_wallet(self, password, folders=[]):
        """Creates a valid random wallet for the tests"""
        uri = f"{self.layer}://{admin_address}/v1/create"

        wallet, data = generate_data_wallet(password=password)
        
        private_key = ed25519.Ed25519PrivateKey.generate()
        public_key = private_key.public_key()
        public_bytes = public_key.public_bytes(
            encoding=serialization.Encoding.DER,
            format=serialization.PublicFormat.SubjectPublicKeyInfo
        )
        private_bytes = private_key.private_bytes(
            encoding=serialization.Encoding.DER,
            format=serialization.PrivateFormat.PKCS8,
            encryption_algorithm=serialization.NoEncryption()
        )

        sk = None
          
        pubkey64 = base64.b64encode(public_bytes).decode("utf-8")
        privkey64 = base64.b64encode(private_bytes).decode("utf-8")
        data["ed25519_public_key"] = pubkey64
        data["wallet"] = wallet
        if (len(folders) > 0):
            data["folders"] = folders

        res = do_post(requests, uri, json=data, headers=creation_headers)
        self.assertEqual(res.status_code, 201)
        return wallet, private_key 

    def test01(self):
        """Creates a valid wallet with a ed25519 public key on it"""
        wallet, _ = self.__create_wallet("AAAAA")
        self.assertGreater(len(wallet), 16)

    def test02(self):
        """Generate a ed25519 signed block and verify is readable"""

        user_password = "user_password"
        wallet, privkey = self.__create_wallet(user_password, folders=["policies"])
        self.assertGreater(len(wallet), 16)

        session = requests.Session()

        uri = f"{self.layer}://{address}/v1/open"
        open_data = {
            "wallet": wallet,
            "password": hashlib.sha256(user_password.encode("utf-8")).hexdigest().upper()
        }
        res = do_post(session, uri, json=open_data)
        self.assertEqual(res.status_code ,200)
        self.assertIn("Petalus-SessionId", res.headers)
        good_session = res.headers["Petalus-SessionId"]

        hash_sha256 = hashlib.sha256()
 
        data1 = "This information is signed by the user with ED22519"
        hash_sha256.update(data1.encode("utf-8"))

        sb = base64.b64encode(privkey.sign(hash_sha256.digest()))
        headers = {
            "Petalus-SessionId": good_session,
            "Petalus-User-Signature": sb,
            "Content-Type": "application/octet-stream"
        }

        uri = f"{self.layer}://{address}/v1/block/{wallet}"
        res = do_post(session, uri, data=data1, headers=headers)
        self.assertEqual(res.status_code ,200)

        data2 = "This information is not signed by the user"
        headers = {"Petalus-SessionId" : good_session}
        uri = f"{self.layer}://{address}/v1/block/{wallet}"
        res = do_post(session, uri, data=data2, headers=headers)
        self.assertEqual(res.status_code ,200)

        # Now read the written blocks
        uri = f"{self.layer}://{address}/v1/block/{wallet}/4"
        headers = {"Petalus-SessionId" : good_session}
        res = do_get(session, uri, headers=headers)
        self.assertEqual(res.status_code ,200)
        self.assertEqual(res.text, data1)

        uri = f"{self.layer}://{address}/v1/block/{wallet}/5"
        headers = {"Petalus-SessionId" : good_session}
        res = do_get(session, uri, headers=headers)
        self.assertEqual(res.status_code ,200)
        self.assertEqual(res.text, data2)

        session.close()

    def test03(self):
        """Generate a ed25519 signed block and modify the original data"""

        user_password = "user_password"
        wallet, privkey = self.__create_wallet(user_password)
        self.assertGreater(len(wallet), 16)

        session = requests.Session()

        uri = f"{self.layer}://{address}/v1/open"
        open_data = {
            "wallet": wallet,
            "password": hashlib.sha256(user_password.encode("utf-8")).hexdigest().upper()
        }
        res = do_post(session, uri, json=open_data)
        self.assertEqual(res.status_code ,200)
        self.assertIn("Petalus-SessionId", res.headers)
        good_session = res.headers["Petalus-SessionId"]

        hash_sha256 = hashlib.sha256()

        data = "This information is signed by the user with ED22519 but will be modified"
        hash_sha256.update(data.encode("utf-8"))

        sb = base64.b64encode(privkey.sign(hash_sha256.digest()))
        headers = {
            "Petalus-SessionId": good_session,
            "Petalus-User-Signature": sb,
            "Content-Type": "application/octet-stream"
        }

        # This will generate a wrong sha256
        data += "AAAA"
        uri = f"{self.layer}://{address}/v1/block/{wallet}"
        res = do_post(session, uri, data=data, headers=headers)
        self.assertEqual(res.status_code ,500)

        session.close()

    def test04(self):
        """Generate a wallet with ed25519 and sign with ECDSA"""

        user_password = "user_password"
        wallet, privkey = self.__create_wallet(user_password)
        self.assertGreater(len(wallet), 16)

        session = requests.Session()

        uri = f"{self.layer}://{address}/v1/open"
        open_data = {
            "wallet": wallet,
            "password": hashlib.sha256(user_password.encode("utf-8")).hexdigest().upper()
        }
        res = do_post(session, uri, json=open_data)
        self.assertEqual(res.status_code ,200)
        self.assertIn("Petalus-SessionId", res.headers)
        good_session = res.headers["Petalus-SessionId"]

        data = b"This information is signed by the user with ECDSA"
        # Generate a ecdsa key 
        sk = ecdsa.SigningKey.generate(curve=ecdsa.SECP256k1)

        # sign and base64 the generated ECDSA signature
        sb = base64.b64encode(sk.sign(data, hashfunc=hashlib.sha256))

        headers = {
            "Petalus-SessionId": good_session,
            "Petalus-User-Signature": sb,
            "Content-Type": "application/octet-stream"
        }

        uri = f"{self.layer}://{address}/v1/block/{wallet}"
        res = do_post(session, uri, data=data, headers=headers)
        self.assertEqual(res.status_code ,500)

        # Now write with the correct signature
        hash_sha256 = hashlib.sha256()

        data_ok = "This information is signed by the user with ED22519"
        hash_sha256.update(data_ok.encode("utf-8"))

        sb = base64.b64encode(privkey.sign(hash_sha256.digest()))
        headers = {
            "Petalus-SessionId": good_session,
            "Petalus-User-Signature": sb,
            "Content-Type": "application/octet-stream"
        }

        uri = f"{self.layer}://{address}/v1/block/{wallet}"
        res = do_post(session, uri, data=data_ok, headers=headers)
        self.assertEqual(res.status_code ,200)

        session.close()

class PetalusTestSuite16 (unittest.TestCase):
    """The aim of this test suite is to verify the minimum functionality
    of the server with TLS RSA encryption on the TCP communications and
    have support for mtls."""

    @classmethod
    def setUpClass(self):
        """Now we generate a private/public key with a self signed certificate"""
        self.root_key, self.root_cert = generate_ca()
        self.private_key, self.certificate = generate_certificate("rsa",
                                                                  domain=u"server.mysite.com",
                                                                  root_cert=self.root_cert,
                                                                  root_key=self.root_key)
        self.layer = "https"

        with open("server_cert.pem", "w") as fd:
            fd.write(self.certificate.decode("utf-8"))

        self.certificate_authority = self.root_cert.public_bytes(serialization.Encoding.PEM) 
        # Delete the previous data runs
        self.data_directory = "./%s_data" % self.__name__.lower()
        shutil.rmtree(self.data_directory, ignore_errors=True)

    def setUp(self):
        self.p = Process(target = self.__micro_service)
        self.p.daemon = True
        self.p.start()
        time.sleep(server_starttime)
        return

    def tearDown(self):
        # Stop and delete the server
        self.p.terminate()
        return

    def __micro_service(self):
        """Micro service :)"""
        import petalus
        sw = petalus.SessionWalletManager()
        s = petalus.Server()
        sw.data_directory = self.data_directory
        sw.increase_allocated_memory(5)
        s.session_wallet_manager = sw
        s.address = ip
        s.port = user_port_1
        s.administration_address = ip
        s.administration_port = admin_port
        s.administration_ip_addresses = [ip]
        s.authorization_token = authorization_token
        s.private_key = self.private_key
        s.certificate = self.certificate
        s.certificate_authority = self.certificate_authority
        s.mtls =  True 
        s.start()
        s.run()
        s.stop()

    def __create_wallet(self, password):
        """Creates a valid random wallet for the tests"""
        wallet, data = generate_data_wallet(password = password)
        uri = f"{self.layer}://localhost:{admin_port}/v1/create"

        private_key, certificate = generate_certificate("rsa",
                                                        domain=u"server.mysite.com",
                                                        root_cert=self.root_cert,
                                                        root_key=self.root_key)

        with open("client_private.key", "w") as fd:
            fd.write(private_key.decode("utf-8"))

        with open("root_cert.pem", "w") as fd:
            fd.write(self.certificate_authority.decode("utf-8"))

        with open("client_cert.pem", "w") as fd:
            fd.write(certificate.decode("utf-8"))

        res = do_post(requests, uri, json=data, headers=creation_headers,
                      cert=("client_cert.pem", "client_private.key"), verify="root_cert.pem")
        self.assertEqual(res.status_code, 201)
        return wallet

    def test01(self):
        """Create a basic wallet"""

        wallet = self.__create_wallet("AAAAA")
        self.assertGreater(len(wallet), 16)

    def test02(self):
        """Do basic operations, open read, write and close"""

        password = "Good morning1234"
        wallet = self.__create_wallet(password)
        self.assertGreater(len(wallet), 16)

        private_key, certificate = generate_certificate("ec",
                                                        domain=u"client.mysite.com",
                                                        root_cert=self.root_cert,
                                                        root_key=self.root_key)
        session = requests.Session()
        session.cert = ("client_cert.pem", "client_private.key")
        session.verify = "root_cert.pem"
 
        with open("client_private.key", "w") as fd:
            fd.write(private_key.decode("utf-8"))

        with open("client_cert.pem", "w") as fd:
            fd.write(certificate.decode("utf-8"))

        uri = f"{self.layer}://localhost:{user_port1}/v1/open"
        open_data = {
            "wallet": wallet,
            "password": hashlib.sha256(password.encode('utf-8')).hexdigest().upper()
        }
        res = do_post(session, uri, json=open_data, verify="root_cert.pem")
        self.assertEqual(res.status_code ,200)
        self.assertIn("Petalus-SessionId", res.headers)
        good_session = res.headers["Petalus-SessionId"]

        data1 = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
        data2 = "........................................................."
        data3 = "*********************************************************"

        uri = f"{self.layer}://localhost:{user_port1}/v1/block/{wallet}"
        headers = {
            "Petalus-SessionId": good_session,
            "Content-Type": "text/plain"
        }
        res = do_post(session, uri, data=data1, headers=headers)
        self.assertEqual(res.status_code ,200)
        res = do_post(session, uri, data=data2, headers=headers)
        self.assertEqual(res.status_code ,200)
        res = do_post(session, uri, data=data3, headers=headers)
        self.assertEqual(res.status_code ,200)

        uri = f"{self.layer}://localhost:{user_port1}/v1/block/{wallet}/5"
        res = do_get(session, uri, headers=headers)
        self.assertEqual(res.status_code ,200)
        session.close()    

if __name__ == "__main__":
    """Check some not used ports for the tests"""

    user_port_1 = get_new_port(5555, 5599)
    address = ip + ":" + str(user_port_1)

    admin_port = get_new_port(user_port_1 + 1, user_port_1 + 1000)
    admin_address = ip + ":" + str(admin_port)

    # Check if there is a loopback device with an extra IP
    readonly_ip = ""
    readonly_device = ""
    for dev in netifaces.interfaces():
        if (dev == "lo:0"):
            item = netifaces.ifaddresses(dev)
            readonly_device = str(dev)
            readonly_ip = str(item[netifaces.AF_INET][0]['addr'])

    if not readonly_ip or not readonly_device:
        print(bcolors.FAIL + "The tests needs a extra IP address on the loopback device")
        print("Please setup the IP with 'ifconfig lo:0 127.0.0.2 up' command" + bcolors.ENDC)
        sys.exit(-1)

    print(f"{bcolors.HEADER}Running Petalus tests suites{bcolors.ENDC}")
    print(f"\tServer address {address}")
    print(f"\tAdministrative address {admin_address}")
    print(f"\tReadonly IP {readonly_ip}") 
    logfile = "petalus-00000.log"
    if (os.path.isfile(logfile)):
        os.remove(logfile)

    r =  unittest.main(exit = False)

    if (len(sys.argv) == 1):
        # All the test cases are going to be executed
        if (len(r.result.errors) == 0)and(len(r.result.failures) == 0):
            print(f"{bcolors.OKGREEN}Ready for version {py_major}.{py_minor}!{bcolors.ENDC}")
        else:
            print(f"{bcolors.FAIL}Not ready for version {py_major}.{py_minor}!{bcolors.ENDC}")

    print(f"HTTP Post operations:{total_http_posts}")
    print(f"HTTP Get operations :{total_http_gets}")

    sys.exit(0)
