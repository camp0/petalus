/*
 * Petalus a crypto micro service wallet database.
 *
 * Copyright (C) 2018-2022  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */

#ifndef SRC_HTTPSTATISTICS_H_
#define SRC_HTTPSTATISTICS_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>

class HTTPStatistics {
public:
	explicit HTTPStatistics() {}
	virtual ~HTTPStatistics() {}

	int32_t total_http_errors = 0;
	int32_t total_http_method_gets = 0;
	int32_t total_http_method_posts = 0;
	int32_t total_http_method_unknown = 0;
	int32_t total_sessions_not_found = 0;
	int32_t total_blocks_reads = 0;
	int32_t total_blocks_not_found = 0;
	int32_t total_blocks_writes = 0;
	int32_t total_blocks_write_fails = 0;
	int32_t total_wrong_wallet_uri = 0;
	int32_t total_ssl_handshake_errors = 0;
	int32_t total_ssl_handshakes = 0;
	int32_t total_http_body_limit_exceeds = 0;
	int32_t total_http_success_auths = 0;
	int32_t total_http_fail_auths = 0;

	friend std::ostream& operator<< (std::ostream &out, const HTTPStatistics &stats) {

        	out << "\t" << "Total http gets:        " << std::setw(10) << stats.total_http_method_gets << "\n";
        	out << "\t" << "Total http posts:       " << std::setw(10) << stats.total_http_method_posts << "\n";
        	out << "\t" << "Total http unknows:     " << std::setw(10) << stats.total_http_method_unknown << "\n";
        	out << "\t" << "Total http body exceeds:" << std::setw(10) << stats.total_http_body_limit_exceeds << "\n";
        	out << "\t" << "Total http errors:      " << std::setw(10) << stats.total_http_errors << "\n";
        	out << "\t" << "Total http auths:       " << std::setw(10) << stats.total_http_success_auths << "\n";
        	out << "\t" << "Total http fail auths:  " << std::setw(10) << stats.total_http_fail_auths << "\n";
        	out << "\t" << "Total ssl handshakes:   " << std::setw(10) << stats.total_ssl_handshakes << "\n";
       	 	out << "\t" << "Total ssl errors:       " << std::setw(10) << stats.total_ssl_handshake_errors << "\n";
        	out << "\t" << "Total unknow sessions:  " << std::setw(10) << stats.total_sessions_not_found << "\n";
        	out << "\t" << "Total read blocks:      " << std::setw(10) << stats.total_blocks_reads << "\n";
        	out << "\t" << "Total write blocks:     " << std::setw(10) << stats.total_blocks_writes << "\n";
        	out << "\t" << "Total write block fails:" << std::setw(10) << stats.total_blocks_write_fails << "\n";
        	out << "\t" << "Total not found blocks: " << std::setw(10) << stats.total_blocks_not_found << "\n";
        	out << "\t" << "Total wrong wallets:    " << std::setw(10) << stats.total_wrong_wallet_uri << "\n";
		return out;
	}
};

#endif  // SRC_HTTPSTATISTICS_H_
