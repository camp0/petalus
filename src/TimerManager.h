/*
 * Petalus a crypto micro service wallet database.
 *
 * Copyright (C) 2018-2022  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */

#ifndef SRC_TIMERMANAGER_H_
#define SRC_TIMERMANAGER_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>
#include <boost/bind/bind.hpp>
#if defined(PYTHON_BINDING)
#include <boost/python.hpp>
#endif
#include "Timer.h"

namespace petalus {

class TimerManager {
public:
        explicit TimerManager(boost::asio::io_context &io):
                io_(io) {}

        virtual ~TimerManager() { timers_.clear(); }

#if defined(PYTHON_BINDING)
        void addTimer(PyObject *callback, int seconds);
        void statistics(std::basic_ostream<char> &out) const;
        friend std::ostream& operator<< (std::ostream &out, const TimerManager &tm);
#endif

private:
#if defined(PYTHON_BINDING)
        void start_timer(const SharedPointer<Timer> timer);
        void stop_timer(const SharedPointer<Timer> timer);
        void scheduler_handler(boost::system::error_code error, const WeakPointer<Timer> timer);
#endif
        boost::asio::io_context &io_;
        std::map<int, SharedPointer<Timer>> timers_;
};

} // namespace petalus

#endif  // SRC_TIMERMANAGER_H_

