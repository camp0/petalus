/*
 * Petalus a crypto micro service wallet database.
 *
 * Copyright (C) 2018-2022  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */

#ifndef SRC_HTTPSESSION_H_
#define SRC_HTTPSESSION_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <functional>
#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/ssl.hpp>
#include <boost/beast/version.hpp>
#include <boost/asio/dispatch.hpp>
#include <boost/asio.hpp>
#include <boost/config.hpp>
#include <boost/algorithm/string.hpp>
#include <iostream>
#include <memory>
#include <set>
#include "Pointer.h"
#include "SessionWalletManager.h"
#include "json.hpp"
#include "HTTPStatistics.h"
#include "Logger.h"
#include "PetalusUris.h"

namespace ip = boost::asio::ip;         // from <boost/asio.hpp>
using tcp = boost::asio::ip::tcp;       // from <boost/asio.hpp>
namespace bhttp = boost::beast::http;    // from <boost/beast/http.hpp>

using namespace petalus::blockchain;

namespace petalus::http {

typedef boost::beast::tcp_stream TCPSocket;
typedef boost::beast::ssl_stream<boost::beast::tcp_stream> SSLSocket;

class HTTPSession : public std::enable_shared_from_this<HTTPSession>
{
public:
    	explicit HTTPSession(tcp::socket socket): 
		send_response(*this),
		tls_support_(false),
		tcp_stream_(new TCPSocket(std::move(socket))) {

		parser.body_limit((std::numeric_limits<std::uint64_t>::max)());
	}
    	
	explicit HTTPSession(tcp::socket socket, boost::asio::ssl::context& ctx): 
		send_response(*this),
		tls_support_(true),
		ssl_stream_(new SSLSocket(std::move(socket), ctx)) {

		parser.body_limit((std::numeric_limits<std::uint64_t>::max)());
	}


	virtual ~HTTPSession() { swm.reset(); }

	boost::beast::string_view http_session_header_value = "Petalus-SessionId";
	boost::beast::string_view http_user_signature_header_value = "Petalus-User-Signature";

    	void start();

	void setSessionWalletManager(const SharedPointer<SessionWalletManager> &sw);
	void setProcessHandler(const std::function <void (bhttp::request<bhttp::dynamic_body>&&)> func); 
	
	// Sets a pointer to the global statistics
	void setHTTPStatistics(HTTPStatistics *stats) { gstats = stats; }

	bool haveTLSSupport() const { return tls_support_; }

	const char *getSocketIPAddress() const;
	const HTTPStatistics &getHTTPStatistics() const { return stats; }

	int32_t getTotalRequests() const { return total_requests_; }
	int32_t getTotalResponses() const { return total_responses_; }
	int64_t getTotalBytes() const { return total_bytes_; }
	std::time_t getLastTime() const { return last_time_; }

    	// This is the C++11 equivalent of a generic lambda.
    	// The function object is used to send an HTTP response message.
    	struct send_lambda {
        	HTTPSession& self_;

        	explicit send_lambda(HTTPSession& self) : self_(self) {}

        	template<bool isRequest, class Body, class Fields>
        	void operator()(bhttp::message<isRequest, Body, Fields>&& msg) const {
            		// The lifetime of the message has to extend
			// for the duration of the async operation so
            		// we use a shared_ptr to manage it.
            		auto sp = std::make_shared<
                		bhttp::message<isRequest, Body, Fields>>(std::move(msg));

            		// Store a type-erased version of the shared
            		// pointer in the class to keep it alive.
            		self_.res_ = sp;

			if (self_.tls_support_)
				bhttp::async_write(
					*self_.ssl_stream_, *sp,
                			boost::beast::bind_front_handler(
                    				&HTTPSession::handle_write,
                    				self_.shared_from_this(),
                    				sp->need_eof()));
			else
            			bhttp::async_write(
                			*self_.tcp_stream_, *sp,
                			boost::beast::bind_front_handler(
                    				&HTTPSession::handle_write,
                    				self_.shared_from_this(),
                    				sp->need_eof()));
        	}
    	};

	void close_session();

	const char *getSessionId() const { return sessionid.c_str(); }

protected:
	bhttp::request<bhttp::dynamic_body> request;
	HTTPStatistics *gstats;
	HTTPStatistics stats;
    	send_lambda send_response;
	std::string ipsrc = "";
	std::string sessionid = "";
	int portsrc = 0;
	SharedPointer<SessionWalletManager> swm = nullptr;
private:
	void handle_write(bool close, boost::beast::error_code ec, std::size_t bytes_transferred);
	void handle_read(boost::beast::error_code ec, std::size_t bytes_transferred);
	void do_close();
	void do_read();
    	void handle_shutdown(boost::beast::error_code ec);
	void handle_handshake(boost::beast::error_code ec);

	bhttp::response_parser<bhttp::file_body> parser;
	bool tls_support_ = false;
	int16_t timeout_ = 30;
	int32_t total_requests_ = 0;
	int32_t total_responses_ = 0;
	int64_t total_bytes_ = 0;
	std::time_t last_time_ = (std::time_t)0;
	SharedPointer<TCPSocket> tcp_stream_ = nullptr;
	SharedPointer<SSLSocket> ssl_stream_ = nullptr;
	std::function <void (bhttp::request<bhttp::dynamic_body>&&)> process_session_request = [&] (bhttp::request<bhttp::dynamic_body>&&) { return; };
	boost::beast::flat_buffer buffer_;
    	std::shared_ptr<void> res_;
};

} // namespace petalus::http

#endif // SRC_HTTPSESSION_H_
