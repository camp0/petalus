/*
 * Petalus a crypto micro service wallet database.
 *
 * Copyright (C) 2018-2022  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */

#ifndef SRC_SERVER_H_
#define SRC_SERVER_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>
#include <boost/beast/core.hpp>
#include <boost/asio.hpp>
#include <boost/bind/bind.hpp>
#include <boost/asio/bind_executor.hpp>
#include <boost/asio/ssl.hpp>
#include <boost/exception/diagnostic_information.hpp>
#include <system_error>
#include <execinfo.h> // for backtrace
#if defined(PYTHON_BINDING)
#include <boost/python.hpp>
#include "TimerManager.h"
#include "Interpreter.h"
#endif
#include "Pointer.h"
#include "HTTPAdministrationSession.h"
#include "HTTPUserSession.h"
#include "HTTPStatistics.h"
#include "System.h"
#include "Logger.h"
#include "TCPConnectionTable.h"

using namespace petalus::blockchain;
using namespace petalus::http;

namespace petalus {

class Server {
public:
	explicit Server(): signals_(io_, SIGINT, SIGTERM), 
		user_socket_(io_), 
		admin_socket_(io_), 
		user_acceptor_(io_), 
		admin_acceptor_(io_),
		stats() {
		signals_.async_wait(
                	boost::bind(&boost::asio::io_service::stop, &io_));
		ssl_ctx_.set_options(
        		boost::asio::ssl::context::default_workarounds |
        		boost::asio::ssl::context::no_sslv2 |
        		boost::asio::ssl::context::no_tlsv1 |
        		boost::asio::ssl::context::no_tlsv1_1 |
        		boost::asio::ssl::context::single_dh_use);
	}

	virtual ~Server() { io_.stop(); }

	void statistics(std::basic_ostream<char> &out) const;

	void setSessionWalletManager(const SharedPointer<SessionWalletManager> &sw);
	SharedPointer<SessionWalletManager> getSessionWalletManager() const { return sw_; }

	void setIPAddress(const std::string &ip) { internet_address_ = boost::asio::ip::make_address(ip.c_str()); }
	const char *getIPAddress() const { return internet_address_.to_string().c_str(); }

	void setPort(uint16_t port) { internet_port_ = port; }
	uint16_t getPort() const { return internet_port_; }

	void setAdministrationIPAddress(const std::string &ip) { administration_address_ = boost::asio::ip::make_address(ip.c_str()); }
	const char *getAdministrationIPAddress() const { return administration_address_.to_string().c_str(); }

	void setAdministrationPort(uint16_t port) { administration_port_ = port; }
	uint16_t getAdministrationPort() const { return administration_port_; }

	void setProxySupport(bool value);
	bool getProxySupport() const { return proxy_support_; }

	// Use the openssl interface
        // openssl dhparam -out dh.pem 2048
        // openssl req -newkey rsa:2048 -nodes -keyout key.pem -x509 -days 10000 -out cert.pem -subj "//C=US\ST=CA\L=Los Angeles\O=Beast\CN=www.example.com"
	void setPrivateKey(const std::string &key);
	void setPrivateKeyPassword(const std::string &pass);
	void setCertificate(const std::string &cert);
	void setCertificateAuthority(const std::string &cert);
	void setAuthorizationToken(const std::string &token); 

	void start(); 
	void run(); 
	void stop();

	void addAdministrationIPAddress(const std::string &ip);
	void setLicenseExpirationTime(const boost::posix_time::ptime &expire_time) { license_expire_time_ = expire_time; } 
	void setLicenseMaxFoldersPerWallet(int value) { max_folders_per_wallet_ = value; }

#if defined(PYTHON_BINDING) 
	void addTimer(PyObject *callback, int seconds);

	void setShell(bool enable);
	bool getShell() const;

	void setMTLS(bool value); 
	bool getMTLS() const; 

	void show() const { statistics(std::cout); }
	void showConnections() { show_connections(OutputManager::getInstance()->out()); }
	void showConnection(const std::string &id) { show_connection(OutputManager::getInstance()->out(), id); }
	
	const char *getPrivateKey() const { return short_private_key_.c_str(); } 
	const char *getCertificate() const { return short_certificate_.c_str(); } 
	const char *getPrivateKeyPassword() const { return private_key_password_.c_str(); }
	const char *getAuthorizationToken() const { return authorization_token_.c_str(); }

	void addAdministrationIPAddress(const boost::python::list &items);
	boost::python::list getAdministrationIPAddresses() const;
	void showAdministrationIPAddresses() { show_administration_ips(OutputManager::getInstance()->out()); }

	void closeConnection(const std::string &id);

	void addSecurityCallback(PyObject *callback);
	PyObject *getSecurityCallback() const { return security_callback_->getCallback(); }

#endif

#if defined(STAND_ALONE_TEST) || defined(TESTING)

	int32_t getTotalCreatedWallets() const { return total_wallets_; }
	int32_t getTotalWallets() const { return cache_file_.size(); }

	int32_t getTotalBytes() const { return total_bytes_; }
	int32_t getTotalWrites() const { return total_writes_; }

	SharedPointer<Wallet> getWallet(const std::string &wallet) { return get_wallet_from_cache(wallet); }
#endif
	friend std::ostream& operator<< (std::ostream &out, const Server &bs);

private:

	const char *show_connection_header = "%-32s %-32s %-12s %-8s %-8s %-8s";
        const char *show_connection_format = "%-32s %-32s %-12d %-8d %-8s %-8s";

	const char *show_rep_connection_header = "%-56s %-12s %-8s %-8s %-8s %-8s";
        const char *show_rep_connection_format = "%-56s %-12d %-8d %-8d %-8s %-8s";

	void show_connections(std::basic_ostream<char> &out);
	void show_user_connections(std::basic_ostream<char> &out);
	void show_administration_connections(std::basic_ostream<char> &out);
	void show_administration_ips(std::basic_ostream<char> &out);

#if defined(PYTHON_BINDING)
	void start_read_user_input(void);
	void show_connection(std::basic_ostream<char> &out, const std::string &id);
#endif	
	void accept_user_connections();
	void accept_administration_connections();
	std::string private_key_password_callback();
	std::string retrieve_secure_data(const std::string &data);

	boost::asio::io_context io_;
	boost::asio::signal_set signals_;
	boost::asio::ip::address internet_address_;
	boost::asio::ip::address administration_address_;
	uint16_t internet_port_ = 0;
	uint16_t administration_port_ = 0;
	bool ready_ = true; // The server is ready for accept connections
	bool tls_support_ = false; // By default the server works without TLS
	bool proxy_support_ = false; // If the instance is behind a proxy
	bool mtls_ = false; // Enable mtls support
	boost::asio::ip::tcp::socket user_socket_;
	boost::asio::ip::tcp::socket admin_socket_;
	boost::asio::ip::tcp::acceptor user_acceptor_;
	boost::asio::ip::tcp::acceptor admin_acceptor_;
	boost::asio::ssl::context ssl_ctx_ {boost::asio::ssl::context::sslv23};
	SharedPointer<SessionWalletManager> sw_ = nullptr;
	HTTPStatistics stats;
	std::time_t last_timeout_time_ = std::time(nullptr); 
	boost::posix_time::ptime license_expire_time_ = boost::posix_time::from_time_t(std::time(nullptr) + 3600);
	int max_folders_per_wallet_ = 16;

	// TCP connection tables for tracking
	SharedPointer<TCPTable<HTTPUserSession>>  user_connections_ = SharedPointer<TCPTable<HTTPUserSession>>(new TCPTable<HTTPUserSession>());
	SharedPointer<TCPTable<HTTPAdministrationSession>> admin_connections_ = SharedPointer<TCPTable<HTTPAdministrationSession>>(new TCPTable<HTTPAdministrationSession>());

	// Set with the authorized ip addresss for administration
	std::set<std::string> administration_ips_ {};

	std::string short_private_key_ = ""; 
	std::string short_certificate_ = "";
	std::string private_key_password_ = "";
	std::string authorization_token_ = "";
#if defined(PYTHON_BINDING) 
	SharedPointer<TimerManager> tm_ = SharedPointer<TimerManager>(new TimerManager(io_));
	SharedPointer<Interpreter> user_shell_ = SharedPointer<Interpreter>(new Interpreter(io_));
	SharedPointer<Callback> security_callback_ = SharedPointer<Callback>(new Callback());
#endif
	SharedPointer<System> sys_ = SharedPointer<System>(new System());
	CryptoPP::SecByteBlock authorization_token_key_ {};
};

} // namespace petalus

#endif  // SRC_SERVER_H_
