/*
 * Petalus a crypto micro service wallet database.
 *
 * Copyright (C) 2018-2022  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <execinfo.h>
#include <unistd.h>
#include <fstream>
#include <iostream>
#include <functional>
#include <cctype>
#include <csignal>
#include <boost/program_options.hpp>
#include <fstream>
#include <cryptopp/files.h>
#include <boost/date_time/posix_time/posix_time_io.hpp>
#include <termio.h>
#include "BlockTypes.h"
#include "PetalusBlock.h"
#include "BlockFile.h"
#include "System.h"

#define BINARY_NAME "pgen"

using namespace petalus;
using namespace petalus::blockchain;

std::string filename = "";
std::string password_type = "aes";

void show_package_banner() {

	std::cout << "\tGCC version:" << __GNUG__ << "." << __GNUC_MINOR__ << "." << __GNUC_PATCHLEVEL__ << std::endl;
	std::cout << "\tBoost version:" << BOOST_VERSION / 100000 << "." << BOOST_VERSION / 100 % 1000 << std::endl;
        std::cout << std::endl;
}


void generate_password(const std::string &buffer, const std::string &ptype) {

	std::string tag = "{AES}";
	PetalusBlock block(buffer);

        EncryptionKeyType type = EncryptionKeyType::AES;
        if (ptype.compare("blowfish") == 0) {
        	type = EncryptionKeyType::BLOWFISH;
                tag = "{BLOWFISH}";
                if ((block.length() < CryptoPP::Blowfish::MIN_KEYLENGTH)or(block.length() > CryptoPP::Blowfish::MAX_KEYLENGTH)) {
                	petalus::warning_message("Blowfish encryption keys must be between 4 and 56 bytes length");
                        exit(-1);
                }
	} else {
        	if ((block.length() != 16)and(block.length() != 24)and(block.length() != 32)) {
                	petalus::warning_message("AES encryption keys can be only 16, 24 or 32 bytes length");
                        exit(-1);
                }
	}
	CryptoPP::SecByteBlock key;

	updateKey(key);

	EncryptionKey ekey(key);

	block.encrypt(ekey);

	std::string outbuffer;
	CryptoPP::HexEncoder encoder;
	encoder.Attach( new CryptoPP::StringSink(outbuffer) );
	encoder.Put((const uint8_t*)block.data(), block.length());
	encoder.MessageEnd();
	std::cout << tag << outbuffer << std::endl;
}


int main(int argc, char* argv[]) {

	namespace po = boost::program_options;
	po::variables_map var_map;

	po::options_description mandatory_ops("Optional arguments");
	mandatory_ops.add_options()
                ("password-type,t",    po::value<std::string>(&password_type)->default_value("aes"),
                        "Sets the password type for the encryption key (aes, blowfish).")
                ("filename,f",    po::value<std::string>(&filename)->default_value(""),
                        "Use the SHA256 of the file as input for the generated password for generate a genesis file")
                ;

	try {
		if (isatty(fileno(stdin))) 
        		po::store(po::parse_command_line(argc, argv, mandatory_ops), var_map);
		else 
        		po::store(po::parse_config_file(std::cin, mandatory_ops), var_map);

        	if (var_map.count("help")) {
            		std::cout << PACKAGE " " BINARY_NAME " " VERSION << std::endl;
            		std::cout << mandatory_ops << std::endl;
			std::cout << std::endl;
			//show_gpl_banner();
            		exit(0);
        	}
        	if (var_map.count("version")) {
            		std::cout << PACKAGE " " BINARY_NAME " " VERSION << std::endl;
			std::cout << std::endl;
			// show_gpl_banner();
			std::cout << std::endl;
			show_package_banner();
            		exit(0);
        	}

        	po::notify(var_map);
    	
	} catch(boost::program_options::required_option& e) {
            	std::cout << PACKAGE " " BINARY_NAME " " VERSION << std::endl;
        	std::cerr << "Error: " << e.what() << std::endl;
		std::cout << mandatory_ops << std::endl;
        	exit(-1);
	} catch(std::exception& e) {
            	std::cout << PACKAGE " " BINARY_NAME  " " VERSION << std::endl;
        	std::cerr << "Unsupported option." << e.what() << std::endl;
		std::cout << mandatory_ops << std::endl;
        	exit(-2);
    	}

        // The user use a file for generate the genesis block file
        if (filename.length() > 0) {
        	if (std::filesystem::exists(filename)) {
        		// Both temp files should have the same sha256
			uint8_t seed[HASH_BLOCK_SIZE];
			std::string source, string_seed;
        		CryptoPP::SHA256 hash;

        		CryptoPP::FileSource ff(filename.c_str(), true,
                		new CryptoPP::HashFilter(hash, new CryptoPP::HexEncoder(new CryptoPP::StringSink(source))));
			std::string destination;

			CryptoPP::StringSource ss(source, true /*PumpAll*/,
    				new CryptoPP::HexDecoder(
        				new CryptoPP::StringSink(string_seed) 
    				)
			);

			generate_password(string_seed, password_type);

			std::memcpy(seed, string_seed.c_str(), HASH_BLOCK_SIZE);

			System sys;
			std::ostringstream out, gccversion;

			out << "master." << getpid() << ".mblk";

                        std::time_t now = std::time(nullptr);

                        char mbstr[64];
			char loginname[64];
                        std::strftime(mbstr, 64, "%X %D", std::localtime(&now));
			nlohmann::json j;

			getlogin_r(loginname, sizeof(loginname));

			gccversion << __GNUG__ << "." << __GNUC_MINOR__ << "." << __GNUC_PATCHLEVEL__;
			j["gcc"] = gccversion.str();
			j["os_name"] = sys.getOSName();
			j["hostname"] = sys.getNodeName();
			j["release"] = sys.getReleaseName();
			j["machine"] = sys.getMachineName();
			j["timestamp"] = mbstr;
			j["user"] = loginname;

			// Generate the genesys master block file
			auto bfile = BlockFile(2048);

			bfile.open(out.str());

                        bfile.setSeed(seed);

			PetalusBlock block(j);
        		block.type(BlockType::SYS_JSON_MASTER_ENTRY);
        		bfile.write(block);
        		bfile.close();
			
			exit(0);
            	} else {
          		std::cout << "File " << filename << " do not exists";
			exit(-1);
            	}


	} else {
		// The user just want to have the output of a password
		std::string buffer;
		std::cout << "Insert the password:" << std::endl;
                
		termios oldt;
                tcgetattr(STDIN_FILENO, &oldt);
                termios newt = oldt;
                newt.c_lflag &= ~ECHO;
                tcsetattr(STDIN_FILENO, TCSANOW, &newt);

		std::getline(std::cin, buffer);

		tcsetattr(STDIN_FILENO, TCSANOW, &oldt);

		generate_password(buffer, password_type);
		exit(0);
	}
	return 0;
}

