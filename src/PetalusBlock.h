/*
 * Petalus a crypto micro service wallet database.
 *
 * Copyright (C) 2018-2022  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */

#pragma once
#ifndef SRC_PETALUSBLOCK_H_
#define SRC_PETALUSBLOCK_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>
#include <string>
#include <boost/utility/string_ref.hpp>
#include <cryptopp/gcm.h>
#include "CryptoOperations.h"
#include "BlockTypes.h"
#include "Message.h"
#include "json.hpp"
#include "EncryptionKey.h"

namespace petalus::blockchain {

class EncryptionKey;

class PetalusBlock 
{
public:
	PetalusBlock() {}
	PetalusBlock(const nlohmann::json &j);
	PetalusBlock(const char *data):data_(data) {}
	PetalusBlock(const boost::string_ref &data):data_(data) {}
	PetalusBlock(const std::string &data):data_(data) {}
	PetalusBlock(const boost::string_ref &data, BlockType type):data_(data), type_(type) {}
	PetalusBlock(const std::string &data, BlockType type):data_(data), type_(type) {}

	virtual ~PetalusBlock() {}

	void data(const std::string &data) { data_ = data; }
	void data(const boost::string_ref &data) { data_.assign(data.data(), data.length()); }
      	const char *data() const { return data_.c_str(); }
	std::size_t length() const { return data_.length(); }

	void prepend(const boost::string_ref &data) { data_.insert(0, data.data(), data.length()); }

	void signature(const boost::string_ref &signature);
	const std::byte *signature() const { return &signature_.front(); }
	std::size_t signature_length() const { return signature_.size(); }

	bool have_signature() const { return have_signature_; }

	void type(BlockType type) { type_ = type; }
	BlockType type() const { return type_; }

	void timestamp(const std::time_t time) { timestamp_ = time; }
	std::time_t timestamp() const { return timestamp_; }	

	bool isUserBlock() const { return (type_ <= BlockType::USR_SOURCE_CRYPT); } 

#if defined(PYTHON_BINDING)

	void accept_block(bool value) { accept_block_ = value; }
	bool accept_block() const { return accept_block_; }

	bool getAccept() const { return accept_block(); }
	void setAccept(bool value) { accept_block(value); }	

	void setData(const std::string &data) { data_ = data; }
	const char *getData() const { return data_.c_str(); }
#endif       
	friend std::ostream& operator<< (std::ostream &out, const PetalusBlock &pb); 

	bool operator==(const PetalusBlock &pb);
	bool operator!=(const PetalusBlock &pb);

	int encrypt(const EncryptionKey &ekey);
	int decrypt(const EncryptionKey &ekey);

private:
	std::string data_ = "";
#if defined(PYTHON_BINDING)
	bool accept_block_ = true;
#endif       
	BlockType type_ = BlockType::USR_PLAIN;
	std::vector<std::byte> signature_ {};
	bool have_signature_ = false;
	std::time_t timestamp_ = std::time(nullptr);
};

} // namespace petalus::blockchain

#endif  // SRC_PETALUSBLOCK_H_

