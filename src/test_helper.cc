/*
 * Petalus a crypto micro service wallet database.
 *
 * Copyright (C) 2018-2022  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */

#include <string>
#include <fstream>
#include <boost/algorithm/string.hpp>
#include <boost/random/random_device.hpp>
#include <boost/random/uniform_int_distribution.hpp>
#include <cryptopp/filters.h>
#include <cryptopp/files.h>
#include <cryptopp/base64.h>
#include <boost/test/unit_test.hpp>
#include "CryptoOperations.h"
#include "PetalusBlock.h"
#include "test_helper.h"

using namespace petalus::blockchain;

namespace petalus {
namespace blockchain {

std::string test_data = "./petalustest_data0";

void setDataDirectory(const std::string &path) {

	test_data = path; 
}

const char *getDataDirectory() { return test_data.c_str(); }

int generateRandomInt(int max) {

    	boost::random::random_device rng;
    	boost::random::uniform_int_distribution<> index_dist(1, max);

	return (int)index_dist(rng);
}

std::string generateRandomString(int length) {
	std::string chars(
        	"abcdefghijklmnopqrstuvwxyz"
        	"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        	"1234567890"
        	"!@#$%^&*()"
        	"`~-_=+[{]}|;:'\",<.>/? ");
	std::stringstream out;

    	boost::random::random_device rng;
    	boost::random::uniform_int_distribution<> index_dist(0, chars.size() - 1);
    	for(int i = 0; i < length; ++i) {
        	out << chars[index_dist(rng)];
    	}
	return out.str();
}

std::string generateBlockFileName(const std::string &tag) {

	std::ostringstream out;

	auto suite_id = boost::unit_test::framework::current_test_case().p_parent_id;

	out << test_data << "/suite" << suite_id;
	out << boost::unit_test::framework::current_test_case().p_name;

	std::string extension ("mblk");
	if (tag.length() > 0) {
		out << "." << tag;
		extension = "fblk";
	}
	out << "." << extension;

	// TODO boost 1.60 have full_name	std::cout << boost::unit_test::framework::current_test_case().full_name();
	//std::cout << boost::unit_test::framework::get<boost::unit_test::test_suite>(suite_id) << std::endl;

	return out.str();
}

std::string generateHexBlockFilename(const std::string &tag) {

	std::string filename(generateBlockFileName(tag));
        std::string hash;

        uint8_t digest[HASH_BLOCK_SIZE];

	computeSHA256(digest, (const uint8_t*)filename.c_str(), filename.length());

        CryptoPP::HexEncoder encoder;
        hash.clear();
        encoder.Attach( new CryptoPP::StringSink(hash) );
        encoder.Put( digest, sizeof(digest) );
        encoder.MessageEnd();

	return hash;
}

std::string generateTestPath(const std::string &wallet) {

	return std::string(test_data + std::string("/") + wallet[0] + std::string("/") + wallet[1] + std::string("/"));
}

std::string generateTestFile(const std::string wallet, int folder) {

	std::string filename;
        std::string p(test_data + std::string("/") + wallet[0] + std::string("/") + wallet[1] + std::string("/") + wallet);

	std::string extension("mblk");
	filename = p;
	if (folder > 0) { 
		filename += std::string(".") + std::to_string(folder);
		extension = "fblk";
	}
	return filename + std::string(".") + extension + std::string("0000");
}

std::string generateAESPassword(const std::string &pass) {
	CryptoPP::SecByteBlock key { 32 };
	PetalusBlock block(pass);

        updateKey(key);
	EncryptionKey ekey(key);

        block.encrypt(ekey);

        std::string outbuffer;
        CryptoPP::HexEncoder encoder;
        encoder.Attach( new CryptoPP::StringSink(outbuffer) );
        encoder.Put((const uint8_t*)block.data(), block.length());
        encoder.MessageEnd();
        
	return std::string("{AES}" + outbuffer);
}

std::string generateBlowfishPassword(const std::string &pass) {
        CryptoPP::SecByteBlock key { 32 };
        PetalusBlock block(pass);

        updateKey(key);

	EncryptionKey ekey(key);
	// ekey.type(EncryptionKeyType::ENCRYPTION_KEY_BLOWFISH);

        block.encrypt(ekey);

        std::string outbuffer;
        CryptoPP::HexEncoder encoder;
        encoder.Attach( new CryptoPP::StringSink(outbuffer) );
        encoder.Put((const uint8_t*)block.data(), block.length());
        encoder.MessageEnd();

        return std::string("{BLOWFISH}" + outbuffer);
}

std::string generateSHA256(const std::string &pass) {

	uint8_t digest[HASH_BLOCK_SIZE];
        std::string hash;

        computeSHA256(digest, (const uint8_t*)pass.c_str(), pass.length());

        CryptoPP::HexEncoder encoder;
        encoder.Attach( new CryptoPP::StringSink(hash) );
        encoder.Put( digest, sizeof(digest) );
        encoder.MessageEnd();

	return hash;
}

std::string generateBase64PublicKey(PrivateKey &priv, PublicKey &pub, CryptoPP::OID curve) {

	std::string b64pub;
        CryptoPP::AutoSeededRandomPool prng;

        priv.Initialize( prng, curve );

        // Generate the public from the private
        priv.MakePublicKey( pub );
        pub.AccessGroupParameters().SetPointCompression(true);

        std::string pkey;
        CryptoPP::StringSink ss(pkey);
        pub.Save(ss); // Save the public key on a string

        // Convert to a base64 string
        CryptoPP::StringSource(pkey, true, new CryptoPP::Base64Encoder(new CryptoPP::StringSink(b64pub)));
	return b64pub;
}

std::string generateBase64ED25519PublicKey(ED25519PrivateKey &priv, ED25519PublicKey &pub) {

        std::string b64pub;
        CryptoPP::AutoSeededRandomPool prng;
	CryptoPP::AlgorithmParameters parameters;
        EDSigner signer;

        signer.AccessPrivateKey().GenerateRandom(prng);

        // priv = static_cast<ED25519PrivateKey&>(signer.GetPrivateKey());
	// priv.GenerateRandom(prng, parameters);
	// TODO

	priv.MakePublicKey(pub);

        EDVerifier verifier(signer);

        std::string pkey;
        CryptoPP::StringSink ss(pkey);
        verifier.GetPublicKey().Save(ss); // Save the public key on a string

	///std::cout << "PublicKey:" << pkey << std::endl;
        // Convert to a base64 string
        CryptoPP::StringSource(pkey, true, new CryptoPP::Base64Encoder(new CryptoPP::StringSink(b64pub)));
        return b64pub;
}

} // namespace blockchain
} // namespace petalus
