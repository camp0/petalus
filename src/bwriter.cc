/*
 * Petalus a crypto micro service wallet database.
 *
 * Copyright (C) 2018-2022  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <unistd.h>
#include <fstream>
#include <iostream>
#include <functional>
#include <cctype>
#include <csignal>
#include <boost/program_options.hpp>
#include <fstream>
#include <boost/date_time/posix_time/posix_time_io.hpp>
#include "BlockTypes.h"
#include "PetalusBlock.h"
#include "BlockFile.h"
#include "Message.h"

using namespace petalus;
using namespace blockchain;

std::string option_filename;
std::string option_message;
std::string prev_file;
std::string hash_method;

void show_package_banner() {

	std::cout << "\tGCC version:" << __GNUG__ << "." << __GNUC_MINOR__ << "." << __GNUC_PATCHLEVEL__ << std::endl;
	std::cout << "\tBoost version:" << BOOST_VERSION / 100000 << "." << BOOST_VERSION / 100 % 1000 << std::endl;
        std::cout << std::endl;
}

int main(int argc, char* argv[]) {

	namespace po = boost::program_options;
	po::variables_map var_map;

	po::options_description mandatory_ops("Mandatory arguments");
	mandatory_ops.add_options()
		("input,i",   po::value<std::string>(&option_filename),
			"Block file name.")
		("message,m",   po::value<std::string>(&option_message),
			"Message that will be added to the file.")
        	;

        po::options_description optional_ops("Optional arguments");
        optional_ops.add_options()
                ("prev,p",      po::value<std::string>(&prev_file),
                                "Previous block file for sign the current file.")
                ("hash,h",      po::value<std::string>(&hash_method),
                                "Sets the hashing method (sha256, blake2s and sha3-256).")
                ;

        mandatory_ops.add(optional_ops);

	try {
	
        	po::store(po::parse_command_line(argc, argv, mandatory_ops), var_map);

        	if (var_map.count("help")) {
            		std::cout << PACKAGE " " VERSION << std::endl;
            		std::cout << mandatory_ops << std::endl;
			std::cout << std::endl;
			//show_gpl_banner();
            		exit(0);
        	}
        	if (var_map.count("version")) {
            		std::cout << PACKAGE " " VERSION << std::endl;
			std::cout << std::endl;
			// show_gpl_banner();
			std::cout << std::endl;
			show_package_banner();
            		exit(0);
        	}
		if (var_map.count("input") == 0) {
            		std::cout << PACKAGE " " VERSION << std::endl;
            		std::cout << mandatory_ops << std::endl;
			exit(-1);
		}
		if (var_map.count("message") == 0) {
            		std::cout << PACKAGE " " VERSION << std::endl;
            		std::cout << mandatory_ops << std::endl;
			exit(-1);
		}

        	po::notify(var_map);
    	
	} catch(boost::program_options::required_option& e) {
            	std::cout << PACKAGE " " VERSION << std::endl;
        	std::cerr << "Error: " << e.what() << std::endl;
		std::cout << mandatory_ops << std::endl;
        	exit(-1);
	} catch(std::exception& e) {
            	std::cout << PACKAGE " " VERSION << std::endl;
        	std::cerr << "Unsupported option." << e.what() << std::endl;
		std::cout << mandatory_ops << std::endl;
        	exit(-2);
    	}

	std::vector<std::string> inputs;
	namespace fs = std::filesystem;

        uint8_t initial_digest[CryptoPP::SHA256::DIGESTSIZE]; //  { 0xFA };
        //uint8_t initial_digest[CryptoPP::SHA256::DIGESTSIZE] = { 0xFF };
	std::memset(initial_digest, 0x00, CryptoPP::SHA256::DIGESTSIZE);

        if (prev_file.length() > 0) {
                // The user pass a initial block file for validation after
                if (std::filesystem::exists(prev_file)) {
                        auto seedblock = BlockFile(0);

                        seedblock.open(prev_file, boost::iostreams::mapped_file_base::mapmode::readonly);
                        int32_t blocks = seedblock.getTotalBlocks();
                        if (blocks > 0) {
                                seedblock.readBlockHash(blocks, initial_digest);
                                CryptoPP::HexEncoder encoder;
                                std::string hash;
                                encoder.Attach( new CryptoPP::StringSink(hash) );
                                encoder.Put( initial_digest, sizeof(initial_digest) );
                                encoder.MessageEnd();
                                petalus::information_message("Initial digest on memory");
                                petalus::information_message(hash);
                        }
                }
        }

	PetalusBlock block(option_message);

	// check if option_message is a file
	if (fs::exists(option_message)) {
		std::ofstream file;
		file.open (option_message, std::ofstream::in | std::ofstream::binary);

		if (file.is_open()) {
			std::stringstream ss;
    			ss << file.rdbuf();

			block.data(ss.str());
			block.type(BlockType::USR_BINARY_FILE);
		}
		file.close();
	}

	int32_t new_alloc_size = block.length() + sizeof(petalus_block_file_v1);

        if (fs::exists(option_filename)) {
        	std::uintmax_t filesize = 0;
                std::error_code ec;

                // Take the size of the file
                filesize = fs::file_size(option_filename, ec);
                if (ec) {
                	std::cout << "ERROR on path:" << option_filename << " ec:" << ec << std::endl;
                        return 0;
                }

                new_alloc_size += filesize;
	} else {
		new_alloc_size += sizeof(petalus_header_file); 
	}

	auto bfile = BlockFile(new_alloc_size);

	bfile.open(option_filename);

	// Verify if the file have blocks or not
	if (bfile.getTotalBlocks() == 0) {
		bfile.setSeed(initial_digest);
	}
	if (bfile.write(block) > 0) {
		std::cout << "Data has been written on the block file " << option_filename << "\n";
	} else {
		std::cout << "Data can not be written on the block file " << option_filename << "\n";
	}

	bfile.close();

	return 0;
}

