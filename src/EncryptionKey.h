/*
 * Petalus a crypto micro service wallet database.
 *
 * Copyright (C) 2018-2022  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */

#ifndef SRC_ENCRYPTIONKEY_H_
#define SRC_ENCRYPTIONKEY_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>
#include <string>
#include <cryptopp/secblock.h>
#include "PetalusBlock.h"

namespace petalus::blockchain {

// Encryption Key types
enum class EncryptionKeyType : uint8_t {
	AES =		0x00,
	BLOWFISH = 	0x01
};

class EncryptionKey 
{
public:
	EncryptionKey() {}
	EncryptionKey(CryptoPP::SecByteBlock &key);
	virtual ~EncryptionKey() {}

	bool password(const std::string &password);
	const CryptoPP::SecByteBlock &key() const { return key_; }

	EncryptionKeyType type() const { return type_; }
	void type(EncryptionKeyType type) { type_ = type; }

	void assign(const uint8_t *data, int len);

	bool operator==(const EncryptionKey &ekey);
	bool operator!=(const EncryptionKey &ekey);

	void reset() { std::memset(key_, 0, key_.size()); }
#if defined(STAND_ALONE_TEST) || defined(TESTING)
        const char *getDecryptedPassword() const { return decrypted_password_.c_str(); }
#endif

private:
	CryptoPP::SecByteBlock key_ {};
	EncryptionKeyType type_ = EncryptionKeyType::AES;
#if defined(STAND_ALONE_TEST) || defined(TESTING)
	std::string decrypted_password_ = "";
#endif
};

} // namespace petalus::crypto

#endif  // SRC_ENCRYPTIONKEY_H_

