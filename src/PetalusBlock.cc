/*
 * Petalus a crypto micro service wallet database.
 *
 * Copyright (C) 2018-2022  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */

#include <cryptopp/eax.h>
#include "PetalusBlock.h"


namespace petalus::blockchain { 

void PetalusBlock::signature(const boost::string_ref &signature) { 

	signature_.insert(signature_.end(), (std::byte*)signature.begin(), (std::byte*)signature.end()); 
	have_signature_ = true;
}

// EAX mode is the only mode supported  right now, may be GCM could be also
#define HAVE_EAX_MODE 1

#if defined(HAVE_EAX_MODE)
static uint8_t aiv[CryptoPP::AES::BLOCKSIZE * 16] = { 0x00 };
static uint8_t biv[CryptoPP::Blowfish::BLOCKSIZE] = { 0x00 };
#endif

int PetalusBlock::decrypt(const EncryptionKey &ekey) {

        // Check if the blocks has been encrypted
        if ((type_ == BlockType::USR_CRYPT_PLAIN)
                or(type_ == BlockType::USR_CRYPT_JSON_STRING)
                or(type_ == BlockType::USR_CRYPT_BINARY_FILE)) {

                // std::vector<uint8_t> recover;
                std::string recover = "";
                try {
                        const uint8_t *ptr = (uint8_t*)data_.c_str();
                        int len = data_.length();

#if defined(HAVE_EAX_MODE)
			if (ekey.type() == EncryptionKeyType::BLOWFISH) {
                        	CryptoPP::EAX< CryptoPP::Blowfish >::Decryption dec;

                        	dec.SetKeyWithIV(ekey.key().data(), ekey.key().size(), biv, sizeof(biv));

                        	CryptoPP::StringSource as(ptr, len, true,
                                	new CryptoPP::AuthenticatedDecryptionFilter(dec,
                                        	new CryptoPP::StringSink(recover)
                                	) // AuthenticatedDecryptionFilter
                        	);
			} else {
                        	CryptoPP::EAX< CryptoPP::AES >::Decryption dec;

                        	dec.SetKeyWithIV(ekey.key().data(), ekey.key().size(), aiv, sizeof(aiv));

                        	CryptoPP::StringSource as(ptr, len, true,
                                	new CryptoPP::AuthenticatedDecryptionFilter(dec,
                                        	new CryptoPP::StringSink(recover)
                                	) // AuthenticatedDecryptionFilter
                        	);
			}
#endif
                } catch(const CryptoPP::Exception& e) {
			// std::cout << "fucking error:" << e.what() << std::endl;
                        return -2;
                }

                if (type_ == BlockType::USR_CRYPT_PLAIN)
                        type_ = BlockType::USR_PLAIN;
                else if (type_ == BlockType::USR_CRYPT_JSON_STRING)
                        type_ = BlockType::USR_JSON_STRING;
                else if (type_ == BlockType::USR_CRYPT_BINARY_FILE)
                        type_ = BlockType::USR_BINARY_FILE;

                data(recover);
                return 0;
        }
        return -1;
}

int PetalusBlock::encrypt(const EncryptionKey &ekey) {

        if ((type_ == BlockType::USR_PLAIN)
                or(type_ == BlockType::USR_JSON_STRING)
                or(type_ == BlockType::USR_BINARY_FILE)) {
                std::string ciphertext;
                try {
                        const uint8_t *ptr = (uint8_t*)data_.c_str();
                        int len = data_.length();

#if defined(HAVE_EAX_MODE)
			if (ekey.type() == EncryptionKeyType::BLOWFISH) {
                        	CryptoPP::EAX< CryptoPP::Blowfish >::Encryption enc;

                        	enc.SetKeyWithIV(ekey.key().data(), ekey.key().size(), biv, sizeof(biv));

                        	CryptoPP::StringSource ss(ptr, len , true,
                                	new CryptoPP::AuthenticatedEncryptionFilter(enc,
                                        	new CryptoPP::StringSink(ciphertext)
                               		) // AuthenticatedEncryptionFilter
				); // StringSource
			} else {
                        	CryptoPP::EAX< CryptoPP::AES >::Encryption enc;

                        	enc.SetKeyWithIV(ekey.key().data(), ekey.key().size(), aiv, sizeof(aiv));

                        	CryptoPP::StringSource ss(ptr, len , true,
                                	new CryptoPP::AuthenticatedEncryptionFilter(enc,
                                        	new CryptoPP::StringSink(ciphertext)
                                	) // AuthenticatedEncryptionFilter
                        	); // StringSource
			}
#endif
                } catch(const CryptoPP::Exception& e) {
                        return -2;
                }

                if (type_ == BlockType::USR_PLAIN)
                        type_ = BlockType::USR_CRYPT_PLAIN;
                else if (type_ == BlockType::USR_JSON_STRING)
                        type_ = BlockType::USR_CRYPT_JSON_STRING;
                else if (type_ == BlockType::USR_BINARY_FILE)
                        type_ = BlockType::USR_CRYPT_BINARY_FILE;

                data(ciphertext);
                return 0;
        }
        return -1;
}

PetalusBlock::PetalusBlock(const nlohmann::json &j) {

	std::ostringstream out;

	out << j;

	data_ = out.str();
	type_ = BlockType::USR_JSON_STRING;
}

bool PetalusBlock::operator == (const PetalusBlock &pb) {

	return ((data_.length() == pb.data_.length())and(data_.compare(pb.data_) == 0));
}

bool PetalusBlock::operator != (const PetalusBlock &pb) {

	return ((data_.length() != pb.data_.length())or(data_.compare(pb.data_) != 0));
}

std::ostream& operator<< (std::ostream &out, const PetalusBlock &pb) {
	out << "Data:" << pb.data_ << "\n";
	out << "Len:" << pb.data_.length() << "\n";
	out << "Type:" << (int)pb.type_ << "\n";

	// DEBUG showData(out, pb.data_.data(), pb.data_.length());
	return out;
} 

} // namespace petalus::blockchain
