#!/usr/bin/env python
import os, signal, socket, sys
import logging, traceback
import dns.resolver
import petalus

rotation_time = 60 * 5

def timer1():
    print("Timer for 5 seconds")
    print(s)

def timer10():
    print("Timer for 12 seconds")
    sw.show_sessions()

def read_trigger(w):
    print("read python callback baby")
    print(w.block.data)
 
def write_trigger(w):
    print("write python callback baby")
    print("data:%s" % w.block.data)
    w.block.metadata = "Yes this is metadata!"
    print("metadata:%s " % w.block.metadata)

def dns_key_callback():

    print(sw.encryption_password)
    txt = dns.resolver.query("_spf.facebook.com", 'TXT')
    for i in txt.response.answer:
        for j in i.items:
            value = j.to_text().replace("\"", "")
            if (value.startswith("key=")):
                password_candidate = value.replace("key=", "")
                if (password_candidate != sw.encryption_password):
                    """ Do the rotation process """
               


def sec_callback(a):

    print(a.ip, a.http_errors, a.http_gets, a.http_no_sessions, a.http_posts)
    print(a.http_unknowns, a.http_wrong_wallet_uris)
    if (a.http_posts > 1):
        print("Closing connection")
        a.close()

def rotation_key_callback():
    """ Process for make a rotation of the key while processing traffic """
    newkey = "{BLOWFISH}25D49C27970336850812AB1B2F37F4029E63548F3912C03C3424A970962CC106BC57B4FC1020B0E8"
    sr.session_wallet_manager = None
    print("Rotating key %s" % newkey)
    sw.rotate_encryption_key(newkey)
    sw.encryption_password = newkey
    sr.session_wallet_manager = sw
    sr.add_timer(None, rotation_time)

if __name__ == "__main__":

    port = 5555

    if (len(sys.argv) == 3):
        """ variable for multiple instances on the log file """
        instance_name = sys.argv[1]
        port = int(sys.argv[2])

    sr = petalus.Server()
    sw = petalus.SessionWalletManager()
    rm = petalus.ReplicationManager()

    # sw.data_directory = "./myfilesystem/data1"
    sw.data_directory = "./petalustest_data"
    sw.increase_allocated_memory(100)

    """ p * 16 """
    rm.distributed_token = "{AES}D5E74BBD249E2DE3AA8D8D2EF3B5477F9302E60A2F672F634DEBB955CDBB9C7B"

    sr.address = "0.0.0.0"
    sr.port = port
    sr.administration_port = port + 1 
    sr.replication_port = 7778
    sr.session_wallet_manager = sw
    sr.replication_manager = rm
    sr.proxy_support = True

    """ hola 4 times """
    #sr.private_key_password = "{AES}CDF857AC3C8131F2B292913FEBAA5B6E92922DF0794E9248A88D13D5DB9B7DB4"

    """ openssl req -newkey rsa:2048 -nodes -keyout key.pem -x509 -days 10000 -out cert.pem -subj '/CN=www.example.com' """
    sr.private_key = open("key.pem").read()
    sr.certificate = open("cert.pem").read()

    # 192.168.122.1
    sr.administration_ip_addresses = ["127.0.0.1", "192.168.122.1"]
    # a * 16
    sr.authorization_token = "{AES}C4F65AAC358F3CF2BB9C9C3FE2A4566E2D4B21C415C34B7F065A817A29F6940E"
    # sr.security_callback = sec_callback

    # sr.add_timer(rotation_key_callback, rotation_time)

    #sr.add_timer(timer1, 5)
    # sr.add_timer(dns_key_callback, 10)

    sw.hash_type = "blake2s"
    # sw.encryption_password ="{AES}C9E252BE389B34E0B688942DEFB05E7C525C031D93CC2E9CB34D7951CC9F8881F3202E499AE8A51732680005ADD47D1F"
    sw.encryption_password = "{AES}94A60AFC65DF6CA2EBCCCC6FB2F4063EB0A48054E114D344360445F4B63665D1"
    sw.timeout = 60 
    sw.max_blockfile_size = 0

    """ Settings related to the master block file """
    # {AES}D2684E300CFE348F29A7C7CB238A343529C05A7BEC9640C0FD31A12E716A8E6F3ECDA1F556C2CA53D230E06F3CDF9035
    # master.24438.mblk
    # sw.master_blockfile_password = "{AES}0CC94DBDA71A176469181099F8E56C4F7313B947CB7D8AC7E6174AED6E270D09D721E2B8CAA6036C250F6BBE7CC95BB4"
    # sw.master_blockfile = "master.mblk"


    #sw.add_read_trigger("F7E6DAA644E159C363ED3C185F51327E36A2BFED6791D5157857E097F78FB0C0", read_trigger)
    #sw.add_write_trigger("F7E6DAA644E159C363ED3C185F51327E36A2BFED6791D5157857E097F78FB0C0", write_trigger)

    sr.enable_shell = True

    try:
        sr.start()
        sr.run()
    except Exception as e: 
        logging.error(traceback.format_exc()) 

    sr.stop()

    sw.add_read_trigger("F7E6DAA644E159C363ED3C185F51327E36A2BFED6791D5157857E097F78FB0C0", None)
    sw.add_write_trigger("F7E6DAA644E159C363ED3C185F51327E36A2BFED6791D5157857E097F78FB0C0", None)

    sys.exit(0)

