/*
 * Petalus a crypto micro service wallet database.
 *
 * Copyright (C) 2018-2022  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */

#ifndef SRC_PETALUSURIBLOCK_H_
#define SRC_PETALUSURIBLOCK_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>
#include "PetalusUris.h"

namespace petalus {

// The format of the URI here is:
//     /v1/block/825258D0514817B64DA41428A717730FBB4333861565C1839AB0F7A8D4151005/1
//     /v1/block/825258D0514817B64DA41428A717730FBB4333861565C1839AB0F7A8D4151005/folder/12
//

class PetalusUriBlock {
public:
        explicit PetalusUriBlock(const std::string &uri) {

		if (uri.compare(0, PetalusUris::rw_block_wallet.length(), PetalusUris::rw_block_wallet.data()) == 0) {
			std::vector<std::string> uri_items;

			boost::split(uri_items, uri, boost::is_any_of("/"));
			if (uri_items.size() > 3) { // There is a wallet parameter
				wallet = uri_items[3];

				if (uri_items.size() == 5) {
					block_number = std::atoi(uri_items[4].c_str());
					if (block_number == 0)
						folder = uri_items[4];
				} else if (uri_items.size() == 6) {
					folder = uri_items[4];
					block_number = std::atoi(uri_items[5].c_str());
				}
			}
		}
	}

        virtual ~PetalusUriBlock() {}

	std::string wallet = "";
	std::string folder = "";
	int block_number = 0;
};

} // namespace petalus

#endif  // SRC_TIMER_H_

