/*
 * Petalus a crypto micro service wallet database.
 *
 * Copyright (C) 2018-2022  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */

#include "BlockFile.h"
#include "Utils.h"
#include <iomanip>

namespace petalus::blockchain {

void BlockFile::setPublicKey(const PublicKey &pkey) { 

	ed25519_public_key_.reset();
	ec_public_key_.reset(new PublicKey(pkey));
}

void BlockFile::setPublicKey(const ED25519PublicKey &pkey) { 

	ec_public_key_.reset();
	ed25519_public_key_.reset(new ED25519PublicKey(pkey));
}

BlockFile::BlockFile(const BlockFile &block):BlockFile(block.getMaxFileSize()) {

        // Get the last block
        if (const petalus_block_file_v1 *last_block = block.findBlock(block.getTotalBlocks()); last_block) 
                compute_block_hash(*last_block, initial_hash_);
}

void BlockFile::create_header_file() {

	hdr_.magic = petalus_header_file::magic_token;
	hdr_.version = 1;
	hdr_.blocks = 0;
	hdr_.last_block_size = 0;

	update_header_file();

	total_bytes_ += sizeof(hdr_);
}

void BlockFile::update_header_file() {

	std::memcpy(mfile_.data(), &hdr_, sizeof(hdr_));
}

// For synchronize the master file
void BlockFile::sync() {

	msync(mfile_.data(), total_bytes_, MS_ASYNC);
}


bool BlockFile::is_lock() const {

#if defined(__LINUX__)
	if (lockf(fd_, F_TEST, O_RDWR) == 0)
		return false;
	else
		return true;
#endif

}

bool BlockFile::lock(const char *filename) {

#if defined(__LINUX__)
        // Lock the file if has been opend for readwrite
	fd_ = ::open(filename,  O_RDWR);
	if (lockf(fd_, F_TLOCK, total_bytes_) != 0) {
		file_lock_ = true;
		close();
		return false;
	}
#endif
	return true;
}

void BlockFile::unlock() {

#if defined(__LINUX__)
	if (fd_ > 0) {
		::close(fd_);
		lockf(fd_, F_ULOCK, 0);
		// Dont update file_lock_;
	}
#endif

}

void BlockFile::close() {

	if (mfile_.is_open()) {
		auto flags = mfile_.flags();
		mfile_.close();
		if (flags == boost::iostreams::mapped_file_base::mapmode::readwrite) {
			std::error_code ec;
			// Truncate the file to the exact size of it
			std::filesystem::resize_file(path_, total_bytes_, ec);
		}
		total_bytes_ = 0;
		access_password_ = "";
	}
	unlock();
}

void BlockFile::open(const std::filesystem::path &path, const boost::iostreams::mapped_file_base::mapmode flags) {

	boost::iostreams::mapped_file_params params;
        params.path = path.string();
        params.offset = 0;
	params.flags = flags;

	if (std::filesystem::exists(path)) {
		total_bytes_ = std::filesystem::file_size(path); // Take the original size of the file

		// Just lock the file if we want to read on it
		if (flags == boost::iostreams::mapped_file_base::mapmode::readwrite)
			if (lock(path.string().c_str())) { // Lock the file
				std::error_code ec;
				// Truncate to the maximum possible
				std::filesystem::resize_file(path, max_size_per_file_, ec);
				params.length = max_size_per_file_;
			}

        	mfile_.open(params);
		if (mfile_.is_open()) {
			// Copy the header to the variable 
			std::memcpy(&hdr_, mfile_.const_data(), sizeof(petalus_header_file));

			if (hdr_.magic == petalus_header_file::magic_token) {
				if (!populate_blocks_index())
					close();
			} else {
				++ total_header_corruptions_;
				close();
			}
		} else {
			std::cout << "Can not open the file\n";
		}
	} else {
        	params.new_file_size = max_size_per_file_;
        	params.length = max_size_per_file_;
        	mfile_.open(params);
		if (mfile_.is_open())
			if (lock(path.string().c_str()))
				create_header_file();
	}
        path_ = path;
}

void BlockFile::setBlockHashType(BlockHashTypes type) {

	if (type < BlockHashTypes::MAX_BLOCK_HASH_TYPES)
        	hash_func_type_ = type; 
}

void BlockFile::setSeed(uint8_t digest[]) {

	std::memcpy(initial_hash_, digest, HASH_BLOCK_SIZE);
}

void BlockFile::getSeed(uint8_t digest[]) const {

	if (const petalus_block_file_v1 *block = find_block(1); block)
		std::memcpy(digest, block->prev_hash, HASH_BLOCK_SIZE);
}

int32_t BlockFile::write(PetalusBlock &pblock) {

	if ((pblock.length() > 0)and(mfile_.is_open())) {
		bool valid_signature = false;

		// There is configured a public key and the block have a signature
		// TODO missing a big signature check
		if ((havePublicKey())and(pblock.signature_length() > 0)) {
        		uint8_t digest[HASH_BLOCK_SIZE];

			// Compute the sha256 of the given data
        		computeSHA256(digest, (const uint8_t*)pblock.data(), pblock.length());
#ifdef MYDEBUG
			std::cout << "BEGIN DATA\n";
			showData(std::cout, (const uint8_t*)pblock.data(), pblock.length());
      			std::cout << "BEGIN SHA256\n"; 
			showData(std::cout, digest, HASH_BLOCK_SIZE);
      			std::cout << "END SHA256\n"; 
      			std::cout << "BEGIN SIGNATURE\n"; 
			showData(std::cout, (uint8_t*)pblock.signature(), pblock.signature_length());
      			std::cout << "END SIGNATURE\n"; 
#endif 
			if (isECPublicKey()) {
				Verifier verifier(getECPublicKey());

        			valid_signature = verifier.VerifyMessage((const uint8_t*)digest, HASH_BLOCK_SIZE, 
					(const uint8_t*)pblock.signature(), pblock.signature_length());
			} else {
				EDVerifier verifier(getED25519PublicKey().GetPublicElement());

        			valid_signature = verifier.VerifyMessage((const uint8_t*)digest, HASH_BLOCK_SIZE, 
					(const uint8_t*)pblock.signature(), pblock.signature_length());
			}

			if (!valid_signature) {
#ifdef MYDEBUG
				std::cout << "SIGNATURE FAILS\n";
#endif
				// Verification error
				write_error_code_ = BlockFileWriteCode::BLOCKFILE_WRITE_SIGN_FAIL;
				return -1;
			}
		}
		
		if (!ekey_.key().empty()) // There is a password key so data should be encrypted
			pblock.encrypt(ekey_);

		if (valid_signature)  // Have to generate a block with the data and the signature that is correct 
			pblock.prepend(boost::string_ref((char*)pblock.signature(), pblock.signature_length()));
	
		// Update the timestamp of the block file
		pblock.timestamp(std::time(nullptr));
	
		return write_block_on_file(pblock);
	}
	write_error_code_ = BlockFileWriteCode::BLOCKFILE_WRITE_FILE_NOT_OPEN;
	return -1;
}

/* This is the mother of all the functions */
int32_t BlockFile::write_block_on_file(PetalusBlock &pblock) {

	// Verify if there is space for the new block
	if (haveSpaceOnFile(pblock.length())) {
		char *header_block_data = &mfile_.data()[total_bytes_];
		petalus_block_file_v1 block;

		block.length = (int32_t)pblock.length();
		block.type = (uint8_t)pblock.type();
		block.hash_type = (uint8_t)hash_func_type_;
		block.signature_length = pblock.signature_length();
		block.timestamp = pblock.timestamp();

		const uint8_t *data = (const uint8_t*)pblock.data();
#ifdef DEBUG
		std::cout << __FILE__ << ":" << __func__ << ":bytes:" << pblock.length() << std::endl;
#endif
		if (const petalus_block_file_v1 *prev_block = find_block(hdr_.blocks); prev_block) {
			const char *ptr = reinterpret_cast<const char*>(prev_block) + sizeof(uint32_t) + sizeof(int32_t);
                	int16_t length = sizeof(time_t) + sizeof(uint8_t) + sizeof(uint8_t) + (2 * HASH_BLOCK_SIZE);

                	// Compute the hash of the previous block
			hash_handlers[prev_block->hash_type].func((uint8_t*)&block.prev_hash, (const uint8_t*)ptr , length);
        	} else { // Is the first block
        		std::memcpy(block.prev_hash, initial_hash_, sizeof(block.prev_hash));
        	}

		// compute the hash of data 
		hash_handlers[block.hash_type].func((uint8_t*)&block.hash, data, block.length);

		/* Copy into memory the header of the block */
		std::memcpy(header_block_data, &block, sizeof(petalus_block_file_v1));

		/* Copy into memory the data of the block */
		std::memcpy(header_block_data + sizeof(petalus_block_file_v1), data, block.length);

		// Update the copied bytes
		total_data_bytes_ += block.length;
		if (pblock.isUserBlock()) total_user_data_bytes_ += block.length;
		total_bytes_ += block.length + sizeof(petalus_block_file_v1);
		++ hdr_.blocks;
		hdr_.last_block_size = block.length + sizeof(petalus_block_file_v1);
		block_index_[hdr_.blocks] = reinterpret_cast<petalus_block_file_v1*>(header_block_data);

		/* Update the header file, so the block that has been added is in there */
        	update_header_file();
		write_error_code_ = BlockFileWriteCode::BLOCKFILE_WRITE_SUCCESS;
		return block.length;
	}
	write_error_code_ = BlockFileWriteCode::BLOCKFILE_WRITE_NO_SPACE;
	return -1;
}

bool BlockFile::haveSpaceOnFile(int32_t bytes) {

	if (bytes + (int32_t)sizeof(petalus_block_file_v1) + total_bytes_ <= max_size_per_file_)
		return true;
	else
		return false;
}

void BlockFile::show_block(std::basic_ostream<char> &out, const petalus_block_file_v1 *block, int32_t number) const {

	out << "Block number:" << number << *block;
	out << "\t" << "Data dump\n";

	const char *ptr = reinterpret_cast<const char*>(block->data);
	int len = block->length;

	if (block->signature_length > 0) { // There is a signature on it
		ptr = reinterpret_cast<const char*>(block->data + block->signature_length);
		len = block->length - block->signature_length;
	
		showData(out, (const uint8_t*)block->data, block->signature_length);
	}

	PetalusBlock pblock(std::string(ptr, len), (BlockType)block->type);

        if (!ekey_.key().empty())
        	pblock.decrypt(ekey_);

	showData(out, (const uint8_t*)pblock.data(), pblock.length());
}

void BlockFile::show_block(nlohmann::json &out, const petalus_block_file_v1 *block, int32_t number) const {

	out["block"] = number;

	const char *ptr = reinterpret_cast<const char*>(block->data);
	int len = block->length;

	if (block->signature_length > 0) {
		ptr = reinterpret_cast<const char*>(block->data + block->signature_length);
		len = block->length - block->signature_length;
	
		for (int i = 0; i < len ; ++i) 
			out["signature"].push_back(ptr[i]);
	}

	PetalusBlock pblock(std::string(ptr, len), (BlockType)block->type);

        if (!ekey_.key().empty())
        	pblock.decrypt(ekey_);

	out["type"] = (int)pblock.type();	
	ptr = pblock.data();	
	for (int i = 0; i < (int)pblock.length() ; ++i) 
		out["data"].push_back(ptr[i]);
}

// Populate the blocks that are present on the file and do some checks on
// some specific blocks
bool BlockFile::populate_blocks_index() {

	bool value = false;

	if (mfile_.is_open()) {
		int32_t read_bytes = sizeof(petalus_header_file);
		const char *offset = &mfile_.const_data()[read_bytes];
		const petalus_block_file_v1 *block = nullptr;
		int32_t previous_block_size = 0;

		for (int i = 0; i < hdr_.blocks; ++ i) {
			offset += previous_block_size;
			block = reinterpret_cast<const petalus_block_file_v1*>(offset);

			if (block->magic != petalus_block_file_v1::magic_token) {
				++ total_block_corruptions_;
				break;
			}
			if (read_bytes + block->length + (int32_t)sizeof(petalus_block_file_v1) > total_bytes_) {
				++ total_length_corruptions_;
				break;
			}
			if (block->hash_type >= static_cast<uint8_t>(BlockHashTypes::MAX_BLOCK_HASH_TYPES)) {
				++ total_block_corruptions_;
				break;
			}

			block_index_[i + 1] = block;	

			// Check if there is a block with file access information
			// for example the user password for access the wallet or a ECDSA public key	
			if (block->type == static_cast<const uint8_t>(BlockType::SYS_JSON_BLOCKCHAIN_ACCESS)) {
				std::string data(reinterpret_cast<const char*>(block->data), block->length);				
				auto j = nlohmann::json::parse(data);

				if (j.find("salt") != j.end()) {
					if (j["salt"].is_string()) {
						salt_ = j["salt"];
						if (j.find("password_sha256") != j.end()) {
							if (j["password_sha256"].is_string()) {
								access_password_ = j["password_sha256"];
							}
						} else if (j.find("password_md5") != j.end()) {
							if (j["password_md5"].is_string()) {
								access_password_ = j["password_md5"];
							}
						}
					}
				}
				// The user can setup a public key in base64 
				if (j.find("ec_public_key") != j.end()) {
					if (j["ec_public_key"].is_string()) {
						// Convert the base64 to a binary string
						std::string decoded;
						std::string pubkey = j["ec_public_key"];

                        			CryptoPP::StringSource(pubkey, true, new CryptoPP::Base64Decoder(new CryptoPP::StringSink(decoded)));
#ifdef DEBUG	
                        			std::cout << "BEGIN PUBLIC KEY\n";
                        			showData(std::cout, (uint8_t*)decoded.c_str(), decoded.length());
                        			std::cout << "END PUBLIC KEY\n";
#endif
						CryptoPP::StringSource ss(decoded, true);

						ec_public_key_.reset(new PublicKey());
						ec_public_key_->Load(ss); // Load the public key in memory
					}
				} else if (j.find("ed25519_public_key") != j.end()) {
					if (j["ed25519_public_key"].is_string()) {
						// Convert the base64 to a binary string
                                                std::string decoded;
                                                std::string pubkey = j["ed25519_public_key"];

                                                CryptoPP::StringSource(pubkey, true, new CryptoPP::Base64Decoder(new CryptoPP::StringSink(decoded)));
#ifdef DEBUG
                                                std::cout << "BEGIN PUBLIC KEY\n";
                                                showData(std::cout, (uint8_t*)decoded.c_str(), decoded.length());
                                                std::cout << "END PUBLIC KEY\n";
#endif
                                                CryptoPP::StringSource ss(decoded, true);

                                                ed25519_public_key_.reset(new ED25519PublicKey());
                                                ed25519_public_key_->Load(ss); // Load the public key in memory
					}
				}
                	} else if (block->type == static_cast<const uint8_t>(BlockType::SYS_FOLDER_DEFINITION)) {
                        	PetalusBlock pblock;
                        	const char *ptr = reinterpret_cast<const char*>(block->data);
                        	pblock.type((BlockType)block->type);
                        	pblock.data(std::string(ptr, block->length));

                        	nlohmann::json j = nlohmann::json::parse(pblock.data());;

                        	if ((j.find("folder_name") != j.end())and(j.find("block_filename") != j.end())) {
                                	if ((j["folder_name"].is_string())and(j["block_filename"].is_string())) {
                                        	std::string folder_name = j["folder_name"];
						std::string filename = j["block_filename"];

						// Insert the folder name and the name of the file on the map
						folders_[folder_name] = std::make_pair(i + 1, filename);
					}
				}
			} else if (block->type <= static_cast<const uint8_t>(BlockType::USR_CRYPT_BINARY_FILE))
				total_user_data_bytes_ += block->length;


			// We use for hashing the blocks the last block of the file
			hash_func_type_ = static_cast<BlockHashTypes>(block->hash_type);

			previous_block_size = block->length + sizeof(petalus_block_file_v1);
			read_bytes += previous_block_size;
			total_data_bytes_ += block->length;
		}

		// Do some checks for integrity
		if ((int32_t)block_index_.size() != hdr_.blocks) {
			value = false;
			if (debug_) {
				std::cout << "Warning can not rebuild the index block\n";
				std::cout << "\tTotal block corruptions: " << total_block_corruptions_ << std::endl;
				std::cout << "\tTotal length corruptions:" << total_length_corruptions_ << std::endl;
				std::cout << "\tTotal bytes reads:" << read_bytes << std::endl;
				std::cout << "\tTotal bytes expected:" << total_bytes_ << std::endl;
				std::cout << "\tLast valid block:" << block_index_.size() << std::endl;
			}
		} else if (read_bytes < total_bytes_) {
			++ total_missing_blocks_;

			if (debug_) {
				std::cout << "Warning can not rebuild the index block\n";
				std::cout << "Read bytes:" << read_bytes << std::endl;
				std::cout << "Bytes on file:" << total_bytes_ << std::endl;
				std::cout << "header size:" << sizeof(petalus_header_file) << std::endl;
			}
			// Check if offset + 64 bytes are equals to zero

			if (show_missing_bytes_ > 0) {
				std::cout << "Showing previous block" << std::endl;
				show_block(std::cout, block, hdr_.blocks);
				offset += previous_block_size;
				std::cout << "Showing missing bytes" << std::endl;
				showData(std::cout, (const uint8_t*)offset, show_missing_bytes_);
				std::cout << std::dec << std::endl;;
				std::cout << "Last valid byte:" << read_bytes << std::endl;
				std::cout << "Bytes on file:" << total_bytes_ << std::endl;
			}
			value = false;
		} else {
			value = true;
		}
	}
	return value;
}


const petalus_block_file_v1 *BlockFile::find_block(int32_t number) const {

	if (mfile_.is_open()) {
                if (auto it = block_index_.find(number); it != block_index_.end()) {
			const petalus_block_file_v1 *block = it->second;
			return block;
		}
	} 
	return nullptr;
}

// Verify if the content has been signed properly
bool BlockFile::verify_signature_block(const petalus_block_file_v1 &block) const {

	bool valid = false;

	if ((block.signature_length > 0)and(havePublicKey())) {
        	uint8_t digest[HASH_BLOCK_SIZE];
                const uint8_t *ptr = block.data + block.signature_length;
                int len = block.length - block.signature_length;
		PetalusBlock pblock(boost::string_ref(reinterpret_cast<const char*>(ptr), len));

		pblock.type((BlockType)block.type);

                if (!ekey_.key().empty())
                	pblock.decrypt(ekey_);

                computeSHA256(digest, (const uint8_t*)pblock.data(), pblock.length());
#ifdef MYDEBUG
		std::cout << "BEGIN DATA len:" << pblock.length() << std::endl;
		showData(std::cout, (const uint8_t*)pblock.data(), pblock.length());
                std::cout << "BEGIN SHA256\n";
                showData(std::cout, digest, HASH_BLOCK_SIZE);
                std::cout << "END SHA256\n";
                std::cout << "BEGIN SIGNATURE\n";
                showData(std::cout, (uint8_t*)block.data, block.signature_length);
                std::cout << "END SIGNATURE\n";
#endif

		if (isECPublicKey()) {
                	Verifier verifier(getECPublicKey());

                	valid = verifier.VerifyMessage((const uint8_t*)digest, HASH_BLOCK_SIZE,
                		(const uint8_t*)block.data, block.signature_length);
		} else if (isED25519PublicKey()) {
			EDVerifier verifier(getED25519PublicKey().GetPublicElement());

        		valid = verifier.VerifyMessage((const uint8_t*)digest, HASH_BLOCK_SIZE, 
				(const uint8_t*)block.data, block.signature_length);
		}
	}
	return valid;
}

// Verify the integrity of the block
bool BlockFile::verify_block(const petalus_block_file_v1 &block) const {

	bool valid = false;
	uint8_t hash[HASH_BLOCK_SIZE];

        hash_handlers[block.hash_type].func((uint8_t*)hash, (const uint8_t*)block.data, block.length);

        if (std::memcmp(hash, block.hash, HASH_BLOCK_SIZE) == 0)
		valid = true;
	
	return valid;
}

bool BlockFile::verifyBlock(int32_t number, uint8_t digest[]) const {

	bool value = false;

	if (const petalus_block_file_v1 *block = find_block(number); block)
		if (std::memcmp(digest, block->prev_hash, HASH_BLOCK_SIZE) == 0)
			// The chain has not change, verify the block is correct
			value = verify_block(*block);

	return value;
} 

void BlockFile::compute_block_hash(const petalus_block_file_v1 &block, uint8_t digest[]) const {

	const char *ptr = reinterpret_cast<const char*>(&block) + sizeof(uint32_t) + sizeof(int32_t);
        int16_t length = sizeof(time_t) + sizeof(uint8_t) + sizeof(uint8_t) + (2 * HASH_BLOCK_SIZE);

        // Compute the hash of the block
	hash_handlers[block.hash_type].func((uint8_t*)digest, (const uint8_t*)ptr, length);
}

bool BlockFile::verifySignatureBlock(int32_t number) const {

	if (const petalus_block_file_v1 *block = find_block(number); block)
		return verify_signature_block(*block);

	return false;
}


bool BlockFile::verifyBlock(int32_t number) const {

	// Compute the hash of the previous block
	if (const petalus_block_file_v1 *prev_block = find_block(number - 1); prev_block) {
		uint8_t digest[HASH_BLOCK_SIZE];
	
		compute_block_hash(*prev_block, digest);		

		return verifyBlock(number, digest);
	}
	return false;
}

/* Verify that the received BlockFile is the one that belongs to the current BlockFile object */
bool BlockFile::verify(const BlockFile &bf) const {

	bool valid = false;
	uint8_t digest[HASH_BLOCK_SIZE] = { 0xFF };

	// Get the last block
	if (const petalus_block_file_v1 *last_block = bf.findBlock(bf.getTotalBlocks()); last_block) {

		compute_block_hash(*last_block, digest);
#ifdef DEBUG
		std::string hash;
        	CryptoPP::HexEncoder encoder;
        	encoder.Attach( new CryptoPP::StringSink(hash) );
        	encoder.Put( digest, sizeof(digest) );
        	encoder.MessageEnd();

		std::cout << __FILE__ << ":" << __func__ << ":block:" << bf.getTotalBlocks();
		std::cout << " hash:" << hash << std::endl;
#endif
		if (const petalus_block_file_v1 *block = find_block(1); block) {
			if (std::memcmp(digest, block->prev_hash, HASH_BLOCK_SIZE) == 0) {
				// Verify that the hash of the file is correct
                        	uint8_t hash[HASH_BLOCK_SIZE];

				// TODO shoudl be use verify_block
				hash_handlers[block->hash_type].func((uint8_t*)hash, (const uint8_t*)last_block->data, last_block->length);

                        	if (std::memcmp(hash, last_block->hash, HASH_BLOCK_SIZE) == 0) 
                                	valid = true;
			}
		}
	}
	return valid;
}

int32_t BlockFile::read(int32_t number, PetalusBlock &pblock) const {

	pblock.data(std::string("")); // reset the previous value

#ifdef DEBUG
	std::cout << __FILE__ << ":" << __func__ << ":block:" << number << std::endl;
#endif
 
        if (number <= hdr_.blocks) {
                if (const petalus_block_file_v1 *block = find_block(number); block) {
			const char *ptr = reinterpret_cast<const char*>(block->data);

			pblock.type((BlockType)block->type);
			pblock.timestamp(block->timestamp);

			if (block->signature_length > 0) {  // The block got the signed signature on it, that is the first 64 bytes
				pblock.data(boost::string_ref(ptr + (int)block->signature_length, block->length - (int)block->signature_length));
				pblock.signature(boost::string_ref(ptr, (int)block->signature_length)); // copy also the signature
			} else 
				pblock.data(boost::string_ref(ptr, block->length));

			if (!ekey_.key().empty()) 
				pblock.decrypt(ekey_);
	
			return pblock.length();
		}
	}
	return 0;
}

void BlockFile::readBlockHash(int32_t number, uint8_t digest[]) const {

	if (number <= hdr_.blocks) {
		if (const petalus_block_file_v1 *block = find_block(number); block)
			compute_block_hash(*block, digest);
	}
}

// Compute the SHA256 of the data of a given block, this is use for replication of the blocks
void BlockFile::readBlockDataHash(int32_t number, uint8_t digest[]) const {

	PetalusBlock pblock;

	if (read(number, pblock) > 0) 
		computeSHA256(digest, (const uint8_t*)pblock.data(), pblock.length());
}

SharedPointer<BlockFile> BlockFile::getFolder(const std::string &name) const {

	// Find The folder
	if (auto it = folders_.find(name); it != folders_.end()) {
		std::filesystem::path path(path_);
		std::string filename = ((*it).second).second;
		path.remove_filename();
		path /= filename;

		// TODO missing multiple files on folders
		boost::uintmax_t filesize = 0;
		if (std::filesystem::exists(path)) {
			std::error_code ec;

			filesize = std::filesystem::file_size(path, ec);
			if (ec) {
				std::cout << "ERROR on path:" << path << " ec:" << ec << std::endl;
				return nullptr;
			}
		}

		int32_t new_alloc_size = filesize;
		if (max_size_per_file_ == 0) 
			new_alloc_size += (BlockFile::BufferBlockSize * boost::iostreams::mapped_file::alignment());
		else 
			if (max_size_per_file_ >= (int32_t)filesize)
				new_alloc_size = max_size_per_file_;

		if (auto bfile = SharedPointer<BlockFile>(new BlockFile(new_alloc_size)); bfile) {
			bfile->open(path);
			bfile->setBlockHashType(hash_func_type_);
			bfile->setEncryptionKey(ekey_);
			
			// Propagate the public key to the folders
			if (havePublicKey()) {
				if (isECPublicKey())
					bfile->setPublicKey(getECPublicKey());
				else
					bfile->setPublicKey(getED25519PublicKey());
			}
			return bfile;
		}
	}
	return nullptr;
}

#if defined(PYTHON_BINDING)
std::string BlockFile::pyread(int32_t number) {

	PetalusBlock block;

	read(number, block);
	return block.data();
}

int32_t BlockFile::pywrite(const std::string &data) {

	PetalusBlock block(data);

	return write(block);
}

#endif

void BlockFile::showBlock(std::basic_ostream<char> &out, int32_t number) const {

        if (const petalus_block_file_v1 *block = findBlock(number); block)
                show_block(out, block, number);
}

void BlockFile::showBlock(nlohmann::json &out, int32_t number) const {

        if (const petalus_block_file_v1 *block = findBlock(number); block)
                show_block(out, block, number);
}

void BlockFile::showBlocks(std::basic_ostream<char> &out) const {

        if (mfile_.is_open()) {
                char *offset = &mfile_.data()[sizeof(petalus_header_file)];

                int32_t previous_block_size = 0;
                for (int i = 0; i < hdr_.blocks; ++i) {
                        offset += previous_block_size;
                        petalus_block_file_v1 *block = reinterpret_cast<petalus_block_file_v1*>(offset);
			// TODO verify that the block is fine
                        show_block(out, block, i + 1);
                        previous_block_size = block->length + sizeof(petalus_block_file_v1);
                }
        }
}

#if defined(STAND_ALONE_TEST) || defined(TESTING)

void BlockFile::writeOffset(int32_t offset, const uint8_t *data, int length) {

	if (offset <= total_bytes_) {
                char *offset_file = &mfile_.data()[offset];
		// std::cout << __FILE__ << ":" << __func__ << ":corrupting offset:" << offset << std::endl;
		std::memcpy(offset_file, data, length);
	}
}

#endif

std::ostream& operator<< (std::ostream &out, const BlockFile &bf) {

	out << "Filename:" << bf.path_.filename() << "\n";
        out << "\t" << "Version:                " << std::setw(10) << bf.hdr_.version << "\n";
	out << "\t" << "HashType:";
	if (bf.hash_func_type_ < BlockHashTypes::MAX_BLOCK_HASH_TYPES) 
		out << hash_handlers[static_cast<uint8_t>(bf.hash_func_type_)].name << "\n";
	else
		out << "WARNING: unknown\n";

        out << "\t" << "Total blocks:           " << std::setw(10) << bf.hdr_.blocks << "\n";
        out << "\t" << "Last block size:        " << std::setw(10) << bf.hdr_.last_block_size << "\n";
	out << "\t" << "Total header corruptions:" << std::setw(9) << bf.total_header_corruptions_ << "\n";
	out << "\t" << "Total block corruptions:" << std::setw(10) << bf.total_block_corruptions_ << "\n";
	out << "\t" << "Total length corruptions:" << std::setw(9) << bf.total_length_corruptions_ << "\n";
        out << "\t" << "Access password:" << bf.access_password_ << "\n";
	out << "\t" << "Public key:" << (bf.havePublicKey() ? "Yes":"No" ) << "\n";
	out << "\t" << "Folders:";
	for (auto &it: bf.folders_) out << it.first << " ";
	out << "\n";
	if (bf.total_missing_blocks_ > 0)
        	out << "WARNING the current file may content missing blocks\n";

	return out;
}

void BlockFile::statistics(std::basic_ostream<char> &out) const {

        out << *this;
}

// Read all the blocks of the bfile and rewrite on the current BlockFile
void BlockFile::clone(const BlockFile &bfile) {

	for (int32_t i = 1; i <= bfile.getTotalBlocks(); ++i) {
		PetalusBlock pblock;
		int32_t read_bytes;

		if ((read_bytes = bfile.read(i, pblock)) > 0) {

			if (!ekey_.key().empty()) // There is a password key so data should be encrypted with the new key
                        	pblock.encrypt(ekey_);

			if (pblock.have_signature()) 
                         	pblock.prepend(boost::string_ref((char*)pblock.signature(), pblock.signature_length()));

                	if (int32_t write_bytes = write_block_on_file(pblock); write_bytes == 0) {
				std::cout << "Error cloining write_bytes:" << write_bytes << " read_bytes:" << read_bytes << std::endl;
			}
		}
	}
}

} // namespace petalus::blockchain

