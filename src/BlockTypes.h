/*
 * Petalus a crypto micro service wallet database.
 *
 * Copyright (C) 2018-2022  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */

#ifndef SRC_BLOCKCHAIN_BLOCKTYPES_H_
#define SRC_BLOCKCHAIN_BLOCKTYPES_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>
#include <iomanip> // setw
#include <cstring>

namespace petalus::blockchain { 

// Supported types of blocks
enum class BlockType : uint8_t {
	USR_PLAIN = 			0x00,
	USR_CRYPT_PLAIN = 		0x01,
	USR_JSON_STRING = 		0x02,
	USR_CRYPT_JSON_STRING = 	0x03,
	USR_BINARY_FILE = 		0x04,
	USR_CRYPT_BINARY_FILE = 	0x05,
	USR_SOURCE_CRYPT = 		0x06, // The user encrypt the block from the source
	BLOCKCHAIN_FILE = 		0x08,
	SYS_FOLDER_DEFINITION =		0xE0,
	SYS_FOLDER_NAME =		0xE1,
	SYS_JSON_BLOCKCHAIN_ACCESS = 	0xE2, // application/access-block
	SYS_JSON_MASTER_ENTRY = 	0xE3 // For the master block file
};

} // namespace petalus::blockchain 

#endif  // SRC_BLOCKCHAIN_BLOCKTYPES_H_
