/*
 * Petalus a crypto micro service wallet database.
 *
 * Copyright (C) 2018-2022  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */

#ifndef SRC_HTTPUSERSESSION_H_
#define SRC_HTTPUSERSESSION_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <functional>
#include <iostream>
#include <memory>
#include <set>
#include "Pointer.h"
#include "SessionWalletManager.h"
#include "json.hpp"
#include "HTTPStatistics.h"
#include "HTTPSession.h"
#include "PetalusUriBlock.h"
#include "Logger.h"

namespace ip = boost::asio::ip;         // from <boost/asio.hpp>
using tcp = boost::asio::ip::tcp;       // from <boost/asio.hpp>
namespace bhttp = boost::beast::http;    // from <boost/beast/http.hpp>

using namespace petalus::blockchain;

namespace petalus::http {

class HTTPUserSession : public HTTPSession
{
public:
    	explicit HTTPUserSession(tcp::socket socket, boost::asio::ssl::context& ctx):
		HTTPSession(std::move(socket), ctx) {
		super_::setProcessHandler(std::bind(&HTTPUserSession::process_request, this, std::placeholders::_1));
	}

    	explicit HTTPUserSession(tcp::socket socket):
		HTTPSession(std::move(socket)) {
		super_::setProcessHandler(std::bind(&HTTPUserSession::process_request, this, std::placeholders::_1));
	}

	virtual ~HTTPUserSession() { }

	void enableProxySupport(bool value) { proxy_support_ = value; }
	bool haveProxySupport() const { return proxy_support_; }

	const char *getIP() const { return ipsrc.c_str(); }

#if defined(PYTHON_BINDING)

	// The connection has been closed from the security callback
	void close_connection(); 

	void addSecurityCallback(SharedPointer<Callback> sec) { security_callback_ = sec; }

	int32_t getHTTPErrors() const { return stats.total_http_errors; }
	int32_t getHTTPMethodGets() const { return stats.total_http_method_gets; }
	int32_t getHTTPMethodPosts() const { return stats.total_http_method_posts; }
	int32_t getHTTPMethodUnknowns() const { return stats.total_http_method_unknown; }
	int32_t getHTTPSessionsNotFound() const { return stats.total_sessions_not_found; }
	int32_t getHTTPWrongWalletUris() const { return stats.total_wrong_wallet_uri; }
	int32_t getHTTPBodyLimitExceeds() const { return stats.total_http_body_limit_exceeds; }

	const char *getIPAddress() const { return ipsrc.c_str(); } 
#endif

private:
	typedef HTTPSession super_;

    	// Asynchronously receive a complete request message.
    	void process_request(bhttp::request<bhttp::dynamic_body>&& req);
	void handle_get_message(bhttp::response<bhttp::dynamic_body> &response);
	void handle_post_message(bhttp::response<bhttp::dynamic_body> &response);
	void handle_user_open_wallet(bhttp::response<bhttp::dynamic_body> &response);
	void handle_user_close_wallet(bhttp::response<bhttp::dynamic_body> &response, const std::string &session);
	void handle_user_write_wallet(bhttp::response<bhttp::dynamic_body> &response, const SessionContext &ctx);

	bool tls_support_ = false;
	bool proxy_support_ = false;

#if defined(PYTHON_BINDING)
	SharedPointer<Callback> security_callback_ {};
#endif
};

} // namespace petalus

#endif // SRC_HTTPCONNECTION_H_
