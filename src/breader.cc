/*
 * Petalus a crypto micro service wallet database.
 *
 * Copyright (C) 2018-2022  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <unistd.h>
#include <fstream>
#include <iostream>
#include <functional>
#include <cctype>
#include <csignal>
#include <boost/program_options.hpp>
#include <fstream>
#include <boost/date_time/posix_time/posix_time_io.hpp>
#include <termios.h>
#include "PetalusBlock.h"
#include "BlockTypes.h"
#include "BlockFile.h"
#include "Message.h"

#define BINARY_NAME "breader"

using namespace petalus;
using namespace petalus::blockchain;

int32_t block_number = 0;
std::string option_input;
std::string prev_file;
std::string password_type;
int32_t prev_block_file = 0;
bool option_set_password = false;
bool option_verify_file = false;
bool option_set_debug = false;
int32_t show_missing_bytes = 0;

void show_package_banner() {

	std::cout << "\tGCC version:" << __GNUG__ << "." << __GNUC_MINOR__ << "." << __GNUC_PATCHLEVEL__ << std::endl;
	std::cout << "\tBoost version:" << BOOST_VERSION / 100000 << "." << BOOST_VERSION / 100 % 1000 << std::endl;
        std::cout << std::endl;
}

int main(int argc, char* argv[]) {

	namespace po = boost::program_options;
	po::variables_map var_map;

	po::options_description mandatory_ops("Mandatory arguments");
	mandatory_ops.add_options()
		("input,i",   po::value<std::string>(&option_input),
			"Block file or directory with block files.")
        	;

	po::options_description optional_ops("Optional arguments");
        optional_ops.add_options()
                ("verify,V",    "Verifies the integrity of the given file.")
                ("block,b",    	po::value<int32_t>(&block_number),
				"extract the block number to stdout")
                ("prev,p",    	po::value<std::string>(&prev_file),
				"Previous block file for verification.")
                ("pblock,l",    po::value<int32_t>(&prev_block_file),
				"Block number of the previous block file verification.")
                ("password,P", 	"Sets the encryption password of the block file.")
                ("password-type,t",    po::value<std::string>(&password_type)->default_value("aes"),
                        "Sets the password type for the encryption key (aes, blowfish).")
                ("force,f",     po::value<int32_t>(&show_missing_bytes),
				"Forces to show missing bytes if exists.")
                ("debug,d", 	"Debug mode on.")
                ;

	mandatory_ops.add(optional_ops);

	try {
	
        	po::store(po::parse_command_line(argc, argv, mandatory_ops), var_map);

        	if (var_map.count("help")) {
            		std::cout << PACKAGE " " BINARY_NAME " " VERSION << std::endl;
            		std::cout << mandatory_ops << std::endl;
			std::cout << std::endl;
			//show_gpl_banner();
            		exit(0);
        	}
        	if (var_map.count("version")) {
            		std::cout << PACKAGE " " BINARY_NAME " " VERSION << std::endl;
			std::cout << std::endl;
			// show_gpl_banner();
			std::cout << std::endl;
			show_package_banner();
            		exit(0);
        	}
		if (var_map.count("input") == 0) {
            		std::cout << PACKAGE " " BINARY_NAME " " VERSION << std::endl;
            		std::cout << mandatory_ops << std::endl;
			exit(-1);
		}

		if (var_map.count("verify")) option_verify_file = true;
		if (var_map.count("password")) option_set_password = true;
		if (var_map.count("debug")) option_set_debug = true;

        	po::notify(var_map);
    	
	} catch(boost::program_options::required_option& e) {
            	std::cout << PACKAGE " " BINARY_NAME " " VERSION << std::endl;
        	std::cerr << "Error: " << e.what() << std::endl;
		std::cout << mandatory_ops << std::endl;
        	exit(-1);
	} catch(std::exception& e) {
            	std::cout << PACKAGE " " BINARY_NAME " " VERSION << std::endl;
        	std::cerr << "Unsupported option." << e.what() << std::endl;
		std::cout << mandatory_ops << std::endl;
        	exit(-2);
    	}

	std::vector<std::string> inputs;
	namespace fs = std::filesystem;

	try {
		if (fs::is_directory(option_input.c_str())) {
			fs::recursive_directory_iterator it(option_input.c_str());
    			fs::recursive_directory_iterator endit;
    
			while (it != endit) {
      				if (fs::is_regular_file(*it)and((it->path().extension() == ".mblk"))) {
					std::ostringstream os;
			
					os << it->path().c_str();
      					inputs.push_back(os.str());
				} else {
					// TOit.no_push();
				}
				++it;
			}
			sort(inputs.begin(), inputs.end());
		} else {
			inputs.push_back (option_input.c_str());
		}
	} catch (std::exception const&  ex) {
		std::ostringstream msg;

		msg << "Can not read " << option_input.c_str() << " Reason:" << ex.what();
		petalus::error_message(msg.str()); 
		exit(0);
	}

        uint8_t initial_digest[CryptoPP::SHA256::DIGESTSIZE] = { 0xFF };
	SharedPointer<BlockFile> seedblock = nullptr;

	if (prev_file.length() > 0) {
		// The user pass a initial block file for validation after
		if (std::filesystem::exists(prev_file)) {
			seedblock = SharedPointer<BlockFile>(new BlockFile(0));
			seedblock->open(prev_file , boost::iostreams::mapped_file_base::mapmode::readonly);

			if (seedblock->is_open()) {
				if (prev_block_file > seedblock->getTotalBlocks())
					prev_block_file = seedblock->getTotalBlocks();

				if (prev_block_file > 0) {
					seedblock->readBlockHash(prev_block_file, initial_digest);
					CryptoPP::HexEncoder encoder;
        				std::string hash;
        				encoder.Attach( new CryptoPP::StringSink(hash) );
        				encoder.Put( initial_digest, sizeof(initial_digest) );
        				encoder.MessageEnd();
					std::ostringstream out;

					out << "Initial digest on memory with block " << prev_block_file;
					petalus::information_message(out.str());
					petalus::information_message(hash);
				}
			}
		}
	}

        EncryptionKey ekey;

	for (auto& entry: inputs) {

		if (!std::filesystem::exists(entry)) {
			petalus::information_message("File dont exist");
			break;
		}

		std::uintmax_t filesize = 0;
                std::error_code ec;

                // Take the size of the file
                filesize = std::filesystem::file_size(entry, ec);
		std::cout << " vamos" << std::endl;
		auto bfile = BlockFile(filesize); // The file will be allocated with the size of the file

		if (option_set_password) { // Ask the user the password from input
			std::cout << "Set the encryption password:" << std::endl;
			termios oldt;
    			tcgetattr(STDIN_FILENO, &oldt);
    			termios newt = oldt;
    			newt.c_lflag &= ~ECHO;
    			tcsetattr(STDIN_FILENO, TCSANOW, &newt);

    			std::string password;
    			std::getline(std::cin, password);

			tcsetattr(STDIN_FILENO, TCSANOW, &oldt);

		        EncryptionKeyType type = EncryptionKeyType::AES;
                	if (password_type.compare("blowfish") == 0)
                        	type = EncryptionKeyType::BLOWFISH;
               
			// The password is in raw mode 
        		ekey.assign((const uint8_t*)password.data(), password.length());
			ekey.type(type);

        		bfile.setEncryptionKey(ekey);
		}

		std::cout << "leches ne vinagre joe" << std::endl;
		try {
			bfile.setShowMissingBlocks(show_missing_bytes);
			bfile.setDebug(option_set_debug);
                	bfile.open(entry, boost::iostreams::mapped_file_base::mapmode::priv);

			std::cout << "leches ne vinagre" << std::endl;

			if (block_number > 0) {
				bfile.showBlock(std::cout, block_number);
			} else {	
				bfile.statistics(std::cout);
				if (option_verify_file) {
					int32_t verify_blocks = 0;
					int32_t broke_blocks = 0;

					// Verify the first block if exists the seed file
					if (!seedblock) {
						petalus::warning_message("Can not verify initial block");
						++ broke_blocks;
					} else if (!bfile.verifyBlock(1, initial_digest)) {
						petalus::warning_message("Can not verify initial block from previous block file");
						++ broke_blocks;
					} else {
						petalus::information_message("Initial block verified");
						++ verify_blocks;
					}	
					for (int i = 2; i < bfile.getTotalBlocks() + 1; ++i) {
						if (!bfile.verifyBlock(i)) {
							std::ostringstream out;

							out << "Warning: can not verify block " << i;
							petalus::warning_message(out.str());
							++broke_blocks;
						} else {
							++verify_blocks;	
						}
					}
					std::cout << "Total verified blocks:" << verify_blocks << "\n";
					std::cout << "Total broken blocks:  " << broke_blocks << "\n";	
					bfile.readBlockHash(bfile.getTotalBlocks(), initial_digest);
				} else {
					bfile.showBlocks(std::cout);	
				}
			}
		} catch (std::exception& e) {
			std::ostringstream msg;

			msg << "Error: " << e.what(); 
		}
		bfile.close();
	}

	return 0;
}

