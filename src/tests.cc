/*
 * Petalus a crypto micro service wallet database.
 *
 * Copyright (C) 2018-2022  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */

#include <sys/stat.h> 
#include <string>
#include <fstream>
#include <boost/algorithm/string.hpp>
#include <cryptopp/filters.h>
#include <cryptopp/files.h>
#include <cryptopp/base64.h>
#include <cryptopp/eax.h>
#include "CryptoOperations.h"
#include "PetalusBlock.h"
#include "BlockFile.h"
#include "SessionWalletManager.h"

#define BOOST_TEST_ALTERNATIVE_INIT_API
#define BOOST_TEST_NO_MAIN
#define BOOST_TEST_DYN_LINK
#ifdef STAND_ALONE_TEST
#define BOOST_TEST_MODULE blockchain
#endif
#include <boost/test/unit_test.hpp>
#include "test_helper.h"

using namespace boost::unit_test;
using namespace petalus::blockchain;

struct RedirectOutput {
        std::streambuf *old_cout;
        std::ostringstream cout;

        RedirectOutput():
                old_cout(std::cout.rdbuf()),
                cout() {

                std::cout.rdbuf(cout.rdbuf());
        }

        ~RedirectOutput() { std::cout.rdbuf(old_cout); }
};

BOOST_AUTO_TEST_SUITE(crypto_suite)

BOOST_AUTO_TEST_CASE (test01_sha256)
{
	// Compute the sha256 of the word Hola
        uint8_t digest[HASH_BLOCK_SIZE];
	std::string data("Hola"); // E633F4FC79BADEA1DC5DB970CF397C8248BAC47CC3ACF9915BA60B5D76B0E88F

	computeSHA256(digest, (const uint8_t*)data.c_str(), data.length());

	CryptoPP::SecByteBlock key;// { CryptoPP::AES::DEFAULT_KEYLENGTH };

	BOOST_CHECK(key.empty() == true);

        CryptoPP::HexEncoder encoder;
        std::string hash;
        encoder.Attach( new CryptoPP::StringSink(hash) );
        encoder.Put( digest, sizeof(digest) );
        encoder.MessageEnd();

	std::string expected("E633F4FC79BADEA1DC5DB970CF397C8248BAC47CC3ACF9915BA60B5D76B0E88F");
	BOOST_CHECK(expected.compare(hash) == 0);		
}

BOOST_AUTO_TEST_CASE (test02_sha256)
{
        uint8_t digest[HASH_BLOCK_SIZE];
	std::string data("Good morning");
        std::string hash;

	computeSHA256(digest, (const uint8_t*)data.c_str(), data.length());

        CryptoPP::HexEncoder encoder;
        encoder.Attach( new CryptoPP::StringSink(hash) );
        encoder.Put( digest, sizeof(digest) );
        encoder.MessageEnd();

	std::string expected("90a90a48e23dcc51ad4a821a301e3440ffeb5e986bd69d7bf347a2ba2da23bd3");
	BOOST_CHECK(boost::iequals(hash, expected) == true);
}
	
BOOST_AUTO_TEST_CASE (test03_blake2s)
{
        uint8_t digest[HASH_BLOCK_SIZE];
	std::string data("abc");
        std::string hash;

	computeBLAKE2s(digest, (const uint8_t*)data.c_str(), data.length());

        CryptoPP::HexEncoder encoder;
        encoder.Attach( new CryptoPP::StringSink(hash) );
        encoder.Put( digest, sizeof(digest) );
        encoder.MessageEnd();

	std::string expected("508C5E8C327C14E2E1A72BA34EEB452F37458B209ED63A294D999B4C86675982");
	BOOST_CHECK(boost::iequals(hash, expected) == true);
}
	
BOOST_AUTO_TEST_CASE (test04_sha3)
{
        uint8_t digest[HASH_BLOCK_SIZE];
	std::string data("test");
        std::string hash;

	computeSHA3_256(digest, (const uint8_t*)data.c_str(), data.length());

        CryptoPP::HexEncoder encoder;
        encoder.Attach( new CryptoPP::StringSink(hash) );
        encoder.Put( digest, sizeof(digest) );
        encoder.MessageEnd();

	std::string expected("36f028580bb02cc8272a9a020f4200e346e276ae664e45ee80745574e2f5ab80");
	BOOST_CHECK(boost::iequals(hash, expected) == true);
}

// Verify sign ad verify ecdsa messages, basic example of how works
BOOST_AUTO_TEST_CASE (test05_ec_sanity)
{
	std::string data("hola");
	std::string binary_signature;
        std::string public_key;

	CryptoPP::AutoSeededRandomPool prng;
	PrivateKey privateKey;
	PublicKey publicKey;

	// Supported privateKey.Initialize( prng, CryptoPP::ASN1::secp192k1() );
	// Supported privateKey.Initialize( prng, CryptoPP::ASN1::secp384r1() );
	// Supported privateKey.Initialize( prng, CryptoPP::ASN1::secp256k1() );
	// Supported privateKey.Initialize( prng, CryptoPP::ASN1::secp521r1() );
	privateKey.Initialize( prng, CryptoPP::ASN1::secp224r1() );

	// Generate the public from the private
	privateKey.MakePublicKey( publicKey );

	auto ec = publicKey.GetGroupParameters().GetCurve();

	// std::cout << ec.GetField().MaxElementByteLength() << std::endl;
	// std::cout << publicKey.GetGroupParameters().GetSubgroupOrder().BitCount() << std::endl; // 161
	// std::cout << publicKey.GetGroupParameters().GetMaxExponent();
	// publicKey.AccessGroupParameters().GetAlgorithmID().DEREncode(sss);

	Signer signer(privateKey);

        std::string data_hash;
        uint8_t digest[HASH_BLOCK_SIZE];

        computeSHA256(digest, (const uint8_t*)data.c_str(), data.length());

	// Sign the sha256 hash of data
	CryptoPP::StringSource s( digest, HASH_BLOCK_SIZE, true /*pump all*/,
    		new CryptoPP::SignerFilter( prng,
        		signer,
        		new CryptoPP::StringSink( binary_signature )
    		) // SignerFilter
	); // StringSource

	std::string signature;

        CryptoPP::HexEncoder encoder;
        encoder.Attach( new CryptoPP::StringSink(signature) );
        encoder.Put( (uint8_t*)binary_signature.c_str(), binary_signature.length() );
        encoder.MessageEnd();

	// std::cout << "from public:" << publicKey.GetGroupParameters().GetSubgroupOrder().BitCount() / 4 << std::endl; 
	// std::cout << "signature length:" << binary_signature.length() << " sig:" << signature.length() << std::endl;

	std::string pkey;
	CryptoPP::StringSink stringSink(pkey);
	publicKey.DEREncode(stringSink);


	std::string hpkey;
	encoder.Attach( new CryptoPP::StringSink(hpkey));
        encoder.Put((uint8_t*)pkey.c_str(), pkey.length());
        encoder.MessageEnd();

	{
		std::string encoded;
		CryptoPP::StringSource(binary_signature, true, new CryptoPP::Base64Encoder(new CryptoPP::StringSink(encoded)));
	}

	// Variables data and sig travelssssss
	// The public key is stored on the server
	// The generate hash of data will be generated on server and client
	std::string client_signature;
	std::string client_hash;

	CryptoPP::HexDecoder decoder;
	decoder.Attach( new CryptoPP::StringSink(client_signature));
	decoder.Put( (uint8_t*)signature.c_str(), signature.size() );
	decoder.MessageEnd();

	Verifier verifier(publicKey);

        bool result = verifier.VerifyMessage( (const uint8_t*)&digest[0], HASH_BLOCK_SIZE, (const uint8_t*)&client_signature[0], client_signature.size() );
	BOOST_CHECK(result == true);
}

// Test AES encryption and decryption
BOOST_AUTO_TEST_CASE (test06_aes_sanity)
{
        std::vector<std::string> items {
                { generateRandomString(17) },
                { generateRandomString(32) },
                { generateRandomString(64) },
                { generateRandomString(128) },
                { generateRandomString(256) },
                { generateRandomString(443) },
                { generateRandomString(503) },
                { generateRandomString(991) },
                { generateRandomString(1193) }
        };

	uint8_t key[ CryptoPP::AES::DEFAULT_KEYLENGTH ], iv[ CryptoPP::AES::BLOCKSIZE ];
    	memset( key, 0x00, CryptoPP::AES::DEFAULT_KEYLENGTH );
    	memset( iv, 0x00, CryptoPP::AES::BLOCKSIZE );

	CryptoPP::ECB_Mode<CryptoPP::AES>::Encryption enc;
	CryptoPP::ECB_Mode<CryptoPP::AES>::Decryption dec;

	std::string plain("Good morinig my friend");

	enc.SetKey(key, sizeof(key));
	dec.SetKey(key, sizeof(key));

	for (auto &item: items) {
		std::string cipher;
		std::string recovered;
		
        	CryptoPP::StringSource(item, true,
        		new CryptoPP::StreamTransformationFilter(enc,
                                new CryptoPP::StringSink(cipher)
                        ) // StreamTransformationFilter      
                );

        	CryptoPP::StringSource s(cipher, true, 
                        new CryptoPP::StreamTransformationFilter(dec,
                                new CryptoPP::StringSink(recovered)
                        ) // StreamTransformationFilter
                );		
		BOOST_CHECK(item.compare(recovered) == 0);
	}
}

// Verify we can encrypt and decrypt any type of data
BOOST_AUTO_TEST_CASE (test07)
{
	uint8_t ptr[] = "\xAA\xBB\xDD\x00\x00\x00\x00\x00\xCC\x00";
	boost::string_ref data(reinterpret_cast<const char*>(ptr), 10);
	PetalusBlock block(data);
	PetalusBlock read_block;
	std::string str(data.begin(), data.end());

	CryptoPP::SecByteBlock key { CryptoPP::AES::DEFAULT_KEYLENGTH };

        CryptoPP::OS_GenerateRandomBlock(false, key, CryptoPP::AES::DEFAULT_KEYLENGTH);

	block.encrypt(key);

	BOOST_CHECK(std::memcmp(block.data(), ptr, 10) != 0);

	block.decrypt(key);

	BOOST_CHECK(std::memcmp(block.data(), ptr, 10) == 0);
}

// Verify we can encrypt and decrypt any type of data
BOOST_AUTO_TEST_CASE (test08)
{
	uint8_t buffer[2048 + 13];
        CryptoPP::SecByteBlock key { CryptoPP::AES::DEFAULT_KEYLENGTH };

        CryptoPP::OS_GenerateRandomBlock(false, buffer, sizeof(buffer));
        CryptoPP::OS_GenerateRandomBlock(false, key, CryptoPP::AES::DEFAULT_KEYLENGTH);

	boost::string_ref data(reinterpret_cast<const char*>(buffer), sizeof(buffer));
	PetalusBlock block(data);
	PetalusBlock read_block;

	BOOST_CHECK(block.length() == sizeof(buffer));

        block.encrypt(key);

        BOOST_CHECK(std::memcmp(block.data(), buffer, sizeof(buffer)) != 0);

        block.decrypt(key);

        BOOST_CHECK(std::memcmp(block.data(), buffer, sizeof(buffer)) == 0);
}

// Verify that the blowfish encryption key decrypts correctly
BOOST_AUTO_TEST_CASE (test09)
{
	std::string password("{BLOWFISH}CDF857AC6A25AEB91C9F64AF4ACC9F4C681EEA1D");
	std::string dpass("hola");
	EncryptionKey ekey;

	BOOST_CHECK(ekey.password(password) == true);	
	BOOST_CHECK(ekey.type() == EncryptionKeyType::BLOWFISH);

	BOOST_CHECK(dpass.compare(ekey.getDecryptedPassword()) == 0);
}

// Verify sign ad verify ed25519 messages, basic example of how works
BOOST_AUTO_TEST_CASE (test10_ed25519_sanity)
{
        std::string data("This is something for sign");
        std::string signature;

        CryptoPP::AutoSeededRandomPool prng;

	EDSigner signer_init;

	signer_init.AccessPrivateKey().GenerateRandom(prng);

	const ED25519PrivateKey &private_key = dynamic_cast<const CryptoPP::ed25519PrivateKey&>(signer_init.GetPrivateKey());

	ED25519PublicKey pubKey;
	private_key.MakePublicKey(pubKey);

	// With different context
	EDVerifier verifier(pubKey.GetPublicElement());
	EDSigner signer(private_key.GetPrivateExponent());

	// Determine maximum signature size
	size_t siglen = signer.MaxSignatureLength();
	signature.resize(siglen);

	// Sign, and trim signature to actual size
	siglen = signer.SignMessage(CryptoPP::NullRNG(), (const uint8_t*)&data[0], data.size(), (uint8_t*)&signature[0]);
	signature.resize(siglen);	

	bool valid = verifier.VerifyMessage((const uint8_t*)&data[0], data.size(),
                               (const uint8_t*)&signature[0], signature.size());

	BOOST_CHECK(valid == true);
	valid = verifier.VerifyMessage((const uint8_t*)&data[0], data.size() - 1,
                               (const uint8_t*)&signature[0], signature.size());
	BOOST_CHECK(valid == false);
}

// Verify sign ad verify ed25519 messages, basic example of how works
BOOST_AUTO_TEST_CASE (test11_ed25519_sanity)
{
        std::string data("This is something for sign with another example");
        std::string signature;

        CryptoPP::AutoSeededRandomPool prng;
	ED25519PublicKey pubKey;

	EDSigner signer_init;

        signer_init.AccessPrivateKey().GenerateRandom(prng);

        const ED25519PrivateKey &private_key = dynamic_cast<const CryptoPP::ed25519PrivateKey&>(signer_init.GetPrivateKey());

	private_key.MakePublicKey(pubKey);

	// With different context
	EDVerifier verifier(pubKey.GetPublicElement());
	EDSigner signer(private_key.GetPrivateExponent());

	// Determine maximum signature size
	size_t siglen = signer.MaxSignatureLength();
	signature.resize(siglen);

	// Sign, and trim signature to actual size
	siglen = signer.SignMessage(prng, (const uint8_t*)&data[0], data.size(), (uint8_t*)&signature[0]);
	signature.resize(siglen);	

	bool valid = verifier.VerifyMessage((const uint8_t*)&data[0], data.size(),
                               (const uint8_t*)&signature[0], signature.size());

	BOOST_CHECK(valid == true);
	valid = verifier.VerifyMessage((const uint8_t*)&data[0], data.size() - 1,
                               (const uint8_t*)&signature[0], signature.size());
	BOOST_CHECK(valid == false);
}

// Verify sign and verify ed25519 messages
BOOST_AUTO_TEST_CASE (test12_ed25519_sanity)
{
        std::string data("AAAAAng for sign with another example");
        std::string signature;

	std::string pub64key = "MCowBQYDK2VwAyEA5iNKMhMRkv7J4rXLRCPw9nlCGW/7zuMlNAfvlggwt9I="; 
	std::string priv64key = "MC4CAQAwBQYDK2VwBCIEIM8d+qTCcX5VXBI9KHoZwt2DiRRZ1dcaPPBXFq0UsgPv"; 

        CryptoPP::AutoSeededRandomPool prng;
        ED25519PublicKey pubKey;
        ED25519PrivateKey private_key;

	std::string decoded_pub, decoded_priv, binary;

        CryptoPP::StringSource(pub64key, true, new CryptoPP::Base64Decoder(new CryptoPP::StringSink(decoded_pub)));
        CryptoPP::StringSource(priv64key, true, new CryptoPP::Base64Decoder(new CryptoPP::StringSink(decoded_priv)));

        CryptoPP::StringSource pub_ss(decoded_pub, true);
        CryptoPP::StringSource priv_ss(decoded_priv, true);

	private_key.Load(priv_ss);
        pubKey.Load(pub_ss);

        EDVerifier verifier(pubKey.GetPublicElement());
        EDSigner signer(private_key.GetPrivateExponent());

	bool valid = verifier.GetPublicKey().Validate(prng, 3);
	BOOST_CHECK(valid == true);

        // Determine maximum signature size
        size_t siglen = signer.MaxSignatureLength();
        signature.resize(siglen);

        // Sign, and trim signature to actual size
        siglen = signer.SignMessage(prng, (const uint8_t*)&data[0], data.size(), (uint8_t*)&signature[0]);
        signature.resize(siglen);

        valid = verifier.VerifyMessage((const uint8_t*)&data[0], data.size(),
                               (const uint8_t*)&signature[0], signature.size());

        BOOST_CHECK(valid == true);
        valid = verifier.VerifyMessage((const uint8_t*)&data[0], data.size() - 1,
                               (const uint8_t*)&signature[0], signature.size());
        BOOST_CHECK(valid == false);
}

// Another example to verify and sign with ed25519 messages
BOOST_AUTO_TEST_CASE (test13_ed25519_sanity)
{
        std::string data("AAAAAng for sign with another example");
        std::string signature;
        std::string pub64key;
        CryptoPP::AutoSeededRandomPool prng;
        ED25519PublicKey pubKey;
        EDSigner signer_init;

        signer_init.AccessPrivateKey().GenerateRandom(prng);

        const ED25519PrivateKey& private_key = dynamic_cast<const ED25519PrivateKey&>(signer_init.GetPrivateKey());

        private_key.MakePublicKey(pubKey);

        EDVerifier verifier_init(signer_init);

        std::string pkey;
        CryptoPP::StringSink ss(pkey);
        verifier_init.GetPublicKey().Save(ss); // Save the public key on a string

        // Convert to a base64 string
        CryptoPP::StringSource(pkey, true, new CryptoPP::Base64Encoder(new CryptoPP::StringSink(pub64key)));

        std::string decoded_pub, binary;

        CryptoPP::StringSource(pub64key, true, new CryptoPP::Base64Decoder(new CryptoPP::StringSink(decoded_pub)));

        CryptoPP::StringSource pub_ss(decoded_pub, true);

        EDVerifier verifier(pubKey.GetPublicElement());
        EDSigner signer(private_key.GetPrivateExponent());

        bool valid = verifier.GetPublicKey().Validate(prng, 3);
        BOOST_CHECK(valid == true);

        // Determine maximum signature size
        size_t siglen = signer.MaxSignatureLength();
        signature.resize(siglen);

        // Sign, and trim signature to actual size
        siglen = signer.SignMessage(prng, (const uint8_t*)&data[0], data.size(), (uint8_t*)&signature[0]);
        signature.resize(siglen);

        valid = verifier.VerifyMessage((const uint8_t*)&data[0], data.size(),
                               (const uint8_t*)&signature[0], signature.size());

        BOOST_CHECK(valid == true);
        valid = verifier.VerifyMessage((const uint8_t*)&data[0], data.size() - 1,
                               (const uint8_t*)&signature[0], signature.size());
        BOOST_CHECK(valid == false);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(block_file_suite)

/* Create a block with just 16 bytes and reopen it */
BOOST_AUTO_TEST_CASE (test01_sanity_14bytes) 
{
	auto bfile1 = BlockFile(sizeof(petalus_header_file));
	std::filesystem::path filename(generateBlockFileName());

	// remove the file if exists from previous runs
	if (std::filesystem::exists(filename)) std::filesystem::remove(filename);

	BOOST_CHECK(bfile1.is_lock() == false);
	bfile1.open(filename);
	BOOST_CHECK(bfile1.getTotalBlocks() == 0);
	BOOST_CHECK(bfile1.getTotalDataBytes() == 0);
	BOOST_CHECK(bfile1.getTotalUserDataBytes() == 0);
	BOOST_CHECK(bfile1.getTotalBytes() == sizeof(petalus_header_file));
	BOOST_CHECK(bfile1.getLastWrittenBlockSize() == 0);
	BOOST_CHECK(bfile1.haveSpaceOnFile(32) == false);
	// BOOST_CHECK(bfile1.is_lock() == true);

	bfile1.close();

	std::ifstream in1(filename.string(), std::ifstream::ate | std::ifstream::binary);
        BOOST_CHECK(in1.tellg() == sizeof(petalus_header_file));

	// Now open again and verify the integrity	
	auto bfile2 = BlockFile(sizeof(petalus_header_file) + sizeof(petalus_block_file_v1) + 4);

	bfile2.open(filename);
	BOOST_CHECK(bfile2.getTotalBlocks() == 0);
	BOOST_CHECK(bfile2.getTotalDataBytes() == 0);
	BOOST_CHECK(bfile1.getTotalUserDataBytes() == 0);
	BOOST_CHECK(bfile2.getTotalBytes() == sizeof(petalus_header_file));
	BOOST_CHECK(bfile2.getLastWrittenBlockSize() == 0);
	
	// Write some data on the file, the first block
	PetalusBlock block("AAAA");

	BOOST_CHECK(block.isUserBlock() == true);
	
	// check if there is space for 4 bytes
	BOOST_CHECK(bfile2.haveSpaceOnFile(block.length()) == true);
	BOOST_CHECK(bfile2.haveSpaceOnFile(5) == false);
	BOOST_CHECK(bfile2.haveSpaceOnFile(2048) == false);
	BOOST_CHECK(bfile2.write(block) == block.length());
	BOOST_CHECK(bfile2.getTotalBlocks() == 1);
	BOOST_CHECK(bfile2.getTotalUserDataBytes() == block.length());
	BOOST_CHECK(bfile2.getTotalBytes() == sizeof(petalus_header_file) + block.length() + sizeof(petalus_block_file_v1));
	BOOST_CHECK(bfile2.getTotalDataBytes() == block.length());
	BOOST_CHECK(bfile2.getLastWrittenBlockSize() == block.length() + sizeof(petalus_block_file_v1));

	uint8_t digest[HASH_BLOCK_SIZE];

	bfile2.readBlockHash(1, digest);

	BOOST_CHECK(bfile2.getTotalHeaderCorruptions() == 0);
	BOOST_CHECK(bfile2.getTotalBlockCorruptions() == 0);
	BOOST_CHECK(bfile2.getTotalLengthCorruptions() == 0);

	bfile2.close();

	// Check that we can not write on the file
	BOOST_CHECK(bfile2.write(block) == -1);
	BOOST_CHECK(bfile2.getWriteErrorCode() == BlockFileWriteCode::BLOCKFILE_WRITE_FILE_NOT_OPEN);

	// Now open the file with a regular c++ API
	std::ifstream f;
	petalus_header_file hdr;
	char buffer[sizeof(petalus_header_file)];
	f.open(filename.string(), std::ios::binary);

	f.seekg(0, std::ios::end);
	int file_size = f.tellg(); 
	f.seekg(0, std::ios::beg);

	f.read(buffer, sizeof(petalus_header_file));
	BOOST_CHECK(f.tellg() == sizeof(petalus_header_file));

	std::memcpy(&hdr, (uint8_t*)buffer, sizeof(petalus_header_file));

	BOOST_CHECK(hdr.magic == petalus_header_file::magic_token); 
	BOOST_CHECK(hdr.version == 1); 
	BOOST_CHECK(hdr.blocks == 1); 
	BOOST_CHECK(hdr.last_block_size == block.length() + sizeof(petalus_block_file_v1)); 

	// Now read the last block of the file
	int offset = file_size - hdr.last_block_size;
	f.seekg(offset, std::ios::beg);
	char databuffer[hdr.last_block_size] = { 0x00 };

	petalus_block_file_v1 rblock;

	f.read(databuffer, sizeof(databuffer));

	std::memcpy(&rblock, databuffer, sizeof(rblock));

	BOOST_CHECK(rblock.magic == petalus_block_file_v1::magic_token);

	uint8_t prev_hash[HASH_BLOCK_SIZE] = { 0x00 };
	BOOST_CHECK(std::memcmp(prev_hash, rblock.prev_hash, HASH_BLOCK_SIZE) == 0);
	BOOST_CHECK(std::memcmp(&databuffer[sizeof(rblock)], block.data(), block.length()) == 0);

	f.close();
}

/* Generate a file with 3 blocks and check if they exists */
BOOST_AUTO_TEST_CASE (test02_three_blocks) 
{
	auto bfile = BlockFile();
	std::string filename(generateBlockFileName());

	// remove the file if exists from previous runs
	if (std::filesystem::exists(filename)) std::filesystem::remove(filename);

	bfile.open(filename);
	BOOST_CHECK(bfile.getTotalBlocks() == 0);

	PetalusBlock data;
	PetalusBlock block1("AAAA");
	PetalusBlock block2("BBBBBBBB");
	PetalusBlock datastr("CCCCCCCCCCCCCCCC");
	PetalusBlock block3(datastr);

	int32_t bytes = 0;
	// Write some data
	BOOST_CHECK(bfile.write(block1) == block1.length());
	BOOST_CHECK(bfile.getTotalBlocks() == 1);
	bytes = sizeof(petalus_header_file) + block1.length() + sizeof(petalus_block_file_v1);
	BOOST_CHECK(bfile.getTotalBytes() == bytes);
	BOOST_CHECK(bfile.getTotalDataBytes() == block1.length());
	BOOST_CHECK(bfile.getTotalUserDataBytes() == block1.length());
	BOOST_CHECK(bfile.getLastWrittenBlockSize() == block1.length() + sizeof(petalus_block_file_v1));

	BOOST_CHECK(bfile.write(block2) == block2.length());
	bytes += block2.length() + sizeof(petalus_block_file_v1);
	BOOST_CHECK(bfile.getTotalBlocks() == 2);
	BOOST_CHECK(bfile.getTotalBytes() == bytes);
	BOOST_CHECK(bfile.getTotalDataBytes() == block1.length() + block2.length());
	BOOST_CHECK(bfile.getTotalUserDataBytes() == block1.length() + block2.length());
	BOOST_CHECK(bfile.getLastWrittenBlockSize() == block2.length() + sizeof(petalus_block_file_v1));

	bytes += block3.length() + sizeof(petalus_block_file_v1);
	BOOST_CHECK(bfile.write(block3) == block3.length());
	BOOST_CHECK(bfile.getTotalBlocks() == 3);
	BOOST_CHECK(bfile.getTotalBytes() == bytes);
	BOOST_CHECK(bfile.getTotalDataBytes() == block1.length() + block2.length() + block3.length());
	BOOST_CHECK(bfile.getTotalUserDataBytes() == block1.length() + block2.length() + block3.length());
	BOOST_CHECK(bfile.getLastWrittenBlockSize() == block3.length() + sizeof(petalus_block_file_v1));

	// Read the data
	bfile.read(1, data);
	BOOST_CHECK(block1 == data);
	
	bfile.read(2, data);
	BOOST_CHECK(block2 == data);

	bfile.read(3, data);
	BOOST_CHECK(block3 == data);

	BOOST_CHECK(bfile.getTotalHeaderCorruptions() == 0);
	BOOST_CHECK(bfile.getTotalBlockCorruptions() == 0);
	BOOST_CHECK(bfile.getTotalLengthCorruptions() == 0);

	// After the reads the size should be the same
	BOOST_CHECK(bfile.getTotalBytes() == bytes);
	BOOST_CHECK(bfile.getTotalDataBytes() == block1.length() + block2.length() + block3.length());
	BOOST_CHECK(bfile.getTotalUserDataBytes() == block1.length() + block2.length() + block3.length());
	bfile.close();
}

/* Generate a file with X blocks and check if they exists */
BOOST_AUTO_TEST_CASE (test03_X_blocks)
{
        std::vector<PetalusBlock> items {
                { "This is some data to write" },
                { "more data" },
                { "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" },
                { "XYZXXXXXXXXXXXX" },
                { "buuuuuu,aaaaaa,bbbbb,cccccc" },
                { "X" },
                { "Y" }
        };

        auto bfile1 = BlockFile();
        std::string filename(generateBlockFileName());

	// remove the file if exists from previous runs
	if (std::filesystem::exists(filename)) std::filesystem::remove(filename);

	int32_t last_block_size = 0;
	int32_t bytes = 0;
	int32_t data_bytes = 0;
        bfile1.open(filename);

	for (auto &item: items) {
		BOOST_CHECK(bfile1.write(item) == item.length());
		data_bytes += item.length();
		last_block_size = item.length() + sizeof(petalus_block_file_v1);
		bytes += last_block_size;
	}

	BOOST_CHECK(bfile1.getTotalBlocks() == items.size());
	BOOST_CHECK(bfile1.getTotalBytes() == bytes + sizeof(petalus_header_file));
	BOOST_CHECK(bfile1.getTotalDataBytes() == data_bytes);
	BOOST_CHECK(bfile1.getTotalUserDataBytes() == data_bytes);
	BOOST_CHECK(bfile1.getLastWrittenBlockSize() == last_block_size);
	bfile1.close();

	// Reopen the file and check the information
	auto bfile2 = BlockFile();
	bfile2.open(filename);

	BOOST_CHECK(bfile2.getTotalBlocks() == items.size());
	BOOST_CHECK(bfile2.getTotalBytes() == bytes + sizeof(petalus_header_file));
	BOOST_CHECK(bfile2.getTotalDataBytes() == data_bytes);
	BOOST_CHECK(bfile2.getTotalUserDataBytes() == data_bytes);
	BOOST_CHECK(bfile2.getLastWrittenBlockSize() == last_block_size);

	PetalusBlock data;

	// Read the first block
	bfile2.read(1, data);
	BOOST_CHECK(items[0] == data);

	// Read the last block
	bfile2.read(bfile2.getTotalBlocks(), data);
	BOOST_CHECK(items[items.size() - 1] == data);

	// Read a block that dont exists
	bfile2.read(items.size() + 1, data);
	BOOST_CHECK(data.length() == 0);
	BOOST_CHECK(bfile2.getTotalBytes() == bytes + sizeof(petalus_header_file));
	BOOST_CHECK(bfile2.getTotalDataBytes() == data_bytes);

        nlohmann::json j;

        bfile2.showBlock(j, 4);

	BOOST_CHECK(j["block"] == 4);
	BOOST_CHECK(j["data"][0] == 88); // X
	BOOST_CHECK(j["data"][1] == 89); // Y
	BOOST_CHECK(j["data"][2] == 90); // Z
	BOOST_CHECK(j["data"].size() == items[3].length());

	bfile2.close();
}

/* Generate a file with encode JSON as string*/
BOOST_AUTO_TEST_CASE (test04_json_encoding_string)
{
        nlohmann::json j1;
        nlohmann::json j2;
        nlohmann::json j3;

        j1["user1"] = "Bob";
        j1["user2"] = "Alice";
        j1["data"] = "Some data";

        j2["user1"] = "John";
        j2["user2"] = "Steve";
        j2["data"] = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";

	std::string user1("XXXXXXXXX");
	std::string user2("John smith");
	std::string data1("data1");
	std::string data2("YYYYYYY                  YYYYYYY");
        j3["user1"] = user1;
        j3["user2"] = user2;
	std::vector<std::string> items {{ data1 } , { data2 }}; 
        j3["data"] = items;
	j3["number"] = 123456;

        auto bfile1 = BlockFile();
        std::string filename(generateBlockFileName());

	// remove the file if exists from previous runs
	if (std::filesystem::exists(filename)) std::filesystem::remove(filename);

        bfile1.open(filename);
       
	PetalusBlock block1(j1);
	PetalusBlock block2(j2);
	PetalusBlock block3(j3);

	bfile1.write(block1);
	bfile1.write(block2);
	bfile1.write(block3);

	BOOST_CHECK(bfile1.verifyBlock(1) == false);
	BOOST_CHECK(bfile1.verifyBlock(2) == true);
	BOOST_CHECK(bfile1.verifyBlock(3) == true);

	int32_t bytes = sizeof(petalus_header_file);

	bytes += block1.length() + sizeof(petalus_block_file_v1);
	bytes += block2.length() + sizeof(petalus_block_file_v1);
	bytes += block3.length() + sizeof(petalus_block_file_v1);

	BOOST_CHECK(bfile1.getTotalBytes() == bytes);
        BOOST_CHECK(bfile1.getTotalBlocks() == 3);

	{
		RedirectOutput out;

		out.cout << block1;

		bfile1.statistics(out.cout);
		bfile1.showBlocks(out.cout);
		bfile1.showBlock(out.cout, 2);
	}

	bfile1.close();

	// Now reopen the file and get some data
	auto bfile2 = BlockFile();
	bfile2.open(filename);

        BOOST_CHECK(bfile2.getTotalBlocks() == 3);
	BOOST_CHECK(bfile2.verifyBlock(1) == false);
	BOOST_CHECK(bfile2.verifyBlock(2) == true);
	BOOST_CHECK(bfile2.verifyBlock(3) == true);

	PetalusBlock data;

	bfile2.read(3, data);

	auto j4 = nlohmann::json::parse(data.data());
	BOOST_CHECK(user1.compare(j4["user1"]) == 0);
	BOOST_CHECK(user2.compare(j4["user2"]) == 0);
	BOOST_CHECK(data1.compare(j4["data"][0]) == 0);
	BOOST_CHECK(data2.compare(j4["data"][1]) == 0);
	BOOST_CHECK(j4["number"] == 123456);
      
	bfile2.close(); 

	// Reopen again and check the sizes
	auto bfile3 = BlockFile();
	bfile3.open(filename);

	BOOST_CHECK(bfile3.getTotalBytes() == bytes);
        BOOST_CHECK(bfile3.getTotalBlocks() == 3);

	BOOST_CHECK(bfile3.verifyBlock(1) == false);
	BOOST_CHECK(bfile3.verifyBlock(2) == true);
	BOOST_CHECK(bfile3.verifyBlock(3) == true);

	PetalusBlock more_data("ABCDEFGH");
	bfile3.write(more_data);	

	BOOST_CHECK(bfile3.getTotalBytes() == bytes + more_data.length() + sizeof(petalus_block_file_v1));
        BOOST_CHECK(bfile3.getTotalBlocks() == 4);

	BOOST_CHECK(bfile3.verifyBlock(1) == false);
	BOOST_CHECK(bfile3.verifyBlock(2) == true);
	BOOST_CHECK(bfile3.verifyBlock(3) == true);
	BOOST_CHECK(bfile3.verifyBlock(4) == true);

	bfile3.close();
}

BOOST_AUTO_TEST_CASE (test05_verify_blocks) // Verify the blocks
{
        auto bfile = BlockFile();
        std::string filename(generateBlockFileName());

        // remove the file if exists from previous runs
        if (std::filesystem::exists(filename)) std::filesystem::remove(filename);

        bfile.open(filename);
        BOOST_CHECK(bfile.getTotalBlocks() == 0);

        PetalusBlock data1("AAAA");
        PetalusBlock data2("BBBBBBBB");
        PetalusBlock data3("CCCCCCCCCCCCCCCC");

        bfile.write(data1);
        BOOST_CHECK(bfile.getTotalBlocks() == 1);

	// The first block can not be verified
	BOOST_CHECK(bfile.verifyBlock(0) == false);

	// There is no block 1 so return false
	BOOST_CHECK(bfile.verifyBlock(1) == false);

	// Write another block
	bfile.write(data2);

	// The first block can not be verified again
	BOOST_CHECK(bfile.verifyBlock(1) == false);

	// The first block is ok 
	BOOST_CHECK(bfile.verifyBlock(2) == true);

	// There is no block 2 so return false
	BOOST_CHECK(bfile.verifyBlock(3) == false);

	BOOST_CHECK(bfile.getTotalHeaderCorruptions() == 0);
	BOOST_CHECK(bfile.getTotalBlockCorruptions() == 0);
	BOOST_CHECK(bfile.getTotalLengthCorruptions() == 0);

        bfile.close();
}

BOOST_AUTO_TEST_CASE (test06_verify_and_corrupt_block) // Verify the blocks and corrupt one of them
{
        auto bfile = BlockFile();
        std::string filename(generateBlockFileName());

        // remove the file if exists from previous runs
        if (std::filesystem::exists(filename)) std::filesystem::remove(filename);

        bfile.open(filename);
        BOOST_CHECK(bfile.getTotalBlocks() == 0);

        PetalusBlock data1("AAAA");
        PetalusBlock data2("BBBBBBBB");
        PetalusBlock data3("CCCCCCCCCCCCCCCC");

        bfile.write(data1);
        bfile.write(data2);
        bfile.write(data3);
        BOOST_CHECK(bfile.getTotalBlocks() == 3);

        // The first block can not be verified
        BOOST_CHECK(bfile.verifyBlock(1) == false);
        BOOST_CHECK(bfile.verifyBlock(2) == true);
        BOOST_CHECK(bfile.verifyBlock(3) == true);

	// Write on the third block just one byte for corrupt
	const uint8_t *data = reinterpret_cast<const uint8_t*>("DDDD");
	int32_t offset = sizeof(petalus_header_file); 
	offset += data1.length() + sizeof(petalus_block_file_v1);
	offset += data2.length() + sizeof(petalus_block_file_v1);
	offset += sizeof(petalus_block_file_v1) + 4;

	bfile.writeOffset(offset , data, 4); 

        BOOST_CHECK(bfile.verifyBlock(1) == false);
        BOOST_CHECK(bfile.verifyBlock(2) == true);
        BOOST_CHECK(bfile.verifyBlock(3) == false); // This block is data corrupted

	BOOST_CHECK(bfile.getTotalHeaderCorruptions() == 0);
	BOOST_CHECK(bfile.getTotalBlockCorruptions() == 0);
	BOOST_CHECK(bfile.getTotalLengthCorruptions() == 0);
	BOOST_CHECK(bfile.getTotalMissingBlocks() == 0);
	BOOST_CHECK(bfile.getLastBlockLoaded() == 3);

        bfile.close();
}

/* Generate a file with X blocks and check if they exists */
BOOST_AUTO_TEST_CASE (test07)
{
        std::vector<PetalusBlock> items {
                { "This is some data to write on the first block" },
                { "more data on the second" },
                { "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" },
                { "BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB" },
                { "CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC" },
                { "XXXXXXXXXXXXXX" },
                { "buuuuuu,aaaaaa,bbbbb,cccccc" },
                { "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX" },
                { "1111111111111111111111111111111111111111111111111111111111111111111111111" },
                { "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX" },
                { "YYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY" }
        };

       	std::filesystem::path filename(generateBlockFileName());
        // remove the file if exists from previous runs
        if (std::filesystem::exists(filename)) std::filesystem::remove(filename);

        int32_t bytes = 0;
	for (int i = 0; i < 4; ++i) {
        	auto bfile = BlockFile();

        	bfile.open(filename);

		for (int j = 0; j < 10 ; ++j) {
        		for (auto &item: items) {
				if (bfile.haveSpaceOnFile(item.length())) {
                			bfile.write(item);
                			bytes += item.length() + sizeof(petalus_block_file_v1);
				}
			}
		}
		bfile.close();
        }

       	auto bfile_out = BlockFile();

	bfile_out.open(filename);
	
        BOOST_CHECK(bfile_out.getTotalBlocks() == items.size() * 10 * 4);
        BOOST_CHECK(bfile_out.getTotalBytes() == bytes + sizeof(petalus_header_file));
        bfile_out.close();
}

/* Verify that we can not write more than the size of the file */
BOOST_AUTO_TEST_CASE (test08)
{
       	std::string filename(generateBlockFileName());
        // remove the file if exists from previous runs
        if (std::filesystem::exists(filename)) std::filesystem::remove(filename);

	auto bfile = BlockFile(90 + sizeof(petalus_header_file));

	bfile.open(filename);

	BOOST_CHECK(bfile.getMaxFileSize() == 90 + sizeof(petalus_header_file));
	PetalusBlock data("AAAA");

	BOOST_CHECK(bfile.write(data) == data.length());
        BOOST_CHECK(bfile.getTotalBlocks() == 1);
        BOOST_CHECK(bfile.getTotalBytes() == sizeof(petalus_header_file) + data.length() + sizeof(petalus_block_file_v1));

	BOOST_CHECK(bfile.write(data) == -1);
	BOOST_CHECK(bfile.getWriteErrorCode() == BlockFileWriteCode::BLOCKFILE_WRITE_NO_SPACE);
        BOOST_CHECK(bfile.getTotalBlocks() == 1);
        BOOST_CHECK(bfile.getTotalBytes() == sizeof(petalus_header_file) + data.length() + sizeof(petalus_block_file_v1));

	bfile.close();
}

/* Verify that we can not open a non block file */
BOOST_AUTO_TEST_CASE (test09_non_block_file)
{
        std::string filename(generateBlockFileName());
        // remove the file if exists from previous runs
        if (std::filesystem::exists(filename)) std::filesystem::remove(filename);

        std::ofstream out(filename);

	out << "This is just a regular file";
	out.close();

        auto bfile = BlockFile();

        bfile.open(filename);

	BOOST_CHECK(bfile.getTotalHeaderCorruptions() == 1);
	BOOST_CHECK(bfile.getTotalBlockCorruptions() == 0);
	BOOST_CHECK(bfile.getTotalLengthCorruptions() == 0);

        bfile.close();
}

/* Create a file with one block and corrupt the length of the block */
BOOST_AUTO_TEST_CASE (test10_corrupt_length)
{
        std::string filename(generateBlockFileName());
        // remove the file if exists from previous runs
        if (std::filesystem::exists(filename)) std::filesystem::remove(filename);

        auto bfile = BlockFile();

	PetalusBlock data("This is another message that needs to be on a block");
        bfile.open(filename);
	bfile.write(data);

        BOOST_CHECK(bfile.getTotalHeaderCorruptions() == 0);
        BOOST_CHECK(bfile.getTotalBlockCorruptions() == 0);
        BOOST_CHECK(bfile.getTotalLengthCorruptions() == 0);
        BOOST_CHECK(bfile.getTotalBlocks() == 1);

	const petalus_block_file_v1 *block = bfile.findBlock(1);
	BOOST_CHECK(block != nullptr);
	int32_t correct_length = block->length;

	BOOST_CHECK(correct_length == data.length());
        bfile.close();

	// Open the file an corrupt the length of the first block
        std::ofstream out(filename, std::ifstream::out | std::ofstream::binary | std::fstream::in);

	int32_t offset = sizeof(petalus_header_file) + sizeof(petalus_header_file::magic);

	out.seekp(offset, std::ofstream::ios_base::beg);
	out.write("X", 1);
	out.close();

	bfile = BlockFile();

	{
		RedirectOutput out;

		bfile.setDebug(true);
		bfile.open(filename);
	}
	
	BOOST_CHECK(bfile.getTotalHeaderCorruptions() == 0);
	BOOST_CHECK(bfile.getTotalBlockCorruptions() == 0);
	BOOST_CHECK(bfile.getTotalLengthCorruptions() == 1);
        BOOST_CHECK(bfile.getTotalBlocks() == 1);
}

/* Create a file with two blocks and corrupt the length of the second block */
BOOST_AUTO_TEST_CASE (test11_corrupt_length)
{
        std::string filename(generateBlockFileName());
        // remove the file if exists from previous runs
        if (std::filesystem::exists(filename)) std::filesystem::remove(filename);

        auto bfile = BlockFile();

        PetalusBlock data("This is another message that needs to be on a block");
        bfile.open(filename);
        bfile.write(data);
        bfile.write(data);

        BOOST_CHECK(bfile.getTotalHeaderCorruptions() == 0);
        BOOST_CHECK(bfile.getTotalBlockCorruptions() == 0);
        BOOST_CHECK(bfile.getTotalLengthCorruptions() == 0);
        BOOST_CHECK(bfile.getTotalBlocks() == 2);
        bfile.close();

        // Open the file an corrupt the length of the first block
        std::ofstream out(filename, std::ifstream::out | std::ofstream::binary | std::fstream::in);

        int32_t offset = sizeof(petalus_header_file);

	offset += sizeof(petalus_block_file_v1) + data.length();
	offset += sizeof(petalus_header_file::magic);

        out.seekp(offset, std::ofstream::ios_base::beg);
        out.write("X", 1);
        out.close();

        bfile = BlockFile();
        bfile.open(filename);

        BOOST_CHECK(bfile.getTotalHeaderCorruptions() == 0);
        BOOST_CHECK(bfile.getTotalBlockCorruptions() == 0);
        BOOST_CHECK(bfile.getTotalLengthCorruptions() == 1);
        BOOST_CHECK(bfile.getTotalBlocks() == 2);
}

/* Create a file with tree blocks and corrupt the prev hash of the second block */
BOOST_AUTO_TEST_CASE (test12_corrupt_hash)
{
        std::string filename(generateBlockFileName());
        // remove the file if exists from previous runs
        if (std::filesystem::exists(filename)) std::filesystem::remove(filename);

        auto bfile = BlockFile();

        PetalusBlock data1("This is another message that needs to be on a block");
        PetalusBlock data2("ABCDEFGHIJK");
        bfile.open(filename);
        bfile.write(data1);
        bfile.write(data1);
        bfile.write(data2);

        BOOST_CHECK(bfile.getTotalHeaderCorruptions() == 0);
        BOOST_CHECK(bfile.getTotalBlockCorruptions() == 0);
        BOOST_CHECK(bfile.getTotalLengthCorruptions() == 0);
        BOOST_CHECK(bfile.getTotalBlocks() == 3);
        bfile.close();

        // Open the file an corrupt the length of the first block
        std::ofstream out(filename, std::ifstream::out | std::ofstream::binary | std::fstream::in);

        int32_t offset = sizeof(petalus_header_file);

        offset += sizeof(petalus_block_file_v1) + data1.length();
        offset += sizeof(petalus_block_file_v1) + data1.length();
        offset += sizeof(petalus_header_file::magic) + sizeof(int32_t) + sizeof(time_t) + sizeof(uint16_t);

        out.seekp(offset, std::ofstream::ios_base::beg);
        out.write("LUIS", 4);
        out.close();

        bfile = BlockFile();
        bfile.open(filename);

	BOOST_CHECK(bfile.verifyBlock(1) == false);
	BOOST_CHECK(bfile.verifyBlock(2) == true);
	BOOST_CHECK(bfile.verifyBlock(3) == false); // Corrupted block

        BOOST_CHECK(bfile.getTotalHeaderCorruptions() == 0);
        BOOST_CHECK(bfile.getTotalBlockCorruptions() == 0);
        BOOST_CHECK(bfile.getTotalLengthCorruptions() == 0);
        BOOST_CHECK(bfile.getTotalBlocks() == 3);
}

/* verify that the latest block of a file chains with another file */
BOOST_AUTO_TEST_CASE (test13_link_blocks)
{
        std::string filename1(generateBlockFileName("part1"));
        std::string filename2(generateBlockFileName("part2"));

        if (std::filesystem::exists(filename1)) std::filesystem::remove(filename1);
        if (std::filesystem::exists(filename2)) std::filesystem::remove(filename2);
	PetalusBlock block1("AAAABBBBCCCC");
	PetalusBlock block2("XXXX");
	PetalusBlock block3("Lets see how goes");
	PetalusBlock block4("Lets see how goes, but should work");

        auto bfile1 = BlockFile();

        bfile1.open(filename1);
        bfile1.write(block1);
        bfile1.write(block2);

	uint8_t digest[HASH_BLOCK_SIZE];

	bfile1.readBlockHash(2, digest);
	bfile1.close();

	std::string digeststr1;
	CryptoPP::HexEncoder encoder;
        encoder.Attach( new CryptoPP::StringSink(digeststr1) );
        encoder.Put( digest, sizeof(digest) );
        encoder.MessageEnd();

	auto bfile2 = BlockFile();
        
	bfile2.open(filename2);
	bfile2.setSeed(digest);
	bfile2.write(block3);
	bfile2.write(block4);
	bfile2.close();

	// Now verify that the two files are linked
	auto bfile3 = BlockFile();

	bfile2.open(filename1);
	bfile2.readBlockHash(2, digest);
	bfile2.close();

	auto bfile4 = BlockFile();
	bfile4.open(filename2);
	const petalus_block_file_v1 *block = bfile4.findBlock(1);	

	BOOST_CHECK(block != nullptr);
	BOOST_CHECK(std::memcmp(digest, block->prev_hash, HASH_BLOCK_SIZE) == 0);

	bfile4.close();	
}

// Check that only one block can verify a file with multiple blocks
BOOST_AUTO_TEST_CASE (test14) 
{
        std::string filename1(generateBlockFileName("part1"));
        std::string filename2(generateBlockFileName("part2"));

        if (std::filesystem::exists(filename1)) std::filesystem::remove(filename1);
        if (std::filesystem::exists(filename2)) std::filesystem::remove(filename2);

        auto bfile1 = BlockFile();

        bfile1.open(filename1);
        std::string key("This is a secret hash of a message or whatever");
	std::string hash;

        uint8_t digest[HASH_BLOCK_SIZE];

        CryptoPP::SHA256 shash;

        shash.CalculateDigest(digest, (uint8_t*) key.c_str(), key.length());

        CryptoPP::HexEncoder encoder;
        encoder.Attach( new CryptoPP::StringSink(hash) );
        encoder.Put( digest, sizeof(digest) );
        encoder.MessageEnd();

	PetalusBlock block(hash);	
	// Now we right the hash on the first block
	bfile1.write(block);

	bfile1.readBlockHash(1, digest); // get the hash of the block
	bfile1.close();
       
	// Now generates a new file with the previous blockfile 
	auto bfile2 = BlockFile();

	bfile2.open(filename2);
        bfile2.setSeed(digest);

	// Write 10 blocks
	for (int i = 0; i < 10; ++i) {
		std::ostringstream out;
		
		out << "Another message on the block number " << i;
		PetalusBlock block(out.str());
		bfile2.write(block);
	}

	for (int i = 2; i <= 10; ++i ) {
		BOOST_CHECK(bfile2.verifyBlock(i) == true);
	}
        bfile2.close();
}

// Corrupt a initial block file that was working
BOOST_AUTO_TEST_CASE (test15)
{
        std::string filename1(generateBlockFileName("part1"));
        std::string filename2(generateBlockFileName("part2"));

        if (std::filesystem::exists(filename1)) std::filesystem::remove(filename1);
        if (std::filesystem::exists(filename2)) std::filesystem::remove(filename2);

        uint8_t digest1[HASH_BLOCK_SIZE];
        uint8_t digest2[HASH_BLOCK_SIZE];
	// Create the initial file
        auto bfile1 = BlockFile();

        bfile1.open(filename1);
        PetalusBlock data("This is a secret hash of a message or whatever");
        PetalusBlock data1("888888888888888888888888888888888888888888888888888");
        PetalusBlock data2("-----------------------------");

	bfile1.write(data);
	bfile1.readBlockHash(1, digest1); // get the hash of the written block
	bfile1.close();

	// Create another file 
	auto bfile2 = BlockFile();

	bfile2.open(filename2);
	bfile2.setSeed(digest1);
	bfile2.write(data1);
	bfile2.write(data2);
	bfile2.close();

	// Open the two files again and verify that are linked
	bfile1 = BlockFile();
	bfile1.open(filename1);
	bfile1.readBlockHash(1, digest2); // get the hash of the written block
	BOOST_CHECK(std::memcmp(digest1, digest2, HASH_BLOCK_SIZE) == 0);
	BOOST_CHECK(bfile1.getTotalBlocks() == 1);

	bfile2 = BlockFile();
	bfile2.open(filename2);
	BOOST_CHECK(bfile2.getTotalBlocks() == 2);

	// The two files are consecutive
	BOOST_CHECK(bfile2.verify(bfile1) == true);

	bfile1.close();
	bfile2.close();

	auto bfile3 = BlockFile();
	bfile3.open(filename1);

	// Now corrupt the initial file and verify again
	const char *ptr = "EVER";
        const uint8_t *corrupt_data = reinterpret_cast<const uint8_t*>("EVER");
        int32_t offset = sizeof(petalus_header_file);
        offset += data.length() + sizeof(petalus_block_file_v1);
        offset =  offset - 4;

        bfile3.writeOffset(offset , corrupt_data, 4);
	bfile3.close();

	// Open the two files and verify
	bfile1 = BlockFile();
	bfile2 = BlockFile();

	bfile1.open(filename1);
	bfile2.open(filename2);

	BOOST_CHECK(bfile2.verify(bfile1) == false);

        BOOST_CHECK(bfile1.getTotalBlocks() == 1);
        BOOST_CHECK(bfile2.getTotalBlocks() == 2);

	bfile1.close();
	bfile2.close();
}

/* Open write and close file several times */
BOOST_AUTO_TEST_CASE (test16)
{
        std::string filename(generateBlockFileName());
        // remove the file if exists from previous runs
        if (std::filesystem::exists(filename)) std::filesystem::remove(filename);

        std::vector<PetalusBlock> items {
                { "BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB" },
                { "CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC" },
                { "XXXXXXXXXXXXXX" },
                { "buuuuuu,aaaaaa,bbbbb,cccccc" },
                { "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX" },
                { "1111111111111111111111111111111111111111111111111" },
                { "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX" },
                { "YYYYYYYYYYYYYYYYYYYYY" }
        };

	int32_t bytes = sizeof(petalus_header_file);
	int blocks = 0;
	for (auto &item: items) {
        	auto bfile = BlockFile();
        	bfile.open(filename);
        	bfile.write(item);
		++blocks;
		bytes += item.length() + sizeof(petalus_block_file_v1);

        	BOOST_CHECK(bfile.getTotalHeaderCorruptions() == 0);
        	BOOST_CHECK(bfile.getTotalBlockCorruptions() == 0);
        	BOOST_CHECK(bfile.getTotalLengthCorruptions() == 0);
		BOOST_CHECK(bfile.getTotalMissingBlocks() == 0);
		BOOST_CHECK(bfile.getLastBlockLoaded() == blocks);
        	BOOST_CHECK(bfile.getTotalBlocks() == blocks);
        	BOOST_CHECK(bfile.getTotalBytes() == bytes);
	}
}

/* Verify the password functionality of the blocks */
BOOST_AUTO_TEST_CASE (test17)
{
        std::string filename(generateBlockFileName());
        // remove the file if exists from previous runs
        if (std::filesystem::exists(filename)) std::filesystem::remove(filename);
	std::string password1("kjdalfkja;sdjfad");
	std::string password2("XXXXXX");
	PetalusBlock data1("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
	PetalusBlock data2("BBBBBBBBBBBBBBBBBBBBBBBBBBBBB");
	PetalusBlock data3("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");

	std::string salt1, salt2;

	generateRandomBytes(salt1, 8);
	generateRandomBytes(salt2, 8);

	nlohmann::json j1, j2;	
	j1["password_md5"] = password1;
	j1["salt"] = salt1;
	j1["other info"] = "I dont know";

	j2["password_sha256"] = password2;
	j2["salt"] = salt2;
	j2["value1"] = "asdf;lkj";

	PetalusBlock block1(j1);
	block1.type(BlockType::SYS_JSON_BLOCKCHAIN_ACCESS);

	PetalusBlock block2(j2);
	block2.type(BlockType::SYS_JSON_BLOCKCHAIN_ACCESS);

	auto bfile1 = BlockFile();
	bfile1.open(filename);

	// First write some blocks and just one password
	bfile1.write(data1);
	bfile1.write(block1);
       	bfile1.write(data2);
	bfile1.close();

	auto bfile2 = BlockFile();
	bfile2.open(filename); // The password should be populated

	BOOST_CHECK(salt1.compare(bfile2.getSalt()) == 0);
	BOOST_CHECK(password1.compare(bfile2.getAccessPassword()) == 0);

	// Write more blocks and another password block
	bfile2.write(block2);
       	bfile2.write(data3);

	// The password is the old one
	BOOST_CHECK(password1.compare(bfile2.getAccessPassword()) == 0);
	bfile2.close();

	// Reopen again the file and check all the password that should be the last one
	auto bfile3 = BlockFile();
	bfile3.open(filename);

	// The password is the newone one
	BOOST_CHECK(password2.compare(bfile3.getAccessPassword()) == 0);

	bfile3.close();
}

BOOST_AUTO_TEST_CASE (test18) // Verify that sign with blake2s works as expected
{
        std::string filename(generateBlockFileName());
        // remove the file if exists from previous runs
        if (std::filesystem::exists(filename)) std::filesystem::remove(filename);
        std::string expected("508C5E8C327C14E2E1A72BA34EEB452F37458B209ED63A294D999B4C86675982");

        PetalusBlock data("abc");

        auto bfile1 = BlockFile();
        bfile1.open(filename);

	// By default is set to sha256
	BOOST_CHECK(std::strcmp(bfile1.getBlockHashTypeName(), "sha256") == 0);

	// Change the hashing algorithm
	bfile1.setBlockHashType(BlockHashTypes::BLAKE2S);
	BOOST_CHECK(std::strcmp(bfile1.getBlockHashTypeName(), "blake2s") == 0);
	BOOST_CHECK(bfile1.getBlockHashType() == BlockHashTypes::BLAKE2S);

        bfile1.write(data);
        bfile1.write(data);
        bfile1.write(data);

        BOOST_CHECK(bfile1.getTotalHeaderCorruptions() == 0);
        BOOST_CHECK(bfile1.getTotalBlockCorruptions() == 0);
        BOOST_CHECK(bfile1.getTotalLengthCorruptions() == 0);
        BOOST_CHECK(bfile1.getTotalBlocks() == 3);
	BOOST_CHECK(data.length() == 3);
	BOOST_CHECK(bfile1.getTotalBytes() == sizeof(petalus_header_file) + (3 * (data.length() + sizeof(petalus_block_file_v1))));

        bfile1.close();
	// Reopen the file and do the checks
	auto bfile2 = BlockFile();
	bfile2.open(filename);

        BOOST_CHECK(bfile2.getTotalHeaderCorruptions() == 0);
        BOOST_CHECK(bfile2.getTotalBlockCorruptions() == 0);
        BOOST_CHECK(bfile2.getTotalLengthCorruptions() == 0);
        BOOST_CHECK(bfile2.getTotalBlocks() == 3);
	BOOST_CHECK(bfile2.getTotalBytes() == sizeof(petalus_header_file) + (3 * (data.length() + sizeof(petalus_block_file_v1))));

	// The three blocks should have the same hash on the data
	for (int i = 1; i <= bfile2.getTotalBlocks(); ++i) {
	        const petalus_block_file_v1 *block = bfile2.findBlock(i);
        	BOOST_CHECK(block != nullptr);

		std::string hash;

        	CryptoPP::HexEncoder encoder;
        	encoder.Attach( new CryptoPP::StringSink(hash) );
        	encoder.Put( block->hash, sizeof(block->hash) );
        	encoder.MessageEnd();

        	BOOST_CHECK(hash.compare(expected) == 0);
	}

        // The first block can not be verified
        BOOST_CHECK(bfile2.verifyBlock(1) == false);
        BOOST_CHECK(bfile2.verifyBlock(2) == true);
        BOOST_CHECK(bfile2.verifyBlock(3) == true);

	bfile2.close();
}

BOOST_AUTO_TEST_CASE (test19) // Verify that sign with different algorithms dont break the chain
{
        std::string filename(generateBlockFileName());
        // remove the file if exists from previous runs
        if (std::filesystem::exists(filename)) std::filesystem::remove(filename);

        PetalusBlock data1("1234567890qwertyuiopasdfghjklzxcvbnm");
        PetalusBlock data2("1234567890AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
        PetalusBlock data3("1234567890XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");

        auto bfile1 = BlockFile();
        bfile1.open(filename);

        BOOST_CHECK(bfile1.write(data1) == data1.length());
        BOOST_CHECK(bfile1.write(data2) == data2.length());
        BOOST_CHECK(bfile1.write(data3) == data3.length());

        // Change the hashing algorithm
        bfile1.setBlockHashType(BlockHashTypes::BLAKE2S);

        BOOST_CHECK(bfile1.write(data2) == data2.length());
        BOOST_CHECK(bfile1.write(data3) == data3.length());
        BOOST_CHECK(bfile1.write(data1) == data1.length());
        
	// Change the hashing algorithm again
        bfile1.setBlockHashType(BlockHashTypes::SHA3_256);

        BOOST_CHECK(bfile1.write(data3) == data3.length());
        BOOST_CHECK(bfile1.write(data1) == data1.length());
        BOOST_CHECK(bfile1.write(data2) == data2.length());

        BOOST_CHECK(bfile1.getTotalHeaderCorruptions() == 0);
        BOOST_CHECK(bfile1.getTotalBlockCorruptions() == 0);
        BOOST_CHECK(bfile1.getTotalLengthCorruptions() == 0);
        BOOST_CHECK(bfile1.getTotalBlocks() == 9);

	int32_t total_bytes = sizeof(petalus_header_file);
	total_bytes += (3 * (data1.length() + sizeof(petalus_block_file_v1)));
	total_bytes += (3 * (data2.length() + sizeof(petalus_block_file_v1)));
	total_bytes += (3 * (data3.length() + sizeof(petalus_block_file_v1)));

        BOOST_CHECK(bfile1.getTotalBytes() == total_bytes);

        bfile1.close();

	// Now we reopen the file and do some checks on the blocks 

        auto bfile2 = BlockFile();
        bfile2.open(filename);

	// The file is set to the last blocks writen on this case sha3
	BOOST_CHECK(std::strcmp(bfile2.getBlockHashTypeName(), "sha3-256") == 0);
        BOOST_CHECK(bfile2.getTotalHeaderCorruptions() == 0);
        BOOST_CHECK(bfile2.getTotalBlockCorruptions() == 0);
        BOOST_CHECK(bfile2.getTotalLengthCorruptions() == 0);
        BOOST_CHECK(bfile2.getTotalBlocks() == 9);
        BOOST_CHECK(bfile2.getTotalBytes() == total_bytes);

        // The first block can not be verified
        BOOST_CHECK(bfile2.verifyBlock(1) == false);
        BOOST_CHECK(bfile2.verifyBlock(2) == true);
        BOOST_CHECK(bfile2.verifyBlock(3) == true);
        BOOST_CHECK(bfile2.verifyBlock(4) == true);
        BOOST_CHECK(bfile2.verifyBlock(5) == true);
        BOOST_CHECK(bfile2.verifyBlock(6) == true);
        BOOST_CHECK(bfile2.verifyBlock(7) == true);
        BOOST_CHECK(bfile2.verifyBlock(8) == true);
        BOOST_CHECK(bfile2.verifyBlock(9) == true);

	// Verify some values of the blocks that has been used sha256
        const petalus_block_file_v1 *block = bfile2.findBlock(1);
        BOOST_CHECK(block != nullptr);
	BOOST_CHECK(block->hash_type == static_cast<uint8_t>(BlockHashTypes::SHA256));

        block = bfile2.findBlock(2);
        BOOST_CHECK(block != nullptr);
	BOOST_CHECK(block->hash_type == static_cast<uint8_t>(BlockHashTypes::SHA256));

        block = bfile2.findBlock(3);
        BOOST_CHECK(block != nullptr);
	BOOST_CHECK(block->hash_type == static_cast<uint8_t>(BlockHashTypes::SHA256));

	// BLAKE blocks
        block = bfile2.findBlock(4);
        BOOST_CHECK(block != nullptr);
	BOOST_CHECK(block->hash_type == static_cast<uint8_t>(BlockHashTypes::BLAKE2S));

        block = bfile2.findBlock(5);
        BOOST_CHECK(block != nullptr);
	BOOST_CHECK(block->hash_type == static_cast<uint8_t>(BlockHashTypes::BLAKE2S));

        block = bfile2.findBlock(6);
        BOOST_CHECK(block != nullptr);
	BOOST_CHECK(block->hash_type == static_cast<uint8_t>(BlockHashTypes::BLAKE2S));

	// SHA3 blocks
        block = bfile2.findBlock(7);
        BOOST_CHECK(block != nullptr);
	BOOST_CHECK(block->hash_type == static_cast<uint8_t>(BlockHashTypes::SHA3_256));

        block = bfile2.findBlock(8);
        BOOST_CHECK(block != nullptr);
	BOOST_CHECK(block->hash_type == static_cast<uint8_t>(BlockHashTypes::SHA3_256));

        block = bfile2.findBlock(9);
        BOOST_CHECK(block != nullptr);
	BOOST_CHECK(block->hash_type == static_cast<uint8_t>(BlockHashTypes::SHA3_256));

	bfile2.close();
}

BOOST_AUTO_TEST_CASE (test20_hash_type) // Verify the hash_type of one block 
{
        auto bfile1 = BlockFile();
        std::string filename(generateBlockFileName());

        // remove the file if exists from previous runs
        if (std::filesystem::exists(filename)) std::filesystem::remove(filename);

        bfile1.open(filename);
        BOOST_CHECK(bfile1.getTotalBlocks() == 0);

        PetalusBlock data1("AAAA");
        PetalusBlock data2("BBBBBBBB");
        PetalusBlock data3("CCCCCCCCCCCCCCCC");

        bfile1.write(data1);
        bfile1.write(data2);
        bfile1.write(data3);
        BOOST_CHECK(bfile1.getTotalBlocks() == 3);

        // The first block can not be verified
        BOOST_CHECK(bfile1.verifyBlock(1) == false);
        BOOST_CHECK(bfile1.verifyBlock(2) == true);
        BOOST_CHECK(bfile1.verifyBlock(3) == true);

        // Write on the thrid block just one byte for corrupt
        const uint8_t data = 0xFA;
        int32_t offset = sizeof(petalus_header_file);
        offset += data1.length() + sizeof(petalus_block_file_v1);
        offset += data2.length() + sizeof(petalus_block_file_v1);
        offset += sizeof(uint32_t) + sizeof(int32_t) + sizeof(time_t) + sizeof(uint8_t);

        bfile1.writeOffset(offset , &data, 1); // Corrupt the hash_type of a block

	bfile1.close();

	// Reopen the file and do some checks
	auto bfile2 = BlockFile();
	bfile2.open(filename);

        BOOST_CHECK(bfile2.getTotalHeaderCorruptions() == 0);
        BOOST_CHECK(bfile2.getTotalBlockCorruptions() == 1);
        BOOST_CHECK(bfile2.getTotalLengthCorruptions() == 0);

        BOOST_CHECK(bfile2.verifyBlock(1) == false);
        BOOST_CHECK(bfile2.verifyBlock(2) == false);
        BOOST_CHECK(bfile2.verifyBlock(3) == false);

        bfile2.close();
}

/* Put more items on the header, so the file claims that there is X blocks but in really is X-4 */
BOOST_AUTO_TEST_CASE (test21)
{
        std::string filename(generateBlockFileName());
        // remove the file if exists from previous runs
        if (std::filesystem::exists(filename)) std::filesystem::remove(filename);

        auto bfile = BlockFile();

        PetalusBlock block("This is another message that needs to be on a block");
        bfile.open(filename);

	for (int i = 0; i < 4; ++i) bfile.write(block);

        BOOST_CHECK(bfile.getTotalHeaderCorruptions() == 0);
        BOOST_CHECK(bfile.getTotalBlockCorruptions() == 0);
        BOOST_CHECK(bfile.getTotalLengthCorruptions() == 0);
        BOOST_CHECK(bfile.getTotalBlocks() == 4);
        BOOST_CHECK(bfile.getTotalBytes() == sizeof(petalus_header_file) + (4 * (block.length() + sizeof(petalus_block_file_v1))));
        bfile.close();

        // Open the file an corrupt the length of the header file
        std::ofstream out(filename, std::ifstream::out | std::ofstream::binary | std::fstream::in);

	petalus_header_file header;

	header.blocks = 6;

        out.seekp(0, std::ofstream::ios_base::beg);
        out.write(reinterpret_cast<const char*>(&header), sizeof(petalus_header_file));
        out.close();

        bfile = BlockFile();
        bfile.open(filename);

        BOOST_CHECK(bfile.getTotalHeaderCorruptions() == 0);
        BOOST_CHECK(bfile.getTotalBlockCorruptions() == 1);
        BOOST_CHECK(bfile.getTotalLengthCorruptions() == 0);
        BOOST_CHECK(bfile.getTotalBlocks() == 6); // this value should be 4, but is better not to fix while open the file

	BOOST_CHECK(bfile.is_open() == false);

	bfile.close();
}

/* Simulate a lost block on the file, a block that has been written but not updated on the header */
BOOST_AUTO_TEST_CASE (test22_missing_block)
{
        std::string filename(generateBlockFileName());
        // remove the file if exists from previous runs
        if (std::filesystem::exists(filename)) std::filesystem::remove(filename);

        auto bfile = BlockFile();

        PetalusBlock block("This is another message that needs to be on a block");
        bfile.open(filename);

        for (int i = 0; i < 10; ++i) bfile.write(block);

        BOOST_CHECK(bfile.getTotalHeaderCorruptions() == 0);
        BOOST_CHECK(bfile.getTotalBlockCorruptions() == 0);
        BOOST_CHECK(bfile.getTotalLengthCorruptions() == 0);
        BOOST_CHECK(bfile.getTotalBlocks() == 10);
        BOOST_CHECK(bfile.getTotalBytes() == sizeof(petalus_header_file) + (10 * (block.length() + sizeof(petalus_block_file_v1))));
        bfile.close();

        // Open the file an remove one block from the header
        std::ofstream out(filename, std::ifstream::out | std::ofstream::binary | std::fstream::in);

        petalus_header_file header;

        header.blocks = 9;

        out.seekp(0, std::ofstream::ios_base::beg);
        out.write(reinterpret_cast<const char*>(&header), sizeof(petalus_header_file));
        out.close();

        bfile = BlockFile();

	bfile.setShowMissingBlocks(16); // 16 bytes

	{ 
		RedirectOutput out;
		bfile.setDebug(true);
        	bfile.open(filename);
	}

        BOOST_CHECK(bfile.getTotalHeaderCorruptions() == 0);
        BOOST_CHECK(bfile.getTotalBlockCorruptions() == 0);
        BOOST_CHECK(bfile.getTotalLengthCorruptions() == 0);
        BOOST_CHECK(bfile.getTotalMissingBlocks() == 1);
        BOOST_CHECK(bfile.getTotalBlocks() == 9); // this value should be 10, but is better not to fix while open the file

	{
		RedirectOutput out;

		out.cout << bfile;
	}

        BOOST_CHECK(bfile.is_open() == false);

        bfile.close();
}

// Set and get the public key on a BlockFile
BOOST_AUTO_TEST_CASE (test23_sanity_public_key)
{
        std::string filename(generateBlockFileName());
        // remove the file if exists from previous runs
        if (std::filesystem::exists(filename)) std::filesystem::remove(filename);

        auto bfile = BlockFile();
	bfile.open(filename);

        PrivateKey privateKey;
        PublicKey publicKey;
        std::string b64publickey = generateBase64PublicKey(privateKey, publicKey);

	BOOST_TEST_MESSAGE( "b64publickey:(" << b64publickey << ")");

	nlohmann::json j;

        j["info0"] = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
        j["ec_public_key"] = b64publickey;

        std::ostringstream out;
        out << j;

        PetalusBlock jblock(out.str(), BlockType::SYS_JSON_BLOCKCHAIN_ACCESS);

	bfile.write(jblock);

        BOOST_CHECK(bfile.getTotalBlocks() == 1);
        BOOST_CHECK(bfile.havePublicKey() == false);

	bfile.close();

	// Now we reopen the file and check that the public key is load correctly
        auto bfile2 = BlockFile();
	bfile2.open(filename);

        BOOST_CHECK(bfile2.getTotalBlocks() == 1);
        BOOST_CHECK(bfile2.havePublicKey() == true);
        BOOST_CHECK(bfile2.isECPublicKey() == true);
	BOOST_CHECK(bfile2.getECPublicKey() == publicKey); 
	BOOST_CHECK(bfile2.getSignatureLength() == 64);

	bfile2.close();
}

// Sets a public key on a BlockFile and write a signed block that is correct
BOOST_AUTO_TEST_CASE (test24)
{
        std::string filename(generateBlockFileName());
        // remove the file if exists from previous runs
        if (std::filesystem::exists(filename)) std::filesystem::remove(filename);

        auto bfile1 = BlockFile();
        bfile1.open(filename);

        CryptoPP::AutoSeededRandomPool prng;
        PrivateKey privateKey;
        PublicKey publicKey;
        std::string b64publickey = generateBase64PublicKey(privateKey, publicKey);

	BOOST_TEST_MESSAGE( "b64publickey:(" << b64publickey << ")");

        nlohmann::json j;

        j["info1"] = "SNI";
        j["info2"] = "Passport";
        j["ec_public_key"] = b64publickey;

        std::ostringstream out;
        out << j;

        PetalusBlock jblock(out.str(), BlockType::SYS_JSON_BLOCKCHAIN_ACCESS);

        bfile1.write(jblock);
        BOOST_CHECK(bfile1.getTotalBlocks() == 1);
        BOOST_CHECK(bfile1.havePublicKey() == false);

        bfile1.close();

	// Reopen the file and write a block signed
	// Generate a signed block
	std::string data(generateRandomString());
	PetalusBlock block(data);

	BOOST_TEST_MESSAGE( "data:(" << data << ")");
	Signer signer(privateKey);

        std::string binary_signature;
        uint8_t digest[HASH_BLOCK_SIZE];

        computeSHA256(digest, (const uint8_t*)block.data(), block.length());

        // Sign the sha256 hash of data
        CryptoPP::StringSource s( digest, HASH_BLOCK_SIZE, true /*pump all*/,
                new CryptoPP::SignerFilter( prng,
                        signer,
                        new CryptoPP::StringSink( binary_signature )
                ) // SignerFilter
        ); // StringSource

	BOOST_CHECK(binary_signature.length() == 64);
	block.signature(binary_signature);

	// Reopen the file and write the block
        auto bfile2 = BlockFile();
        bfile2.open(filename);

        BOOST_CHECK(bfile2.getTotalBlocks() == 1);
        BOOST_CHECK(bfile2.havePublicKey() == true);
	BOOST_CHECK(block.signature_length() == 64);
	BOOST_CHECK(block.length() == data.length());

	bfile2.write(block);
       
	// The block has been modified
	BOOST_CHECK(block.signature_length() == 64);
	BOOST_CHECK(block.length() == data.length() + 64);
	BOOST_CHECK(bfile2.getTotalBlocks() == 2);
        BOOST_CHECK(bfile2.havePublicKey() == true);

	// Check the size of the block that has been written
        const petalus_block_file_v1 *rblock = bfile2.findBlock(2);
        BOOST_CHECK(rblock != nullptr);
        BOOST_CHECK(rblock->type == static_cast<const uint8_t>(BlockType::USR_PLAIN));	
        BOOST_CHECK(rblock->length == block.length());	
	BOOST_CHECK(rblock->signature_length == block.signature_length()); // the block has been signed


	nlohmann::json js;

        bfile2.showBlock(js, 2);

	BOOST_CHECK(js.find("data") != js.end());
	BOOST_CHECK(js.find("signature") != js.end());

	PetalusBlock read_block;

	bfile2.read(2, read_block);

	BOOST_CHECK(data.compare(read_block.data()) == 0);

	{
		RedirectOutput out;

		bfile2.showBlock(out.cout, 2);
	}

	bfile2.close();
}

// Sets a public key on a BlockFile and write a signed block that has been modified 
BOOST_AUTO_TEST_CASE (test25)
{
        std::string filename(generateBlockFileName());
        // remove the file if exists from previous runs
        if (std::filesystem::exists(filename)) std::filesystem::remove(filename);

        auto bfile1 = BlockFile();
        bfile1.open(filename);

        CryptoPP::AutoSeededRandomPool prng;
        PrivateKey privateKey;
        PublicKey publicKey;
        std::string b64publickey = generateBase64PublicKey(privateKey, publicKey);

        nlohmann::json j;

        j["info0"] = "Extra information not needed";
        j["ec_public_key"] = b64publickey; 

        std::ostringstream out;
        out << j;

        PetalusBlock jblock(out.str(), BlockType::SYS_JSON_BLOCKCHAIN_ACCESS);

        bfile1.write(jblock);
        BOOST_CHECK(bfile1.getTotalBlocks() == 1);
        BOOST_CHECK(bfile1.havePublicKey() == false);

        bfile1.close();

        // Reopen the file and write a block signed
        // Generate a signed block
        std::string data(generateRandomString());
        PetalusBlock block(data);

        Signer signer(privateKey);

        std::string binary_signature;
        uint8_t digest[HASH_BLOCK_SIZE];

        computeSHA256(digest, (const uint8_t*)block.data(), block.length());

        // Sign the sha256 hash of data
        CryptoPP::StringSource s( digest, HASH_BLOCK_SIZE, true /*pump all*/,
                new CryptoPP::SignerFilter( prng,
                        signer,
                        new CryptoPP::StringSink( binary_signature )
                ) // SignerFilter
        ); // StringSource

        BOOST_CHECK(binary_signature.length() == 64);
        block.signature(binary_signature);

        // Reopen the file and write the block
        auto bfile2 = BlockFile();
        bfile2.open(filename);

        BOOST_CHECK(bfile2.getTotalBlocks() == 1);
        BOOST_CHECK(bfile2.havePublicKey() == true);

	// We add one character to the original data, enought to fail the verification
	block.data(data + "X");

        BOOST_CHECK(bfile2.write(block) == -1);
	BOOST_CHECK(bfile2.getWriteErrorCode() == BlockFileWriteCode::BLOCKFILE_WRITE_SIGN_FAIL);
        BOOST_CHECK(bfile2.getTotalBlocks() == 1);
        BOOST_CHECK(bfile2.havePublicKey() == true);
}

// Sets a public key on a BlockFile and write an verify the method verifyBlock works 
BOOST_AUTO_TEST_CASE (test26)
{
        std::string filename(generateBlockFileName());
        // remove the file if exists from previous runs
        if (std::filesystem::exists(filename)) std::filesystem::remove(filename);

        auto bfile1 = BlockFile();
        bfile1.open(filename);

        CryptoPP::AutoSeededRandomPool prng;
        PrivateKey privateKey;
        PublicKey publicKey;
        std::string b64publickey = generateBase64PublicKey(privateKey, publicKey, CryptoPP::ASN1::secp224k1());

        nlohmann::json j;

        j["ec_public_key"] = b64publickey;

        std::ostringstream out;
        out << j;

        PetalusBlock jblock(out.str(), BlockType::SYS_JSON_BLOCKCHAIN_ACCESS);

        bfile1.write(jblock);
        bfile1.close();

        // Reopen the file and write the block
        auto bfile2 = BlockFile();
        bfile2.open(filename);

        std::string data(generateRandomString(2411));
        PetalusBlock block(data);

        Signer signer(privateKey);

        std::string binary_signature;
        uint8_t digest[HASH_BLOCK_SIZE];

        computeSHA256(digest, (const uint8_t*)block.data(), block.length());

        // Sign the sha256 hash of data
        CryptoPP::StringSource s1( digest, HASH_BLOCK_SIZE, true /*pump all*/,
                new CryptoPP::SignerFilter( prng,
                        signer,
                        new CryptoPP::StringSink( binary_signature )
                ) // SignerFilter
        ); // StringSource

        BOOST_CHECK(binary_signature.length() == 58);
        block.signature(binary_signature);

        BOOST_CHECK(bfile2.getTotalBlocks() == 1);
        BOOST_CHECK(bfile2.havePublicKey() == true);
        BOOST_CHECK(bfile2.write(block) > 0);
        BOOST_CHECK(bfile2.getTotalBlocks() == 2);
	BOOST_CHECK(bfile2.verifyBlock(1) == false);
	BOOST_CHECK(bfile2.verifyBlock(2) == true);

	bfile2.close();	

	// Now we simulate that a bad administrator generates a block signed
	// with is own private key
        PrivateKey badPrivateKey;

	badPrivateKey.Initialize( prng, CryptoPP::ASN1::secp256k1() );

        // Reopen the file and write the bad block
        auto bfile3 = BlockFile();
        bfile3.open(filename);

        std::string baddata("This data is generated by a bad bad administrator");
        PetalusBlock badblock(data);

        Signer badsigner(badPrivateKey);

        std::string bad_binary_signature;

        computeSHA256(digest, (const uint8_t*)badblock.data(), badblock.length());

        // Sign the sha256 hash of data
        CryptoPP::StringSource s2( digest, HASH_BLOCK_SIZE, true /*pump all*/,
                new CryptoPP::SignerFilter( prng,
                        badsigner,
                        new CryptoPP::StringSink( bad_binary_signature )
                ) // SignerFilter
        ); // StringSource

	badblock.prepend(boost::string_ref((char*)bad_binary_signature.c_str(), bad_binary_signature.length()));	

        BOOST_CHECK(bfile3.getTotalBlocks() == 2);
        BOOST_CHECK(bfile3.verifyBlock(1) == false);
        BOOST_CHECK(bfile3.verifyBlock(2) == true);
        BOOST_CHECK(bfile3.verifyBlock(3) == false);

	bfile3.writeRawBlock(badblock, 0x01);
	
        BOOST_CHECK(bfile3.getTotalBlocks() == 3);

        BOOST_CHECK(bfile3.verifyBlock(2) == true);
        BOOST_CHECK(bfile3.verifySignatureBlock(2) == true);
        BOOST_CHECK(bfile3.verifySignatureBlock(10) == false); // The block dont exist

        BOOST_CHECK(bfile3.verifyBlock(3) == true);
        BOOST_CHECK(bfile3.verifySignatureBlock(3) == false);


	bfile3.close();
}

// Sets a public key on a BlockFile with other eliptic curve(secp192k1) and verify evertying works
BOOST_AUTO_TEST_CASE (test27)
{
        std::string filename(generateBlockFileName());
        // remove the file if exists from previous runs
        if (std::filesystem::exists(filename)) std::filesystem::remove(filename);

        auto bfile1 = BlockFile();
        bfile1.open(filename);

        CryptoPP::AutoSeededRandomPool prng;
        PrivateKey privateKey;
        PublicKey publicKey;
        std::string b64publickey = generateBase64PublicKey(privateKey, publicKey, CryptoPP::ASN1::secp192k1()); // Generates a signature of 48 bytes

        nlohmann::json j;

        j["ec_public_key"] = b64publickey;

        std::ostringstream out;
        out << j;

        PetalusBlock jblock(out.str(), BlockType::SYS_JSON_BLOCKCHAIN_ACCESS);

        bfile1.write(jblock);
        bfile1.close();

        // Reopen the file and write the block
        auto bfile2 = BlockFile();
        bfile2.open(filename);
	BOOST_CHECK(bfile2.havePublicKey() == true);
	BOOST_CHECK(bfile2.getSignatureLength() == 48);

        std::string data(generateRandomString(2339));
        PetalusBlock block(data);

        Signer signer(privateKey);

        std::string binary_signature;
        uint8_t digest[HASH_BLOCK_SIZE];

        computeSHA256(digest, (const uint8_t*)block.data(), block.length());

        // Sign the sha256 hash of data
        CryptoPP::StringSource s1( digest, HASH_BLOCK_SIZE, true /*pump all*/,
                new CryptoPP::SignerFilter( prng,
                        signer,
                        new CryptoPP::StringSink( binary_signature )
                ) // SignerFilter
        ); // StringSource

        BOOST_CHECK(binary_signature.length() == 48);
        block.signature(binary_signature);

        BOOST_CHECK(bfile2.getTotalBlocks() == 1);
        BOOST_CHECK(bfile2.havePublicKey() == true);
        BOOST_CHECK(bfile2.write(block) > 0);
        BOOST_CHECK(bfile2.getTotalBlocks() == 2);
        BOOST_CHECK(bfile2.verifyBlock(1) == false);
        BOOST_CHECK(bfile2.verifyBlock(2) == true);

	PetalusBlock read_block;

	BOOST_CHECK(bfile2.read(2, read_block) > 0);
	BOOST_CHECK(data.compare(read_block.data()) == 0);

        // Check the size of the block that has been written
        const petalus_block_file_v1 *rblock = bfile2.findBlock(2);
        BOOST_CHECK(rblock != nullptr);
        BOOST_CHECK(rblock->type == static_cast<const uint8_t>(BlockType::USR_PLAIN));
        BOOST_CHECK(rblock->length == read_block.length() + 48);
        BOOST_CHECK(rblock->signature_length == read_block.signature_length()); // the block has been signed

        bfile2.close();
}

// Sets a public key on a BlockFile with other eliptic curve(secp384r1) and verify evertying works
BOOST_AUTO_TEST_CASE (test28)
{
        std::string filename(generateBlockFileName());
        // remove the file if exists from previous runs
        if (std::filesystem::exists(filename)) std::filesystem::remove(filename);

        auto bfile1 = BlockFile();
        bfile1.open(filename);

        CryptoPP::AutoSeededRandomPool prng;
        PrivateKey privateKey;
        PublicKey publicKey;
        std::string b64publickey = generateBase64PublicKey(privateKey, publicKey, CryptoPP::ASN1::secp384r1()); // Generates a signature of 96 bytes

        nlohmann::json j;

        j["ec_public_key"] = b64publickey;

        std::ostringstream out;
        out << j;

        PetalusBlock jblock(out.str(), BlockType::SYS_JSON_BLOCKCHAIN_ACCESS);

        bfile1.write(jblock);
        bfile1.close();

        // Reopen the file and write the block
        auto bfile2 = BlockFile();
        bfile2.open(filename);
        BOOST_CHECK(bfile2.havePublicKey() == true);
        BOOST_CHECK(bfile2.getSignatureLength() == 96);

        std::string data(generateRandomString(1999));
        PetalusBlock block(data);

        Signer signer(privateKey);

        std::string binary_signature;
        uint8_t digest[HASH_BLOCK_SIZE];

        computeSHA256(digest, (const uint8_t*)block.data(), block.length());

        // Sign the sha256 hash of data
        CryptoPP::StringSource s1( digest, HASH_BLOCK_SIZE, true /*pump all*/,
                new CryptoPP::SignerFilter( prng,
                        signer,
                        new CryptoPP::StringSink( binary_signature )
                ) // SignerFilter
        ); // StringSource

        BOOST_CHECK(binary_signature.length() == 96);
        block.signature(binary_signature);

        BOOST_CHECK(bfile2.getTotalBlocks() == 1);
        BOOST_CHECK(bfile2.havePublicKey() == true);
        BOOST_CHECK(bfile2.write(block) > 0);
        BOOST_CHECK(bfile2.getTotalBlocks() == 2);
        BOOST_CHECK(bfile2.verifyBlock(1) == false);
        BOOST_CHECK(bfile2.verifyBlock(2) == true);

        PetalusBlock read_block;

        BOOST_CHECK(bfile2.read(2, read_block) > 0);
        BOOST_CHECK(data.compare(read_block.data()) == 0);

        // Check the size of the block that has been written
        const petalus_block_file_v1 *rblock = bfile2.findBlock(2);
        BOOST_CHECK(rblock != nullptr);
        BOOST_CHECK(rblock->type == static_cast<const uint8_t>(BlockType::USR_PLAIN));
        BOOST_CHECK(rblock->length == read_block.length() + 96);
        BOOST_CHECK(rblock->signature_length == block.signature_length()); // the block has been signed

        bfile2.close();
}

// Sets a public key on a BlockFile with other eliptic curve(secp521r1) and verify evertying works
BOOST_AUTO_TEST_CASE (test29)
{
        std::string filename(generateBlockFileName());
        // remove the file if exists from previous runs
        if (std::filesystem::exists(filename)) std::filesystem::remove(filename);

        auto bfile1 = BlockFile();
        bfile1.open(filename);

        CryptoPP::AutoSeededRandomPool prng;
        PrivateKey privateKey;
        PublicKey publicKey;
        std::string b64publickey = generateBase64PublicKey(privateKey, publicKey, CryptoPP::ASN1::secp521r1()); // Generates a signature of 132 bytes

        nlohmann::json j;

        j["ec_public_key"] = b64publickey;

        std::ostringstream out;
        out << j;

        PetalusBlock jblock(out.str(), BlockType::SYS_JSON_BLOCKCHAIN_ACCESS);

        bfile1.write(jblock);
        bfile1.close();

        // Reopen the file and write the block
        auto bfile2 = BlockFile();
        bfile2.open(filename);
        BOOST_CHECK(bfile2.havePublicKey() == true);
        BOOST_CHECK(bfile2.getSignatureLength() == 132);

        std::string data(generateRandomString(2297));
        PetalusBlock block(data);

        Signer signer(privateKey);

        std::string binary_signature;
        uint8_t digest[HASH_BLOCK_SIZE];

        computeSHA256(digest, (const uint8_t*)block.data(), block.length());

        // Sign the sha256 hash of data
        CryptoPP::StringSource s1( digest, HASH_BLOCK_SIZE, true /*pump all*/,
                new CryptoPP::SignerFilter( prng,
                        signer,
                        new CryptoPP::StringSink( binary_signature )
                ) // SignerFilter
        ); // StringSource

        BOOST_CHECK(binary_signature.length() == 132);
        block.signature(binary_signature);

        BOOST_CHECK(bfile2.getTotalBlocks() == 1);
        BOOST_CHECK(bfile2.havePublicKey() == true);
        BOOST_CHECK(bfile2.write(block) > 0);
        BOOST_CHECK(bfile2.getTotalBlocks() == 2);
        BOOST_CHECK(bfile2.verifyBlock(1) == false);
        BOOST_CHECK(bfile2.verifyBlock(2) == true);

        PetalusBlock read_block;

        BOOST_CHECK(bfile2.read(2, read_block) > 0);
        BOOST_CHECK(data.compare(read_block.data()) == 0);

        // Check the size of the block that has been written
        const petalus_block_file_v1 *rblock = bfile2.findBlock(2);
        BOOST_CHECK(rblock != nullptr);
        BOOST_CHECK(rblock->type == static_cast<const uint8_t>(BlockType::USR_PLAIN));
        BOOST_CHECK(rblock->length == read_block.length() + 132);
        BOOST_CHECK(rblock->signature_length == read_block.signature_length()); // the block has been signed
        BOOST_CHECK(rblock->signature_length == 132); // the block has been signed

        bfile2.close();
}

// Sets a public key on a BlockFile with other eliptic curve(secp224r1) and verify evertying works
BOOST_AUTO_TEST_CASE (test30)
{
        std::string filename(generateBlockFileName());
        // remove the file if exists from previous runs
        if (std::filesystem::exists(filename)) std::filesystem::remove(filename);

        auto bfile1 = BlockFile();
        bfile1.open(filename);

        CryptoPP::AutoSeededRandomPool prng;
        PrivateKey privateKey;
        PublicKey publicKey;
        std::string b64publickey = generateBase64PublicKey(privateKey, publicKey, CryptoPP::ASN1::secp224r1()); // Generates a signature of 56 bytes

        nlohmann::json j;

        j["ec_public_key"] = b64publickey;

        std::ostringstream out;
        out << j;

        PetalusBlock jblock(out.str(), BlockType::SYS_JSON_BLOCKCHAIN_ACCESS);

        bfile1.write(jblock);
        bfile1.close();

        // Reopen the file and write the block
        auto bfile2 = BlockFile();
        bfile2.open(filename);
        BOOST_CHECK(bfile2.havePublicKey() == true);
        BOOST_CHECK(bfile2.getSignatureLength() == 56);

        std::string data(generateRandomString(2213));
        PetalusBlock block(data);

        Signer signer(privateKey);

        std::string binary_signature;
        uint8_t digest[HASH_BLOCK_SIZE];

        computeSHA256(digest, (const uint8_t*)block.data(), block.length());

        // Sign the sha256 hash of data
        CryptoPP::StringSource s1( digest, HASH_BLOCK_SIZE, true /*pump all*/,
                new CryptoPP::SignerFilter( prng,
                        signer,
                        new CryptoPP::StringSink( binary_signature )
                ) // SignerFilter
        ); // StringSource

        BOOST_CHECK(binary_signature.length() == 56);
        block.signature(binary_signature);

        BOOST_CHECK(bfile2.getTotalBlocks() == 1);
        BOOST_CHECK(bfile2.havePublicKey() == true);
        BOOST_CHECK(bfile2.write(block) > 0);
        BOOST_CHECK(bfile2.getTotalBlocks() == 2);
        BOOST_CHECK(bfile2.verifyBlock(1) == false);
        BOOST_CHECK(bfile2.verifyBlock(2) == true);

        PetalusBlock read_block;

        BOOST_CHECK(bfile2.read(2, read_block) > 0);
        BOOST_CHECK(data.compare(read_block.data()) == 0);

        // Check the size of the block that has been written
        const petalus_block_file_v1 *rblock = bfile2.findBlock(2);
        BOOST_CHECK(rblock != nullptr);
        BOOST_CHECK(rblock->type == static_cast<const uint8_t>(BlockType::USR_PLAIN));
        BOOST_CHECK(rblock->length == read_block.length() + 56);
        BOOST_CHECK(rblock->signature_length == 56); // the block has been signed

        bfile2.close();

}

// Sets a public key on a BlockFile with other eliptic curve(secp128r2) and verify evertying works
BOOST_AUTO_TEST_CASE (test31)
{
        std::string filename(generateBlockFileName());
        // remove the file if exists from previous runs
        if (std::filesystem::exists(filename)) std::filesystem::remove(filename);

        auto bfile1 = BlockFile();
        bfile1.open(filename);

        CryptoPP::AutoSeededRandomPool prng;
        PrivateKey privateKey;
        PublicKey publicKey;
        std::string b64publickey = generateBase64PublicKey(privateKey, publicKey, CryptoPP::ASN1::secp128r2()); // Generates a signature of 32 bytes

        nlohmann::json j;

        j["ec_public_key"] = b64publickey;

        std::ostringstream out;
        out << j;

        PetalusBlock jblock(out.str(), BlockType::SYS_JSON_BLOCKCHAIN_ACCESS);

        bfile1.write(jblock);
        bfile1.close();

        // Reopen the file and write the block
        auto bfile2 = BlockFile();
        bfile2.open(filename);
        BOOST_CHECK(bfile2.havePublicKey() == true);
        BOOST_CHECK(bfile2.getSignatureLength() == 32);

        std::string data(generateRandomString(2447));
        PetalusBlock block(data);

        Signer signer(privateKey);

        std::string binary_signature;
        uint8_t digest[HASH_BLOCK_SIZE];

        computeSHA256(digest, (const uint8_t*)block.data(), block.length());

        // Sign the sha256 hash of data
        CryptoPP::StringSource s1( digest, HASH_BLOCK_SIZE, true /*pump all*/,
                new CryptoPP::SignerFilter( prng,
                        signer,
                        new CryptoPP::StringSink( binary_signature )
                ) // SignerFilter
        ); // StringSource

        BOOST_CHECK(binary_signature.length() == 32);
        block.signature(binary_signature);

        BOOST_CHECK(bfile2.getTotalBlocks() == 1);
        BOOST_CHECK(bfile2.havePublicKey() == true);
        BOOST_CHECK(bfile2.write(block) > 0);
        BOOST_CHECK(bfile2.getTotalBlocks() == 2);
        BOOST_CHECK(bfile2.verifyBlock(1) == false);
        BOOST_CHECK(bfile2.verifyBlock(2) == true);

        PetalusBlock read_block;

        BOOST_CHECK(bfile2.read(2, read_block) > 0);
        BOOST_CHECK(data.compare(read_block.data()) == 0);

        // Check the size of the block that has been written
        const petalus_block_file_v1 *rblock = bfile2.findBlock(2);
        BOOST_CHECK(rblock != nullptr);
        BOOST_CHECK(rblock->type == static_cast<const uint8_t>(BlockType::USR_PLAIN));
        BOOST_CHECK(rblock->length == read_block.length() + read_block.signature_length());
        BOOST_CHECK(rblock->signature_length == read_block.signature_length()); // the block has been signed

        bfile2.close();
}

BOOST_AUTO_TEST_CASE (test32_read_block_data_hash)
{
        std::string filename(generateBlockFileName());
        // remove the file if exists from previous runs
        if (std::filesystem::exists(filename)) std::filesystem::remove(filename);

	// LUIS
        auto bfile = BlockFile();
        bfile.open(filename);
	std::string data(generateRandomString(1024));

	PetalusBlock block(data);

        uint8_t digest1[HASH_BLOCK_SIZE];
        uint8_t digest2[HASH_BLOCK_SIZE];

        computeSHA256(digest1, (const uint8_t*)data.c_str(), data.length());

	bfile.write(block);

	bfile.readBlockDataHash(1, digest2);

	BOOST_CHECK(std::memcmp(digest1, digest2, HASH_BLOCK_SIZE) == 0);	

	bfile.close();
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(petalus_block_file_suite)

BOOST_AUTO_TEST_CASE (test01)
{
	auto bfile = BlockFile();
        std::string filename(generateBlockFileName());

        // remove the file if exists from previous runs
        if (std::filesystem::exists(filename)) std::filesystem::remove(filename);

        bfile.open(filename);

	PetalusBlock block1("This is for the first block");
	PetalusBlock block2("AAAABBBB");
	PetalusBlock block3("XXXXXXXX");

	bfile.write(block1);
	bfile.write(block2);
	bfile.write(block3);

        BOOST_CHECK(bfile.getTotalBlocks() == 3);

        // The first block can not be verified
        BOOST_CHECK(bfile.verifyBlock(1) == false);
        BOOST_CHECK(bfile.verifyBlock(2) == true);
        BOOST_CHECK(bfile.verifyBlock(3) == true);
        BOOST_CHECK(bfile.verifyBlock(4) == false);

	BOOST_CHECK(bfile.getTotalHeaderCorruptions() == 0);
	BOOST_CHECK(bfile.getTotalBlockCorruptions() == 0);
	BOOST_CHECK(bfile.getTotalLengthCorruptions() == 0);

	PetalusBlock block4;

	bfile.read(1, block4);

	BOOST_CHECK(block1.length() == block4.length());
	BOOST_CHECK(block1.type() == block4.type());
	BOOST_CHECK(std::memcmp(block1.data(), block4.data(), block1.length()) == 0);

	bfile.close();
}

BOOST_AUTO_TEST_CASE (test02) // Corrupt the length of one of the records
{
        auto bfile = BlockFile();
        std::string filename(generateBlockFileName());

        // remove the file if exists from previous runs
        if (std::filesystem::exists(filename)) std::filesystem::remove(filename);

        bfile.open(filename);

        PetalusBlock block1("This is for the first block");
        PetalusBlock block2("AAAABBBBYYYYYYYY");
        PetalusBlock block3("XXXXXXXX");
        PetalusBlock block4("WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW");

        bfile.write(block1);
        bfile.write(block2);
        bfile.write(block3);
        bfile.write(block4);

	// Corrupt the length of the second record
        // Write on the third block just one byte for corrupt
	const uint8_t *data = reinterpret_cast<const uint8_t*>("A");
        int32_t offset = sizeof(petalus_header_file);
        offset += block1.length() + sizeof(petalus_block_file_v1);
        offset += block2.length() + sizeof(petalus_block_file_v1);
        offset +=  4 + 2;

        bfile.writeOffset(offset , data, 1);
        BOOST_CHECK(bfile.getTotalBlocks() == 4);

	bfile.close();
	bfile = BlockFile();
	bfile.open(filename);

	// Verify the integrity

	BOOST_CHECK(bfile.getTotalHeaderCorruptions() == 0);
	BOOST_CHECK(bfile.getTotalBlockCorruptions() == 0);
	BOOST_CHECK(bfile.getTotalLengthCorruptions() == 1);

        // The first block can not be verified
        BOOST_CHECK(bfile.verifyBlock(1) == false);
        BOOST_CHECK(bfile.verifyBlock(2) == false);
        BOOST_CHECK(bfile.verifyBlock(3) == false);
        BOOST_CHECK(bfile.verifyBlock(4) == false);

	BOOST_CHECK(bfile.getTotalHeaderCorruptions() == 0);
	BOOST_CHECK(bfile.getTotalBlockCorruptions() == 0);
	BOOST_CHECK(bfile.getTotalLengthCorruptions() == 1);

        bfile.close();
}

BOOST_AUTO_TEST_CASE (test03) // Corrupt the magic token of one of the records
{
        auto bfile1 = BlockFile();
        std::string filename(generateBlockFileName());

        // remove the file if exists from previous runs
        if (std::filesystem::exists(filename)) std::filesystem::remove(filename);

        bfile1.open(filename);

        PetalusBlock block1("This is for the first block");
        PetalusBlock block2("AAAABBBBYYYYYYYY");
        PetalusBlock block3("XXXXXXXX");
        PetalusBlock block4("XXXXXXXXAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");

        bfile1.write(block1);
        bfile1.write(block2);
        bfile1.write(block3);
        bfile1.write(block4);

        // Corrupt the length of the second record
        // Write on the third block just one byte for corrupt
        const uint8_t *data = reinterpret_cast<const uint8_t*>("A");
        int32_t offset = sizeof(petalus_header_file);
        offset += block1.length() + sizeof(petalus_block_file_v1);
        offset +=  3;

        bfile1.writeOffset(offset , data, 1);

        BOOST_CHECK(bfile1.getTotalBlocks() == 4);
	bfile1.close();

	// Open the file that is corrupted again
        auto bfile2 = BlockFile();

        bfile2.open(filename);

	// The first block can not be verified
        BOOST_CHECK(bfile2.verifyBlock(1) == false);
        BOOST_CHECK(bfile2.verifyBlock(2) == false);
        BOOST_CHECK(bfile2.verifyBlock(3) == false);
        BOOST_CHECK(bfile2.verifyBlock(4) == false);

	BOOST_CHECK(bfile2.getTotalHeaderCorruptions() == 0);
	BOOST_CHECK(bfile2.getTotalBlockCorruptions() == 1);
	BOOST_CHECK(bfile2.getTotalLengthCorruptions() == 0);

        bfile2.close();
}

BOOST_AUTO_TEST_CASE (test04) // Verify the link between two files
{
        std::string filename1(generateBlockFileName("part1"));
        std::string filename2(generateBlockFileName("part2"));

        if (std::filesystem::exists(filename1)) std::filesystem::remove(filename1);
        if (std::filesystem::exists(filename2)) std::filesystem::remove(filename2);

        auto bfile1 = BlockFile();

        bfile1.open(filename1);

        PetalusBlock block1("This is for the first block");
        PetalusBlock block2("AAAABBBBYYYYYYYY");
        PetalusBlock block3("XXXXXXXX");
        PetalusBlock block4("XXXXXXXXAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");

        bfile1.write(block1);
        bfile1.write(block2);
        bfile1.write(block3);
        bfile1.write(block4);

        BOOST_CHECK(bfile1.getTotalBlocks() == 4);

	auto bfile2 = BlockFile(bfile1);

        bfile2.open(filename2);

	PetalusBlock block5("YYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY");

	bfile2.write(block5);
	bfile2.write(block5);
	bfile2.write(block5);

        bfile2.close();
        bfile1.close();

	// reopen the files
	auto bfile3 = BlockFile();
	auto bfile4 = BlockFile();

	bfile3.open(filename1);
	bfile4.open(filename2);

	BOOST_CHECK(bfile4.verify(bfile3) == true);
	BOOST_CHECK(bfile3.verify(bfile4) == false);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(json_block_file_suite)

BOOST_AUTO_TEST_CASE (test01)
{
	auto bfile = BlockFile();
        std::string filename(generateBlockFileName());

        // remove the file if exists from previous runs
        if (std::filesystem::exists(filename)) std::filesystem::remove(filename);

        bfile.open(filename);

	nlohmann::json j1, j2, j3;

	j1["parameter1"] = "This is a string";
	j1["parameter2"] = 1000;

	j2["hash1"] = "db235ea5d02262c11c746c5dbb1519515b0515dbaf88cda5ec036413aef5058a";
	j2["hash2"]= "90a90a48e23dcc51ad4a821a301e3440ffeb5e986bd69d7bf347a2ba2da23bd3";
	j2["arg2"] = 1000;

	std::ostringstream out1, out2;

	out1 << j1;
	out2 << j2;

	PetalusBlock block1(out1.str(), BlockType::USR_JSON_STRING);
	PetalusBlock block2(out2.str(), BlockType::USR_JSON_STRING);
	PetalusBlock block3;

	bfile.write(block1);
	bfile.write(block2);
	bfile.write(block3); // This block is empty

        BOOST_CHECK(bfile.getTotalBlocks() == 2);

        BOOST_CHECK(bfile.verifyBlock(1) == false);
        BOOST_CHECK(bfile.verifyBlock(2) == true);

	bfile.close();
}

BOOST_AUTO_TEST_SUITE_END()
	
BOOST_AUTO_TEST_SUITE(aes_block_file_suite)

BOOST_AUTO_TEST_CASE (test01) 
{
        auto bfile1 = BlockFile();
        std::string filename(generateBlockFileName());
	std::string password("zaqwsxcderfvbgty");

	EncryptionKey key;

	PetalusBlock something("hola");
	BOOST_CHECK(something.encrypt(key) == -2);

	key.assign((const uint8_t*)password.c_str(), password.length());

        // remove the file if exists from previous runs
        if (std::filesystem::exists(filename)) std::filesystem::remove(filename);

        PetalusBlock data1("Good morning every one!");
        PetalusBlock data2("Good day");
        PetalusBlock data3("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX*********");

	// Keep a copy of the original
	PetalusBlock block1(data1);
	PetalusBlock block2(data2);
	PetalusBlock block3(data3);

        bfile1.open(filename);

	// The password is set plain with no schema
	BOOST_CHECK(password.compare(key.getDecryptedPassword()) == 0);

        bfile1.setEncryptionKey(key);
	BOOST_CHECK(bfile1.getEncryptionKey() == key);

	BOOST_CHECK(bfile1.write(data1) == data1.length());
	BOOST_CHECK(bfile1.write(data2) == data2.length());
	BOOST_CHECK(bfile1.write(data3) == data3.length());

	// The blocks has been modified and now are encrypted
	BOOST_CHECK(data1.type() == BlockType::USR_CRYPT_PLAIN);
	BOOST_CHECK(data2.type() == BlockType::USR_CRYPT_PLAIN);
	BOOST_CHECK(data3.type() == BlockType::USR_CRYPT_PLAIN);

	bfile1.close();

	// Reopen the file and do some checks
	auto bfile2 = BlockFile();
	bfile2.open(filename);
	bfile2.setEncryptionKey(key);

        BOOST_CHECK(bfile2.verifyBlock(1) == false);
        BOOST_CHECK(bfile2.verifyBlock(2) == true);
        BOOST_CHECK(bfile2.verifyBlock(3) == true);

	BOOST_CHECK(bfile2.getTotalBlocks() == 3);

	// All the blocks should be encrypted
	for (int i = 1; i <= bfile2.getTotalBlocks(); ++i) {
        	const petalus_block_file_v1 *block = bfile2.findBlock(i);
        	BOOST_CHECK(block != nullptr);
		BOOST_CHECK(block->type == static_cast<const uint8_t>(BlockType::USR_CRYPT_PLAIN)); 
	}

	PetalusBlock data;

	bfile2.read(1, data);
	BOOST_CHECK(data == block1);

	bfile2.read(2, data);
	BOOST_CHECK(block2 == data);

	bfile2.read(3, data);
	BOOST_CHECK(block3 == data);

        {
                RedirectOutput out;

                bfile2.showBlock(out.cout, 2);
        }
	bfile2.close();
}

// Verify that is space on the file after the encryption of the data
BOOST_AUTO_TEST_CASE (test02)
{
	CryptoPP::AutoSeededRandomPool prng1;
	CryptoPP::AutoSeededRandomPool prng2;

        auto bfile = BlockFile(sizeof(petalus_header_file) + sizeof(petalus_block_file_v1) + 10); // Just ten bytes are allowed
        std::string filename(generateBlockFileName());

        CryptoPP::SecByteBlock key1(CryptoPP::AES::DEFAULT_KEYLENGTH);
        CryptoPP::SecByteBlock key2 {};

	CryptoPP::OS_GenerateRandomBlock(true, key1, key1.size());

	EncryptionKey ekey1(key1);
	EncryptionKey ekey2(key2);

	BOOST_CHECK(key1 != key2);
	BOOST_CHECK(ekey1 != ekey2);
	
        // remove the file if exists from previous runs
        if (std::filesystem::exists(filename)) std::filesystem::remove(filename);

        PetalusBlock block1("WWWWWWW");
        PetalusBlock block2(block1);
        bfile.open(filename);
        bfile.setEncryptionKey(key1);

	BOOST_CHECK(bfile.getEncryptionKey() == ekey1);
	BOOST_CHECK(bfile.getEncryptionKey() != ekey2);

	// have space because the 7 bytes fit and are not padding due to the encryption
	BOOST_CHECK(bfile.haveSpaceOnFile(block1.length()) == true);

        BOOST_CHECK(bfile.write(block1) == -1); // Can not write the info
	BOOST_CHECK(bfile.getWriteErrorCode() == BlockFileWriteCode::BLOCKFILE_WRITE_NO_SPACE);
	BOOST_CHECK(bfile.getTotalBlocks() == 0);
        BOOST_CHECK(block1.type() == BlockType::USR_CRYPT_PLAIN); // THE block has been changed

	// Now remove the encryption and save again

	bfile.setEncryptionKey(key2);
	BOOST_CHECK(bfile.getEncryptionKey() != key1);
	BOOST_CHECK(bfile.getEncryptionKey() == key2);

	BOOST_CHECK(bfile.write(block2) == block2.length());
        BOOST_CHECK(block2.type() == BlockType::USR_PLAIN);
	BOOST_CHECK(bfile.getTotalBlocks() == 1);

	// the block is plain
	PetalusBlock read_block;

        const petalus_block_file_v1 *block = bfile.findBlock(1);
        BOOST_CHECK(block != nullptr);
        BOOST_CHECK(block->type == static_cast<const uint8_t>(BlockType::USR_PLAIN));

        bfile.read(1, read_block);
        BOOST_CHECK(read_block == block2);

        bfile.close();
}

// Verify that data is only read by the correct key
BOOST_AUTO_TEST_CASE (test03)
{
        auto bfile = BlockFile(); 
        std::string filename(generateBlockFileName());
        std::string password1("aaaaaaaCCdafedce");
        std::string password2("AaaaaaaCCdafedce");

        EncryptionKey key1;
        EncryptionKey key2;

        key1.assign((const uint8_t*)password1.data(), password1.length());

        key2.assign((const uint8_t*)password2.data(), password2.length());

        // remove the file if exists from previous runs
        if (std::filesystem::exists(filename)) std::filesystem::remove(filename);

        PetalusBlock block1("This data is very good for the test");
        PetalusBlock block2(block1);
        bfile.open(filename);
        bfile.setEncryptionKey(key1);

        BOOST_CHECK(bfile.write(block1) > 0);
	BOOST_CHECK(bfile.getTotalBlocks() == 1);

	// The original block has been changed
	BOOST_CHECK(block1.type() == BlockType::USR_CRYPT_PLAIN);

	// Now check the encryption key and read the block
	bfile.setEncryptionKey(key2);
	PetalusBlock read_block;

        bfile.read(1, read_block);
        BOOST_CHECK(read_block != block2);

	// Put back the correct key
	bfile.setEncryptionKey(key1);

        bfile.read(1, read_block);
        BOOST_CHECK(read_block == block2);

        bfile.close();
}

// Verify that plain and crypt data coexists with no issues
BOOST_AUTO_TEST_CASE (test04)
{
        auto bfile1 = BlockFile(); 
        std::string filename(generateBlockFileName());
	std::string password("aaaaaaaaaaaaaaaaAAAAAAAAAAAAAAAA");
	EncryptionKey empty_key; 

        // remove the file if exists from previous runs
        if (std::filesystem::exists(filename)) std::filesystem::remove(filename);

	std::string data1("Some where over the rainbow......");
	std::string data2("ACDC");
        PetalusBlock block1(data1);
        PetalusBlock block2(data2);
        PetalusBlock block3("Warm regards");
        PetalusBlock block4("For the love of God (Steve vai)");

        bfile1.open(filename);

	EncryptionKey key;

	BOOST_CHECK(empty_key.key().empty() == true);
	BOOST_CHECK(empty_key.key().size() == 0);
	BOOST_CHECK(key.key().empty() == true);
	BOOST_CHECK(key.key().size() == 0);

	key.assign((const uint8_t*)password.data(), password.length());

	BOOST_CHECK(key.key().size() == password.length());
	BOOST_CHECK(key.key().empty() == false);

	bfile1.setEncryptionKey(key);

        BOOST_CHECK(bfile1.write(block1) >= block1.length());
        BOOST_CHECK(bfile1.write(block2) >= block2.length());

	BOOST_CHECK(block1.type() == BlockType::USR_CRYPT_PLAIN);
	BOOST_CHECK(block2.type() == BlockType::USR_CRYPT_PLAIN);

	// unset the key
	bfile1.setEncryptionKey(empty_key);

	BOOST_CHECK(bfile1.write(block3) == block3.length());
        BOOST_CHECK(bfile1.write(block4) == block4.length());
	BOOST_CHECK(block3.type() == BlockType::USR_PLAIN);
	BOOST_CHECK(block4.type() == BlockType::USR_PLAIN);
	BOOST_CHECK(bfile1.getTotalBlocks() == 4);

	bfile1.close();

	BOOST_CHECK(key.key().size() == password.length());
	BOOST_CHECK(key.key().empty() == false);

	// Open the file with a new instance
        auto bfile2 = BlockFile(); 
	bfile2.open(filename);

	BOOST_CHECK(bfile2.getEncryptionKey() == empty_key);
	BOOST_CHECK(bfile2.getEncryptionKey().key().empty() == true);

        const petalus_block_file_v1 *block = bfile2.findBlock(1);
        BOOST_CHECK(block != nullptr);
	BOOST_CHECK(block->type == static_cast<const uint8_t>(BlockType::USR_CRYPT_PLAIN)); 

        block = bfile2.findBlock(2);
        BOOST_CHECK(block != nullptr);
	BOOST_CHECK(block->type == static_cast<const uint8_t>(BlockType::USR_CRYPT_PLAIN)); 

        block = bfile2.findBlock(3);
        BOOST_CHECK(block != nullptr);
	BOOST_CHECK(block->type == static_cast<const uint8_t>(BlockType::USR_PLAIN));

        block = bfile2.findBlock(4);
        BOOST_CHECK(block != nullptr);
	BOOST_CHECK(block->type == static_cast<const uint8_t>(BlockType::USR_PLAIN));

	// Verify that without the key the data read is not correct for the first two blocks
	PetalusBlock read_block;
	
	bfile2.read(1, read_block);
        BOOST_CHECK(data1.compare(read_block.data()) != 0);
	bfile2.read(2, read_block);
        BOOST_CHECK(data2.compare(read_block.data()) != 0);
	bfile2.read(3, read_block);
        BOOST_CHECK(read_block == block3);
	bfile2.read(4, read_block);
        BOOST_CHECK(read_block == block4);

	// Put the correct key and read the blocks again
        bfile2.setEncryptionKey(key);
	BOOST_CHECK(bfile2.getEncryptionKey().key().empty() == false);

	bfile2.read(1, read_block);
        BOOST_CHECK(data1.compare(read_block.data()) == 0);
	bfile2.read(2, read_block);
        BOOST_CHECK(data2.compare(read_block.data()) == 0);
	bfile2.read(3, read_block);
        BOOST_CHECK(block3 == read_block);
	bfile2.read(4, read_block);
        BOOST_CHECK(block4 == read_block);

	bfile2.close();
}

// Verify that a wrong password length can not be set 
BOOST_AUTO_TEST_CASE (test05)
{
        auto bfile = BlockFile();
        std::string filename(generateBlockFileName());
	std::string short_password("aaaaaaaaaaaaaaaa");
        EncryptionKey key;

        key.assign((const uint8_t*)short_password.data(), short_password.length());

        // remove the file if exists from previous runs
        if (std::filesystem::exists(filename)) std::filesystem::remove(filename);

        PetalusBlock data("Some where over the rainbow......");
        bfile.open(filename);
        bfile.setEncryptionKey(key);
		
        BOOST_CHECK(bfile.write(data) == data.length()); // Data is writen but not encrypted
	bfile.close();
}

BOOST_AUTO_TEST_CASE (test06) // write multiple data on the file
{
        std::string filename(generateBlockFileName());
        // remove the file if exists from previous runs
        if (std::filesystem::exists(filename)) std::filesystem::remove(filename);
	std::string short_password(generateRandomString(29));
       
	EncryptionKey ekey;

	ekey.assign((const uint8_t*)short_password.data(), short_password.length());

        std::vector<std::string> items {
                { generateRandomString(17) },
                { generateRandomString(31) },
                { generateRandomString(53) },
                { generateRandomString(128) },
                { generateRandomString(256) },
                { generateRandomString(512) },
                { generateRandomString(1024) },
                { generateRandomString(2048) },
                { generateRandomString(4096) },
                { generateRandomString(33) },
                { generateRandomString(17) },
                { generateRandomString(77) },
                { generateRandomString(1048576) }
        };
        
	auto bfile = BlockFile(1048576 * 16); // 16 megabite file :)
	bfile.open(filename);
	bfile.setEncryptionKey(ekey);

	for (auto &it: items) {
		PetalusBlock block(it);

		int ret = bfile.write(block);
		BOOST_TEST_MESSAGE( "Writing " << ret << " bytes block length " << it.length() << " errno:" << (int)bfile.getWriteErrorCode());
		BOOST_CHECK(ret >= it.length());
	}

	int n = 1;
	for (auto &it: items) {
		PetalusBlock read_block;

		int ret = bfile.read(n, read_block);
		BOOST_TEST_MESSAGE( "Reading " << ret << " bytes block length " << it.length());

		BOOST_CHECK(ret == it.length());
		BOOST_CHECK(it.compare(read_block.data()) == 0);
		++n;
	}

	bfile.close();
}

// Verify that writen encrypted blocks with public key 
// can be verified with no encryption key after
BOOST_AUTO_TEST_CASE (test07)
{
        std::string filename(generateBlockFileName());
        // remove the file if exists from previous runs
        if (std::filesystem::exists(filename)) std::filesystem::remove(filename);
        EncryptionKey key;
        std::string short_password(generateRandomString(32));

	// plain password
        key.assign((const uint8_t*)short_password.data(), short_password.length());

        auto bfile1 = BlockFile();
        bfile1.open(filename);

        CryptoPP::AutoSeededRandomPool prng;
        PrivateKey privateKey;
        PublicKey publicKey;
        std::string b64publickey = generateBase64PublicKey(privateKey, publicKey); // Generates a signature of 64 bytes

        nlohmann::json j;

        j["ec_public_key"] = b64publickey;

        std::ostringstream out;
        out << j;

        PetalusBlock jblock(out.str(), BlockType::SYS_JSON_BLOCKCHAIN_ACCESS);

        BOOST_CHECK(bfile1.havePublicKey() == false);

        bfile1.write(jblock);
        bfile1.close();

	// Reopen the file and write a encrypted signed block
	std::string data("Good morning");
	PetalusBlock block(data);

        auto bfile2 = BlockFile();
        bfile2.setEncryptionKey(key);
        bfile2.open(filename);
        BOOST_CHECK(bfile2.havePublicKey() == true);

        Signer signer(privateKey);

        std::string binary_signature;
        uint8_t digest[HASH_BLOCK_SIZE];

        computeSHA256(digest, (const uint8_t*)block.data(), block.length());

        // Sign the sha256 hash of data
        CryptoPP::StringSource s1( digest, HASH_BLOCK_SIZE, true /*pump all*/,
        	new CryptoPP::SignerFilter( prng,
                	signer,
                        new CryptoPP::StringSink( binary_signature )
                ) // SignerFilter
        ); // StringSource

        BOOST_CHECK(binary_signature.length() == 64);
        block.signature(binary_signature);
        BOOST_CHECK(bfile2.write(block) > 0);
        BOOST_CHECK(bfile2.getTotalBlocks() == 2);

        BOOST_CHECK(bfile2.verifyBlock(2) == true);
        BOOST_CHECK(bfile2.verifySignatureBlock(2) == true); 

        bfile2.close();

        // Open again the file but without encryption key
        auto bfile3 = BlockFile();
        bfile3.open(filename);
        BOOST_CHECK(bfile3.havePublicKey() == true);
        BOOST_CHECK(bfile3.getTotalBlocks() == 2);
        BOOST_CHECK(bfile3.verifyBlock(1) == false);
        BOOST_CHECK(bfile3.verifyBlock(2) == true);
        BOOST_CHECK(bfile3.verifySignatureBlock(2) == false); // needs the key

        bfile3.setEncryptionKey(key);

        BOOST_CHECK(bfile3.verifySignatureBlock(2) == true); // now the content is verified
 
        bfile3.close();
}

// Verify that writen encrypted blocks with public key 
// can be verified with no encryption key after
BOOST_AUTO_TEST_CASE (test08)
{
        std::string filename(generateBlockFileName());
        // remove the file if exists from previous runs
        if (std::filesystem::exists(filename)) std::filesystem::remove(filename);
        EncryptionKey key;
        std::string short_password(generateRandomString(32));

        key.assign((const uint8_t*)short_password.data(), short_password.length());

        std::vector<std::string> items {
                { generateRandomString(17) },
                { generateRandomString(37) },
                { generateRandomString(1033) },
                { generateRandomString(4448) },
                { generateRandomString(31) }
        };

        auto bfile1 = BlockFile();
        bfile1.open(filename);

        CryptoPP::AutoSeededRandomPool prng;
        PrivateKey privateKey;
        PublicKey publicKey;
        std::string b64publickey = generateBase64PublicKey(privateKey, publicKey); // Generates a signature of 64 bytes

        nlohmann::json j;

        j["ec_public_key"] = b64publickey;

        std::ostringstream out;
        out << j;

        PetalusBlock jblock(out.str(), BlockType::SYS_JSON_BLOCKCHAIN_ACCESS);

	BOOST_CHECK(bfile1.havePublicKey() == false);

        bfile1.write(jblock);
        bfile1.close();

	// Now reopen the file with encryption and write the blocks
        // Reopen the file and write the block
        auto bfile2 = BlockFile();
        bfile2.setEncryptionKey(key);
	bfile2.open(filename);
	BOOST_CHECK(bfile2.havePublicKey() == true);

        Signer signer(privateKey);

	for (auto &it: items) {
        	std::string binary_signature;
        	uint8_t digest[HASH_BLOCK_SIZE];
		PetalusBlock block(it);

        	computeSHA256(digest, (const uint8_t*)block.data(), block.length());

        	// Sign the sha256 hash of data
        	CryptoPP::StringSource s1( digest, HASH_BLOCK_SIZE, true /*pump all*/,
                	new CryptoPP::SignerFilter( prng,
                        	signer,
                        	new CryptoPP::StringSink( binary_signature )
                	) // SignerFilter
        	); // StringSource

        	BOOST_CHECK(binary_signature.length() == 64);
        	block.signature(binary_signature);
        	BOOST_CHECK(bfile2.write(block) > 0);
	}
        BOOST_CHECK(bfile2.getTotalBlocks() == 1 + items.size());
        bfile2.close();

	// Open again the file but without encryption key
        auto bfile3 = BlockFile();
	bfile3.open(filename);
	BOOST_CHECK(bfile3.havePublicKey() == true);

        BOOST_CHECK(bfile3.verifyBlock(1) == false);
        BOOST_CHECK(bfile3.verifyBlock(2) == true);
        BOOST_CHECK(bfile3.verifySignatureBlock(2) == false); // needs the key
        BOOST_CHECK(bfile3.verifyBlock(3) == true);
        BOOST_CHECK(bfile3.verifySignatureBlock(3) == false); // needs the key
        BOOST_CHECK(bfile3.verifyBlock(4) == true);
        BOOST_CHECK(bfile3.verifySignatureBlock(4) == false); // needs the key
        BOOST_CHECK(bfile3.verifyBlock(5) == true);
        BOOST_CHECK(bfile3.verifySignatureBlock(5) == false); // needs the key
        BOOST_CHECK(bfile3.verifyBlock(6) == true);
        BOOST_CHECK(bfile3.verifySignatureBlock(6) == false); // needs the key

        bfile3.setEncryptionKey(key);

        BOOST_CHECK(bfile3.verifySignatureBlock(2) == true); // now works
        BOOST_CHECK(bfile3.verifySignatureBlock(3) == true); // now works
        BOOST_CHECK(bfile3.verifySignatureBlock(4) == true); // now works
        BOOST_CHECK(bfile3.verifySignatureBlock(5) == true); // now works
        BOOST_CHECK(bfile3.verifySignatureBlock(6) == true); // now works

	bfile3.close();
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(blowfish_block_file_suite)

BOOST_AUTO_TEST_CASE (test01_sanity)
{
        auto bfile1 = BlockFile();
        std::string filename(generateBlockFileName());
        std::string password("aaaaaaaaaaaaaaaa");
	std::string bencpass(generateBlowfishPassword(password));
	std::string aencpass(generateAESPassword(password));

        EncryptionKey key1;

	BOOST_CHECK(key1.password(bencpass) == true);
	BOOST_CHECK(key1.type() == EncryptionKeyType::BLOWFISH);

        // remove the file if exists from previous runs
        if (std::filesystem::exists(filename)) std::filesystem::remove(filename);

        PetalusBlock data1("Good morning every one!");
        PetalusBlock data2("Good day");
        PetalusBlock data3("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX*********");

        // Keep a copy of the original
        PetalusBlock block1(data1);
        PetalusBlock block2(data2);
        PetalusBlock block3(data3);

        bfile1.open(filename);

        bfile1.setEncryptionKey(key1);
        BOOST_CHECK(bfile1.getEncryptionKey() == key1);

        BOOST_CHECK(bfile1.write(data1) == data1.length());
        BOOST_CHECK(bfile1.write(data2) == data2.length());
        BOOST_CHECK(bfile1.write(data3) == data3.length());

        // The blocks has been modified and now are encrypted
        BOOST_CHECK(data1.type() == BlockType::USR_CRYPT_PLAIN);
        BOOST_CHECK(data2.type() == BlockType::USR_CRYPT_PLAIN);
        BOOST_CHECK(data3.type() == BlockType::USR_CRYPT_PLAIN);

        bfile1.close();

        // Reopen the file and do some checks
        auto bfile2 = BlockFile();
        bfile2.open(filename);
        bfile2.setEncryptionKey(key1);

        BOOST_CHECK(bfile2.verifyBlock(1) == false);
        BOOST_CHECK(bfile2.verifyBlock(2) == true);
        BOOST_CHECK(bfile2.verifyBlock(3) == true);

        BOOST_CHECK(bfile2.getTotalBlocks() == 3);

        // All the blocks should be encrypted with blowfish
        for (int i = 1; i <= bfile2.getTotalBlocks(); ++i) {
                const petalus_block_file_v1 *block = bfile2.findBlock(i);
                BOOST_CHECK(block != nullptr);
                BOOST_CHECK(block->type == static_cast<const uint8_t>(BlockType::USR_CRYPT_PLAIN));
        }

        PetalusBlock read_block;

        bfile2.read(1, read_block);
        BOOST_CHECK(read_block == block1);

        bfile2.read(2, read_block);
        BOOST_CHECK(read_block == block2);

        bfile2.read(3, read_block);
        BOOST_CHECK(read_block == block3);

        {
                RedirectOutput out;

                bfile2.showBlock(out.cout, 2);
        }
        bfile2.close();

	// Open with an aes password
        EncryptionKey key2;

        BOOST_CHECK(key2.password(aencpass) == true);
        BOOST_CHECK(key2.type() == EncryptionKeyType::AES);

        // Reopen the file and do some checks
        auto bfile3 = BlockFile();
        bfile3.open(filename);
        bfile3.setEncryptionKey(key2);

	bfile3.read(1, read_block);
        BOOST_CHECK(read_block != block1);

	bfile3.read(2, read_block);
        BOOST_CHECK(read_block != block2);

	bfile3.read(3, read_block);
        BOOST_CHECK(read_block != block3);

	bfile3.close();
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(file_block_file_suite)

BOOST_AUTO_TEST_CASE (test01)
{
        auto bfile = BlockFile();
        std::string filename(generateBlockFileName());
	std::string temp_filename("temporary_file_test01.tmp");

        // remove the file if exists from previous runs
        if (std::filesystem::exists(filename)) std::filesystem::remove(filename);
        if (std::filesystem::exists(temp_filename)) std::filesystem::remove(temp_filename);

	std::ofstream f(temp_filename);

	f << "Good morning every one!" << std::endl;
	f << "Im now on a file";

	f.close();

	std::ifstream t(temp_filename);
	std::stringstream buffer;
	buffer << t.rdbuf();
	t.close();

        bfile.open(filename);

	PetalusBlock block(buffer.str());

	block.type(BlockType::USR_BINARY_FILE);

        BOOST_CHECK(bfile.write(block) == block.length());
        BOOST_CHECK(bfile.getTotalBlocks() == 1);

        const petalus_block_file_v1 *rblock = bfile.findBlock(1);
        BOOST_CHECK(rblock != nullptr);
        BOOST_CHECK(rblock->type == static_cast<const uint8_t>(BlockType::USR_BINARY_FILE));
        BOOST_CHECK(rblock->signature_length == 0);
        BOOST_CHECK(rblock->length == block.length());

        bfile.close();
}

BOOST_AUTO_TEST_CASE (test02) // Verify the encryption of files on the blockchain
{
        std::string filename(generateBlockFileName());
        std::string temp_filename1("temporary_file1_test02.tmp");
        std::string temp_filename2("temporary_file2_test02.tmp");
	std::string password("1234567890qazxsw");
        EncryptionKey key;

        key.assign((const uint8_t*)password.data(), password.length());

        // remove the file if exists from previous runs
        if (std::filesystem::exists(filename)) std::filesystem::remove(filename);
        if (std::filesystem::exists(temp_filename1)) std::filesystem::remove(temp_filename1);
        if (std::filesystem::exists(temp_filename2)) std::filesystem::remove(temp_filename2);

        std::ofstream f(temp_filename1);

	for (int i = 0; i < 100; ++i ) {
		std::string data(generateRandomString(255));
		f << data << std::endl;
	}

        f.close();

	// Now open the generated file on the block chain
        std::ifstream t1(temp_filename1);
        std::stringstream buffer;
        buffer << t1.rdbuf();
        t1.close();

        auto bfile1 = BlockFile();
        bfile1.open(filename);

	bfile1.setEncryptionKey(key);

        PetalusBlock fblock(buffer.str(), BlockType::USR_BINARY_FILE);

        BOOST_CHECK(bfile1.write(fblock) >= fblock.length());
        BOOST_CHECK(bfile1.getTotalBlocks() == 1);
	
        const petalus_block_file_v1 *block = bfile1.findBlock(1);
        BOOST_CHECK(block != nullptr);
        BOOST_CHECK(block->type == static_cast<const uint8_t>(BlockType::USR_CRYPT_BINARY_FILE));

        bfile1.close();

	// Open a different instance of the blockfile
        auto bfile2 = BlockFile();
        bfile2.open(filename);

	// Set the password of the file
	bfile2.setEncryptionKey(key);

	PetalusBlock read_block;

	bfile2.read(1, read_block);
	BOOST_CHECK(read_block.length() <= fblock.length());
	BOOST_CHECK(read_block.type() == BlockType::USR_BINARY_FILE);
	
        std::ofstream t2(temp_filename2);

	t2 << read_block.data();
        t2.close();

	bfile2.close();

	// Both temp files should have the same sha256
        std::string digest1;
        std::string digest2;
        CryptoPP::SHA256 hash1;
        CryptoPP::SHA256 hash2;

        CryptoPP::FileSource ff1(temp_filename1.c_str(), true,
                new CryptoPP::HashFilter(hash1, new CryptoPP::HexEncoder(new CryptoPP::StringSink(digest1))));

        CryptoPP::FileSource ff2(temp_filename2.c_str(), true,
                new CryptoPP::HashFilter(hash2, new CryptoPP::HexEncoder(new CryptoPP::StringSink(digest2))));

	BOOST_CHECK(digest1.compare(digest2) == 0);
}

BOOST_AUTO_TEST_SUITE_END()

// Suite for test cases related to BlockType::USR_SOURCE_CRYPT
BOOST_AUTO_TEST_SUITE(user_crypt_block_file_suite)

BOOST_AUTO_TEST_CASE (test01_sanity)
{
        auto bfile = BlockFile();
        std::string filename(generateBlockFileName());

        // remove the file if exists from previous runs
        if (std::filesystem::exists(filename)) std::filesystem::remove(filename);

        PetalusBlock block("This should be encrypted by the user on the source of the data");

	block.type(BlockType::USR_SOURCE_CRYPT);

        bfile.open(filename);

        BOOST_CHECK(bfile.write(block) == block.length());

	PetalusBlock read_block;

        BOOST_CHECK(bfile.getTotalBlocks() == 1);
	BOOST_CHECK(bfile.read(1, read_block) > 0);
	BOOST_CHECK(read_block.type() == BlockType::USR_SOURCE_CRYPT);
	BOOST_CHECK(read_block == block);
        bfile.close();
}


// The file have encryption but this block should go without it
BOOST_AUTO_TEST_CASE (test02_encrypt_file)
{
        std::string filename(generateBlockFileName());
        // remove the file if exists from previous runs
        if (std::filesystem::exists(filename)) std::filesystem::remove(filename);

        auto bfile1 = BlockFile();
        std::string password("1234567890qazxsw");
        EncryptionKey key;

        key.assign((const uint8_t*)password.data(), password.length());

	std::string data1(generateRandomString(2081));
	std::string data2(generateRandomString(4799));

	PetalusBlock block1(data1);
	PetalusBlock block2(data2, BlockType::USR_SOURCE_CRYPT); 

	bfile1.setEncryptionKey(key);
	bfile1.open(filename);

	BOOST_CHECK(bfile1.write(block1) > 0);
	BOOST_CHECK(bfile1.write(block2) > 0);

	bfile1.close();

	// Open and check without the key
	auto bfile2 = BlockFile();
	bfile2.open(filename);

	PetalusBlock read_block;

	BOOST_CHECK(bfile2.read(1, read_block) > 0);
	BOOST_CHECK(read_block.type() == BlockType::USR_CRYPT_PLAIN);
	BOOST_CHECK(data1.compare(read_block.data()) != 0);

	BOOST_CHECK(bfile2.read(2, read_block) > 0);
	BOOST_CHECK(read_block.type() == BlockType::USR_SOURCE_CRYPT);
	BOOST_CHECK(data2.compare(read_block.data()) == 0);

	bfile2.close();
}

// Write a user encrypt block with signature on it
BOOST_AUTO_TEST_CASE (test03)
{
        std::string filename(generateBlockFileName());
        // remove the file if exists from previous runs
        if (std::filesystem::exists(filename)) std::filesystem::remove(filename);

        std::string password("1234567890qazxsw");
        EncryptionKey key;

        key.assign((const uint8_t*)password.data(), password.length());

        CryptoPP::AutoSeededRandomPool prng;
        PrivateKey privateKey;
        PublicKey publicKey;
        std::string b64publickey = generateBase64PublicKey(privateKey, publicKey); // Generates a signature of 64 bytes

        nlohmann::json j;

        j["ec_public_key"] = b64publickey;

        std::ostringstream out;
        out << j;

        PetalusBlock jblock(out.str(), BlockType::SYS_JSON_BLOCKCHAIN_ACCESS);

	auto bfile1 = BlockFile();
	bfile1.open(filename);
	BOOST_CHECK(bfile1.getTotalBlocks() == 0);
        bfile1.write(jblock);
	BOOST_CHECK(bfile1.getTotalBlocks() == 1);
        bfile1.close();

        // Reopen the file and write a user encrypted signed block
        std::string data(generateRandomString(8863));
        PetalusBlock block(data, BlockType::USR_SOURCE_CRYPT);

        auto bfile2 = BlockFile();
        bfile2.setEncryptionKey(key);
        bfile2.open(filename);
        BOOST_CHECK(bfile2.havePublicKey() == true);
	BOOST_CHECK(bfile2.getTotalBlocks() == 1);

        Signer signer(privateKey);

        std::string binary_signature;
        uint8_t digest[HASH_BLOCK_SIZE];

        computeSHA256(digest, (const uint8_t*)block.data(), block.length());

        // Sign the sha256 hash of data
        CryptoPP::StringSource s1( digest, HASH_BLOCK_SIZE, true /*pump all*/,
                new CryptoPP::SignerFilter( prng,
                        signer,
                        new CryptoPP::StringSink( binary_signature )
                ) // SignerFilter
        ); // StringSource

        BOOST_CHECK(binary_signature.length() == 64);
        block.signature(binary_signature);

	BOOST_CHECK(bfile2.write(block) > 0);
	BOOST_CHECK(bfile2.getTotalBlocks() == 2);
	bfile2.close();

	// Reopen the file and to the checks
	auto bfile3 = BlockFile();
	bfile3.open(filename);
	
	BOOST_CHECK(bfile3.getTotalBlocks() == 2);

	PetalusBlock read_block;

	BOOST_CHECK(bfile3.read(2, read_block) > 0);
	BOOST_CHECK(read_block.type() == BlockType::USR_SOURCE_CRYPT);
	BOOST_CHECK(data.compare(read_block.data()) == 0);

        const petalus_block_file_v1 *rblock = bfile3.findBlock(2);
        BOOST_CHECK(rblock != nullptr);
        BOOST_CHECK(rblock->type == static_cast<const uint8_t>(BlockType::USR_SOURCE_CRYPT));
	BOOST_CHECK(rblock->signature_length == read_block.signature_length());

	bfile3.close();
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(wallet_suite)

BOOST_AUTO_TEST_CASE (test01_sanity)
{
	// Remove the wallet from previous runs
	std::string wallet(generateHexBlockFilename());
	std::filesystem::path tpath(generateTestPath(wallet));

	std::filesystem::path path1(generateTestFile(wallet));
	std::filesystem::path path2(path1);
	std::filesystem::path path3(path1);
	path2.replace_extension("mblk0001");
	path3.replace_extension("mblk0002");
	
        if (std::filesystem::exists(path1)) std::filesystem::remove(path1);
        if (std::filesystem::exists(path2)) std::filesystem::remove(path2);
        if (std::filesystem::exists(path3)) std::filesystem::remove(path3);

	auto wa1 = Wallet(wallet);

	// The wallet is not open so no error code
	BOOST_CHECK(wa1.getWriteErrorCode() == BlockFileWriteCode::BLOCKFILE_WRITE_FILE_NOT_OPEN); 

	// Set a default max size for the file
	wa1.setMaxBlockFileSize(32 * boost::iostreams::mapped_file::alignment());
	wa1.open(tpath, true);

	BOOST_CHECK(wa1.getTotalBytes() == 0);
	BOOST_CHECK(wa1.getTotalBlocks() == 0);
	
	auto p = wa1.getBlockFile()->getPath();

	std::string extension(".mblk0000");
	BOOST_CHECK(extension.compare(p.extension().c_str()) == 0);

	int32_t bytes = sizeof(petalus_header_file);
	boost::string_ref data("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");

	PetalusBlock block(data);

	for (int i = 0; i < 1000; ++i) {
		bytes += wa1.write(block);
		bytes += wa1.write(block);
	}
	// Write some blocks on the wallet
	BOOST_CHECK(wa1.getTotalBlocks() == 2000);
	BOOST_CHECK(wa1.getTotalBytes() == data.length() * 2000);
	
	extension = ".mblk0001";
	BOOST_CHECK(extension.compare(wa1.getBlockFile()->getPath().extension().c_str()) == 0);
	wa1.close();

	// reopen the wallet and verify that points to the last file generated
	auto wa2 = Wallet(wallet);
	wa2.setMaxBlockFileSize(32 * boost::iostreams::mapped_file::alignment());
	wa2.open(tpath);

	BOOST_CHECK(extension.compare(wa2.getBlockFile()->getPath().extension().c_str()) == 0); // on .mblk00001
	BOOST_CHECK(wa2.getTotalBytes() == data.length() * 2000);
	BOOST_CHECK(wa2.getTotalBlocks() == 2000);
	
	for (int i = 0; i < 1000; ++i) {
                bytes += wa2.write(block);
        }

	// Now a new file as been generated
	extension = ".mblk0002";
	BOOST_CHECK(extension.compare(wa2.getBlockFile()->getPath().extension().c_str()) == 0); // on .mblk00002
	BOOST_CHECK(wa2.getTotalBlocks() == 3000);
	BOOST_CHECK(wa2.getTotalBytes() == data.length() * 3000);
	wa2.close();

	// Reopen again but with no size
	auto wa3 = Wallet(wallet);

	wa3.setMaxBlockFileSize(0);
	wa3.open(tpath);

	BOOST_CHECK(extension.compare(wa3.getBlockFile()->getPath().extension().c_str()) == 0);
	BOOST_CHECK(wa3.getTotalBytes() == data.length() * 3000);
	BOOST_CHECK(wa3.getTotalBlocks() == 3000);

	for (int i = 0; i < 1000; ++i) {
                bytes += wa3.write(block);
        }

	BOOST_CHECK(wa3.getTotalBytes() == data.length() * 4000);
	BOOST_CHECK(wa3.getTotalBlocks() == 4000);

	wa3.close();
}

BOOST_AUTO_TEST_CASE (test02_access_md5) // Access the wallet with md5 password
{
        // Remove the wallet from previous runs
        std::string wallet(generateHexBlockFilename());
        std::filesystem::path path1(generateTestFile(wallet));
	std::string password("some password from the user///");
        if (std::filesystem::exists(path1)) std::filesystem::remove(path1);

        auto wa1 = Wallet(wallet);

        wa1.open(path1, true);

	BOOST_CHECK(wa1.validPassword(password) == false);
	BOOST_CHECK(wa1.validMD5Password(password) == false);
	BOOST_CHECK(wa1.validSHA256Password(password) == false);
        BOOST_CHECK(wa1.getTotalBytes() == 0);
        BOOST_CHECK(wa1.getTotalBlocks() == 0);

	wa1.writeMD5Password(password);

	wa1.close();

	// Reopen the wallet
        auto wa2 = Wallet(wallet);

        wa2.open(path1, true);

	BOOST_CHECK(wa2.validMD5Password(password) == true);
	BOOST_CHECK(wa2.validSHA256Password(password) == false);
        BOOST_CHECK(wa2.getTotalBytes() == 77);
        BOOST_CHECK(wa2.getTotalBlocks() == 1);

        wa2.close();
}

BOOST_AUTO_TEST_CASE (test03_folders_sanity) // verify the folders functionality
{
        // Remove the wallet from previous runs
        std::string wallet(generateHexBlockFilename());
        std::filesystem::path path1(generateTestFile(wallet));
	std::filesystem::path path2(generateTestFile(wallet, 2));

        if (std::filesystem::exists(path1)) std::filesystem::remove(path1);
        if (std::filesystem::exists(path2)) std::filesystem::remove(path2);

	auto wa1 = Wallet(wallet);

	wa1.open(path1, true);

	PetalusBlock pblock1("Some text that should go on a folder");
	PetalusBlock pblock2(generateRandomString(67));
	PetalusBlock pblock3(generateRandomString(131));

	// Should not write anything, that folder is not created
	BOOST_CHECK(wa1.writeOnFolder("pepe", pblock1) == 0);
        BOOST_CHECK(wa1.getTotalFolders() == 0);
        BOOST_CHECK(wa1.getTotalBlocks() == 0);
        BOOST_CHECK(wa1.getTotalBytes() == 0);
	BOOST_CHECK(wa1.getLastBlockNumberWritten() == 0);

	BOOST_CHECK(wa1.write(pblock2) > 0);

        wa1.createFolder("notes");
        BOOST_CHECK(wa1.getTotalFolders() == 1);
        BOOST_CHECK(wa1.getTotalBlocks() == 1);
        BOOST_CHECK(wa1.getTotalBytes() == pblock2.length());

	BOOST_CHECK(wa1.write(pblock3) > 0);

	// Now the wallet is on memory for read and write
	PetalusBlock read_block;

	BOOST_CHECK(wa1.readFromFolder("notes", 1, read_block) > 0);
	BOOST_CHECK(read_block.length() > 0);
	BOOST_CHECK(read_block.type() == BlockType::SYS_FOLDER_NAME); 
	
	BOOST_CHECK(wa1.writeOnFolder("notes", pblock1) > 0);
	BOOST_CHECK(wa1.getLastBlockNumberWritten() == 2);
	BOOST_CHECK(wa1.readFromFolder("notes", 2, read_block) > 0);
	BOOST_CHECK(pblock1 == read_block);

	wa1.close();

	// Now reopen again
	auto wa2 = Wallet(wallet);
	wa2.open(path1, false);
	read_block.data(std::string(""));

	BOOST_CHECK(wa2.getTotalFolders() == 0);
	BOOST_CHECK(wa2.readFromFolder("notes", 2, read_block) > 0);
	BOOST_CHECK(wa2.getTotalFolders() == 1);
	BOOST_CHECK(pblock1 == read_block);
	wa2.close();		

	auto bfile = BlockFile();
	bfile.open(path1);

	BOOST_CHECK(bfile.getTotalBlocks() == 3);
	BOOST_CHECK(bfile.verifyBlock(1) == false);
	BOOST_CHECK(bfile.verifyBlock(2) == true);

	// Now verify the folders

	for (auto &it: bfile.getFolders()) {
		BOOST_CHECK(it.first.compare("notes") == 0);
		std::filesystem::path folder_path = bfile.getPath();

		int32_t block_number = it.second.first;
		folder_path.remove_filename();
		folder_path /= it.second.second;	

		auto bfilef = BlockFile();
		bfilef.open(folder_path);
		BOOST_CHECK(bfilef.getTotalBlocks() == 2);

		// The first block should be verified with the parent block
                // Read the last block hash of the main file
        	uint8_t digest[HASH_BLOCK_SIZE];

                bfile.readBlockHash(block_number, digest);// The definition of the folder block is on the first block

		BOOST_CHECK(bfilef.verifyBlock(1, digest) == true);
		BOOST_CHECK(bfilef.verifyBlock(2) == true);

		bfilef.close();	
	}
	bfile.close();
}

BOOST_AUTO_TEST_CASE (test04_change_blocksfiles) // verify that change the way the blockfiles are manage dont affect
{
        // Remove the wallet from previous runs
        std::string wallet(generateHexBlockFilename());
        std::filesystem::path path1(generateTestFile(wallet));
        std::filesystem::path path2(generateTestFile(wallet, 1));

        std::string password("some password from the user///");
        if (std::filesystem::exists(path1)) std::filesystem::remove(path1);
        if (std::filesystem::exists(path2)) std::filesystem::remove(path2);

        auto wa1 = Wallet(wallet);

       	PetalusBlock pblock1("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");

	int32_t filesize = sizeof(petalus_header_file) + (sizeof(petalus_block_file_v1) + pblock1.length()) * 1000;

        wa1.setMaxBlockFileSize(filesize); // Start with fixed size
        wa1.open(path1, true);

        for (int i = 0 ; i < 1000; ++i) {
        	BOOST_CHECK(wa1.write(pblock1) > 0);
	}
        BOOST_CHECK(wa1.getTotalBlocks() == 1000);
        BOOST_CHECK(wa1.getTotalBytes() == 1000 * pblock1.length());
        BOOST_CHECK(wa1.getLastBlockNumberWritten() == 1000);

	// now we change the filesize to unlimited
	wa1.setMaxBlockFileSize(0);

	// The next write will have no space on it
	BOOST_CHECK(wa1.getBlockFile()->haveSpaceOnFile(pblock1.length()) == false);
	std::string extension(".mblk0000");
	BOOST_CHECK(extension.compare(wa1.getBlockFile()->getPath().extension().c_str()) == 0);

	// Now we write other blocks
	PetalusBlock pblock2("YYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY");	

	for (int i = 0; i < 16 ; ++i) 
       		BOOST_CHECK(wa1.write(pblock2) > 0);

        BOOST_CHECK(wa1.getTotalBlocks() == 1016);
        BOOST_CHECK(wa1.getTotalBytes() == (1000 * pblock1.length()) + (16 * pblock2.length()));
        BOOST_CHECK(wa1.getLastBlockNumberWritten() == 1016);

	// no change of file on the wallet
	BOOST_CHECK(wa1.getBlockFile()->haveSpaceOnFile(pblock1.length()) == true); // There is allways space
	BOOST_CHECK(extension.compare(wa1.getBlockFile()->getPath().extension().c_str()) == 0);
}

BOOST_AUTO_TEST_CASE (test05_password_salt) // Access with same passwords two different user, verify the salt works
{
        // Remove the wallet from previous runs
        std::string wallet1(generateHexBlockFilename("1"));
        std::string wallet2(generateHexBlockFilename("2"));
        std::filesystem::path path1(generateTestFile(wallet1));
        std::filesystem::path path2(generateTestFile(wallet2));
        std::string password("same_password");

        if (std::filesystem::exists(path1)) std::filesystem::remove(path1);
        if (std::filesystem::exists(path2)) std::filesystem::remove(path2);

        auto wa1 = Wallet(wallet1);
        auto wa2 = Wallet(wallet2);

        wa1.open(path1, true);
        wa2.open(path2, true);

        BOOST_CHECK(wa1.validPassword(password) == false);
        BOOST_CHECK(wa1.validMD5Password(password) == false);
        BOOST_CHECK(wa1.validSHA256Password(password) == false);
        BOOST_CHECK(wa1.getTotalBytes() == 0);
        BOOST_CHECK(wa1.getTotalBlocks() == 0);

        BOOST_CHECK(wa2.validPassword(password) == false);
        BOOST_CHECK(wa2.validMD5Password(password) == false);
        BOOST_CHECK(wa2.validSHA256Password(password) == false);
        BOOST_CHECK(wa2.getTotalBytes() == 0);
        BOOST_CHECK(wa2.getTotalBlocks() == 0);

        wa1.writeSHA256Password(password);
        wa2.writeSHA256Password(password);

        wa1.close();
        wa2.close();

        // Reopen the wallets and do the checks
        auto wa3 = Wallet(wallet1);
        auto wa4 = Wallet(wallet2);

        wa3.open(path1);
        wa4.open(path2);

        BOOST_CHECK(wa3.validMD5Password(password) == false);
        BOOST_CHECK(wa3.validSHA256Password(password) == true);
        BOOST_CHECK(wa3.getTotalBytes() == 112);
        BOOST_CHECK(wa3.getTotalBlocks() == 1);

        BOOST_CHECK(wa4.validMD5Password(password) == false);
        BOOST_CHECK(wa4.validSHA256Password(password) == true);
        BOOST_CHECK(wa4.getTotalBytes() == 112);
        BOOST_CHECK(wa4.getTotalBlocks() == 1);
        
	// Verify that the sha256 is different on each wallet

	std::string hash1(wa3.getBlockFile()->getAccessPassword());
	std::string hash2(wa4.getBlockFile()->getAccessPassword());

	BOOST_CHECK(hash1.compare(hash2) != 0);
}

// Access the wallet with password and public key
BOOST_AUTO_TEST_CASE (test06_ec_public_key)
{
        // Remove the wallet from previous runs
        std::string wallet(generateHexBlockFilename());
        std::filesystem::path path1(generateTestFile(wallet));
        std::string password("some password from the user///");
        if (std::filesystem::exists(path1)) std::filesystem::remove(path1);

        auto wa1 = Wallet(wallet);

        wa1.open(path1, true);

        CryptoPP::AutoSeededRandomPool prng;
        PrivateKey privateKey;
        PublicKey publicKey;
        std::string b64publickey = generateBase64PublicKey(privateKey, publicKey, CryptoPP::ASN1::secp112r1()); // Generates a signature of 28 bytes

        BOOST_CHECK(wa1.validPassword(password) == false);
        BOOST_CHECK(wa1.validMD5Password(password) == false);
        BOOST_CHECK(wa1.validSHA256Password(password) == false);
        BOOST_CHECK(wa1.getTotalBytes() == 0);
        BOOST_CHECK(wa1.getTotalBlocks() == 0);

	// By default is sha256
        wa1.writePassword(password, b64publickey);

        BOOST_CHECK(wa1.getTotalBlocks() == 0); // there is no user blocks

	wa1.close();

	// Generate a new wallet session
	auto wa2 = Wallet(wallet);
	wa2.open(path1, false);

        BOOST_CHECK(wa2.getTotalBlocks() == 1); // At least one block
	auto bfile = wa2.getBlockFile();

	BOOST_CHECK(bfile->havePublicKey() == true);
	BOOST_CHECK(bfile->getSignatureLength() == 28);

	// Do a write
        Signer signer(privateKey);

	std::string data(generateRandomString(257));
	std::string binary_signature;
        uint8_t digest[HASH_BLOCK_SIZE];

        computeSHA256(digest, (const uint8_t*)data.c_str(), data.length());

        // Sign the sha256 hash of data
        CryptoPP::StringSource s( digest, HASH_BLOCK_SIZE, true /*pump all*/,
                new CryptoPP::SignerFilter( prng,
                        signer,
                        new CryptoPP::StringSink( binary_signature )
                ) // SignerFilter
        ); // StringSource

	PetalusBlock block(data);
	block.signature(binary_signature);

	// Do the write
	wa2.write(block);

        BOOST_CHECK(wa2.getTotalBlocks() == 2); // At least two blocks
	wa2.close();

	// Reopen the wallet and read the last block
	auto wa3 = Wallet(wallet);
	wa3.open(path1, false);

        BOOST_CHECK(wa3.getTotalBlocks() == 2); 

	PetalusBlock read_block;

        // Read from the wallet
        wa3.read(2, read_block);

        BOOST_CHECK(data.compare(read_block.data()) == 0);

	wa3.close();
}

// Access the wallet with password and public key and all encrypted
// at wallet level
BOOST_AUTO_TEST_CASE (test07_encrypt_sanity)
{
        // Remove the wallet from previous runs
        std::string wallet(generateHexBlockFilename());
        std::filesystem::path path(generateTestFile(wallet));
	std::filesystem::path path1(generateTestFile(wallet, 2));
        std::string user_password("some password from the user///");
        if (std::filesystem::exists(path)) std::filesystem::remove(path);
        if (std::filesystem::exists(path1)) std::filesystem::remove(path1);

	BOOST_TEST_MESSAGE( "wallet:(" << wallet << ")");

        auto wa1 = Wallet(wallet);
	BOOST_CHECK(wa1.is_open() == false);
        wa1.open(path, true);
	BOOST_CHECK(wa1.is_open() == true);

        CryptoPP::AutoSeededRandomPool prng;
        PrivateKey privateKey;
        PublicKey publicKey;
        std::string b64publickey = generateBase64PublicKey(privateKey, publicKey, CryptoPP::ASN1::secp256k1()); // Generates a signature of 64 bytes

        // By default is sha256
        wa1.writePassword(user_password, b64publickey);
        wa1.close();

	// Create a password for the new instance of the wallet
        std::string password("zaqwsxcderfvbgty");
        EncryptionKey key;

        key.assign((const uint8_t*)password.data(), password.length());
        
	auto wa2 = Wallet(wallet);
	wa2.setEncryptionKey(key);
        wa2.open(path);

        BOOST_CHECK(wa2.getTotalFolders() == 0);
	// Create a folder
        wa2.createFolder("bills");

        BOOST_CHECK(wa2.getTotalFolders() == 1);

	// Generate the signature for the first block 
        Signer signer(privateKey);
	std::string data1("Good morning");
	PetalusBlock block1(data1);

        std::string binary_signature;
        uint8_t digest[HASH_BLOCK_SIZE];

        computeSHA256(digest, (const uint8_t*)data1.c_str(), data1.length());

        // Sign the sha256 hash of data
        CryptoPP::StringSource s1( digest, HASH_BLOCK_SIZE, true /*pump all*/,
                new CryptoPP::SignerFilter( prng,
                        signer,
                        new CryptoPP::StringSink( binary_signature )
                ) // SignerFilter
        ); // StringSource

        {
                CryptoPP::HexEncoder encoder;
                std::string signature_hash;
                encoder.Attach( new CryptoPP::StringSink(signature_hash) );
                encoder.Put( (uint8_t*)binary_signature.c_str(), binary_signature.length() );
                encoder.MessageEnd();

                BOOST_TEST_MESSAGE( "signature1:(" << signature_hash << ")");
        }

        // Set the signature on the first block
        block1.signature(binary_signature);
	BOOST_CHECK(wa2.write(block1) > 0); // Write the first block

	// Read also the block that has been written
	PetalusBlock read_block;

        BOOST_CHECK(wa2.read(3, read_block) > 0);
        BOOST_CHECK(data1.compare(read_block.data()) == 0);

        // Generate the signature for the second block
	std::string data2("O nooooo another bill :( that goes on the bill folder");
	PetalusBlock block2(data2);

	// Clean the string :)
	binary_signature.clear();

        computeSHA256(digest, (const uint8_t*)data2.c_str(), data2.length());

        // Sign the sha256 hash of data
        CryptoPP::StringSource s2( digest, HASH_BLOCK_SIZE, true /*pump all*/,
                new CryptoPP::SignerFilter( prng,
                        signer,
                        new CryptoPP::StringSink( binary_signature )
                ) // SignerFilter
        ); // StringSource

        {
                CryptoPP::HexEncoder encoder;
                std::string signature_hash;
                encoder.Attach( new CryptoPP::StringSink(signature_hash) );
                encoder.Put( (uint8_t*)binary_signature.c_str(), binary_signature.length() );
                encoder.MessageEnd();

                BOOST_TEST_MESSAGE( "signature2:(" << signature_hash << ")");
        }

        // Set the signature on the second block
        block2.signature(binary_signature);
        BOOST_CHECK(wa2.writeOnFolder("bills", block2) > 0);
	BOOST_CHECK((int) wa2.getWriteErrorCode() == 0);

        // Read the blocks and verify that are correct 
	BOOST_CHECK(wa2.readFromFolder("bills", 1, read_block) > 0);
	BOOST_CHECK(read_block.length() > 0);
	BOOST_CHECK(read_block.type() == BlockType::SYS_FOLDER_NAME); 
	BOOST_CHECK(wa2.readFromFolder("bills", 2, read_block) > 0);
	BOOST_CHECK(read_block.length() > 0);
	BOOST_CHECK(read_block.type() == BlockType::USR_PLAIN); 
	BOOST_CHECK(data2.compare(read_block.data()) == 0);

	wa2.close();
}

// Access the wallet with password and public key and all encrypted
// at wallet level and write on folders
BOOST_AUTO_TEST_CASE (test08_encrypt_sanity)
{
        // Remove the wallet from previous runs
        std::string wallet(generateHexBlockFilename());
        std::filesystem::path path(generateTestFile(wallet));
	std::filesystem::path path1(generateTestFile(wallet, 2));
	std::filesystem::path path2(generateTestFile(wallet, 3));

        std::string user_password("some password from the user///");
        if (std::filesystem::exists(path)) std::filesystem::remove(path);
        if (std::filesystem::exists(path1)) std::filesystem::remove(path1);
        if (std::filesystem::exists(path2)) std::filesystem::remove(path2);

        BOOST_TEST_MESSAGE( "wallet:(" << wallet << ")");

        auto wa1 = Wallet(wallet);
        wa1.open(path, true);

        CryptoPP::AutoSeededRandomPool prng;
        PrivateKey privateKey;
        PublicKey publicKey;
        std::string b64publickey = generateBase64PublicKey(privateKey, publicKey); // Generates a signature of 64 bytes

        // By default is sha256
        wa1.writePassword(user_password, b64publickey);

        BOOST_CHECK(wa1.getTotalFolders() == 0);

        // Create two folders
        wa1.createFolder("bills");
        wa1.createFolder("notes");

        BOOST_CHECK(wa1.getTotalFolders() == 2);
        wa1.close();

        // Create a password for the new instance of the wallet
        std::string password("zaqwsxcderfvbgty");
        EncryptionKey key;

        key.assign((const uint8_t*)password.data(), password.length());

        auto wa2 = Wallet(wallet);
        wa2.setEncryptionKey(key);
        wa2.open(path);

	// Folders are loaded on demand
        BOOST_CHECK(wa2.getTotalFolders() == 0);

        // Generate the signature for the first block
        Signer signer(privateKey);
        std::string data1("Good morning, this data goes to one folder");
        std::string data2(generateRandomString(1109));
        PetalusBlock block1(data1);
        PetalusBlock block2(data2);

        std::string binary_signature1;
        std::string binary_signature2;
        uint8_t digest[HASH_BLOCK_SIZE];

        computeSHA256(digest, (const uint8_t*)data1.c_str(), data1.length());

        // Sign the sha256 hash of data1
        CryptoPP::StringSource s1( digest, HASH_BLOCK_SIZE, true /*pump all*/,
                new CryptoPP::SignerFilter( prng,
                        signer,
                        new CryptoPP::StringSink( binary_signature1 )
                ) // SignerFilter
        ); // StringSource

        computeSHA256(digest, (const uint8_t*)data2.c_str(), data2.length());

        // Sign the sha256 hash of data2
        CryptoPP::StringSource s2( digest, HASH_BLOCK_SIZE, true /*pump all*/,
                new CryptoPP::SignerFilter( prng,
                        signer,
                        new CryptoPP::StringSink( binary_signature2 )
                ) // SignerFilter
	);

        // Set the signature on the first block
        block1.signature(binary_signature1);
        block2.signature(binary_signature2);

        BOOST_CHECK(wa2.writeOnFolder("notes", block1) > 0); // Write the first block
        BOOST_CHECK(wa2.writeOnFolder("bills", block2) > 0); // Write the first block

	// Folders are loaded on demand
        BOOST_CHECK(wa2.getTotalFolders() == 2);

	// Verify the content of the folder files
	auto fo = wa2.getFolders();

	auto bfile_notes = fo["notes"];
	auto bfile_bills = fo["bills"];

	BOOST_CHECK(bfile_notes->getSignatureLength() == 64);

        const petalus_block_file_v1 *rblock = bfile_notes->findBlock(2);
        BOOST_CHECK(rblock != nullptr);
	BOOST_CHECK(rblock->signature_length > 0); // Signed
	BOOST_CHECK(rblock->type == (uint16_t)BlockType::USR_CRYPT_PLAIN);

	rblock = bfile_bills->findBlock(2);
        BOOST_CHECK(rblock != nullptr);
	BOOST_CHECK(rblock->signature_length > 0); // Signed
	BOOST_CHECK(rblock->type == (uint16_t)BlockType::USR_CRYPT_PLAIN);

	wa2.close();
	// Open the wallet again and read the folder content

        auto wa3 = Wallet(wallet);
        wa3.setEncryptionKey(key);
        wa3.open(path);

	PetalusBlock read_block;

        BOOST_CHECK(wa3.readFromFolder("notes", 2, read_block) > 0);
        BOOST_CHECK(read_block.length() > 0);
        BOOST_CHECK(read_block.type() == BlockType::USR_PLAIN);
        BOOST_CHECK(data1.compare(read_block.data()) == 0);

        BOOST_CHECK(wa3.readFromFolder("bills", 2, read_block) > 0);
        BOOST_CHECK(read_block.length() > 0);
        BOOST_CHECK(read_block.type() == BlockType::USR_PLAIN);
        BOOST_CHECK(data2.compare(read_block.data()) == 0);

        // Verify the content of the folder files again!
        fo = wa3.getFolders();

        bfile_notes = fo["notes"];
        bfile_bills = fo["bills"];

        rblock = bfile_notes->findBlock(2);
        BOOST_CHECK(rblock != nullptr);
        BOOST_CHECK(rblock->signature_length > 0); // Signed
        BOOST_CHECK(rblock->type == (uint16_t)BlockType::USR_CRYPT_PLAIN);

        rblock = bfile_bills->findBlock(2);
        BOOST_CHECK(rblock != nullptr);
        BOOST_CHECK(rblock->signature_length > 0); // Signed
        BOOST_CHECK(rblock->type == (uint16_t)BlockType::USR_CRYPT_PLAIN);

	wa3.close();	
}

// Corrupts the main file of the wallet and reopen to verify the status
BOOST_AUTO_TEST_CASE (test09_corruption)
{
        // Remove the wallet from previous runs
        std::string wallet(generateHexBlockFilename());
        std::filesystem::path path(generateTestFile(wallet));

        std::string user_password("some password from the user///");
        if (std::filesystem::exists(path)) std::filesystem::remove(path);

        BOOST_TEST_MESSAGE( "wallet:(" << wallet << ")");

        auto wa = Wallet(wallet);
        wa.open(path, true);

        PetalusBlock pblock1(generateRandomString(61));
        PetalusBlock pblock2(generateRandomString(89));
        PetalusBlock pblock3("This information will be corrupted");

        BOOST_CHECK(wa.getTotalFolders() == 0);
        BOOST_CHECK(wa.getTotalBlocks() == 0);
        BOOST_CHECK(wa.getTotalBytes() == 0);
        BOOST_CHECK(wa.getLastBlockNumberWritten() == 0);

        BOOST_CHECK(wa.write(pblock1) > 0);
        BOOST_CHECK(wa.write(pblock2) > 0);
        BOOST_CHECK(wa.write(pblock3) > 0);

        BOOST_CHECK(wa.getTotalBlocks() == 3);
        BOOST_CHECK(wa.getLastBlockNumberWritten() == 3);

	auto bfile = wa.getBlockFile();

	auto file_path = wa.getBlockFile()->getPath();
      	int32_t file_size = bfile->getTotalBytes(); // Total size of the file

	wa.close();
	BOOST_CHECK(bfile->is_open() == false);

	// Basically the idea is to simulate that the os could not write the
	// user data, the header is correct but not the length of the user data
	std::error_code ec;

        std::filesystem::resize_file(file_path, file_size - 1, ec);
	
	auto bfile_c = BlockFile();	

	bfile_c.open(file_path);
	BOOST_CHECK(bfile_c.is_open() == false);

	BOOST_CHECK(bfile_c.getTotalHeaderCorruptions() == 0);
	BOOST_CHECK(bfile_c.getTotalBlockCorruptions() == 0);
	BOOST_CHECK(bfile_c.getTotalLengthCorruptions() == 1);
	BOOST_CHECK(bfile_c.getTotalMissingBlocks() == 0);
	BOOST_CHECK(bfile_c.getLastBlockLoaded() == 2);
	
	bfile_c.close();

	auto wa2 = Wallet(wallet);
        wa2.open(path, false);

	BOOST_CHECK(wa2.is_open() == false);
	BOOST_CHECK(wa2.getTotalBytes() == 0);
	BOOST_CHECK(wa2.getTotalBlocks() == 0);

	wa2.close();
}

// Corrupts the main file of the wallet and reopen to verify the status
// Similar as the previous test but we add data to the last block
BOOST_AUTO_TEST_CASE (test10_corruption)
{
        // Remove the wallet from previous runs
        std::string wallet(generateHexBlockFilename());
        std::filesystem::path path(generateTestFile(wallet));

        std::string user_password("some password from the user///");
        if (std::filesystem::exists(path)) std::filesystem::remove(path);

        BOOST_TEST_MESSAGE( "wallet:(" << wallet << ")");

        auto wa = Wallet(wallet);
        wa.open(path, true);

        PetalusBlock pblock1(generateRandomString(67));
        PetalusBlock pblock2(generateRandomString(83));
        PetalusBlock pblock3("This information will be corrupted by adding bytes");

        BOOST_CHECK(wa.getTotalFolders() == 0);
        BOOST_CHECK(wa.getTotalBlocks() == 0);
        BOOST_CHECK(wa.getTotalBytes() == 0);
        BOOST_CHECK(wa.getLastBlockNumberWritten() == 0);

        BOOST_CHECK(wa.write(pblock1) > 0);
        BOOST_CHECK(wa.write(pblock2) > 0);
        BOOST_CHECK(wa.write(pblock3) > 0);

        BOOST_CHECK(wa.getTotalBlocks() == 3);
        BOOST_CHECK(wa.getLastBlockNumberWritten() == 3);

        auto bfile = wa.getBlockFile();

        auto file_path = wa.getBlockFile()->getPath();
        int32_t file_size = bfile->getTotalBytes(); // Total size of the file

        wa.close();
        BOOST_CHECK(bfile->is_open() == false);

        // Basically the idea is to simulate that the os could not write the
        // user data and write garbage, the header is correct but not the length of the user data
        std::error_code ec;

        // Truncate the file to add 1 bytes 
        std::filesystem::resize_file(file_path, file_size + 1, ec);

        auto bfile_c = BlockFile();

        bfile_c.open(file_path);
        BOOST_CHECK(bfile_c.is_open() == false);

        BOOST_CHECK(bfile_c.getTotalHeaderCorruptions() == 0);
        BOOST_CHECK(bfile_c.getTotalBlockCorruptions() == 0);
        BOOST_CHECK(bfile_c.getTotalLengthCorruptions() == 0);
        BOOST_CHECK(bfile_c.getTotalMissingBlocks() == 1);
        BOOST_CHECK(bfile_c.getLastBlockLoaded() == 3);

        bfile_c.close();

        auto wa2 = Wallet(wallet);
        wa2.open(path, false);

        BOOST_CHECK(wa2.is_open() == false);
        BOOST_CHECK(wa2.getTotalBytes() == 0);
        BOOST_CHECK(wa2.getTotalBlocks() == 0);

        wa2.close();
}

// Create two access blocks on the same wallet with password and ec
BOOST_AUTO_TEST_CASE (test11_two_access_blocks)
{
        // Remove the wallet from previous runs
        std::string wallet(generateHexBlockFilename());
        std::filesystem::path path(generateTestFile(wallet));

        std::string user_password1("1234567890qwerty");
        std::string user_password2("1234567890A~AAAA");

        if (std::filesystem::exists(path)) std::filesystem::remove(path);

        BOOST_TEST_MESSAGE( "wallet:(" << wallet << ")");

        auto wa1 = Wallet(wallet);
        wa1.open(path, true);

        CryptoPP::AutoSeededRandomPool prng;
        PrivateKey privateKey1, privateKey2;
        PublicKey publicKey1, publicKey2;
        std::string b64publickey1 = generateBase64PublicKey(privateKey1, publicKey1, CryptoPP::ASN1::secp256k1());
        std::string b64publickey2 = generateBase64PublicKey(privateKey2, publicKey2, CryptoPP::ASN1::secp192k1());

        wa1.writePassword(user_password1, b64publickey1);
        wa1.close();

	// Open the wallet and the first public key will be loaded
        auto wa2 = Wallet(wallet);
        wa2.open(path);

        // Generate the signature for the first block
        Signer signer1(privateKey1);
        std::string data1(generateRandomString(111));
        PetalusBlock block1(data1);

        std::string binary_signature1;
        uint8_t digest[HASH_BLOCK_SIZE];

        computeSHA256(digest, (const uint8_t*)data1.c_str(), data1.length());

        // Sign the sha256 hash of data1
        CryptoPP::StringSource s1( digest, HASH_BLOCK_SIZE, true /*pump all*/,
                new CryptoPP::SignerFilter( prng,
                        signer1,
                        new CryptoPP::StringSink( binary_signature1 )
                ) // SignerFilter
        ); // StringSource

        // Set the signature on the first block
        block1.signature(binary_signature1);
       
	BOOST_CHECK(wa2.write(block1) > 0);
	wa2.close();

        // Verify that the second block can be verified
        auto bfile1 = BlockFile();
        bfile1.open(path);
        BOOST_CHECK(bfile1.getTotalBlocks() == 2);
        BOOST_CHECK(bfile1.havePublicKey() == true);
        BOOST_CHECK(bfile1.isECPublicKey() == true);
        BOOST_CHECK(bfile1.isED25519PublicKey() == false);
        BOOST_CHECK(bfile1.getECPublicKey() == publicKey1);
        BOOST_CHECK(bfile1.verifySignatureBlock(1) == false);
        BOOST_CHECK(bfile1.verifySignatureBlock(2) == true);
        bfile1.close();

	// Update the wallet with the new key and password
	auto wa3 = Wallet(wallet);
	wa3.open(path);
	// Write another access block with different password and keys
        wa3.writePassword(user_password2, b64publickey2);
	wa3.close();

	auto wa4 = Wallet(wallet);
	wa4.open(path);
        // Generate the signature for the first block
        Signer signer2(privateKey2);
        std::string data2(generateRandomString(1110));
        PetalusBlock block2(data2);

        std::string binary_signature2;

        computeSHA256(digest, (const uint8_t*)data2.c_str(), data2.length());

        // Sign the sha256 hash of data2
        CryptoPP::StringSource s2( digest, HASH_BLOCK_SIZE, true /*pump all*/,
                new CryptoPP::SignerFilter( prng,
                        signer2,
                        new CryptoPP::StringSink( binary_signature2 )
                ) // SignerFilter
        ); // StringSource

        block2.signature(binary_signature2);

        BOOST_CHECK(wa4.write(block2) > 0);
	wa4.close();

	// Open the block file and make the checks
        // Verify that the second block can not be verified
        auto bfile2 = BlockFile();
        bfile2.open(path);
        BOOST_CHECK(bfile2.getTotalBlocks() == 4);
        BOOST_CHECK(bfile2.havePublicKey() == true);
        BOOST_CHECK(bfile2.isECPublicKey() == true);
        BOOST_CHECK(bfile2.isED25519PublicKey() == false);
        BOOST_CHECK(bfile2.getECPublicKey() == publicKey2);
        BOOST_CHECK(bfile2.verifySignatureBlock(1) == false);
        BOOST_CHECK(bfile2.verifySignatureBlock(2) == false);
        BOOST_CHECK(bfile2.verifySignatureBlock(3) == false);
        BOOST_CHECK(bfile2.verifySignatureBlock(4) == true);
        bfile1.close();
}

// Access the wallet with password and ed25519 public key
BOOST_AUTO_TEST_CASE (test12_ed25519_public_key)
{
        // Remove the wallet from previous runs
        std::string wallet(generateHexBlockFilename());
        std::filesystem::path path1(generateTestFile(wallet));
        std::string password("some password from the user///");
        if (std::filesystem::exists(path1)) std::filesystem::remove(path1);

        auto wa1 = Wallet(wallet);

        wa1.open(path1, true);

        CryptoPP::AutoSeededRandomPool prng;
        ED25519PublicKey public_key;
      	EDSigner signer_init;
 
	signer_init.AccessPrivateKey().GenerateRandom(prng);

        const ED25519PrivateKey& private_key = dynamic_cast<const ED25519PrivateKey&>(signer_init.GetPrivateKey());

        private_key.MakePublicKey(public_key);

	EDVerifier verifier_init(signer_init);

        std::string pkey, public_key64;
        CryptoPP::StringSink ss(pkey);
        verifier_init.GetPublicKey().Save(ss); // Save the public key on a string

	// Convert to a base64 string
        CryptoPP::StringSource(pkey, true, new CryptoPP::Base64Encoder(new CryptoPP::StringSink(public_key64)));

	BOOST_CHECK(public_key64.length() > 0);
        BOOST_CHECK(wa1.validPassword(password) == false);
        BOOST_CHECK(wa1.validMD5Password(password) == false);
        BOOST_CHECK(wa1.validSHA256Password(password) == false);
        BOOST_CHECK(wa1.getTotalBytes() == 0);
        BOOST_CHECK(wa1.getTotalBlocks() == 0);

        // By default is sha256
        wa1.writePassword(password, public_key64, "ed25519_public_key");

        BOOST_CHECK(wa1.getTotalBlocks() == 0); // there is no user blocks

        wa1.close();

        // Generate a new wallet session
        auto wa2 = Wallet(wallet);
        wa2.open(path1, false);

        BOOST_CHECK(wa2.getTotalBlocks() == 1); // At least one block
        auto bfile = wa2.getBlockFile();

        BOOST_CHECK(bfile->havePublicKey() == true);
        BOOST_CHECK(bfile->isECPublicKey() == false);
        BOOST_CHECK(bfile->isED25519PublicKey() == true);
        BOOST_CHECK(bfile->getSignatureLength() == 64);

        // Do a write
        EDSigner signer(private_key.GetPrivateExponent());

        std::string data(generateRandomString(257));
        std::string binary_signature;
        uint8_t digest[HASH_BLOCK_SIZE];

        computeSHA256(digest, (const uint8_t*)data.c_str(), data.length());

	size_t siglen = signer.MaxSignatureLength();
    	binary_signature.resize(siglen);

    	// Sign, and trim signature to actual size
    	siglen = signer.SignMessage(prng, (const uint8_t*)&digest[0], HASH_BLOCK_SIZE,
                                (uint8_t*)&binary_signature[0]);
    	binary_signature.resize(siglen);

        PetalusBlock block(data);
        block.signature(binary_signature);

        // Do the write
        wa2.write(block);

        BOOST_CHECK(wa2.getTotalBlocks() == 2); // At least two blocks

        wa2.close();
        // Reopen the wallet and read the last block
        auto wa3 = Wallet(wallet);
        wa3.open(path1, false);

        BOOST_CHECK(wa3.getTotalBlocks() == 2);

        PetalusBlock read_block;

        // Read from the wallet
        wa3.read(2, read_block);

        BOOST_CHECK(data.compare(read_block.data()) == 0);

        wa3.close();
}


BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(sessionwalletmanager_suite)

BOOST_AUTO_TEST_CASE (test01_sanity)
{
	// Remove the wallet from previous runs
	std::string wallet(generateHexBlockFilename());
	std::string path(generateTestFile(wallet));
	std::string hpass(generateSHA256("bu"));
        if (std::filesystem::exists(path)) std::filesystem::remove(path);
	std::string nokey;

        auto sw = SessionWalletManager();
	sw.setDataDirectory(getDataDirectory());

	BOOST_CHECK(nokey.compare(sw.getEncryptionPassword()) == 0);

	// Allocate one wallet on the cache
	sw.increaseAllocatedMemory(1);

	auto c = sw.getWalletCache();

	// Verify some cache values
	BOOST_CHECK(c->getTotal() == 1);
	BOOST_CHECK(c->getTotalAcquires() == 0);
	BOOST_CHECK(c->getTotalReleases() == 0);
	BOOST_CHECK(c->getTotalFails() == 0);
	BOOST_CHECK(c->isDynamicAllocatedMemory() == false);

	PetalusBlock data1("More data");
	PetalusBlock data2("More data999999999999----------------------");

	nlohmann::json j;

	j["password"] = hpass;
	PetalusBlock jblock(j);
	// Create a wallet and get the session id of the assigned wallet
	sw.createWallet(wallet, jblock);
	
	BOOST_CHECK(c->getTotalAcquires() == 1); // For get the memory and files
	BOOST_CHECK(c->getTotalReleases() == 1); // For release the wallet
	BOOST_CHECK(sw.getTotalCreatedWallets() == 1);
	BOOST_CHECK(sw.getTotalOpenWallets() == 0);

	std::string session_id = sw.openSession(wallet, hpass);

	BOOST_CHECK(sw.getTotalCreatedWallets() == 1);
	BOOST_CHECK(sw.getTotalOpenWallets() == 1);
	BOOST_CHECK(sw.getTotalWallets() == 1); // Wallets in memory
	BOOST_CHECK(session_id.length() > 0);

	SessionContext ctx(wallet, session_id);

	sw.write(ctx, data1);
	sw.write(ctx, data2);

	auto wa = sw.getWallet(session_id);
	BOOST_CHECK(wa != nullptr);

	BOOST_CHECK(sw.getLastBlockNumberWritten(session_id) == 3);
	BOOST_CHECK(wa->getLastBlockNumberWritten() == 3);
	BOOST_CHECK(wa->getTotalBlocks() == 3);
	BOOST_CHECK(wa->getTotalBytes() == data1.length() + data2.length() + 112);
	BOOST_CHECK(sw.getTotalCreatedWallets() == 1);
	BOOST_CHECK(sw.getTotalWallets() == 1);
	BOOST_CHECK(sw.getTotalBytesWrites() == data1.length() + data2.length());
	BOOST_CHECK(sw.getTotalWrites() == 2);

	sw.close();

	BOOST_CHECK(sw.getTotalCreatedWallets() == 1);
	BOOST_CHECK(sw.getTotalWallets() == 0);
	BOOST_CHECK(sw.getTotalBytesWrites() == data1.length() + data2.length());
	BOOST_CHECK(sw.getTotalWrites() == 2);
}

// Create two wallets and verify the information of the block files
BOOST_AUTO_TEST_CASE (test02_sanity_two_wallets)
{
	// Remove the wallet from previous runs
	std::string wallet1(generateHexBlockFilename("1"));
	std::string wallet2(generateHexBlockFilename("2"));
	std::string path1(generateTestFile(wallet1));
	std::string path2(generateTestFile(wallet2));

        if (std::filesystem::exists(path1)) std::filesystem::remove(path1);
        if (std::filesystem::exists(path2)) std::filesystem::remove(path2);

	std::string hpass1(generateSHA256("hola"));
	std::string hpass2(generateSHA256("adios"));

        auto sw = SessionWalletManager();
	sw.setDataDirectory(getDataDirectory());

        // Allocate two wallets on the cache
        sw.increaseAllocatedMemory(2);

	nlohmann::json j1, j2;

	j1["password"] = hpass1;
	j2["password"] = hpass2;

	PetalusBlock jblock1(j1), jblock2(j2);

	sw.createWallet(wallet1, jblock1);
	sw.createWallet(wallet2, jblock2);

	BOOST_CHECK(sw.getTotalCreatedWallets() == 2);
	BOOST_CHECK(sw.getTotalWallets() == 0); // Nothing in memory

	std::string id1 = sw.openSession(wallet1, hpass1);
	std::string id2 = sw.openSession(wallet2, hpass2);

	SessionContext ctx1(wallet1, id1);
	SessionContext ctx2(wallet2, id2);

	BOOST_CHECK(sw.getTotalCreatedWallets() == 2);
	BOOST_CHECK(sw.getTotalWallets() == 2);

	PetalusBlock data1("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
	PetalusBlock data2("YYYYYY");
	PetalusBlock data3("AAAAAAAAAAAAAAAAAXXXXXXXXXXXXXXXXXXXXX");
	PetalusBlock data4("-------------------------------------");
	PetalusBlock data5("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");

	sw.write(ctx1, data1);
	sw.write(ctx1, data2);

	sw.write(ctx2, data3);
	sw.write(ctx2, data4);
	sw.write(ctx2, data5);

	int32_t t = data1.length() + data2.length() + data3.length() + data4.length();
	t += data5.length();
	BOOST_CHECK(sw.getTotalBytesWrites() == t);
	BOOST_CHECK(sw.getTotalWrites() == 5);

	auto wa = sw.getWallet(id1);
	BOOST_CHECK(wa != nullptr);

	// now close the manager and the open with BlockFiles
	sw.close();

        {
		RedirectOutput out;

		out.cout << *wa;
        	sw.statistics(out.cout);
	}

	// check some of the json parameters of the wallet also
	nlohmann::json j;

	j << *wa;

	// Nothing really on the wallet just created and released
	BOOST_CHECK(j["bytes"] == 0);
	BOOST_CHECK(j["blocks"] == 0);

	BOOST_CHECK(sw.getTotalWallets() == 0); // nothing in memory

	// check the first file
        auto bfile1 = BlockFile();
        bfile1.open(path1);

	BOOST_CHECK(bfile1.is_open() == true);
        BOOST_CHECK(bfile1.getTotalBlocks() == 3);

        PetalusBlock read_block;

	// The first block is the password access
        bfile1.read(2, read_block);
	BOOST_CHECK(data1 == read_block);
        bfile1.read(3, read_block);
	BOOST_CHECK(data2 == read_block);

	// check the second file
        auto bfile2 = BlockFile();
        bfile2.open(path2);

	BOOST_CHECK(bfile2.is_open() == true);
        BOOST_CHECK(bfile2.getTotalBlocks() == 4);

	// The first block is the password

        bfile2.read(2, read_block);
	BOOST_CHECK(read_block == data3);
        bfile2.read(3, read_block);
	BOOST_CHECK(read_block == data4);
        bfile2.read(4, read_block);
	BOOST_CHECK(read_block == data5);

	bfile1.close();
	bfile2.close();

	BOOST_CHECK(bfile1.is_open() == false);
	BOOST_CHECK(bfile2.is_open() == false);
}

BOOST_AUTO_TEST_CASE (test03) // Just accept valid wallets identifiers
{
        // Remove the wallet from previous runs
        std::string wallet("ttaaaa" + generateBlockFileName());
        std::string path(getDataDirectory() + std::string("/a/a/") + wallet);
        if (std::filesystem::exists(path)) std::filesystem::remove(path);

        auto sm = SessionWalletManager();
        sm.setDataDirectory(getDataDirectory());
        PetalusBlock data1("More data");
        PetalusBlock data2("Hi");

	SessionContext ctx(wallet, "");

        sm.write(ctx, data1);
        sm.write(ctx, data2);

        auto wa = sm.getWallet(wallet);
        BOOST_CHECK(wa == nullptr);
}

BOOST_AUTO_TEST_CASE (test04_open_close) // Open and close wallet
{
        // Remove the wallet from previous runs
        std::string wallet(generateHexBlockFilename());
        std::string path(generateTestFile(wallet));
	std::string hpass(generateSHA256("XX"));

        if (std::filesystem::exists(path)) std::filesystem::remove(path);

        auto sw = SessionWalletManager();
        sw.setDataDirectory(getDataDirectory());

        // Allocate one wallets on the cache
        sw.increaseAllocatedMemory(1);

	PetalusBlock block1("More dataiaaa");
	PetalusBlock block2("123456789");
	PetalusBlock block3("ABCDEFGHIJ");
	PetalusBlock block4("****************");

        nlohmann::json j;
	j["password"] = hpass;
	PetalusBlock creation_block(j);

	sw.createWallet(wallet, creation_block);
	std::string id = sw.openSession(wallet, hpass);

	SessionContext ctx1(wallet, id);

	sw.write(ctx1, block1);
        sw.write(ctx1, block2);
        sw.write(ctx1, block3);

        auto wa1 = sw.getWallet(id);
        BOOST_CHECK(wa1 != nullptr);
	BOOST_CHECK(wa1->getTotalBytes() == block1.length() + block2.length() + block3.length() + 112);
	BOOST_CHECK(wa1->getTotalBlocks() == 4);
	BOOST_CHECK(sw.getTotalWallets() == 1);

        // check some of the json parameters of the wallet also
        nlohmann::json j1;

        j1 << *wa1;

        // Nothing really on the wallet just created and released
        BOOST_CHECK(wallet.compare(j1["name"]) == 0);
        BOOST_CHECK(j1["blocks"] == 4);

	sw.close();

	BOOST_CHECK(sw.getTotalWallets() == 0);

	// Add data again to the wallet
	id = sw.openSession(wallet, hpass);

	SessionContext ctx2(wallet, id);

	sw.write(ctx2, block4);

	BOOST_CHECK(sw.getTotalWallets() == 1);
	auto wa2 = sw.getWallet(id);
	BOOST_CHECK(sw.getTotalWallets() == 1);
	BOOST_CHECK(wa2 != nullptr);
	int32_t t = block1.length() + block2.length() + block3.length() + block4.length() + 112;

	BOOST_CHECK(wa2->getTotalBytes() == t);

	std::string name1(wa1->getName());
	std::string name2(wa2->getName());
	// The wallets are the same
	BOOST_CHECK(name1.compare(name2) == 0);

	PetalusBlock read_block;

	// Read from the wallet
	wa2->read(4, read_block);
	BOOST_CHECK(read_block == block3);
	
	// Read from the manager the block number 3
	sw.read(ctx2, read_block, 3);

	BOOST_CHECK(read_block == block2);
}

BOOST_AUTO_TEST_CASE (test05_write_two_files) // Verify that the wallet writes on two files and both are linked
{
        // Remove the wallet from previous runs
        std::string wallet(generateHexBlockFilename());
        std::string spath1(generateTestFile(wallet));
	std::string spath2(spath1.substr(0, spath1.length() - 1) + std::string("1"));
	std::string hpass(generateSHA256("password"));

        if (std::filesystem::exists(spath1)) std::filesystem::remove(spath1);
        if (std::filesystem::exists(spath2)) std::filesystem::remove(spath2);
	

        auto sw = SessionWalletManager();
        sw.setDataDirectory(getDataDirectory());

	sw.setMaxBlockFileSize(32 * boost::iostreams::mapped_file::alignment()); // This will allow to create two files

	// Allocate one wallet on the cache
        sw.increaseAllocatedMemory(1);

        PetalusBlock data1("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxXXXXXXXXXXXXXXXXXXXXXXXXXXXX");

        nlohmann::json j;
        j["password"] = hpass;
        PetalusBlock creation_block(j);

	sw.createWallet(wallet, creation_block);
	std::string id = sw.openSession(wallet, hpass);

	SessionContext ctx(wallet, id);

	sw.write(ctx, data1);

	// get the wallet
        auto wa = sw.getWallet(id);
        BOOST_CHECK(sw.getTotalWallets() == 1);
        BOOST_CHECK(wa != nullptr);

	auto path1 = wa->getBlockFile()->getPath();

	for (int i = 0; i < 500; ++i) {
        	sw.write(ctx, data1);
        	sw.write(ctx, data1);
        	sw.write(ctx, data1);
	}

	auto path2 = wa->getBlockFile()->getPath();
        sw.close();

	// Verify the files are linked
	auto b1 = BlockFile();
	b1.open(path1);
	auto b2 = BlockFile();
	b2.open(path2);

	BOOST_CHECK(b2.verify(b1) == true);

	b1.close();
	b2.close();
}

BOOST_AUTO_TEST_CASE (test06_folder) // Verify that a folder is created and properly linked
{
        // Remove the wallet from previous runs
        std::string wallet(generateHexBlockFilename());
        std::string spath1(generateTestFile(wallet));
        std::string spath2(generateTestFile(wallet, 3));
	std::string hpass(generateSHA256("asdfasdf"));

        if (std::filesystem::exists(spath1)) std::filesystem::remove(spath1);
        if (std::filesystem::exists(spath2)) std::filesystem::remove(spath2);

	auto sw = SessionWalletManager();
        sw.setDataDirectory(getDataDirectory());
	
        // Allocate one wallet on the cache
        sw.increaseAllocatedMemory(1);

	PetalusBlock pblock1("AAAAAAAAA");
	PetalusBlock pblock2("XXXXXXXXXXXXXXXXXXXXX");

        nlohmann::json jc;
        jc["password"] = hpass;
        PetalusBlock creation_block(jc);

	sw.createWallet(wallet, creation_block);
	std::string session_id = sw.openSession(wallet, hpass);

	BOOST_CHECK(session_id.length() > 0);

	SessionContext ctx(wallet, session_id);

	sw.write(ctx, pblock1);
	auto wa = sw.getWallet(session_id);
	BOOST_CHECK(wa != nullptr);
	BOOST_CHECK(wa->getTotalFolders() == 0);
	BOOST_CHECK(wa->getTotalBlocks() == 2);
	
	wa->createFolder("claims");
	
	BOOST_CHECK(wa->getTotalFolders() == 1);
	BOOST_CHECK(wa->getTotalBlocks() == 2);

	// Write some blocks
	sw.write(ctx, pblock1);
	BOOST_CHECK(wa->writeOnFolder("claims", pblock1) == pblock1.length());
	BOOST_CHECK(wa->writeOnFolder("claims", pblock2) > 0);
	BOOST_CHECK(wa->getTotalBlocks() == 5);

	{ RedirectOutput out; out.cout << *wa; }

        nlohmann::json j;

	wa->showBlock(j, 2);

	BOOST_CHECK(j["block"] == 2);
	BOOST_CHECK(j["type"] == 0);
	BOOST_CHECK(j["data"][0] == 65); // A
	BOOST_CHECK(j["data"].size() == pblock1.length());

	// Close everything
	sw.close();

	// Open the files and verify them
	auto bfile1 = BlockFile();
	auto bfile2 = BlockFile();
	bfile1.open(spath1);
	bfile2.open(spath2);

	BOOST_CHECK(bfile1.is_open() == true);
	BOOST_CHECK(bfile2.is_open() == true);
	BOOST_CHECK(bfile1.getTotalBlocks() == 4);
	BOOST_CHECK(bfile2.getTotalBlocks() == 3);
	BOOST_CHECK(bfile2.verify(bfile1) == false); // Can not be verified because are not sequential

        uint8_t digest[HASH_BLOCK_SIZE];

        bfile1.readBlockHash(3, digest); // Is the folder two;

        const petalus_block_file_v1 *block = bfile2.findBlock(1);
        BOOST_CHECK(block != nullptr);
        BOOST_CHECK(std::memcmp(digest, block->prev_hash, HASH_BLOCK_SIZE) == 0);
}

BOOST_AUTO_TEST_CASE (test07_wallet_wrong_password) // Verify that a wrong password can not access to the wallet 
{
        std::string wallet(generateHexBlockFilename());
        std::string spath(generateTestFile(wallet));
        std::string hpass(generateSHA256("aka;kdsfa;jfdasd"));
        std::string hpass_crack(generateSHA256("AAAAAA"));

        if (std::filesystem::exists(spath)) std::filesystem::remove(spath);

        auto sw = SessionWalletManager();
        sw.setDataDirectory(getDataDirectory());

        // Allocate one wallet on the cache
        sw.increaseAllocatedMemory(1);

        boost::string_ref data("AAAAAAAAA");

        nlohmann::json j;
        j["password"] = hpass;
        PetalusBlock creation_block(j);

        sw.createWallet(wallet, creation_block);
        std::string id1 = sw.openSession(wallet, hpass);

        // get the wallet
        auto wa = sw.getWallet(id1);
        BOOST_CHECK(sw.getTotalWallets() == 1);
        BOOST_CHECK(sw.getTotalOpenWallets() == 1);
        BOOST_CHECK(sw.getTotalCreatedWallets() == 1);
        BOOST_CHECK(sw.getTotalFailOpenWallets() == 0);
        BOOST_CHECK(wa != nullptr);
        auto bfile = wa->getBlockFile();

        // First write some blocks

        PetalusBlock block1("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
        PetalusBlock block2("--------------------------------");

        bfile->write(block1);
        bfile->write(block2);

        // Close everything
        sw.close();

        // Accessing the first time to the wallet
        auto sw2 = SessionWalletManager();
        sw2.setDataDirectory(getDataDirectory());

        // Allocate one wallet on the cache
        sw2.increaseAllocatedMemory(1);

	std::string id2;
	// The password is not valid
	for (int i = 0; i < 10; ++i) { 
        	id2 = sw2.openSession(wallet, hpass_crack);
		if (id2.length() > 0) break;
	}	

	BOOST_CHECK(id2.length() == 0);
        BOOST_CHECK(sw2.getTotalWallets() == 0);
        BOOST_CHECK(sw2.getTotalOpenWallets() == 0);
        BOOST_CHECK(sw2.getTotalFailOpenWallets() == 0);
        BOOST_CHECK(sw2.getTotalWrongCredentials() == 10);
}

BOOST_AUTO_TEST_CASE (test08_access_wallet_password) // create wallet with password and access
{
        std::string wallet(generateHexBlockFilename());
        std::string spath(generateTestFile(wallet));
        std::string hpass(generateSHA256("this is the password for access the wallet"));

        if (std::filesystem::exists(spath)) std::filesystem::remove(spath);
	PetalusBlock pblock("some data to write on the block");

        auto sw = SessionWalletManager();
        sw.setDataDirectory(getDataDirectory());

        // Allocate one wallet on the cache
        sw.increaseAllocatedMemory(1);

        BOOST_CHECK(sw.getTotalWallets() == 0);
        BOOST_CHECK(sw.getTotalCreatedWallets() == 0);

        nlohmann::json j;
        j["password"] = hpass;
        PetalusBlock creation_block(j);

        sw.createWallet(wallet, creation_block);
        BOOST_CHECK(sw.getTotalWallets() == 0);
        BOOST_CHECK(sw.getTotalCreatedWallets() == 1);
	sw.close();

	// Create a new SessionWalletManager 
	auto sw1 = SessionWalletManager();
        sw1.setDataDirectory(getDataDirectory());

        // Allocate one wallet on the cache
        sw1.increaseAllocatedMemory(1);

	std::string id = sw1.openSession(wallet, std::string("00000AAAAaaabbbCCCC"));	
       	BOOST_CHECK(id.length() == 0);
        BOOST_CHECK(sw1.getTotalWallets() == 0);
	BOOST_CHECK(sw1.getTotalOpenWallets() == 0);
        BOOST_CHECK(sw1.getTotalFailOpenWallets() == 0);
        BOOST_CHECK(sw1.getTotalWrongCredentials() == 1);
 
	id = sw1.openSession(wallet, hpass);	
       	BOOST_CHECK(id.length() > 0);
        BOOST_CHECK(sw1.getTotalWallets() == 1);
	BOOST_CHECK(sw1.getTotalOpenWallets() == 1);
        BOOST_CHECK(sw1.getTotalFailOpenWallets() == 0);
        BOOST_CHECK(sw1.getTotalWrongCredentials() == 1);

	SessionContext ctx(wallet, id);

	// Write on the wallet
	sw1.write(ctx, pblock);
	sw1.write(ctx, pblock);
	sw1.write(ctx, pblock);

        // get the wallet and verify the information
        auto wa = sw1.getWallet(id);
        BOOST_CHECK(wa != nullptr);
	BOOST_CHECK(wa->getTotalBytes() > pblock.length() * 3);
	BOOST_CHECK(wa->getTotalBlocks() == 4); // Total blocks on memory
	sw.close();
}

BOOST_AUTO_TEST_CASE (test09_reopen_session_password) // create wallet with password unload the session and operate with other session 
{
        std::string wallet(generateHexBlockFilename());
        std::string spath(generateTestFile(wallet));
        std::string hpass(generateSHA256("this is the password for access the wallet"));
        std::string data("some data to write on the blockYYYYYYYYYYYYY");

        if (std::filesystem::exists(spath)) std::filesystem::remove(spath);

        auto sw = SessionWalletManager();
        sw.setDataDirectory(getDataDirectory());

        // Allocate one wallet on the cache
        sw.increaseAllocatedMemory(1);

        BOOST_CHECK(sw.getTotalWallets() == 0);
        BOOST_CHECK(sw.getTotalCreatedWallets() == 0);
	BOOST_CHECK(sw.getTotalOpenWallets() == 0);

        nlohmann::json j;
        j["password"] = hpass;
        PetalusBlock creation_block(j);

        sw.createWallet(wallet, creation_block);

        BOOST_CHECK(sw.getTotalWallets() == 0);
        BOOST_CHECK(sw.getTotalCreatedWallets() == 1);
	BOOST_CHECK(sw.getTotalOpenWallets() == 0);

	std::string id1 = sw.openSession(wallet, hpass);       

	BOOST_CHECK(id1.length() > 0);
        BOOST_CHECK(sw.getTotalWallets() == 1);
        BOOST_CHECK(sw.getTotalCreatedWallets() == 1);
	BOOST_CHECK(sw.getTotalOpenWallets() == 1);
	BOOST_CHECK(sw.getTotalCloseWallets() == 0);

 	sw.close(id1);

        BOOST_CHECK(sw.getTotalWallets() == 0);
        BOOST_CHECK(sw.getTotalCreatedWallets() == 1);
	BOOST_CHECK(sw.getTotalOpenWallets() == 1);
	BOOST_CHECK(sw.getTotalCloseWallets() == 1);

	std::string id2 = sw.openSession(wallet, hpass);	
       	BOOST_CHECK(id2.length() > 0);
        BOOST_CHECK(sw.getTotalWallets() == 1);
	BOOST_CHECK(sw.getTotalOpenWallets() == 2);
        BOOST_CHECK(sw.getTotalFailOpenWallets() == 0);
 	BOOST_CHECK(id1.compare(id2) != 0);

	sw.close();
}

BOOST_AUTO_TEST_CASE (test10_reopen_session) // Reopen the session two times
{
        std::string wallet(generateHexBlockFilename());
        std::string spath(generateTestFile(wallet));
        std::string hpass(generateSHA256("this is the password for access the wallet"));

        if (std::filesystem::exists(spath)) std::filesystem::remove(spath);

        auto sw = SessionWalletManager();
        sw.setDataDirectory(getDataDirectory());

        // Allocate one wallet on the cache
        sw.increaseAllocatedMemory(1);

        BOOST_CHECK(sw.getTotalWallets() == 0);
        BOOST_CHECK(sw.getTotalCreatedWallets() == 0);
        BOOST_CHECK(sw.getTotalOpenWallets() == 0);

        nlohmann::json j;
        j["password"] = hpass;
        PetalusBlock creation_block(j);

        sw.createWallet(wallet, creation_block);

        BOOST_CHECK(sw.getTotalWallets() == 0);
        BOOST_CHECK(sw.getTotalCreatedWallets() == 1);
        BOOST_CHECK(sw.getTotalOpenWallets() == 0);

        std::string id1 = sw.openSession(wallet, hpass);

        BOOST_CHECK(id1.length() > 0);
        BOOST_CHECK(sw.getTotalWallets() == 1);
        BOOST_CHECK(sw.getTotalCreatedWallets() == 1);
        BOOST_CHECK(sw.getTotalOpenWallets() == 1);
        BOOST_CHECK(sw.getTotalCloseWallets() == 0);

        std::string id2 = sw.openSession(wallet, hpass);
        BOOST_CHECK(id2.length() > 0);
        BOOST_CHECK(sw.getTotalWallets() == 1);
        BOOST_CHECK(sw.getTotalOpenWallets() == 1);
        BOOST_CHECK(sw.getTotalFailOpenWallets() == 0);
        BOOST_CHECK(id1.compare(id2) == 0);

	// can not open the wallet
        std::string id3 = sw.openSession(wallet, std::string("1234567890abcedef"));
        BOOST_CHECK(id3.length() == 0);
        BOOST_CHECK(sw.getTotalWallets() == 0);
        BOOST_CHECK(sw.getTotalOpenWallets() == 1);
        BOOST_CHECK(sw.getTotalFailOpenWallets() == 0);
        BOOST_CHECK(sw.getTotalWrongCredentials() == 1);
        BOOST_CHECK(sw.getTotalCloseWallets() == 1);
}

BOOST_AUTO_TEST_CASE (test11_password_encryption) // Verify the encryption/decryption of the password of the Session Manager
{
        // Remove the wallet from previous runs
        std::string wallet(generateHexBlockFilename());
        std::string path(generateTestFile(wallet));
        if (std::filesystem::exists(path)) std::filesystem::remove(path);
        std::string password("This-Is1234#G00d?%pass12"); /* 24 length */

        auto sw = SessionWalletManager();
        sw.setDataDirectory(getDataDirectory());

        // Allocate one wallet on the cache
        sw.increaseAllocatedMemory(1);

        PetalusBlock block(password);

        CryptoPP::SecByteBlock key { 32 };
        updateKey(key);

        block.encrypt(key);

        CryptoPP::HexEncoder encoder;
        std::string pout;
        encoder.Attach( new CryptoPP::StringSink(pout) );
        encoder.Put( (const uint8_t*)block.data(), block.length() );
        encoder.MessageEnd();

        std::ostringstream badout, out;

	// Try to set a wrong password
	badout << "{AES}" << "1234567890";
	sw.setEncryptionPassword(badout.str());

	BOOST_CHECK(strlen(sw.getEncryptionPassword()) == 0);

        out << "{AES}" << pout;
        sw.setEncryptionPassword(out.str());

	BOOST_CHECK(out.str().compare(sw.getEncryptionPassword()) == 0);

        sw.close();
}

BOOST_AUTO_TEST_CASE (test12_password) // Use the password on the SessionWalletManager
{
        std::string wallet(generateHexBlockFilename());
        std::string spath(generateTestFile(wallet));
        if (std::filesystem::exists(spath)) std::filesystem::remove(spath);
	std::string orig_password("1234567890qwerty");
        std::string password(generateAESPassword(orig_password));
        std::string hpass(generateSHA256("---BBBBCCCC"));
	PetalusBlock pblock("Some user data that should be encrypted");

	EncryptionKey key;

	BOOST_CHECK(key.password(password) == true);
	BOOST_CHECK(orig_password.compare(key.getDecryptedPassword()) == 0);

        auto sw = SessionWalletManager();
        sw.setDataDirectory(getDataDirectory());

	BOOST_CHECK(sw.getEncryptionKey().key().empty() == true);

	sw.setEncryptionPassword(password);
	
	BOOST_CHECK(sw.getEncryptionKey().key().empty() == false);
	BOOST_CHECK(password.compare(sw.getEncryptionPassword()) == 0);

        // Allocate one wallet on the cache
        sw.increaseAllocatedMemory(1);

        nlohmann::json j;
        j["password"] = hpass;
        PetalusBlock creation_block(j);

        sw.createWallet(wallet, creation_block);

        BOOST_CHECK(sw.getTotalWallets() == 0);
        BOOST_CHECK(sw.getTotalCreatedWallets() == 1);
        BOOST_CHECK(sw.getTotalOpenWallets() == 0);

        std::string id = sw.openSession(wallet, password);
	BOOST_CHECK(id.length() == 0); // wrong password

        id = sw.openSession(wallet, hpass);
	BOOST_CHECK(id.length() > 0); // correct password

	SessionContext ctx(wallet, id);

	// Write on the wallet
        sw.write(ctx, pblock);

        // get the wallet and verify the information
        auto wa = sw.getWallet(id);
        BOOST_CHECK(wa != nullptr);
	auto bfile = wa->getBlockFile();	
        BOOST_CHECK(wa->getTotalBlocks() == 2); // Total blocks on memory
	BOOST_CHECK(wa->getEncryptionKey() == sw.getEncryptionKey());
	BOOST_CHECK(wa->getEncryptionKey().key().empty() == false);
	BOOST_CHECK(bfile->getEncryptionKey().key().empty() == false);
	BOOST_CHECK(bfile->getEncryptionKey() == sw.getEncryptionKey());

        const petalus_block_file_v1 *block = bfile->findBlock(2);
        BOOST_CHECK(block != nullptr);
	BOOST_CHECK(block->type == (uint16_t)BlockType::USR_CRYPT_PLAIN);

	// Now remove the password from the sw
	sw.setEncryptionPassword("");

	sw.write(ctx, pblock);
        BOOST_CHECK(wa->getTotalBlocks() == 3); // Total blocks on memory
	block = bfile->findBlock(3);
	BOOST_CHECK(block != nullptr);
	BOOST_CHECK(block->type == (uint16_t)BlockType::USR_CRYPT_PLAIN);

        sw.close();
}

BOOST_AUTO_TEST_CASE (test13_password) // Use the password on the SessionWalletManager
{
        std::string wallet(generateHexBlockFilename());
        std::string spath(generateTestFile(wallet));
        std::string orig_password("AAAABBBBCCCCDDDD");
	std::string password(generateAESPassword(orig_password));
        std::string hpass(generateSHA256("---BBBBCCCC bu bu"));
	std::string data("Some user data that should be encrypted");
        PetalusBlock pblock(data);

        if (std::filesystem::exists(spath)) std::filesystem::remove(spath);

        auto sw = SessionWalletManager();
        sw.setDataDirectory(getDataDirectory());

        sw.setEncryptionPassword(password);

        // Allocate one wallet on the cache
        sw.increaseAllocatedMemory(1);

        nlohmann::json j;
        j["password"] = hpass;
        PetalusBlock creation_block(j);

        sw.createWallet(wallet, creation_block);

        BOOST_CHECK(sw.getTotalWallets() == 0);
        BOOST_CHECK(sw.getTotalCreatedWallets() == 1);
        BOOST_CHECK(sw.getTotalOpenWallets() == 0);

        std::string id = sw.openSession(wallet, hpass);
        BOOST_CHECK(id.length() > 0); // correct password

	SessionContext ctx(wallet, id);

        // Write on the wallet the block
	// Notice that the pblock variable has been changed on transit
	BOOST_CHECK(pblock.type() == BlockType::USR_PLAIN);

        sw.write(ctx, pblock);

	BOOST_CHECK(sw.getTotalWrites() == 1);
	BOOST_CHECK(sw.getTotalReads() == 0);
	BOOST_CHECK(sw.getTotalBytesWrites() == pblock.length());
	BOOST_CHECK(sw.getTotalBytesReads() == 0);

	BOOST_CHECK(pblock.type() == BlockType::USR_CRYPT_PLAIN);

        // get the wallet and verify the information
        auto wa = sw.getWallet(id);
        BOOST_CHECK(wa != nullptr);
        BOOST_CHECK(wa->getTotalBlocks() == 2); // Total blocks on memory

        auto bfile = wa->getBlockFile();
        const petalus_block_file_v1 *block = bfile->findBlock(2);
        BOOST_CHECK(block != nullptr);
        BOOST_CHECK(block->type == (uint16_t)BlockType::USR_CRYPT_PLAIN);

	// Now read again the block
	PetalusBlock rblock;

	sw.read(ctx, rblock, 2);

	BOOST_CHECK(sw.getTotalWrites() == 1);
	BOOST_CHECK(sw.getTotalReads() == 1);
	BOOST_CHECK(sw.getTotalBytesWrites() == pblock.length());
	BOOST_CHECK(sw.getTotalBytesReads() == rblock.length());
	BOOST_CHECK(rblock.type() == BlockType::USR_PLAIN); // has been changed by the SessionWalletManager;
      
	BOOST_CHECK(rblock.length() == data.length());
	BOOST_CHECK(data.compare(rblock.data()) == 0);

        sw.close();
}

BOOST_AUTO_TEST_CASE (test14_getters_setters) // getters and setters of the SessionWalletManager
{
        std::string wallet(generateHexBlockFilename());
        std::string spath(generateTestFile(wallet));
        std::string hpass(generateSHA256("---BBBBCCCC"));
        std::string data("Some user data that should be encrypted");
        PetalusBlock pblock(data);

        if (std::filesystem::exists(spath)) std::filesystem::remove(spath);

        auto sw = SessionWalletManager();
        sw.setDataDirectory(getDataDirectory());

        // Allocate ten wallet on the cache
        sw.increaseAllocatedMemory(10);
	sw.decreaseAllocatedMemory(10);

	BOOST_CHECK(sw.isDynamicAllocatedMemory() == false);

	sw.setDynamicAllocatedMemory(true);
	
	BOOST_CHECK(sw.isDynamicAllocatedMemory() == true);

        nlohmann::json j;
        j["password"] = hpass;
        j["data"] = "This is something for put on the block";
        PetalusBlock creation_block(j);

	sw.createWallet(wallet, creation_block);
        std::string id = sw.openSession(wallet, hpass);

        {
                RedirectOutput out;

                sw.showSessions(out.cout);
		sw.showSession(out.cout, id, "", 0);
        }

	nlohmann::json js, jss;

	sw.showSessions(js);
	sw.showSession(jss, id, std::string(""), 1);

	BOOST_CHECK(js.size() == 1); 
	BOOST_CHECK(js[0]["wallet"] == wallet);
	BOOST_CHECK(js[0]["sid"] == id);
	BOOST_CHECK(sw.getTotalBlocks(id) == 2);
	BOOST_CHECK(sw.getTotalCacheMisses() == 0);
	BOOST_CHECK(sw.getCurrentUseMemory() > 0);
	BOOST_CHECK(sw.getAllocatedMemory() > 0);
	BOOST_CHECK(sw.getTotalAllocatedMemory() > 0);

	
}

BOOST_AUTO_TEST_CASE (test15_json_encrypt_decrypt) // Encrypt/decrypt json blocks on the SessionWalletManager
{
        std::string wallet(generateHexBlockFilename());
        std::string spath(generateTestFile(wallet));
        std::string orig_password("1234567890qwertyuiopasdfghjklzxc");
        std::string password(generateAESPassword(orig_password));
        std::string hpass(generateSHA256("I love balu"));

        if (std::filesystem::exists(spath)) std::filesystem::remove(spath);

        nlohmann::json j;

        j["user1"] = "Bob";
        j["user2"] = "Alice";
        j["data"] = "Some data";

        std::string data1("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
        std::string data2("YYYYYYY                  YYYYYYY");
        std::vector<std::string> items {{ data1 } , { data2 }};
	std::map<std::string, std::string> mitems {{ "item1", "value1"}};
        j["data_vector"] = items;
        j["data_map"] = mitems;

	PetalusBlock jblock(j);

	nlohmann::json jc;
	jc["password"] = hpass;
	PetalusBlock creation_block(jc);

        auto sw = SessionWalletManager();
        sw.setDataDirectory(getDataDirectory());

        sw.setDynamicAllocatedMemory(true);

        sw.createWallet(wallet, creation_block);
	
	sw.setEncryptionPassword(password); // Encrypt all the user data
	BOOST_CHECK(sw.getEncryptionKey().key().empty() == false);
	BOOST_CHECK(password.compare(sw.getEncryptionPassword()) == 0);

        std::string id = sw.openSession(wallet, hpass);
	BOOST_CHECK(id.length() > 0);

	SessionContext ctx(wallet, id);

        // Write on the wallet the block
        // Notice that the block variable will be changed on transit
        BOOST_CHECK(jblock.type() == BlockType::USR_JSON_STRING);

        sw.write(ctx, jblock);

        BOOST_CHECK(sw.getTotalWrites() == 1);
        BOOST_CHECK(sw.getTotalReads() == 0);
        BOOST_CHECK(sw.getTotalBytesWrites() >= jblock.length());
        BOOST_CHECK(sw.getTotalBytesReads() == 0);
        BOOST_CHECK(jblock.type() == BlockType::USR_CRYPT_JSON_STRING);
        BOOST_CHECK(sw.getTotalBlocks(id) == 2);

        // get the wallet and verify the information
        auto wa = sw.getWallet(id);
        BOOST_CHECK(wa != nullptr);
        BOOST_CHECK(wa->getTotalBlocks() == 2); // Total blocks on memory

        auto bfile = wa->getBlockFile();
        const petalus_block_file_v1 *block = bfile->findBlock(2);
        BOOST_CHECK(block != nullptr);
        BOOST_CHECK(block->type == (uint16_t)BlockType::USR_CRYPT_JSON_STRING);

        // Now read again the block
        PetalusBlock rblock;

        sw.read(ctx, rblock, 2);

        BOOST_CHECK(sw.getTotalWrites() == 1);
        BOOST_CHECK(sw.getTotalReads() == 1);
        BOOST_CHECK(sw.getTotalBytesReads() == rblock.length());
        BOOST_CHECK(rblock.type() == BlockType::USR_JSON_STRING); // has been changed by the SessionWalletManager;

        // BOOST_CHECK(rblock.length() == out.str().length());
        // BOOST_CHECK(out.str().compare(rblock.data()) == 0);

	sw.close();
}

BOOST_AUTO_TEST_CASE (test16_link_folders) // Verify that folders are created and properly linked
{
        // Remove the wallet from previous runs
        std::string wallet(generateHexBlockFilename());
        std::string spath1(generateTestFile(wallet));
        std::string spath2(generateTestFile(wallet, 2));
        std::string spath3(generateTestFile(wallet, 3));
        std::string spath4(generateTestFile(wallet, 4));
        std::string hpass(generateSHA256("1234password"));

        if (std::filesystem::exists(spath1)) std::filesystem::remove(spath1);
        if (std::filesystem::exists(spath2)) std::filesystem::remove(spath2);
        if (std::filesystem::exists(spath3)) std::filesystem::remove(spath3);
        if (std::filesystem::exists(spath4)) std::filesystem::remove(spath4);
	std::string algorithm("blake2s");

        auto sw = SessionWalletManager();
        sw.setDataDirectory(getDataDirectory());
	sw.setBlockHashType(algorithm); // All the files should be blake2s including folder files

	BOOST_CHECK(algorithm.compare(sw.getBlockHashType()) == 0);

        // Allocate one wallet on the cache
        sw.increaseAllocatedMemory(1);

	nlohmann::json j;

	// Information that should be obligatory
        j["name"] = "Bob";
        j["surname"] = "Wick";
        j["age"] = 32;
	j["folders"] = { "claims", "bills", "notes" };
	j["password"] = hpass;

	PetalusBlock creation_block(j);

        sw.createWallet(wallet, creation_block);

        BOOST_CHECK(sw.getTotalWallets() == 0);
        BOOST_CHECK(sw.getTotalCreatedWallets() == 1);
        BOOST_CHECK(sw.getTotalOpenWallets() == 0);

        std::string id = sw.openSession(wallet, hpass);
        BOOST_CHECK(id.length() > 0); // correct password

        // get the wallet and verify the information
        auto wa = sw.getWallet(id);
        BOOST_CHECK(wa != nullptr);
	BOOST_CHECK(algorithm.compare(wa->getBlockHashType()) == 0);

        auto bfile = wa->getBlockFile();
	BOOST_CHECK(algorithm.compare(bfile->getBlockHashTypeName()) == 0);
        BOOST_CHECK(wa->getTotalBlocks() == 5); // Total blocks on memory

	// The blockfiles of the folders are loaded on demand, so no folders on memory
	auto folders = wa->getFolders();
	BOOST_CHECK(folders.size() == 0);

	// Write some info in one of the folders
	PetalusBlock pblock2("This should go to a folder");
	PetalusBlock pblock3("This should go to a different folder");
	PetalusBlock pblock4("This should go to other place");

	BOOST_CHECK(wa->writeOnFolder("notes", pblock2) == pblock2.length());
	BOOST_CHECK(wa->writeOnFolder("claims", pblock3) == pblock3.length());
	BOOST_CHECK(wa->writeOnFolder("bills", pblock4) == pblock4.length());
	BOOST_CHECK(wa->writeOnFolder("no folder", pblock4) == 0);

	folders = wa->getFolders();
	BOOST_CHECK(folders.size() == 3);

	for (auto &folder: folders) {
		auto f = folder.second;

		BOOST_CHECK(algorithm.compare(f->getBlockHashTypeName()) == 0);
        	BOOST_CHECK(f->getTotalBlocks() == 2); 
	
       		const petalus_block_file_v1 *block = f->findBlock(1);
        	BOOST_CHECK(block != nullptr);
        	BOOST_CHECK(block->type == (uint16_t)BlockType::SYS_FOLDER_NAME);
	}
        sw.close();
}

BOOST_AUTO_TEST_CASE (test17) // Verify that the session manager is able to create the directories
{
        // Remove the wallet from previous runs
	std::string directory_path1("./petalustest_data1");
	std::string directory_path2("./other_data_path_not_created");
        if (std::filesystem::exists(directory_path1)) std::filesystem::remove_all(directory_path1);
        if (std::filesystem::exists(directory_path2)) std::filesystem::remove_all(directory_path2);

        auto sw = SessionWalletManager();
        sw.setDataDirectory(directory_path1);
	BOOST_CHECK(directory_path1.compare(sw.getDataDirectory()) == 0);

	BOOST_CHECK(std::filesystem::exists(directory_path1) == true);

	// Can not change the data directory if has been set, things could be messy

        sw.setDataDirectory(directory_path2);
	BOOST_CHECK(directory_path1.compare(sw.getDataDirectory()) == 0);

	BOOST_CHECK(std::filesystem::exists(directory_path2) == false);

	sw.close();
}

BOOST_AUTO_TEST_CASE (test18) // Verify that information on folders is encrypted
{
        // Remove the wallet from previous runs
        std::string wallet(generateHexBlockFilename());
        std::string spath1(generateTestFile(wallet));
        std::string spath2(generateTestFile(wallet, 2));
        std::string spath3(generateTestFile(wallet, 3));
        std::string spath4(generateTestFile(wallet, 4));
        if (std::filesystem::exists(spath1)) std::filesystem::remove(spath1);
        if (std::filesystem::exists(spath2)) std::filesystem::remove(spath2);
        if (std::filesystem::exists(spath3)) std::filesystem::remove(spath3);
        if (std::filesystem::exists(spath4)) std::filesystem::remove(spath4);
        std::string hpass(generateSHA256("asdfasdf"));
        std::string algorithm("sha3-256");

	std::string data1("This should go on the shells folder");
	std::string data2("This should go on other folder, may be is on the caves");
	std::string data3("We love you bobbbbbbbb");

        auto sw = SessionWalletManager();
        sw.setDataDirectory(getDataDirectory());
        sw.setBlockHashType(algorithm); // All the files should be sha3-256 including folder files

        BOOST_CHECK(algorithm.compare(sw.getBlockHashType()) == 0);

        std::string orig_password("1234567890qwertyuiopasdfghjklzxc");
        std::string password("{AES}F63BCF56852F75BF0BE97A2CCE96B29531A2253C3AED63FBE318CE81903AF1F1");
	sw.setEncryptionPassword(password); // Encrypt all the user data

	BOOST_CHECK(sw.getEncryptionKey().key().empty() == false);

        // Allocate one wallet on the cache
        sw.increaseAllocatedMemory(1);

        nlohmann::json j;

        // Information that should be mandatory
        j["name"] = "Bob";
        j["surname"] = "Spounge";
        j["age"] = 80;
        j["folders"] = { "shells", "caves", "sharks" };
	j["password"] = hpass;

        PetalusBlock creation_block(j);

        sw.createWallet(wallet, creation_block);

        BOOST_CHECK(sw.getTotalWallets() == 0);
        BOOST_CHECK(sw.getTotalCreatedWallets() == 1);
        BOOST_CHECK(sw.getTotalOpenWallets() == 0);

        std::string id = sw.openSession(wallet, hpass);
        BOOST_CHECK(id.length() > 0); // correct password

	SessionContext ctx(wallet, id);

	BOOST_TEST_MESSAGE( "sessionid:(" << id << ")");

        BOOST_CHECK(sw.getTotalWallets() == 1);
        BOOST_CHECK(sw.getTotalCreatedWallets() == 1);
        BOOST_CHECK(sw.getTotalOpenWallets() == 1);

	// Write some blocks on the folders

	PetalusBlock pblock2(data1);
	PetalusBlock pblock3(data2);
	PetalusBlock pblock4(data3);

	BOOST_CHECK(sw.write(ctx, pblock2, "shells") > 0);
	BOOST_CHECK(sw.write(ctx, pblock3, "caves") > 0);
	BOOST_CHECK(sw.write(ctx, pblock4, "sharks") > 0);

        // get the wallet and verify the information
        auto wa = sw.getWallet(id);
        BOOST_CHECK(wa != nullptr);
	BOOST_CHECK(wa->getEncryptionKey() == sw.getEncryptionKey());

        auto folders = wa->getFolders();
        BOOST_CHECK(folders.size() == 3);

        for (auto &folder: folders) {
                auto f = folder.second;

                BOOST_CHECK(f->getTotalBlocks() == 2);
		BOOST_CHECK(f->getEncryptionKey().key().empty() == false);
		BOOST_CHECK(f->getEncryptionKey() == sw.getEncryptionKey());

                const petalus_block_file_v1 *block = f->findBlock(1);
                BOOST_CHECK(block != nullptr);
                BOOST_CHECK(block->type == (uint8_t)BlockType::SYS_FOLDER_NAME);
                
		block = f->findBlock(2);
                BOOST_CHECK(block != nullptr);
                BOOST_CHECK(block->type == (uint8_t)BlockType::USR_CRYPT_PLAIN);
        }

	// Now read some blocks
	PetalusBlock read_block;

	BOOST_CHECK(sw.read(ctx, read_block, "shells", 3) == 0); // No block 3
	BOOST_CHECK(sw.read(ctx, read_block, "shells", 2) > 0); 
	BOOST_CHECK(data1.compare(read_block.data()) == 0);
	
	BOOST_CHECK(sw.read(ctx, read_block, "caves", 3) == 0); // No block 3
	BOOST_CHECK(sw.read(ctx, read_block, "caves", 2) > 0); 
	BOOST_CHECK(data2.compare(read_block.data()) == 0);
	
	BOOST_CHECK(sw.read(ctx, read_block, "sharks", 3) == 0); // No block 3
	BOOST_CHECK(sw.read(ctx, read_block, "sharks", 2) > 0); 
	BOOST_CHECK(data3.compare(read_block.data()) == 0);

	sw.close();
}

BOOST_AUTO_TEST_CASE (test19) // Verify that information on folders can be encrypted and plain
{
        // Remove the wallet from previous runs
        std::string wallet(generateHexBlockFilename());
        std::string spath1(generateTestFile(wallet));
        std::string spath2(generateTestFile(wallet, 2));
        if (std::filesystem::exists(spath1)) std::filesystem::remove(spath1);
        if (std::filesystem::exists(spath2)) std::filesystem::remove(spath2);
        std::string hpass(generateSHA256("john1956"));

        std::string data1("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
        std::string data2("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");

        auto sw = SessionWalletManager();
        sw.setDataDirectory(getDataDirectory());

        std::string orig_password("1234567890qwertyuiopasdfghjklzxc");
        std::string password(generateAESPassword(orig_password));
        sw.setEncryptionPassword(password); // Encrypt all the user data

	BOOST_CHECK(password.compare(sw.getEncryptionPassword()) == 0);

        // Allocate one wallet on the cache
        sw.increaseAllocatedMemory(1);

        nlohmann::json j;

        // Information that should be mandatory
        j["name"] = "Bob";
        j["surname"] = "Spounge";
        j["age"] = 80;
        j["folders"] = { "shells" };
	j["password"] = hpass;

        PetalusBlock creation_block(j);

        sw.createWallet(wallet, creation_block);

        std::string id = sw.openSession(wallet, hpass);
        BOOST_CHECK(id.length() > 0); // correct password

	SessionContext ctx(wallet, id);

        // Write one encrypted block on the folder
        PetalusBlock pblock2(data1);

	// this block is encrypted
        BOOST_CHECK(sw.write(ctx, pblock2, "shells") > 0);

        // Now read the block
        PetalusBlock read_block;

        BOOST_CHECK(sw.read(ctx, read_block, "shells", 2) > 0);
        BOOST_CHECK(data1.compare(read_block.data()) == 0); // 

	sw.close();
}

BOOST_AUTO_TEST_CASE (test20) // Verify the timeouts on the SessionWalletManager
{
        // Remove the wallet from previous runs
        std::string wallet1(generateHexBlockFilename("1"));
        std::string wallet2(generateHexBlockFilename("2"));
        std::string wallet3(generateHexBlockFilename("3"));
        std::string spath1(generateTestFile(wallet1));
        std::string spath2(generateTestFile(wallet2));
        std::string spath3(generateTestFile(wallet3));

        if (std::filesystem::exists(spath1)) std::filesystem::remove(spath1);
        if (std::filesystem::exists(spath2)) std::filesystem::remove(spath2);
        if (std::filesystem::exists(spath3)) std::filesystem::remove(spath3);
        std::string hpass(generateSHA256("asdfasdf"));

        std::string data1("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
        std::string data2("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");

        auto sw = SessionWalletManager();
        sw.setDataDirectory(getDataDirectory());

	// set the Timeout for the sessions to one minute
	sw.setSessionTimeout(60);

        // Allocate one wallet on the cache
        sw.increaseAllocatedMemory(3);

        nlohmann::json j1; j1["name"] = "Bob"; j1["surname"] = "Spounge"; j1["age"] = 80; j1["folders"] = { "shells" };
	j1["password"] = hpass;
        PetalusBlock pblock1(j1);
        sw.createWallet(wallet1, pblock1);

        nlohmann::json j2; j2["name"] = "Rob"; j2["surname"] = "Spounge"; j2["age"] = 30;
	j2["password"] = hpass;
        PetalusBlock pblock2(j2);
        sw.createWallet(wallet2, pblock2);

        nlohmann::json j3; j3["name"] = "Jimi"; j3["surname"] = "hendrix"; j3["age"] = 45;
	j3["password"] = hpass;
        PetalusBlock pblock3(j3);
        sw.createWallet(wallet3, pblock3);

	// Open three sessions
        std::string id1 = sw.openSession(wallet1, hpass);
        BOOST_CHECK(id1.length() > 0); // correct password

        std::string id2 = sw.openSession(wallet2, hpass);
        BOOST_CHECK(id2.length() > 0); // correct password

        std::string id3 = sw.openSession(wallet3, hpass);
        BOOST_CHECK(id3.length() > 0); // correct password

	// Now get the wallets
	auto wa1 = sw.getWallet(id1);
	auto wa2 = sw.getWallet(id2);
	auto wa3 = sw.getWallet(id3);

	BOOST_CHECK(sw.getTotalCreatedWallets() == 3);
	BOOST_CHECK(sw.getTotalOpenWallets() == 3);
	BOOST_CHECK(sw.getTotalCloseWallets() == 0);
	BOOST_CHECK(sw.getTotalTimeoutWallets() == 0);
	BOOST_CHECK(sw.getTotalWallets() == 3);

	std::time_t time1 = wa1->getLastTimeOperation();
	std::time_t time2 = wa2->getLastTimeOperation();
	std::time_t time3 = wa3->getLastTimeOperation();

	// Update the time to two minutes on 2 sessions wallets

	sw.updateSessionTime(id1, time3 + 120);
	sw.updateSessionTime(id2, time3 + 120);
	
	BOOST_CHECK(wa1->getLastTimeOperation() == time3 + 120);
	BOOST_CHECK(wa2->getLastTimeOperation() == time3 + 120);
	BOOST_CHECK(wa3->getLastTimeOperation() == time3); // This will timeout

	sw.updateTimers(time3 + 100);

	BOOST_CHECK(sw.getTotalCreatedWallets() == 3);
	BOOST_CHECK(sw.getTotalOpenWallets() == 3);
	BOOST_CHECK(sw.getTotalCloseWallets() == 1);
	BOOST_CHECK(sw.getTotalTimeoutWallets() == 1);
	BOOST_CHECK(sw.getTotalWallets() == 2);

	BOOST_CHECK(sw.getWallet(id1) != nullptr);
	BOOST_CHECK(sw.getWallet(id2) != nullptr);
	BOOST_CHECK(sw.getWallet(id3) == nullptr);

	sw.close();
}

BOOST_AUTO_TEST_CASE (test21) // Verify the removal of metadata on json blocks if exists
{
        // Remove the wallet from previous runs
        std::string wallet(generateHexBlockFilename());
        std::string spath(generateTestFile(wallet));
        if (std::filesystem::exists(spath)) std::filesystem::remove(spath);
        std::string hpass(generateSHA256("asdfasdf"));

        auto sw = SessionWalletManager();
        sw.setDataDirectory(getDataDirectory());

        // Allocate one wallet on the cache
        sw.increaseAllocatedMemory(1);

        nlohmann::json j1; j1["name"] = "Bob"; j1["surname"] = "Spounge"; j1["age"] = 80;
	j1["password"] = hpass;
        PetalusBlock creation_block(j1);
        sw.createWallet(wallet, creation_block);

	// Some information with metadata
        nlohmann::json j2; 

	j2["have_money"] = true; j2["date"] = "Spounge"; j2["description"] = "AAAAAAAAAAAAAAAAAAAAAAAAA";
  	j2["__metadata__"] = "This can not be shown on read operations";

        PetalusBlock pblock2(j2);

        std::string id = sw.openSession(wallet, hpass);
        BOOST_CHECK(id.length() > 0); // correct password

	SessionContext ctx(wallet, id);

        BOOST_CHECK(sw.write(ctx, pblock2) > 0);

        // Now read the block
        PetalusBlock read_block;

        BOOST_CHECK(sw.read(ctx, read_block, 3) > 0);
	BOOST_CHECK(read_block.type() == BlockType::USR_JSON_STRING);

	// Decode and verify there is no metadata on it
	nlohmann::json j4 = nlohmann::json::parse(read_block.data());

	BOOST_CHECK(j4["have_money"] == true);

	sw.close();
}

// Verify that signed messages with ECDSA are correctly encrypted and decrypted with no issues 
BOOST_AUTO_TEST_CASE (test22) 
{
        // Remove the wallet from previous runs
        std::string wallet(generateHexBlockFilename());
        std::string spath(generateTestFile(wallet));
        if (std::filesystem::exists(spath)) std::filesystem::remove(spath);
        std::string hpass(generateSHA256("juice"));

        auto sw = SessionWalletManager();
        sw.setDataDirectory(getDataDirectory());

        // Allocate one wallet on the cache
        sw.increaseAllocatedMemory(1);

	// Create the public key 
        CryptoPP::AutoSeededRandomPool prng;
        PrivateKey privateKey;
        PublicKey publicKey;
        std::string b64publickey = generateBase64PublicKey(privateKey, publicKey); // Generates a signature of 64 bytes

        BOOST_TEST_MESSAGE( "wallet:(" << wallet << ")");
        BOOST_TEST_MESSAGE( "b64publickey:(" << b64publickey << ")");

        nlohmann::json j;

        j["ec_public_key"] = b64publickey;
	j["information"] = "This is critical information from the user";
	j["password"] = hpass;

        PetalusBlock creation_block(j);

	// Encrypt all the data
        std::string password("{AES}F63BCF56852F75BF0BE97A2CCE96B29531A2253C3AED63FBE318CE81903AF1F1");
        sw.setEncryptionPassword(password); // Encrypt all the user data

        BOOST_CHECK(sw.getTotalWallets() == 0);
        BOOST_CHECK(sw.getTotalCreatedWallets() == 0);
        BOOST_CHECK(sw.getTotalOpenWallets() == 0);

	sw.createWallet(wallet, creation_block);

        BOOST_CHECK(sw.getTotalWallets() == 0);
        BOOST_CHECK(sw.getTotalCreatedWallets() == 1);
        BOOST_CHECK(sw.getTotalOpenWallets() == 0);

        std::string id = sw.openSession(wallet, hpass);
        BOOST_CHECK(id.length() > 0); // correct password

	SessionContext ctx(wallet, id);

        BOOST_CHECK(sw.getTotalWallets() == 1);
        BOOST_CHECK(sw.getTotalCreatedWallets() == 1);
        BOOST_CHECK(sw.getTotalOpenWallets() == 1);

        // Write data that has been signed with the Private key

	std::string rdata("------- BEGIN CERTIFICATE --------\r\nOTBhOTBhNDhlMjNkY2M1MWFkNGE4MjFhMzAxZTM0NDBmZmViNWU5ODZiZDY5ZDdiZjM0N2EyYmEyZGEyM2JkMw==");
        PetalusBlock block(rdata);

        Signer signer(privateKey);

        std::string binary_signature;
        uint8_t digest[HASH_BLOCK_SIZE];

        computeSHA256(digest, (const uint8_t*)rdata.c_str(), rdata.length());

        // Sign the sha256 hash of data
        CryptoPP::StringSource s( digest, HASH_BLOCK_SIZE, true /*pump all*/,
                new CryptoPP::SignerFilter( prng,
                        signer,
                        new CryptoPP::StringSink( binary_signature )
                ) // SignerFilter
        ); // StringSource

	{
        	CryptoPP::HexEncoder encoder;
        	std::string signature_hash;
        	encoder.Attach( new CryptoPP::StringSink(signature_hash) );
        	encoder.Put( (uint8_t*)binary_signature.c_str(), binary_signature.length() );
        	encoder.MessageEnd();

        	BOOST_TEST_MESSAGE( "signature:(" << signature_hash << ")");
	}

	// Set the signature on the block
        block.signature(binary_signature);

	// Write the block
        BOOST_CHECK(sw.write(ctx, block) > 0);

	PetalusBlock read_block;

	// Read the block
	BOOST_CHECK(sw.read(ctx, read_block, 3) > 0);
        BOOST_CHECK(read_block.type() == BlockType::USR_PLAIN);
	BOOST_CHECK(read_block.length() == rdata.length());
	BOOST_CHECK(rdata.compare(read_block.data()) == 0);

	sw.close();
}

// Verify that signed random messages with ECDSA are correctly encrypted and decrypted with no issues
// This test case is similar as test22 but with random data generated
BOOST_AUTO_TEST_CASE (test23)
{
        // Remove the wallet from previous runs
        std::string wallet(generateHexBlockFilename("test23.ecdsa"));
        std::string spath(generateTestFile(wallet));
        if (std::filesystem::exists(spath)) std::filesystem::remove(spath);
        std::string hpass(generateSHA256("the crying machine"));
	std::string encryption_password(generateAESPassword("aaaaaaaaaaaaaaaa1111111111111111"));

        auto sw = SessionWalletManager();
        sw.setDataDirectory(getDataDirectory());

        // Allocate one wallet on the cache
        sw.increaseAllocatedMemory(1);

        // Create the public key
        CryptoPP::AutoSeededRandomPool prng;
        PrivateKey privateKey;
        PublicKey publicKey;
        std::string b64publickey = generateBase64PublicKey(privateKey, publicKey); // Generates a signature of 64 bytes

        BOOST_TEST_MESSAGE( "wallet:(" << wallet << ")");
        BOOST_TEST_MESSAGE( "b64publickey:(" << b64publickey << ")");

        nlohmann::json j;

        j["ec_public_key"] = b64publickey;
	j["password"] = hpass;

        PetalusBlock creation_block(j);

	BOOST_CHECK(sw.getEncryptionKey().key().empty() == true);

        // Encrypt all the data
        sw.setEncryptionPassword(encryption_password); // Encrypt all the user data

	BOOST_CHECK(sw.getEncryptionKey().key().empty() == false);
	BOOST_CHECK(sw.getEncryptionKey().key().size() == 32);

        sw.createWallet(wallet, creation_block);
        std::string id = sw.openSession(wallet, hpass);
        BOOST_CHECK(id.length() > 0); // correct password

	SessionContext ctx(wallet, id);

	auto bfile = sw.getWallet(id)->getBlockFile();
	int32_t max = bfile->getMaxFileSize() - 1024;

	// We create a big block of random string
	max = 1024 * 1024;
	std::string bigdata(generateRandomString(max));

       	PetalusBlock block(bigdata);
       	Signer signer(privateKey);

       	std::string binary_signature;
       	uint8_t digest[HASH_BLOCK_SIZE];

       	computeSHA256(digest, (const uint8_t*)bigdata.c_str(), bigdata.length());

       	// Sign the sha256 hash of data
       	CryptoPP::StringSource s( digest, HASH_BLOCK_SIZE, true /*pump all*/,
               	new CryptoPP::SignerFilter( prng,
                       	signer,
                       	new CryptoPP::StringSink( binary_signature )
               	) // SignerFilter
       	); // StringSource

       	block.signature(binary_signature);

	BOOST_CHECK(bfile->getTotalBlocks() == 1);
       	int ret = sw.write(ctx, block);

	BOOST_CHECK(ret > 0); 
	BOOST_CHECK(sw.getTotalWrites() == 1);
	BOOST_CHECK(bfile->getTotalBlocks() == 2);

        const petalus_block_file_v1 *rblock = bfile->findBlock(2);
        BOOST_CHECK(rblock != nullptr);
        BOOST_CHECK(rblock->type == (uint8_t)BlockType::USR_CRYPT_PLAIN);

	PetalusBlock read_block;

	BOOST_CHECK(bfile->read(2, read_block) > 0);
	BOOST_CHECK(bigdata.compare(read_block.data()) == 0);

	sw.close(id);
}

// Verify error codes on the wallet creation 
BOOST_AUTO_TEST_CASE (test24)
{
        // Remove the wallet from previous runs
        std::string wallet(generateHexBlockFilename());
        std::string spath1(generateTestFile(wallet));
        if (std::filesystem::exists(spath1)) std::filesystem::remove(spath1);
        std::string hpass(generateSHA256("asdfasdf"));
        std::string algorithm("blake2s");

        auto sw = SessionWalletManager();
        sw.setDataDirectory(getDataDirectory());

	// No memory available
        sw.increaseAllocatedMemory(0);

        nlohmann::json j;

        j["name"] = "Bob";
        j["surname"] = "Beamon";
        j["age"] = 74;
	j["password"] = hpass;

        PetalusBlock creation_block(j);

        WalletCreateCode code = sw.createWallet(wallet, creation_block);

	BOOST_CHECK(code == WalletCreateCode::WALLET_NOT_AVAILABLE); // No items on the cache so no wallet :(
        BOOST_CHECK(sw.getTotalWallets() == 0);
        BOOST_CHECK(sw.getTotalCreatedWallets() == 0);
        
	code = sw.createWallet("this is not a valid wallet", creation_block);

	BOOST_CHECK(code == WalletCreateCode::WALLET_WRONG_FORMAT); // Wrong naming format
        BOOST_CHECK(sw.getTotalWallets() == 0);
        BOOST_CHECK(sw.getTotalCreatedWallets() == 0);

        sw.increaseAllocatedMemory(1);

	code = sw.createWallet(wallet, creation_block);

	BOOST_CHECK(code == WalletCreateCode::WALLET_CREATED_SUCCESS); 
        BOOST_CHECK(sw.getTotalWallets() == 0);
        BOOST_CHECK(sw.getTotalCreatedWallets() == 1);

	code = sw.createWallet(wallet, creation_block);

	BOOST_CHECK(code == WalletCreateCode::WALLET_EXISTS); 
        BOOST_CHECK(sw.getTotalWallets() == 0);
        BOOST_CHECK(sw.getTotalCreatedWallets() == 1);

	sw.close();
}

// Verify error codes openning the wallet 
BOOST_AUTO_TEST_CASE (test25)
{
        // Remove the wallet from previous runs
        std::string wallet(generateHexBlockFilename());
        std::string spath1(generateTestFile(wallet));
        if (std::filesystem::exists(spath1)) std::filesystem::remove(spath1);
        std::string hpass(generateSHA256("Tender Surrender"));

        auto sw = SessionWalletManager();
        sw.setDataDirectory(getDataDirectory());

        sw.increaseAllocatedMemory(1);

        std::string id = sw.openSession(wallet, hpass);
        BOOST_CHECK(id.length() == 0);
	BOOST_CHECK(sw.getOpenStatusCode() == WalletOpenCode::WALLET_NO_EXISTS);
	BOOST_CHECK(sw.getTotalFailOpenWallets() == 1);
        BOOST_CHECK(sw.getTotalWallets() == 0);
        BOOST_CHECK(sw.getTotalCreatedWallets() == 0);
	BOOST_CHECK(sw.getTotalOpenWallets() == 0);
	
        nlohmann::json j;

        j["name"] = "Bob";
        j["surname"] = "Beamon";
        j["age"] = 74;
	j["password"] = hpass;

        PetalusBlock creation_block(j);

        WalletCreateCode code = sw.createWallet(wallet, creation_block);

        BOOST_CHECK(code == WalletCreateCode::WALLET_CREATED_SUCCESS); 
        BOOST_CHECK(sw.getTotalWallets() == 0);
        BOOST_CHECK(sw.getTotalCreatedWallets() == 1);
	BOOST_CHECK(sw.getTotalFailOpenWallets() == 1);
	BOOST_CHECK(sw.getTotalOpenWallets() == 0);

        id = sw.openSession(wallet, hpass);
        BOOST_CHECK(id.length() > 0);
	BOOST_CHECK(sw.getOpenStatusCode() == WalletOpenCode::WALLET_OPEN_SUCCESS);
	BOOST_CHECK(sw.getTotalFailOpenWallets() == 1);
        BOOST_CHECK(sw.getTotalWallets() == 1);
        BOOST_CHECK(sw.getTotalCreatedWallets() == 1);
	BOOST_CHECK(sw.getTotalOpenWallets() == 1);

        sw.close();
}

// Simulate an error on the session by closing the blockfile
BOOST_AUTO_TEST_CASE (test26)
{
        // Remove the wallet from previous runs
        std::string wallet(generateHexBlockFilename());
        std::string spath1(generateTestFile(wallet));
        if (std::filesystem::exists(spath1)) std::filesystem::remove(spath1);
        std::string hpass(generateSHA256("Here & Now"));

        auto sw = SessionWalletManager();
        sw.setDataDirectory(getDataDirectory());

        sw.increaseAllocatedMemory(1);

        nlohmann::json j;

        j["name"] = "Bob";
        j["surname"] = "Beamon";
        j["age"] = 74;
	j["password"] = hpass;

        PetalusBlock creation_block(j);

        WalletCreateCode code = sw.createWallet(wallet, creation_block);
        BOOST_CHECK(code == WalletCreateCode::WALLET_CREATED_SUCCESS);

        std::string id = sw.openSession(wallet, hpass);
        BOOST_CHECK(id.length() > 0);
        BOOST_CHECK(sw.getOpenStatusCode() == WalletOpenCode::WALLET_OPEN_SUCCESS);

	SessionContext ctx(wallet, id);

	auto wa = sw.getWallet(id);
        BOOST_CHECK(wa != nullptr);
	auto bfile = wa->getBlockFile();

	// check stats before 
        BOOST_CHECK(sw.getTotalCreatedWallets() == 1);
        BOOST_CHECK(sw.getTotalWallets() == 1);
        BOOST_CHECK(sw.getTotalOpenWallets() == 1);
	BOOST_CHECK(sw.getTotalBytesReads() == 0);
	BOOST_CHECK(sw.getTotalWrongCredentials() == 0);
	BOOST_CHECK(sw.getTotalCloseWallets() == 0);
	BOOST_CHECK(sw.getTotalTimeoutWallets() == 0);
        BOOST_CHECK(sw.getTotalBytesWrites() == 0);
        BOOST_CHECK(sw.getTotalWrites() == 0);
        BOOST_CHECK(sw.getTotalReads() == 0);
        BOOST_CHECK(sw.getTotalFailOpenWallets() == 0);
	BOOST_CHECK(sw.getTotalErrorWrites() == 0);

	// Just close the block file associated to the wallet
	bfile->close();

	PetalusBlock block("This wont be on the wallet");

	int ret = sw.write(ctx, block);
        BOOST_CHECK(ret == 0);

	// check stats after
        BOOST_CHECK(sw.getTotalCreatedWallets() == 1);
        BOOST_CHECK(sw.getTotalWallets() == 1);
        BOOST_CHECK(sw.getTotalOpenWallets() == 1);
	BOOST_CHECK(sw.getTotalBytesReads() == 0);
	BOOST_CHECK(sw.getTotalWrongCredentials() == 0);
	BOOST_CHECK(sw.getTotalCloseWallets() == 0);
	BOOST_CHECK(sw.getTotalTimeoutWallets() == 0);
        BOOST_CHECK(sw.getTotalBytesWrites() == 0);
        BOOST_CHECK(sw.getTotalWrites() == 0);
        BOOST_CHECK(sw.getTotalReads() == 0);
        BOOST_CHECK(sw.getTotalFailOpenWallets() == 0);
	BOOST_CHECK(sw.getTotalErrorWrites() == 1);

	sw.close();
}

// Simulate an empty wallet cache for the opening operation
BOOST_AUTO_TEST_CASE (test27)
{
        // Remove the wallet from previous runs
        std::string wallet(generateHexBlockFilename());
        std::string spath1(generateTestFile(wallet));
        if (std::filesystem::exists(spath1)) std::filesystem::remove(spath1);
        std::string hpass(generateSHA256("liberty"));

        auto sw = SessionWalletManager();
        sw.setDataDirectory(getDataDirectory());

        sw.increaseAllocatedMemory(1);

        nlohmann::json j;

        j["name"] = "Bob";
        j["surname"] = "Beamon";
        j["age"] = 74;
	j["password"] = hpass;

        PetalusBlock creation_block(j);

        WalletCreateCode code = sw.createWallet(wallet, creation_block);
        BOOST_CHECK(code == WalletCreateCode::WALLET_CREATED_SUCCESS);

	sw.decreaseAllocatedMemory(1);

        std::string id = sw.openSession(wallet, hpass);
        BOOST_CHECK(id.length() == 0);
        BOOST_CHECK(sw.getOpenStatusCode() == WalletOpenCode::WALLET_NO_RESOURCES);

        BOOST_CHECK(sw.getTotalCreatedWallets() == 1);
        BOOST_CHECK(sw.getTotalWallets() == 0);
        BOOST_CHECK(sw.getTotalOpenWallets() == 0);
        BOOST_CHECK(sw.getTotalBytesReads() == 0);
        BOOST_CHECK(sw.getTotalWrongCredentials() == 0);
        BOOST_CHECK(sw.getTotalCloseWallets() == 0);
        BOOST_CHECK(sw.getTotalTimeoutWallets() == 0);
        BOOST_CHECK(sw.getTotalBytesWrites() == 0);
        BOOST_CHECK(sw.getTotalWrites() == 0);
        BOOST_CHECK(sw.getTotalReads() == 0);
        BOOST_CHECK(sw.getTotalFailOpenWallets() == 1);
        BOOST_CHECK(sw.getTotalErrorWrites() == 0);

	// Change the value of wallet to something else

        id = sw.openSession(std::string("This can not be open"), hpass);
        BOOST_CHECK(id.length() == 0);
        BOOST_CHECK(sw.getOpenStatusCode() == WalletOpenCode::WALLET_WRONG_FORMAT);

        BOOST_CHECK(sw.getTotalCreatedWallets() == 1);
        BOOST_CHECK(sw.getTotalWallets() == 0);
        BOOST_CHECK(sw.getTotalOpenWallets() == 0);
        BOOST_CHECK(sw.getTotalBytesReads() == 0);
        BOOST_CHECK(sw.getTotalWrongCredentials() == 0);
        BOOST_CHECK(sw.getTotalCloseWallets() == 0);
        BOOST_CHECK(sw.getTotalTimeoutWallets() == 0);
        BOOST_CHECK(sw.getTotalBytesWrites() == 0);
        BOOST_CHECK(sw.getTotalWrites() == 0);
        BOOST_CHECK(sw.getTotalReads() == 0);
        BOOST_CHECK(sw.getTotalFailOpenWallets() == 2);
        BOOST_CHECK(sw.getTotalErrorWrites() == 0);
	
        id = sw.openSession(wallet, std::string("This is not a valid password"));
        BOOST_CHECK(id.length() == 0);
        BOOST_CHECK(sw.getOpenStatusCode() == WalletOpenCode::WALLET_WRONG_FORMAT);

	sw.close();
}

// Verify that wrong credentials close the session properly
BOOST_AUTO_TEST_CASE (test28)
{
        // Remove the wallet from previous runs
        std::string wallet(generateHexBlockFilename());
        std::string spath(generateTestFile(wallet));
        if (std::filesystem::exists(spath)) std::filesystem::remove(spath);
        std::string hpass(generateSHA256("The Story of Light"));

        auto sw = SessionWalletManager();
        sw.setDataDirectory(getDataDirectory());

        sw.increaseAllocatedMemory(1);

        nlohmann::json j;

        j["name"] = "Bob";
        j["surname"] = "Beamon";
        j["age"] = 74;
	j["password"] = hpass;

        PetalusBlock creation_block(j);

        WalletCreateCode code = sw.createWallet(wallet, creation_block);
        BOOST_CHECK(code == WalletCreateCode::WALLET_CREATED_SUCCESS);

        std::string id1 = sw.openSession(wallet, hpass);
        BOOST_CHECK(id1.length() > 0);

        BOOST_CHECK(sw.getTotalOpenWallets() == 1);
        BOOST_CHECK(sw.getTotalBytesReads() == 0);
        BOOST_CHECK(sw.getTotalWrongCredentials() == 0);
        BOOST_CHECK(sw.getTotalReuseWallets() == 0);
	BOOST_CHECK(sw.getTotalCloseWallets() == 0);

	std::string id2 = sw.openSession(wallet, hpass);
        BOOST_CHECK(id2.length() > 0);
	BOOST_CHECK(id1.compare(id2) == 0);

        BOOST_CHECK(sw.getTotalOpenWallets() == 1);
        BOOST_CHECK(sw.getTotalWallets() == 1);
        BOOST_CHECK(sw.getTotalBytesReads() == 0);
        BOOST_CHECK(sw.getTotalWrongCredentials() == 0);
        BOOST_CHECK(sw.getTotalReuseWallets() == 1);
	BOOST_CHECK(sw.getTotalCloseWallets() == 0);

	// Now wrong credentials should close the session
        std::string id3 = sw.openSession(wallet, "AAAABBBBCCCCDDDEEEFFF");
        BOOST_CHECK(id3.length() == 0);

        BOOST_CHECK(sw.getTotalOpenWallets() == 1);
        BOOST_CHECK(sw.getTotalWallets() == 0);
        BOOST_CHECK(sw.getTotalBytesReads() == 0);
        BOOST_CHECK(sw.getTotalWrongCredentials() == 1);
        BOOST_CHECK(sw.getTotalReuseWallets() == 1);
        BOOST_CHECK(sw.getTotalCloseWallets() == 1);

	BOOST_CHECK(sw.getWallet(id1) == nullptr);
	BOOST_CHECK(sw.getWallet(id2) == nullptr);

	sw.close();
}

// This test case generates a wallet with no public key and writes
// a signed block and verifies that there is no signature and flag on the block written 
BOOST_AUTO_TEST_CASE (test29)
{
        // Remove the wallet from previous runs
        std::string wallet(generateHexBlockFilename("test29.ecdsa"));
        std::string spath(generateTestFile(wallet));
        if (std::filesystem::exists(spath)) std::filesystem::remove(spath);
        std::string hpass(generateSHA256("Gravity Storm"));
	std::string encryption_password(generateAESPassword("aaaaaaaaaaaaaaaa1111111111111111"));

        auto sw = SessionWalletManager();
        sw.setDataDirectory(getDataDirectory());

        // Allocate one wallet on the cache
        sw.increaseAllocatedMemory(1);

        // Create the public key
        CryptoPP::AutoSeededRandomPool prng;
        PrivateKey privateKey;
        PublicKey publicKey;
        std::string b64publickey = generateBase64PublicKey(privateKey, publicKey); // Generates a signature of 64 bytes

        BOOST_TEST_MESSAGE( "wallet:(" << wallet << ")");

        nlohmann::json j;

        j["nothing"] = "No public key for this wallet";
        j["password"] = hpass;

        PetalusBlock creation_block(j);

	BOOST_CHECK(sw.getEncryptionKey().key().empty() == true);

        // Encrypt all the data
        sw.setEncryptionPassword(encryption_password); // Encrypt all the user data

	BOOST_CHECK(sw.getEncryptionKey().key().empty() == false);
	BOOST_CHECK(sw.getEncryptionKey().key().size() == 32);

        sw.createWallet(wallet, creation_block);
        std::string id = sw.openSession(wallet, hpass);
        BOOST_CHECK(id.length() > 0); // correct password

	SessionContext ctx(wallet, id);

        std::string bigdata("This data can not be signed, because I dont have public key :(");

        PetalusBlock block(bigdata);
        Signer signer(privateKey);

        std::string binary_signature;
        uint8_t digest[HASH_BLOCK_SIZE];

        computeSHA256(digest, (const uint8_t*)bigdata.c_str(), bigdata.length());

        // Sign the sha256 hash of data
        CryptoPP::StringSource s( digest, HASH_BLOCK_SIZE, true /*pump all*/,
                new CryptoPP::SignerFilter( prng,
                        signer,
                        new CryptoPP::StringSink( binary_signature )
                ) // SignerFilter
        ); // StringSource

        block.signature(binary_signature);

	// The block is writen but without the signature and without the flag 0x01
        int ret = sw.write(ctx, block);
	BOOST_CHECK(ret == block.length());

        auto wa = sw.getWallet(id);
        BOOST_CHECK(wa != nullptr);
        auto bfile = wa->getBlockFile();

        // The block has been modified
        BOOST_CHECK(block.signature_length() == 64);
        BOOST_CHECK(block.length() >= bigdata.length());
        BOOST_CHECK(bfile->havePublicKey() == false);

        // Check the size of the block that has been written
        const petalus_block_file_v1 *rblock = bfile->findBlock(3);
        BOOST_CHECK(rblock != nullptr);
        BOOST_CHECK(rblock->type == static_cast<const uint8_t>(BlockType::USR_CRYPT_PLAIN));
        BOOST_CHECK(rblock->length == block.length());
        BOOST_CHECK(rblock->signature_length > 0); // the block is not signed

        // check stats before
        BOOST_CHECK(sw.getTotalCreatedWallets() == 1);
        BOOST_CHECK(sw.getTotalWallets() == 1);
        BOOST_CHECK(sw.getTotalOpenWallets() == 1);
        BOOST_CHECK(sw.getTotalBytesReads() == 0);
        BOOST_CHECK(sw.getTotalWrongCredentials() == 0);
        BOOST_CHECK(sw.getTotalCloseWallets() == 0);
        BOOST_CHECK(sw.getTotalTimeoutWallets() == 0);
        BOOST_CHECK(sw.getTotalBytesWrites() == ret);
        BOOST_CHECK(sw.getTotalWrites() == 1);
        BOOST_CHECK(sw.getTotalReads() == 0);
        BOOST_CHECK(sw.getTotalFailOpenWallets() == 0);
        BOOST_CHECK(sw.getTotalErrorWrites() == 0);

	sw.close();
}

// Verifies that on the SessionWalletManager the data path is correct
BOOST_AUTO_TEST_CASE (test30)
{
        auto sw = SessionWalletManager();
        sw.setDataDirectory("/etc/resolv.conf");

	BOOST_CHECK(strlen(sw.getDataDirectory()) == 0);

        sw.setDataDirectory("/dev/null");
	BOOST_CHECK(strlen(sw.getDataDirectory()) == 0);

	// This generates a excption that is manage internally
	if (geteuid() != 0) {
		sw.setDataDirectory("/root/test_data/");
		BOOST_CHECK(strlen(sw.getDataDirectory()) == 0);
	
		sw.setDataDirectory("/proc/");
		BOOST_CHECK(strlen(sw.getDataDirectory()) == 0);
	}

	std::string newpath(getDataDirectory());

	sw.createDirectoryStructure(newpath);
	BOOST_CHECK(sw.verifyDirectoryStructure(newpath) == true);
	
	// Remove one directory from the data directory
	std::filesystem::path path(newpath);

	path /= "0";
	path /= "1";

	std::filesystem::remove_all(path);
	BOOST_CHECK(std::filesystem::exists(path) == false);
	BOOST_CHECK(sw.verifyDirectoryStructure(newpath) == false);
	
	// Now sets again an the setter will fix the issue
	sw.setDataDirectory(newpath);
	BOOST_CHECK(sw.verifyDirectoryStructure(newpath) == true);
}

// Verifies that on the SessionWalletManager the sessions
// are open correctly even if a session attack happens 
BOOST_AUTO_TEST_CASE (test31)
{
        // Remove the wallet from previous runs
        std::string wallet1(generateHexBlockFilename("1"));
        std::string wallet2(generateHexBlockFilename("2"));
        std::string spath1(generateTestFile(wallet1));
        std::string spath2(generateTestFile(wallet2));

        if (std::filesystem::exists(spath1)) std::filesystem::remove(spath1);
        if (std::filesystem::exists(spath2)) std::filesystem::remove(spath2);
        std::string hpass(generateSHA256("Building the Church"));

        auto sw = SessionWalletManager();
        sw.setDataDirectory(getDataDirectory());
        sw.increaseAllocatedMemory(2);

        nlohmann::json j1;
        nlohmann::json j2;

        j1["name"] = "Craig";
        j1["surname"] = "Kelly";
	j1["password"] = hpass;

        j2["name"] = "Travis";
        j2["surname"] = "Rice";
	j2["password"] = hpass;

        PetalusBlock creation_block1(j1);
        PetalusBlock creation_block2(j2);

        WalletCreateCode code = sw.createWallet(wallet1, creation_block1);
        BOOST_CHECK(code == WalletCreateCode::WALLET_CREATED_SUCCESS);
        code = sw.createWallet(wallet2, creation_block2);
        BOOST_CHECK(code == WalletCreateCode::WALLET_CREATED_SUCCESS);

	// Opens the first wallet
	BOOST_CHECK(sw.getTotalSessionMismatchs() == 0);
        std::string id1 = sw.openSession(wallet1, hpass);
        BOOST_CHECK(id1.length() > 0);
        BOOST_CHECK(sw.getOpenStatusCode() == WalletOpenCode::WALLET_OPEN_SUCCESS);
	BOOST_CHECK(sw.getTotalSessionMismatchs() == 0);

	// Opens the second wallet
        std::string id2 = sw.openSession(wallet2, hpass);
        BOOST_CHECK(id2.length() > 0);
        BOOST_CHECK(sw.getOpenStatusCode() == WalletOpenCode::WALLET_OPEN_SUCCESS);
	BOOST_CHECK(sw.getTotalSessionMismatchs() == 0);

	// Tries to read from another wallet with the same token 	
        SessionContext ctx(wallet2, id1);

        // Now read the block
        PetalusBlock read_block;

        BOOST_CHECK(sw.read(ctx, read_block, 1) == 0);

	BOOST_CHECK(sw.getTotalSessionMismatchs() == 1);

	// Now tries to polute the second wallet

	PetalusBlock polute_block("This can not be written");

        BOOST_CHECK(sw.write(ctx, polute_block) == 0);
	BOOST_CHECK(sw.getTotalSessionMismatchs() == 2);

        {
                RedirectOutput out;

		sw.showSession(out.cout, id1);
		sw.showSession(out.cout, id2);
        }

	BOOST_CHECK(sw.close(id1) == true);
	BOOST_CHECK(sw.close(id2) == true);
	BOOST_CHECK(sw.close(std::string("can not be close")) == false);

	sw.close();
}

// Verifies the read_only property of the SessionWalletManager 
BOOST_AUTO_TEST_CASE (test32)
{
        // Remove the wallet from previous runs
        std::string wallet(generateHexBlockFilename());
        std::string spath(generateTestFile(wallet));

        if (std::filesystem::exists(spath)) std::filesystem::remove(spath);
        std::string hpass(generateSHA256("The Crying Machine"));

        auto sw = SessionWalletManager();
        sw.setDataDirectory(getDataDirectory());
        sw.increaseAllocatedMemory(2);

        nlohmann::json j;

        j["name"] = "Craig";
        j["surname"] = "Kelly";
	j["password"] = hpass;

        PetalusBlock creation_block(j);

	sw.setReadOnly(true);

        WalletCreateCode code = sw.createWallet(wallet, creation_block);
        BOOST_CHECK(code == WalletCreateCode::WALLET_READ_ONLY);

	sw.setReadOnly(false);

        code = sw.createWallet(wallet, creation_block);
        BOOST_CHECK(code == WalletCreateCode::WALLET_CREATED_SUCCESS);

	sw.setReadOnly(true);

	std::string id = sw.openSession(wallet, hpass);
        BOOST_CHECK(id.length() > 0);
        BOOST_CHECK(sw.getOpenStatusCode() == WalletOpenCode::WALLET_OPEN_SUCCESS);

	SessionContext ctx(wallet, id);

        PetalusBlock block("This can not be written");

        BOOST_CHECK(sw.write(ctx, block) == 0);

	sw.setReadOnly(false);

	std::string data(generateRandomString(57));
	block.data(data);
        BOOST_CHECK(sw.write(ctx, block) > 0);

	// Now read the block
        PetalusBlock read_block1;

        BOOST_CHECK(sw.read(ctx, read_block1, 3) > 0);
	BOOST_CHECK(data.compare(read_block1.data()) == 0);

	sw.setReadOnly(true);

	PetalusBlock read_block2;

        BOOST_CHECK(sw.read(ctx, read_block2, 3) > 0);
	BOOST_CHECK(data.compare(read_block2.data()) == 0);

	sw.close();
}

// Tries to create a wallet with a RSA public key
BOOST_AUTO_TEST_CASE (test33)
{
        // Remove the wallet from previous runs
        std::string wallet(generateHexBlockFilename());
        std::string spath(generateTestFile(wallet));

        if (std::filesystem::exists(spath)) std::filesystem::remove(spath);
        std::string hpass(generateSHA256("Bad Horsie"));

        auto sw = SessionWalletManager();
        sw.setDataDirectory(getDataDirectory());
        sw.increaseAllocatedMemory(2);

        nlohmann::json j;

	CryptoPP::AutoSeededRandomPool rng;

	CryptoPP::RSA::PrivateKey privateKey;
	privateKey.GenerateRandomWithKeySize(rng, 3072);

	CryptoPP::RSA::PublicKey publicKey(privateKey);

        std::string pkey;
        CryptoPP::StringSink ss(pkey);
        publicKey.Save(ss); // Save the public key on a string

        // Convert to a base64 string
        std::string b64publickey;
        CryptoPP::StringSource(pkey, true, new CryptoPP::Base64Encoder(new CryptoPP::StringSink(b64publickey)));

        j["name"] = "Shaun";
        j["surname"] = "White";
	j["ec_public_key"] = b64publickey;
	j["password"] = hpass;

        PetalusBlock creation_block(j);

        WalletCreateCode code = sw.createWallet(wallet, creation_block);
        BOOST_CHECK(code == WalletCreateCode::WALLET_WRONG_PUBLIC_KEY);
}

// Exercises the read only functionality on the SessionWalletManager
BOOST_AUTO_TEST_CASE (test34)
{
        // Remove the wallet from previous runs
        std::string wallet(generateHexBlockFilename());
        std::string spath(generateTestFile(wallet));

        if (std::filesystem::exists(spath)) std::filesystem::remove(spath);
        std::string hpass(generateSHA256("asdfasdf"));

        auto sw = SessionWalletManager();
        sw.setDataDirectory(getDataDirectory());
        sw.increaseAllocatedMemory(1);

        nlohmann::json j;

        j["name"] = "Shaun";
        j["surname"] = "White";
        j["email"] = "shaun.white@white.com";
	j["password"] = hpass;

        PetalusBlock creation_block(j);

        WalletCreateCode code = sw.createWallet(wallet, creation_block);
        BOOST_CHECK(code == WalletCreateCode::WALLET_CREATED_SUCCESS);

	std::string id = sw.openReadOnlySession(std::string("this is not a wallet"));
        BOOST_CHECK(id.length() == 0);
        BOOST_CHECK(sw.getOpenStatusCode() == WalletOpenCode::WALLET_WRONG_FORMAT);

        sw.decreaseAllocatedMemory(1);
	id = sw.openReadOnlySession(std::string("AAAABBBCCCDDEEF"));
        BOOST_CHECK(id.length() == 0);
        BOOST_CHECK(sw.getOpenStatusCode() == WalletOpenCode::WALLET_NO_RESOURCES);
        
	sw.increaseAllocatedMemory(1);
	id = sw.openReadOnlySession(std::string("AAAABBBCCCDDEEF"));
        BOOST_CHECK(id.length() == 0);
        BOOST_CHECK(sw.getOpenStatusCode() == WalletOpenCode::WALLET_NO_EXISTS);
	
	id = sw.openReadOnlySession(wallet);
        BOOST_CHECK(id.length() > 0);
        BOOST_CHECK(sw.getOpenStatusCode() == WalletOpenCode::WALLET_OPEN_SUCCESS);
}

// Verifies that readonly works with existing wallet of the user open
BOOST_AUTO_TEST_CASE (test35)
{
        // Remove the wallet from previous runs
        std::string wallet(generateHexBlockFilename());
        std::string spath(generateTestFile(wallet));

        if (std::filesystem::exists(spath)) std::filesystem::remove(spath);
        std::string hpass(generateSHA256("asdfasdf"));

        auto sw = SessionWalletManager();
        sw.setDataDirectory(getDataDirectory());
        sw.increaseAllocatedMemory(1);

        nlohmann::json j;

        j["name"] = "Shaun";
        j["surname"] = "White";
        j["email"] = "shaun.white@white.com";
        j["password"] = hpass;

        PetalusBlock creation_block(j);

        WalletCreateCode code = sw.createWallet(wallet, creation_block);
        BOOST_CHECK(code == WalletCreateCode::WALLET_CREATED_SUCCESS);

	// The user opens his wallet
        std::string id = sw.openSession(wallet, hpass);
        BOOST_CHECK(id.length() > 0);
        BOOST_CHECK(sw.getOpenStatusCode() == WalletOpenCode::WALLET_OPEN_SUCCESS);

        BOOST_CHECK(sw.getTotalCreatedWallets() == 1);
        BOOST_CHECK(sw.getTotalWallets() == 1);
        BOOST_CHECK(sw.getTotalOpenWallets() == 1);
        BOOST_CHECK(sw.getTotalReuseWallets() == 0);
        BOOST_CHECK(sw.getTotalWrongCredentials() == 0);
        BOOST_CHECK(sw.getTotalCloseWallets() == 0);
        BOOST_CHECK(sw.getTotalFailOpenWallets() == 0);

	// The read only systems opens the same wallet
        std::string rid = sw.openReadOnlySession(wallet);
        BOOST_CHECK(rid.length() > 0);
        BOOST_CHECK(sw.getOpenStatusCode() == WalletOpenCode::WALLET_OPEN_SUCCESS);

	// The wallet is in memory 
        BOOST_CHECK(sw.getTotalCreatedWallets() == 1);
        BOOST_CHECK(sw.getTotalWallets() == 1);
        BOOST_CHECK(sw.getTotalOpenWallets() == 1);
        BOOST_CHECK(sw.getTotalReuseWallets() == 1);
        BOOST_CHECK(sw.getTotalWrongCredentials() == 0);
        BOOST_CHECK(sw.getTotalCloseWallets() == 0);
        BOOST_CHECK(sw.getTotalFailOpenWallets() == 0);

	BOOST_CHECK(id.compare(rid) == 0);

        {
                RedirectOutput out;

		sw.showSession(out.cout, id, 1);
		sw.showSession(out.cout, id, std::string("dont_exist"), 1);
        }

	sw.close();
}

// Verifies that user can access to his wallet even if the access password and public keys 
// has been changed
BOOST_AUTO_TEST_CASE (test36_upgrade_public_key_password)
{
        // Remove the wallet from previous runs
        std::string wallet(generateHexBlockFilename());
        std::string spath(generateTestFile(wallet));

        if (std::filesystem::exists(spath)) std::filesystem::remove(spath);
        std::string hpass1(generateSHA256("first1234"));
        std::string hpass2(generateSHA256("second3456"));

        auto sw = SessionWalletManager();
        sw.setDataDirectory(getDataDirectory());

        // Allocate one wallet on the cache
        sw.increaseAllocatedMemory(1);

        // Create the public key
        CryptoPP::AutoSeededRandomPool prng;
        PrivateKey privateKey1, privateKey2;
        PublicKey publicKey1, publicKey2;
        std::string b64publickey1 = generateBase64PublicKey(privateKey1, publicKey1);
        std::string b64publickey2 = generateBase64PublicKey(privateKey2, publicKey2);

        BOOST_TEST_MESSAGE( "wallet:(" << wallet << ")");

       	nlohmann::json j;

        j["ec_public_key"] = b64publickey1;
        j["password"] = hpass1;

        PetalusBlock creation_block(j);

	WalletCreateCode code = sw.createWallet(wallet, creation_block);
        BOOST_CHECK(code == WalletCreateCode::WALLET_CREATED_SUCCESS);

        std::string id = sw.openSession(wallet, hpass1);
        BOOST_CHECK(id.length() > 0); // correct password

        SessionContext ctx1(wallet, id);

        std::string data1(generateRandomString(127));
        std::string data2(generateRandomString(24691));

        PetalusBlock block1(data1);
        Signer signer1(privateKey1);

        std::string binary_signature1;
        uint8_t digest[HASH_BLOCK_SIZE];

        computeSHA256(digest, (const uint8_t*)data1.c_str(), data1.length());

        // Sign the sha256 hash of data
        CryptoPP::StringSource s1( digest, HASH_BLOCK_SIZE, true /*pump all*/,
                new CryptoPP::SignerFilter( prng,
                        signer1,
                        new CryptoPP::StringSink( binary_signature1 )
                ) // SignerFilter
        ); // StringSource

        block1.signature(binary_signature1);

        // The block is writen but without the signature and without the flag 0x01
        BOOST_CHECK(sw.write(ctx1, block1) > 0);

	sw.close();

	auto wa = Wallet(wallet);

        BOOST_CHECK(wa.getWriteErrorCode() == BlockFileWriteCode::BLOCKFILE_WRITE_FILE_NOT_OPEN);
        wa.open(spath, false);

        BOOST_CHECK(wa.getTotalBlocks() == 2);
	wa.writePassword(hpass2, b64publickey2);	
	wa.close();

	// Now the user reopens his wallet

        id = sw.openSession(wallet, hpass1);
        BOOST_CHECK(id.length() == 0); // old password dont work

        id = sw.openSession(wallet, hpass2);
        BOOST_CHECK(id.length() > 0); // new password

        SessionContext ctx2(wallet, id);

	// Write one block with the new publickey
        PetalusBlock block2(data2);
        std::string binary_signature2;
        Signer signer2(privateKey2);

        computeSHA256(digest, (const uint8_t*)data2.c_str(), data2.length());

        // Sign the sha256 hash of data
        CryptoPP::StringSource s2( digest, HASH_BLOCK_SIZE, true /*pump all*/,
                new CryptoPP::SignerFilter( prng,
                        signer2,
                        new CryptoPP::StringSink( binary_signature2 )
                ) // SignerFilter
        ); // StringSource

        block2.signature(binary_signature2);

        BOOST_CHECK(sw.write(ctx2, block2) > 0);

	// Read the written blocks
	PetalusBlock read_block1, read_block2;

        BOOST_CHECK(sw.read(ctx2, read_block1, 2) > 0);
        BOOST_CHECK(data1.compare(read_block1.data()) == 0);

        BOOST_CHECK(sw.read(ctx2, read_block2, 4) > 0);
        BOOST_CHECK(data2.compare(read_block2.data()) == 0);

	sw.close();
}

BOOST_AUTO_TEST_SUITE_END()

// This test suite is for copying block files with different encryption keys
// and preserve the information of the blocks as it was
BOOST_AUTO_TEST_SUITE(blockfile_clone_suite) 

BOOST_AUTO_TEST_CASE (test01_sanity)
{
        std::string filename1(generateBlockFileName("1"));
        std::string filename2(generateBlockFileName("2"));
        auto bfile1 = BlockFile();
        auto bfile2 = BlockFile();

        // remove the file if exists from previous runs
        if (std::filesystem::exists(filename1)) std::filesystem::remove(filename1);
        if (std::filesystem::exists(filename2)) std::filesystem::remove(filename2);

        bfile1.open(filename1);

        PetalusBlock block1("This is for the first block");
        PetalusBlock block2("AAAABBBB");
        PetalusBlock block3("XXXXXXXX");

        bfile1.write(block1);
        bfile1.write(block2);
        bfile1.write(block3);

        BOOST_CHECK(bfile1.getTotalBlocks() == 3);

	bfile2.open(filename2);

	BOOST_CHECK(bfile2.is_open() == true);
        BOOST_CHECK(bfile2.getTotalBlocks() == 0);

	bfile2.clone(bfile1);
        BOOST_CHECK(bfile2.getTotalBlocks() == 3);

	PetalusBlock rblock1, rblock2, rblock3;

	BOOST_CHECK(bfile2.read(1, rblock1) > 0);
	BOOST_CHECK(bfile2.read(2, rblock2) > 0);
	BOOST_CHECK(bfile2.read(3, rblock3) > 0);

	BOOST_CHECK(block1 == rblock1);
	BOOST_CHECK(block2 == rblock2);
	BOOST_CHECK(block3 == rblock3);

	// Also the generated hashes should be the same!
	for (int i = 1; i <= bfile1.getTotalBlocks(); ++i) {
        	uint8_t digest1[HASH_BLOCK_SIZE];
        	uint8_t digest2[HASH_BLOCK_SIZE];

        	bfile1.readBlockHash(i, digest1);
        	bfile2.readBlockHash(i, digest2);

		BOOST_CHECK(std::memcmp(digest1, digest2, HASH_BLOCK_SIZE) == 0);
	}

	BOOST_CHECK(bfile1.getTotalDataBytes() == bfile2.getTotalDataBytes());
	BOOST_CHECK(bfile1.getTotalUserDataBytes() == bfile2.getTotalUserDataBytes());
	BOOST_CHECK(bfile1.getTotalBytes() == bfile2.getTotalBytes());

	bfile1.close();
	bfile2.close();
}

// Similar test as the previous one but we change the hashing model on the destination
BOOST_AUTO_TEST_CASE (test02_sanity_blake2)
{
        std::string filename1(generateBlockFileName("1"));
        std::string filename2(generateBlockFileName("2"));
        auto bfile1 = BlockFile();
        auto bfile2 = BlockFile();

        // remove the file if exists from previous runs
        if (std::filesystem::exists(filename1)) std::filesystem::remove(filename1);
        if (std::filesystem::exists(filename2)) std::filesystem::remove(filename2);

        bfile1.open(filename1);

        PetalusBlock block1(generateRandomString(4159));
        PetalusBlock block2(generateRandomString(5569));
        PetalusBlock block3(generateRandomString(3307));

        bfile1.write(block1);
        bfile1.write(block2);
        bfile1.write(block3);

        BOOST_CHECK(bfile1.getTotalBlocks() == 3);

        bfile2.open(filename2);
	bfile2.setBlockHashType(BlockHashTypes::BLAKE2S);

        BOOST_CHECK(bfile2.is_open() == true);
        BOOST_CHECK(bfile2.getTotalBlocks() == 0);

        bfile2.clone(bfile1);
        BOOST_CHECK(bfile2.getTotalBlocks() == 3);

        PetalusBlock rblock1, rblock2, rblock3;

        BOOST_CHECK(bfile2.read(1, rblock1) > 0);
        BOOST_CHECK(bfile2.read(2, rblock2) > 0);
        BOOST_CHECK(bfile2.read(3, rblock3) > 0);

	// The information is the same
        BOOST_CHECK(block1 == rblock1);
        BOOST_CHECK(block2 == rblock2);
        BOOST_CHECK(block3 == rblock3);

        // The generated hashes are different 
        for (int i = 1; i <= bfile1.getTotalBlocks(); ++i) {
                uint8_t digest1[HASH_BLOCK_SIZE];
                uint8_t digest2[HASH_BLOCK_SIZE];

                bfile1.readBlockHash(i, digest1);
                bfile2.readBlockHash(i, digest2);

                BOOST_CHECK(std::memcmp(digest1, digest2, HASH_BLOCK_SIZE) != 0);
        }

        bfile1.close();
        bfile2.close();
}

// Same as before but both files have different encryption keys
BOOST_AUTO_TEST_CASE (test03_sanity_encryption)
{
        std::string filename1(generateBlockFileName("1"));
        std::string filename2(generateBlockFileName("2"));
        auto bfile1 = BlockFile();
        auto bfile2 = BlockFile();
        std::string password1("1234567890qazxsw");
        std::string password2("----567890HHHHHH");
        EncryptionKey key1, key2;

        key1.assign((const uint8_t*)password1.data(), password1.length());
        key2.assign((const uint8_t*)password2.data(), password2.length());

        // remove the file if exists from previous runs
        if (std::filesystem::exists(filename1)) std::filesystem::remove(filename1);
        if (std::filesystem::exists(filename2)) std::filesystem::remove(filename2);

	bfile1.setEncryptionKey(key1);
        bfile1.open(filename1);

        std::vector<std::string> data_blocks {
                { generateRandomString(generateRandomInt(2048)) },
                { generateRandomString(generateRandomInt(2048)) },
                { generateRandomString(generateRandomInt(2048)) },
                { generateRandomString(generateRandomInt(2048)) },
                { generateRandomString(generateRandomInt(2048)) },
                { generateRandomString(generateRandomInt(2048)) },
                { generateRandomString(generateRandomInt(2048)) },
                { generateRandomString(generateRandomInt(2048)) },
                { generateRandomString(generateRandomInt(2048)) },
                { generateRandomString(generateRandomInt(2048)) },
                { generateRandomString(generateRandomInt(2048)) },
                { generateRandomString(generateRandomInt(2048)) },
                { generateRandomString(generateRandomInt(2048)) }
        };

	for (auto &it: data_blocks) {
		PetalusBlock block(it);
		bfile1.write(block);
	}

        BOOST_CHECK(bfile1.getTotalBlocks() == data_blocks.size());

	bfile2.setEncryptionKey(key2);
        bfile2.open(filename2);

        BOOST_CHECK(bfile2.is_open() == true);
        BOOST_CHECK(bfile2.getTotalBlocks() == 0);

        bfile2.clone(bfile1);
        BOOST_CHECK(bfile2.getTotalBlocks() == bfile1.getTotalBlocks());

	for (int i = 1; i <= bfile1.getTotalBlocks(); ++i) {
		PetalusBlock rblock;
        	BOOST_CHECK(bfile2.read(i, rblock) > 0);

        	// The information is the same
        	BOOST_CHECK(data_blocks[i - 1].compare(rblock.data()) == 0);
	}

        bfile1.close();
        bfile2.close();
}

// Verify that clone works with blocks that are signed
BOOST_AUTO_TEST_CASE (test04_sanity_encryption_clone_signature)
{
        std::string filename1(generateBlockFileName("1"));
        std::string filename2(generateBlockFileName("2"));
        std::string password1(generateAESPassword("aaaaaaaaaaaaaaaa"));
	std::string password2(generateBlowfishPassword("----567890HHHHHH"));
        EncryptionKey key1, key2;

        // remove the file if exists from previous runs
        if (std::filesystem::exists(filename1)) std::filesystem::remove(filename1);
        if (std::filesystem::exists(filename2)) std::filesystem::remove(filename2);

	BOOST_CHECK(key1.password(password1) == true);
	BOOST_CHECK(key2.password(password2) == true);
	BOOST_CHECK(key1.type() == EncryptionKeyType::AES);
	BOOST_CHECK(key2.type() == EncryptionKeyType::BLOWFISH);

        std::string data(generateRandomString(generateRandomInt(2048))); 

        auto bfile1 = BlockFile();
        bfile1.open(filename1);
        bfile1.setEncryptionKey(key1);

        CryptoPP::AutoSeededRandomPool prng;
        PrivateKey privateKey;
        PublicKey publicKey;
        std::string b64publickey = generateBase64PublicKey(privateKey, publicKey); // Generates a signature of 64 bytes

        nlohmann::json j;

        j["ec_public_key"] = b64publickey;

        std::ostringstream out;
        out << j;

        PetalusBlock jblock(out.str(), BlockType::SYS_JSON_BLOCKCHAIN_ACCESS);

        BOOST_CHECK(bfile1.havePublicKey() == false);

        bfile1.write(jblock);
        bfile1.close();

	// Now we reopen the file and write a signed block
        auto bfile2 = BlockFile();
        bfile2.open(filename1);
        bfile2.setEncryptionKey(key1);

        BOOST_CHECK(bfile2.havePublicKey() == true);

        PetalusBlock block(data);
        Signer signer(privateKey);

        std::string binary_signature;
        uint8_t digest[HASH_BLOCK_SIZE];

        computeSHA256(digest, (const uint8_t*)data.c_str(), data.length());

        // Sign the sha256 hash of data
        CryptoPP::StringSource s( digest, HASH_BLOCK_SIZE, true /*pump all*/,
                new CryptoPP::SignerFilter( prng,
                        signer,
                        new CryptoPP::StringSink( binary_signature )
                ) // SignerFilter
        ); // StringSource

        block.signature(binary_signature);

        BOOST_CHECK(bfile2.getTotalBlocks() == 1);
        BOOST_CHECK(bfile2.havePublicKey() == true);
        BOOST_CHECK(block.signature_length() == binary_signature.length());
        BOOST_CHECK(block.length() == data.length());

        bfile2.write(block);

        // The block has been modified
        BOOST_CHECK(block.signature_length() == binary_signature.length());
        BOOST_CHECK(bfile2.getTotalBlocks() == 2);
        BOOST_CHECK(bfile2.havePublicKey() == true);

        // Check the size of the block that has been written
        const petalus_block_file_v1 *rblock = bfile2.findBlock(2);
        BOOST_CHECK(rblock != nullptr);
        BOOST_CHECK(rblock->type == static_cast<const uint8_t>(BlockType::USR_CRYPT_PLAIN));
        BOOST_CHECK(rblock->length == block.length());
        BOOST_CHECK(rblock->signature_length == block.signature_length()); // the block has been signed

	BOOST_CHECK(bfile2.verifySignatureBlock(2) == true);

	// Now we make the cloning operation with a different password
	
        auto bfile3 = BlockFile();
        bfile3.open(filename2);
        bfile3.setEncryptionKey(key2);

        BOOST_CHECK(bfile3.is_open() == true);
        BOOST_CHECK(bfile3.getTotalBlocks() == 0);

        bfile3.clone(bfile2);
        BOOST_CHECK(bfile3.getTotalBlocks() == 2);

	bfile2.close();
	bfile3.close();

	// Now reopen the new file generated

        auto bfile4 = BlockFile();
        bfile4.open(filename2);
        bfile4.setEncryptionKey(key2);

        rblock = bfile4.findBlock(2);
        BOOST_CHECK(rblock != nullptr);
        BOOST_CHECK(rblock->type == static_cast<const uint8_t>(BlockType::USR_CRYPT_PLAIN));
        BOOST_CHECK(rblock->signature_length == binary_signature.length()); // the block has been signed

	// Verify the content and the signature of the block
	PetalusBlock read_block;
	
	bfile4.read(2, read_block);

	BOOST_CHECK(read_block.signature_length() == binary_signature.length());
	BOOST_CHECK(data.compare(read_block.data()) == 0);
	BOOST_CHECK(std::memcmp(read_block.signature(), binary_signature.c_str(), binary_signature.length()) == 0);
	BOOST_CHECK(bfile4.verifySignatureBlock(2) == true);

	bfile4.close();
}

BOOST_AUTO_TEST_SUITE_END()

// This test suite is for tests the functionality of key rotation on the SessionWalletManager 
BOOST_AUTO_TEST_SUITE(sessionwalletmanager_key_rotation)

BOOST_AUTO_TEST_CASE (test01_sanity)
{
	const char *datapath = "./petalustest_data1";
	// Change the data path so we will have just our blockfiles
	setDataDirectory(datapath);

	std::filesystem::remove_all(datapath);

        std::string wallet(generateHexBlockFilename());
        std::string spath(generateTestFile(wallet));
        std::string hpass(generateSHA256("juice"));

        auto sw = SessionWalletManager();
        sw.setDataDirectory(getDataDirectory());

        // Allocate one wallet on the cache
        sw.increaseAllocatedMemory(1);

        BOOST_TEST_MESSAGE( "wallet:(" << wallet << ")");

        std::string data(generateRandomString(generateRandomInt(128)));

	std::string password("{AES}C4F65AAC358F3CF2BB9C9C3FE2A4566E2D4B21C415C34B7F065A817A29F6940E"); // a * 16
	std::string new_password("{AES}C7F559AF368C3FF1B89F9F3CE1A7556DE2C53DAC9C28D51C027C9DEE22E24872"); // b *16
        sw.setEncryptionPassword(password); // Encrypt all the user data

        nlohmann::json j;

        // Information that should be mandatory
        j["name"] = "Bob";
        j["surname"] = "Spounge";
        j["age"] = 80;
        j["password"] = hpass;

        PetalusBlock creation_block(j);

        sw.createWallet(wallet, creation_block);

        std::string id = sw.openSession(wallet, hpass);
        BOOST_CHECK(id.length() > 0); // correct password

        SessionContext ctx1(wallet, id);

        // Write one encrypted block on the folder
        PetalusBlock pblock1(data);

        BOOST_CHECK(sw.write(ctx1, pblock1) > 0);

	// this process will close all the sessions open
	sw.rotateEncryptionKey(new_password);

	BOOST_CHECK(sw.getTotalReencryptedFiles() == 1);
	BOOST_CHECK(sw.getTotalReencryptedErrors() == 0);

	// The new password should be updated
	sw.setEncryptionPassword(new_password);

	id = sw.openSession(wallet, hpass);
	BOOST_CHECK(id.length() > 0);
        
        SessionContext ctx2(wallet, id);
	PetalusBlock readblock;

	BOOST_CHECK(sw.read(ctx2, readblock, 3) > 0);
	BOOST_CHECK(data.compare(readblock.data()) == 0);

	sw.close();
}

// Similar test as before but with folders that needs to be re-encrypted also
BOOST_AUTO_TEST_CASE (test02_sanity_folders)
{
	const char *datapath = "./petalustest_data1";
        // Change the data path so we will have just our blockfiles
        setDataDirectory(std::string(datapath));

	std::filesystem::remove_all(datapath);
  
        std::string wallet(generateHexBlockFilename());
        std::string spath(generateTestFile(wallet));
        std::string hpass(generateSHA256("juice"));

        auto sw = SessionWalletManager();
        sw.setDataDirectory(getDataDirectory());

        // Allocate one wallet on the cache
        sw.increaseAllocatedMemory(1);

        BOOST_TEST_MESSAGE( "wallet:(" << wallet << ")");

        std::string data1(generateRandomString(generateRandomInt(128)));
        std::string data2(generateRandomString(generateRandomInt(128)));
        std::string data3(generateRandomString(generateRandomInt(128)));

        std::string password("{AES}C4F65AAC358F3CF2BB9C9C3FE2A4566E2D4B21C415C34B7F065A817A29F6940E"); // a * 16
        std::string new_password("{AES}C7F559AF368C3FF1B89F9F3CE1A7556DE2C53DAC9C28D51C027C9DEE22E24872"); // b *16
        sw.setEncryptionPassword(password); // Encrypt all the user data

        nlohmann::json j;

        // Information that should be mandatory
        j["name"] = "John";
        j["surname"] = "Spounge";
        j["age"] = 20;
        j["email"] = "john.12324@underthesea.com";
        j["folders"] = { "bills", "payments" };
        j["password"] = hpass;

        PetalusBlock creation_block(j);

        sw.createWallet(wallet, creation_block);

        std::string id = sw.openSession(wallet, hpass);
        BOOST_CHECK(id.length() > 0); // correct password

        SessionContext ctx1(wallet, id);

        // Write one encrypted block on the folder
        PetalusBlock pblock1(data1);
        PetalusBlock pblock2(data2);
        PetalusBlock pblock3(data3);

        BOOST_CHECK(sw.write(ctx1, pblock1) > 0);
        BOOST_CHECK(sw.write(ctx1, pblock2, "bills") > 0);
        BOOST_CHECK(sw.write(ctx1, pblock3, "payments") > 0);

        // this process will close all the sessions open
        sw.rotateEncryptionKey(new_password);

        BOOST_CHECK(sw.getTotalReencryptedFiles() == 3);
        BOOST_CHECK(sw.getTotalReencryptedErrors() == 0);

        // The new password should be updated
        sw.setEncryptionPassword(new_password);

        id = sw.openSession(wallet, hpass);
        BOOST_CHECK(id.length() > 0);

        SessionContext ctx2(wallet, id);
        PetalusBlock readblock;

        BOOST_CHECK(sw.read(ctx2, readblock, 5) > 0);
        BOOST_CHECK(data1.compare(readblock.data()) == 0);

        BOOST_CHECK(sw.read(ctx2, readblock, "bills", 2) > 0);
        BOOST_CHECK(data2.compare(readblock.data()) == 0);

        BOOST_CHECK(sw.read(ctx2, readblock, "payments", 2) > 0);
        BOOST_CHECK(data3.compare(readblock.data()) == 0);

        sw.close();
}

// Similar test as before but with folders that needs to be re-encrypted also
// and one of the rotations is with blowfish
BOOST_AUTO_TEST_CASE (test03_sanity_folders_blowfish)
{
        const char *datapath = "./petalustest_data1";
        // Change the data path so we will have just our blockfiles
        setDataDirectory(std::string(datapath));

        std::filesystem::remove_all(datapath);

        std::string wallet(generateHexBlockFilename());
        std::string spath(generateTestFile(wallet));
        std::string hpass(generateSHA256("juice"));

        auto sw = SessionWalletManager();
        sw.setDataDirectory(getDataDirectory());

        // Allocate one wallet on the cache
        sw.increaseAllocatedMemory(1);

        BOOST_TEST_MESSAGE( "wallet:(" << wallet << ")");

        std::string data1(generateRandomString(generateRandomInt(1609)));
        std::string data2(generateRandomString(generateRandomInt(3499)));
        std::string data3(generateRandomString(generateRandomInt(521)));

        std::string password1(generateAESPassword("aaaaaaaaaaaaaaaa"));

	std::vector<std::string> passwords = { 
		generateBlowfishPassword("eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee"), // 40 
		generateAESPassword("bbbbbbbbbbbbbbbb"),
		generateBlowfishPassword("cccccccccccccccccc"), 
		generateBlowfishPassword("dddddddddddddddddddddddddddddddd"), // 32
		generateBlowfishPassword("ffffffffffffffffffffffffffffffffffffffffffffffff"), // 48 
		generateBlowfishPassword("luis") 
	};
        sw.setEncryptionPassword(password1); // Encrypt all the user data

        nlohmann::json j;

        // Information that should be mandatory
        j["name"] = "John";
        j["surname"] = "Spounge";
        j["age"] = 20;
        j["email"] = "john.12324@underthesea.com";
        j["folders"] = { "bills", "payments" };
        j["password"] = hpass;

        PetalusBlock creation_block(j);

        sw.createWallet(wallet, creation_block);

        std::string id = sw.openSession(wallet, hpass);
        BOOST_CHECK(id.length() > 0); // correct password

        SessionContext ctx1(wallet, id);

        // Write one encrypted block on the folder
        PetalusBlock pblock1(data1);
        PetalusBlock pblock2(data2);
        PetalusBlock pblock3(data3);

        BOOST_CHECK(sw.write(ctx1, pblock1) > 0);
        BOOST_CHECK(sw.write(ctx1, pblock2, "bills") > 0);
        BOOST_CHECK(sw.write(ctx1, pblock3, "payments") > 0);

	// Get the sha256 of the mblk file
	CryptoPP::SHA256 hash;
	std::string digest, lastpassword;
 
	CryptoPP::FileSource f(spath.c_str(), true, new CryptoPP::HashFilter(hash, new CryptoPP::HexEncoder(new CryptoPP::StringSink(digest))));

	BOOST_TEST_MESSAGE( "file:(" << spath << ")");

	int rotations = 0;
	// We make 6 rotations of key with different passwords types
	for (auto &password : passwords) {
		std::string digest_new;
		lastpassword.assign(password);
        	// this process will close all the sessions open
        	sw.rotateEncryptionKey(password);

		// the main file should change
		CryptoPP::FileSource f(spath.c_str(), true, 
			new CryptoPP::HashFilter(hash, 
				new CryptoPP::HexEncoder(new CryptoPP::StringSink(digest_new))
			)
		);

		BOOST_CHECK(digest.compare(digest_new) != 0);

		digest = digest_new;

        	BOOST_CHECK(sw.getTotalReencryptedFiles() == 3);
        	BOOST_CHECK(sw.getTotalReencryptedErrors() == 0);

        	// The new password should be updated
        	sw.setEncryptionPassword(password);
		BOOST_CHECK(password.compare(sw.getEncryptionPassword()) == 0);

		id = sw.openSession(wallet, hpass);
		BOOST_CHECK(id.length() > 0);

		SessionContext ctx2(wallet, id);
		PetalusBlock readblock;

		BOOST_CHECK(sw.read(ctx2, readblock, 5) > 0);
		BOOST_CHECK(data1.compare(readblock.data()) == 0);

		BOOST_CHECK(sw.read(ctx2, readblock, "bills", 2) > 0);
		BOOST_CHECK(data2.compare(readblock.data()) == 0);

        	BOOST_CHECK(sw.read(ctx2, readblock, "payments", 2) > 0);
        	BOOST_CHECK(data3.compare(readblock.data()) == 0);
		++ rotations;
	}

	BOOST_CHECK(rotations == passwords.size());
        sw.close();

	// Now open the main file and read the 5 block

	PetalusBlock read_block;
	EncryptionKey key;

	key.password(lastpassword);
        BOOST_CHECK(key.type() == EncryptionKeyType::BLOWFISH);

	auto bfile = BlockFile();
	bfile.open(spath);
	bfile.setEncryptionKey(key);
	BOOST_CHECK(bfile.is_open() == true);

        bfile.read(5, read_block);
        BOOST_CHECK(read_block == data1);	

	std::string decrypt_password("luis");
	BOOST_CHECK(decrypt_password.compare(key.getDecryptedPassword()) == 0);
	
	bfile.close();
}

// makes a rotation key with signed blocks that must be verified after the rotation process
BOOST_AUTO_TEST_CASE (test04_sanity_signedblock_rotation)
{
        const char *datapath = "./petalustest_data1";
        // Change the data path so we will have just our blockfiles
        setDataDirectory(datapath);

        std::filesystem::remove_all(datapath);

        std::string wallet(generateHexBlockFilename());
        std::string spath(generateTestFile(wallet));
        std::string hpass(generateSHA256("juice"));

        auto sw = SessionWalletManager();
        sw.setDataDirectory(getDataDirectory());

        // Allocate one wallet on the cache
        sw.increaseAllocatedMemory(1);

        BOOST_TEST_MESSAGE( "wallet:(" << wallet << ")");

        std::string data1(generateRandomString(generateRandomInt(1723)));
        std::string data2(generateRandomString(generateRandomInt(3137)));

        std::string password("{AES}C4F65AAC358F3CF2BB9C9C3FE2A4566E2D4B21C415C34B7F065A817A29F6940E"); // a * 16
        std::string new_password("{AES}C7F559AF368C3FF1B89F9F3CE1A7556DE2C53DAC9C28D51C027C9DEE22E24872"); // b *16
        sw.setEncryptionPassword(password); // Encrypt all the user data

        // Create the public key
        CryptoPP::AutoSeededRandomPool prng;
        PrivateKey privateKey;
        PublicKey publicKey;
        std::string b64publickey = generateBase64PublicKey(privateKey, publicKey);

        BOOST_TEST_MESSAGE( "wallet:(" << wallet << ")");
        BOOST_TEST_MESSAGE( "b64publickey:(" << b64publickey << ")");

        nlohmann::json j;

        // Information that should be mandatory
        j["name"] = "Bob";
        j["surname"] = "Spounge";
        j["age"] = 80;
        j["ec_public_key"] = b64publickey;
	j["folders"] = { "payrolls" };
        j["password"] = hpass;

        PetalusBlock creation_block(j);

        sw.createWallet(wallet, creation_block);

        std::string id1 = sw.openSession(wallet, hpass);
        BOOST_CHECK(id1.length() > 0); // correct password

        SessionContext ctx1(wallet, id1);

        // Write one encrypted block
        PetalusBlock pblock1(data1);
        PetalusBlock pblock2(data2);
        Signer signer(privateKey);

        std::string binary_signature1;
        std::string binary_signature2;
        uint8_t digest[HASH_BLOCK_SIZE];

        computeSHA256(digest, (const uint8_t*)data1.c_str(), data1.length());

        // Sign the sha256 hash of data
        CryptoPP::StringSource s1( digest, HASH_BLOCK_SIZE, true /*pump all*/,
                new CryptoPP::SignerFilter( prng,
                        signer,
                        new CryptoPP::StringSink( binary_signature1 )
                ) // SignerFilter
        ); // StringSource

        pblock1.signature(binary_signature1);

        computeSHA256(digest, (const uint8_t*)data2.c_str(), data2.length());

        // Sign the sha256 hash of data
        CryptoPP::StringSource s2( digest, HASH_BLOCK_SIZE, true /*pump all*/,
                new CryptoPP::SignerFilter( prng,
                        signer,
                        new CryptoPP::StringSink( binary_signature2 )
                ) // SignerFilter
        ); // StringSource

        pblock2.signature(binary_signature2);

        BOOST_CHECK(sw.write(ctx1, pblock1) > 0);
        BOOST_CHECK(sw.write(ctx1, pblock2, "payrolls") > 0);

        // this process will close all the sessions open
        sw.rotateEncryptionKey(new_password);

        BOOST_CHECK(sw.getTotalReencryptedFiles() == 2);
        BOOST_CHECK(sw.getTotalReencryptedErrors() == 0);

        // The new password should be updated
        sw.setEncryptionPassword(new_password);

        std::string id2 = sw.openSession(wallet, hpass);
        BOOST_CHECK(id2.length() > 0);
	BOOST_CHECK(id1.compare(id2) != 0);

        SessionContext ctx2(wallet, id2);
        PetalusBlock read_block1;
        PetalusBlock read_block2;

        BOOST_CHECK(sw.read(ctx2, read_block1, 4) > 0);
        BOOST_CHECK(data1.compare(read_block1.data()) == 0);
        BOOST_CHECK(read_block1.signature_length() == binary_signature1.length());
        BOOST_CHECK(std::memcmp(read_block1.signature(), binary_signature1.c_str(), binary_signature1.length()) == 0);

        BOOST_CHECK(sw.read(ctx2, read_block2, "payrolls", 2) > 0);
        BOOST_CHECK(data2.compare(read_block2.data()) == 0);
        BOOST_CHECK(read_block2.signature_length() == binary_signature2.length());
        BOOST_CHECK(std::memcmp(read_block2.signature(), binary_signature2.c_str(), binary_signature2.length()) == 0);

	auto wa = sw.getWallet(id2);
	BOOST_CHECK(wa != nullptr);
	auto bfile5 = wa->getBlockFile();

	BOOST_CHECK(bfile5->is_open() == true);
	BOOST_CHECK(bfile5->havePublicKey() == true);
        BOOST_CHECK(bfile5->verifySignatureBlock(4) == true);

	// Verify also the folder
	auto folders = wa->getFolders();
	auto it = folders.find("payrolls");
	BOOST_CHECK(it != folders.end());
	auto bfilef = it->second;
	BOOST_CHECK(bfilef != nullptr);
        BOOST_CHECK(bfilef->is_open() == true);
	BOOST_CHECK(bfilef->havePublicKey() == true);
        BOOST_CHECK(bfilef->verifySignatureBlock(2) == true);

	sw.close();
}

// makes a rotation key with a corruption on the data so there will be errors on the process 
BOOST_AUTO_TEST_CASE (test05_rotation_error_main_blockfile)
{
        const char *datapath = "./petalustest_data1";
        // Change the data path so we will have just our blockfiles
        setDataDirectory(datapath);

        std::filesystem::remove_all(datapath);

        std::string wallet(generateHexBlockFilename());
        std::string spath(generateTestFile(wallet));
        std::string hpass(generateSHA256("juice"));

        auto sw = SessionWalletManager();
        sw.setDataDirectory(getDataDirectory());

        // Allocate one wallet on the cache
        sw.increaseAllocatedMemory(1);

        BOOST_TEST_MESSAGE( "wallet:(" << wallet << ")");

        std::string data(generateRandomString(generateRandomInt(1777)));

        std::string password("{AES}C4F65AAC358F3CF2BB9C9C3FE2A4566E2D4B21C415C34B7F065A817A29F6940E"); // a * 16
        std::string new_password("{AES}C7F559AF368C3FF1B89F9F3CE1A7556DE2C53DAC9C28D51C027C9DEE22E24872"); // b *16
        sw.setEncryptionPassword(password); // Encrypt all the user data

        nlohmann::json j;

        // Information that should be mandatory
        j["name"] = "John";
        j["surname"] = "Spounge";
        j["age"] = 20;
        j["email"] = "john.12324@underthesea.com";
        j["password"] = hpass;

        PetalusBlock creation_block(j);

        sw.createWallet(wallet, creation_block);

        std::string id = sw.openSession(wallet, hpass);
        BOOST_CHECK(id.length() > 0); // correct password

        SessionContext ctx1(wallet, id);

        // Write one encrypted block on the folder
        PetalusBlock pblock1(data);

        BOOST_CHECK(sw.write(ctx1, pblock1) > 0);
	BOOST_CHECK(sw.close(id) == true);

	// Now we open the block file and remove just one byte from the file 
        std::uintmax_t filesize = std::filesystem::file_size(spath);

        std::error_code ec;

        std::filesystem::resize_file(spath, filesize - 1, ec);

        // this process will close all the sessions open
        sw.rotateEncryptionKey(new_password);

        BOOST_CHECK(sw.getTotalReencryptedFiles() == 0);
        BOOST_CHECK(sw.getTotalReencryptedErrors() == 1);

        // The new password can not be updated
        sw.setEncryptionPassword(new_password);
	BOOST_CHECK(password.compare(sw.getEncryptionPassword()) == 0);

	sw.close();
}

// makes a rotation key with a corruption on the data of the folder file so there will be errors on the process
BOOST_AUTO_TEST_CASE (test06_rotation_error_folder_blockfile)
{
        const char *datapath = "./petalustest_data1";
        // Change the data path so we will have just our blockfiles
        setDataDirectory(datapath);

        std::filesystem::remove_all(datapath);

        std::string wallet(generateHexBlockFilename());
        std::string spath(generateTestFile(wallet));
        std::string spathf(generateTestFile(wallet, 2));
        std::string hpass(generateSHA256("juice"));

        auto sw = SessionWalletManager();
        sw.setDataDirectory(getDataDirectory());

        // Allocate one wallet on the cache
        sw.increaseAllocatedMemory(1);

        BOOST_TEST_MESSAGE( "wallet:(" << wallet << ")");

        std::string data1(generateRandomString(generateRandomInt(773)));
        std::string data2(generateRandomString(generateRandomInt(1747)));

        std::string password("{AES}C4F65AAC358F3CF2BB9C9C3FE2A4566E2D4B21C415C34B7F065A817A29F6940E"); // a * 16
        std::string new_password("{AES}C7F559AF368C3FF1B89F9F3CE1A7556DE2C53DAC9C28D51C027C9DEE22E24872"); // b *16
        sw.setEncryptionPassword(password); // Encrypt all the user data

        nlohmann::json j;

        // Information that should be mandatory
        j["name"] = "John";
        j["surname"] = "Spounge";
        j["age"] = 20;
        j["email"] = "john.12324@underthesea.com";
	j["folders"] = { "things" };
        j["password"] = hpass;

        PetalusBlock creation_block(j);

        sw.createWallet(wallet, creation_block);

        std::string id = sw.openSession(wallet, hpass);
        BOOST_CHECK(id.length() > 0); // correct password

        SessionContext ctx1(wallet, id);

        // Write one encrypted block on the folder
        PetalusBlock pblock1(data1);
        PetalusBlock pblock2(data2);

        BOOST_CHECK(sw.write(ctx1, pblock1) > 0);
        BOOST_CHECK(sw.write(ctx1, pblock2, "things") > 0);
        BOOST_CHECK(sw.close(id) == true);

        // Now we open the folder block file and remove just one byte from the file
        std::uintmax_t filesize = std::filesystem::file_size(spathf);

        std::error_code ec;

        std::filesystem::resize_file(spathf, filesize - 1, ec);

        // this process will close all the sessions open
        sw.rotateEncryptionKey(new_password);

        BOOST_CHECK(sw.getTotalReencryptedFiles() == 1); // The main file is cloned properly
        BOOST_CHECK(sw.getTotalReencryptedErrors() == 1);

        // The new password can not be updated
        sw.setEncryptionPassword(new_password);
        BOOST_CHECK(password.compare(sw.getEncryptionPassword()) == 0);

        sw.close();
}

BOOST_AUTO_TEST_SUITE_END()

// This test suite is designed for check errors when disks are full
BOOST_AUTO_TEST_SUITE(sessionwalletmanager_disk_full)

// This test case will be only executed if the binary is executed with
// root permisions due to the use of mount for create a virtual harddisk
BOOST_AUTO_TEST_CASE (test01_disk_full_ext3)
{
	if (geteuid() != 0) {
		BOOST_CHECK(true);
		return;
	}

	const char *filediskname = "filedisk.ext3.fs";
	const char *directory = "./petalustest_data_disk1";
	if (std::filesystem::exists(filediskname)) std::filesystem::remove(filediskname);
	if (!std::filesystem::exists(directory))
		std::filesystem::create_directory(directory);

	std::ofstream f(filediskname);
	f.close();

	std::error_code ec;
	std::filesystem::resize_file(filediskname, 1024 * 1024 * 4, ec);

	std::string cmd = "mkfs.ext3 " + std::string(filediskname);
	system(cmd.c_str());
	cmd = "mount " + std::string(filediskname) + " " + std::string(directory);
	system(cmd.c_str());

	std::string datadirectory = std::string(directory); // + std::string("/data");

	// Change the data path so we will have just our blockfiles for the tests
	setDataDirectory(datadirectory);

	// Remove the wallet from previous runs
	std::string wallet(generateHexBlockFilename());
	std::string hpass(generateSHA256("Bad Horsie"));

	auto sw = SessionWalletManager();
	sw.setDataDirectory(getDataDirectory());
	sw.increaseAllocatedMemory(2);

        nlohmann::json j;
        j["password"] = hpass;
        PetalusBlock creation_block(j);

	sw.createWallet(wallet, creation_block);

	BOOST_CHECK(sw.getTotalWallets() == 0);
	BOOST_CHECK(sw.getTotalCreatedWallets() == 1);
	BOOST_CHECK(sw.getTotalOpenWallets() == 0);
	BOOST_CHECK(sw.getTotalBytesAvailable() < 1024 * 1024 * 3); // Less than 3 MB for operate

	std::string id = sw.openSession(wallet, hpass);
	BOOST_CHECK(id.length() > 0); // correct password

	SessionContext ctx(wallet, id);

	int blocks_written = 0;
	BOOST_CHECK(sw.isReadOnly() == false);
	for (int i = 0; i < 10; ++i) {
		PetalusBlock block(generateRandomString(1709));

		if (!sw.isReadOnly()) {
			sw.write(ctx, block);
			++ blocks_written;
		}
	}
	BOOST_CHECK(sw.isReadOnly() == true);
	BOOST_CHECK(blocks_written > 0);

	sw.close();
	cmd = "umount " + std::string(directory);
	system(cmd.c_str());
}

// Verify that we can not make a rotation key of the data due to
// there is no space on the directory

BOOST_AUTO_TEST_CASE (test02_disk_full_ext4_rotation_key)
{
	if (geteuid() != 0) {
		BOOST_CHECK(true);
		return;
	}

        const char *filediskname = "filedisk.ext4.fs";
        const char *directory = "./petalustest_data_disk2";
        if (std::filesystem::exists(filediskname)) std::filesystem::remove(filediskname);
        if (!std::filesystem::exists(directory))
                std::filesystem::create_directory(directory);

        std::ofstream f(filediskname);
        f.close();

	std::string password("{AES}C4F65AAC358F3CF2BB9C9C3FE2A4566E2D4B21C415C34B7F065A817A29F6940E"); // a * 16

        std::error_code ec;
        std::filesystem::resize_file(filediskname, 1024 * 1024 * 4, ec);

        std::string cmd = "mkfs.ext4 " + std::string(filediskname);
        system(cmd.c_str());
        cmd = "mount " + std::string(filediskname) + " " + std::string(directory);
        system(cmd.c_str());

        std::string datadirectory = std::string(directory); // + std::string("/data");

        // Change the data path so we will have just our blockfiles for the tests
        setDataDirectory(datadirectory);

        // Remove the wallet from previous runs
        std::string wallet(generateHexBlockFilename());
        std::string hpass(generateSHA256("MAA-1234"));

        auto sw = SessionWalletManager();
        sw.setDataDirectory(getDataDirectory());
        sw.increaseAllocatedMemory(1);

        nlohmann::json j;

        j["name"] = "John";
        j["surname"] = "Spounge";
        j["age"] = 20;
        j["email"] = "john.12324@underthesea.com";
        j["password"] = hpass;

        PetalusBlock creation_block(j);

        sw.createWallet(wallet, creation_block);

        BOOST_CHECK(sw.getTotalWallets() == 0);
        BOOST_CHECK(sw.getTotalCreatedWallets() == 1);
        BOOST_CHECK(sw.getTotalOpenWallets() == 0);
        BOOST_CHECK(sw.getTotalBytesAvailable() < 1024 * 1024 * 3); // Less than 3 MB for operate

        std::string id = sw.openSession(wallet, hpass);
        BOOST_CHECK(id.length() > 0); // correct password

	sw.setReadOnlySafeGuardBytes(1024);
	BOOST_CHECK(sw.getReadOnlySafeGuardBytes() == 1024);

        SessionContext ctx(wallet, id);

        PetalusBlock block(generateRandomString(104009));

	for (int i = 0; i < 20; ++i)
        	sw.write(ctx, block);

        sw.close();

	// There is no space on the disk!!!!
	BOOST_CHECK(sw.computeDataSize() > sw.getTotalBytesAvailable());
        // this process will close all the sessions open
        sw.rotateEncryptionKey(password);

        BOOST_CHECK(sw.getTotalReencryptedFiles() == 0);
        BOOST_CHECK(sw.getTotalReencryptedErrors() == 0);

        cmd = "umount " + std::string(directory);
        system(cmd.c_str());
}

BOOST_AUTO_TEST_CASE (pepe)
{
    std::cout << "Compiled with Crypto++ version " << CRYPTOPP_VERSION;
    #if defined(CRYPTOPP_BOOL_X86)
      std::cout << " CRYPTOPP_BOOL_X86 1";
    #endif
    #if defined(CRYPTOPP_X86_ASM_AVAILABLE)
      std::cout << " CRYPTOPP_X86_ASM_AVAILABLE 1";
    #endif
    #if defined(CRYPTOPP_X64_MASM_AVAILABLE)
      std::cout << " CRYPTOPP_X64_MASM_AVAILABLE 1";
    #endif
    #if defined(CRYPTOPP_GENERATE_X64_MASM)
      std::cout << " CRYPTOPP_GENERATE_X64_MASM 1";
    #endif
    #if defined(CRYPTOPP_BOOL_SSE2_ASM_AVAILABLE)
      std::cout << " CRYPTOPP_BOOL_SSE2_ASM_AVAILABLE 1";
    #endif


}

BOOST_AUTO_TEST_SUITE_END()

// This test suite is designed for verify the master_block_file functionality
// on the Session Wallet manager
BOOST_AUTO_TEST_SUITE(sessionwalletmanager_master_file)

BOOST_AUTO_TEST_CASE (test01_sanity)
{
        const char *datapath = "./petalustest_data2";
        setDataDirectory(datapath);

        std::string wallet(generateHexBlockFilename());
        std::string filename1("master1.mblk0000");
        std::string filename2("master2.mblk0000");
        std::string spath1(std::string(datapath) + std::string("/") + filename1);
        std::string spath2(std::string(datapath) + std::string("/") + filename2);
        std::string string_seed("This is the initial seed of the master block file");

        if (std::filesystem::exists(spath1)) std::filesystem::remove(spath1);
        if (std::filesystem::exists(spath2)) std::filesystem::remove(spath2);

	// Generate two block files, one without seed and other with seed
        auto bfile1 = BlockFile();
        auto bfile2 = BlockFile();

        uint8_t digest[HASH_BLOCK_SIZE];

        CryptoPP::SHA256 shash;

        shash.CalculateDigest(digest, (uint8_t*) string_seed.c_str(), string_seed.length());

	bfile1.setSeed(digest);
	bfile1.open(spath1);
	bfile2.open(spath2);

        nlohmann::json j;

        j["info"] = "Relevant info";

        PetalusBlock block(j);
        block.type(BlockType::SYS_JSON_MASTER_ENTRY);

	bfile1.write(block);
	bfile2.write(block);

	bfile1.close();
	bfile2.close();

        BOOST_CHECK(bfile1.getTotalBlocks() == 1);
        BOOST_CHECK(bfile2.getTotalBlocks() == 1);

	// Now set the password
	std::string seed;

	seed.assign((const char*)digest, HASH_BLOCK_SIZE);
	std::string password(generateBlowfishPassword(seed));

        auto sw = SessionWalletManager();
        sw.setDataDirectory(getDataDirectory());

	// First case with a wrong password and a invalid blockfile
	sw.setMasterBlockFilePassword(std::string("this dont have the format"));
	sw.setMasterBlockFile(filename2);

	BOOST_CHECK(sw.getMasterBlock() == nullptr);

	// Now try with a correct password but the Blockfile hash dont match
	sw.setMasterBlockFilePassword(password);
	sw.setMasterBlockFile(filename2);
	
	BOOST_CHECK(sw.getMasterBlock() == nullptr);

	// Finally the password and the hash matches
	sw.setMasterBlockFilePassword(password);
	sw.setMasterBlockFile(filename1);
	
	BOOST_CHECK(sw.getMasterBlock() != nullptr);

	sw.close();
}

BOOST_AUTO_TEST_CASE (test02_create_two_wallets)
{
        const char *datapath = "./petalustest_data2";
        setDataDirectory(datapath);

        std::string wallet1(generateHexBlockFilename("1"));
        std::string wallet2(generateHexBlockFilename("2"));
        std::string filename("master3.mblk0000");
        std::string spath(std::string(datapath) + std::string("/") + filename);
        std::string spath1(generateTestFile(wallet1));
        std::string spath2(generateTestFile(wallet2));
        std::string string_seed("This is the initial SEED of the master");

        if (std::filesystem::exists(spath)) std::filesystem::remove(spath);
        if (std::filesystem::exists(spath1)) std::filesystem::remove(spath1);
        if (std::filesystem::exists(spath2)) std::filesystem::remove(spath2);

        std::string hpass(generateSHA256("juice"));

        auto sw = SessionWalletManager();
        sw.setDataDirectory(getDataDirectory());

	// Create a master file
        auto bfile = BlockFile();

        uint8_t digest[HASH_BLOCK_SIZE];

        CryptoPP::SHA256 shash;

        shash.CalculateDigest(digest, (uint8_t*) string_seed.c_str(), string_seed.length());

        bfile.setSeed(digest);
        bfile.open(spath);

        nlohmann::json j;

        j["info"] = "Relevant info for this master file";

        PetalusBlock block(j);
        block.type(BlockType::SYS_JSON_MASTER_ENTRY);

        bfile.write(block);
        bfile.close();

        BOOST_CHECK(bfile.getTotalBlocks() == 1);

        // Now set the password
        std::string seed;

        seed.assign((const char*)digest, HASH_BLOCK_SIZE);
        std::string password(generateBlowfishPassword(seed));

        sw.setMasterBlockFilePassword(password);
        sw.setMasterBlockFile(filename);

        BOOST_CHECK(sw.getMasterBlock() != nullptr);

        // Allocate one wallet on the cache
        sw.increaseAllocatedMemory(1);

        BOOST_TEST_MESSAGE("wallet1:(" << wallet1 << ")");
        BOOST_TEST_MESSAGE("wallet2:(" << wallet2 << ")");

        nlohmann::json j1, j2;

        j1["name"] = "John";
        j1["surname"] = "Spounge";
        j1["age"] = 20;
        j1["email"] = "john.12324@underthesea.com";
	j1["password"] = hpass;

        j2["name"] = "John";
        j2["surname"] = "Smith";
        j2["age"] = 22;
        j2["email"] = "john.smith@underthesea.com";
	j2["password"] = hpass;

        PetalusBlock creation_block1(j1);
        PetalusBlock creation_block2(j2);

	WalletCreateCode code;

	auto mbf = sw.getMasterBlock();

	// Create a new wallet
        code = sw.createWallet(wallet1, creation_block1);
	BOOST_CHECK(code == WalletCreateCode::WALLET_CREATED_SUCCESS);
        BOOST_CHECK(sw.getTotalCreatedWallets() == 1);
	BOOST_CHECK(mbf->getTotalBlocks() == 2);

	// Tries to create again the same wallet
        code = sw.createWallet(wallet1, creation_block2);
	BOOST_CHECK(code == WalletCreateCode::WALLET_EXISTS);
        BOOST_CHECK(sw.getTotalCreatedWallets() == 1);
	BOOST_CHECK(mbf->getTotalBlocks() == 2);

	// Create the other wallet	
        code = sw.createWallet(wallet2, creation_block2);
	BOOST_CHECK(code == WalletCreateCode::WALLET_CREATED_SUCCESS);
        BOOST_CHECK(sw.getTotalCreatedWallets() == 2);
	BOOST_CHECK(mbf->getTotalBlocks() == 3);

	sw.close();
}


BOOST_AUTO_TEST_SUITE_END()


int main(int argc, char* argv[], char* envp[]) {

	std::vector<std::string> data_directories { 
		"./petalustest_data0",
		"./petalustest_data1",
		"./petalustest_data2" };

	for (auto &d : data_directories) {
		if (!std::filesystem::exists(d)) {
			SessionWalletManager sw;

			sw.setDataDirectory(d);
		}
	}
  
	return ::boost::unit_test::unit_test_main(init_unit_test, argc, argv);
}
