#pragma once
#ifndef SRC_PYTHONGILCONTEXT_H_
#define SRC_PYTHONGILCONTEXT_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifdef PYTHON_BINDING
#include <boost/python.hpp>
#endif

namespace petalus {

class PythonGilContext {
#if defined(PYTHON_BINDING) && (HAVE_PYTHON_GIl)
public:
        PythonGilContext():status_(PyGILState_Ensure()) {}
        virtual ~PythonGilContext() { PyGILState_Release(state_); }
private:
        PyGILState_STATE state_;
#endif
};

} // namespace petalus

#endif  // SRC_PYTHONGILCONTEXT_H_

