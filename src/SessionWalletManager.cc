/*
 * Petalus a crypto micro service wallet database.
 *
 * Copyright (C) 2018-2022  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */

#include "SessionWalletManager.h"
#include <iomanip>

namespace petalus::blockchain {

std::function <void(int64_t&, std::string&)> unitConverter = [](int64_t &bytes, std::string &unit) noexcept {
        if (bytes >1024) { bytes = bytes / 1024; unit = "KBytes"; }
        if (bytes >1024) { bytes = bytes / 1024; unit = "MBytes"; }
        if (bytes >1024) { bytes = bytes / 1024; unit = "GBytes"; }
        if (bytes >1024) { bytes = bytes / 1024; unit = "TBytes"; }
};


int SessionWalletManager::computeTotalNumberOfWalletsOnDisk() const {

	int value = 0;
	// Take all the main block files of the data directory

	if (path_.string().length() > 0) { // Is set to something
		for (std::filesystem::recursive_directory_iterator it(path_);
			it != std::filesystem::recursive_directory_iterator(); ++it) {

			if(!std::filesystem::is_directory(*it)) {
				std::filesystem::path f(*it);
				std::string extension(f.extension().c_str());

				if (extension.compare(1, 4, "mblk") == 0)
					++ value; 
			}
		}
	}
	return value;
}

void SessionWalletManager::readBlockDataHash(const SessionContext &ctx , int32_t number, uint8_t prev_digest[], const std::string &folder) {

	if (auto wa = get_wallet(ctx); wa) {
		if (folder.length() > 0)
			wa->readBlockDataHashFromFolder(folder, number, prev_digest);
		else
			wa->readBlockDataHash(number, prev_digest);
	}
}

ReplicationBlockWriteCode SessionWalletManager::writeBlockOnDisk(const std::string &wallet, int32_t number, PetalusBlock &block, uint8_t prev_digest[], const std::string &folder) {

	ReplicationBlockWriteCode code = ReplicationBlockWriteCode::WALLET_NO_RESOURCES;

	// std::cout << __FILE__ << ":" << __func__ << ":wallet:" << wallet << " b:" << number << " f:" << folder;
	// std::cout << " sig:" << block.have_signature() << std::endl;

	std::filesystem::path path(path_);

	path /= wallet.substr(0, 1);
	path /= wallet.substr(1, 1);

        // Verify that the file dont exist
        std::filesystem::path filepath(path);

        filepath /= wallet + std::string(".mblk0000");
        if (std::filesystem::exists(filepath)) { 
		if (auto wa = wallet_cache_->acquire(); wa) {
			wa->setName(wallet);
			wa->setBlockHashType(hash_func_type_);
			wa->setEncryptionKey(key_);
			wa->setMaxBlockFileSize(max_blockfile_size_);
			wa->open(path);

			if (wa->is_open()) {
				if (!wa->isLock()) {
                			uint8_t prev_hash[HASH_BLOCK_SIZE];

					if (folder.length() > 0)
                				wa->readBlockDataHashFromFolder(folder, number - 1, prev_hash);
					else
                				wa->readBlockDataHash(number - 1, prev_hash);

					if (std::memcmp(prev_hash, prev_digest, HASH_BLOCK_SIZE) == 0) {
						int32_t bytes = 0;
						if (folder.length() > 0)
							bytes = wa->writeOnFolder(folder, block);
						else	
							bytes = wa->write(block);

						if (bytes > 0) { 
							code = ReplicationBlockWriteCode::BLOCK_WRITTEN;
							++total_replicated_blocks_;
							total_replicated_bytes_ += bytes;
						} else {
							code = ReplicationBlockWriteCode::BLOCK_HASH_INVALID;
						}
					} else {
						code = ReplicationBlockWriteCode::BLOCK_HASH_INVALID;
					}
				} else {
					code = ReplicationBlockWriteCode::WALLET_LOCK;
				}
			}
			wallet_cache_->release(wa);
		}		
	} else { // The wallet dont exists
		if (number == 1) { // This means that is the first block of the wallet 
			auto creation_code = create_wallet(wallet, block);

			if (creation_code == WalletCreateCode::WALLET_CREATED_SUCCESS) {
				++total_replicated_blocks_;
				total_replicated_bytes_ += block.length(); // This value is an aproximation	
				code = ReplicationBlockWriteCode::BLOCK_WRITTEN;
			} else {
				code = ReplicationBlockWriteCode::BLOCK_HASH_INVALID;
			}
		}
        }

	return code;
}

void SessionWalletManager::setMasterBlockFilePassword(const std::string &password) {

	if (password.length() > 0)
		master_blockfile_password_.assign(password);
}

// The master block file should contains a initial seed

void SessionWalletManager::setMasterBlockFile(const std::filesystem::path &master_block_file) {

	// Check first if the data directory has been set
	if (path_.string().length() > 0) {
		std::filesystem::path path(path_);

		path /= master_block_file;

		if (std::filesystem::exists(path)) {
			if (master_blockfile_password_.length() > 0) {
				std::uintmax_t filesize = 0;
				std::error_code ec;

				// Take the size of the file
				filesize = std::filesystem::file_size(path, ec);
				if (!ec) {
					int32_t new_alloc_size = filesize;
					new_alloc_size += (BlockFile::BufferBlockSize * boost::iostreams::mapped_file::alignment());

					if (auto bfile = SharedPointer<BlockFile>(new BlockFile(new_alloc_size)); bfile) {
						bfile->setBlockHashType(hash_func_type_);
						bfile->open(path);	
						if (bfile->is_open()and(bfile->getTotalBlocks() > 0)) {
							EncryptionKey mbp;

							if (mbp.password(master_blockfile_password_)) {
								CryptoPP::SecByteBlock key = mbp.key();			
								uint8_t seed[HASH_BLOCK_SIZE];

								bfile->getSeed(seed);
								if (std::memcmp(key.data(), seed, HASH_BLOCK_SIZE) == 0) {
									master_file_ = bfile;
									PINFO << "Master file " << path.filename().string() << " set";
									return;
								} else {
									PWARN << "Master file " << path.filename().string() << " can not be set";
								}	
							} else {
								PWARN << "Master file password can not be set, check length";
							}
						}
						bfile->close();
					}
				}
			} else
				PWARN << "The master block file password is not set";
		} else
			PWARN << "The file " << path.string() << " do not exists, please check the path";
	} else
		PWARN << "The data directory is not set";
}


void SessionWalletManager::update_available_space() {

	if (path_.string().length() > 0) {
		auto space = std::filesystem::space(path_);

		total_bytes_available_ = space.available;
        }
}

void SessionWalletManager::setDynamicAllocatedMemory(bool value) {

        wallet_cache_->setDynamicAllocatedMemory(value);
}

bool SessionWalletManager::isDynamicAllocatedMemory() const {

        return wallet_cache_->isDynamicAllocatedMemory();
}

int32_t SessionWalletManager::getTotalCacheMisses() const {

        int32_t miss = 0;

        miss += wallet_cache_->getTotalFails();

        return miss;
}

int64_t SessionWalletManager::getCurrentUseMemory() const {

        int64_t mem = sizeof(SessionWalletManager);

        mem += wallet_cache_->getCurrentUseMemory();

        return mem;
}

int64_t SessionWalletManager::getAllocatedMemory() const {

        int64_t mem = sizeof(SessionWalletManager);

        mem += wallet_cache_->getAllocatedMemory();

        return mem;
}

int64_t SessionWalletManager::getTotalAllocatedMemory() const {

        return getAllocatedMemory();
}

void SessionWalletManager::increaseAllocatedMemory(int number) {

        wallet_cache_->create(number);
}

void SessionWalletManager::decreaseAllocatedMemory(int number) {

        wallet_cache_->destroy(number);
}

bool SessionWalletManager::verify_data_structure(const std::filesystem::path &path) const {

        std::vector<std::string> items = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
                "A", "B", "C", "D", "E", "F" };

        for (auto &i: items) {
                for (auto &j: items) {
                        std::filesystem::path check_path(path);
                        check_path /= i;
                        check_path /= j;
			if (!std::filesystem::is_directory(check_path))
				return false; 
                }
        }
	return true;
}


void SessionWalletManager::create_data_structure(const std::filesystem::path &path) {

	std::vector<std::string> items = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
		"A", "B", "C", "D", "E", "F" };

	for (auto &i: items) {
		for (auto &j: items) {
			std::filesystem::path new_path(path);
			new_path /= i;
			new_path /= j;
			if (!std::filesystem::exists(new_path))
				std::filesystem::create_directories(new_path);
		}
	}
}

void SessionWalletManager::setDataDirectory(const std::filesystem::path &path) {

	if (path_.string().length() == 0) {
		try {
			if (std::filesystem::exists(path)) {
				if (std::filesystem::is_directory(path)) {
					// We create a file and write on them to verify if we have real read/write access
					std::ostringstream out;
					std::filesystem::path filename(path);

					out << "temp." << getpid() << ".txt";
					filename /= out.str();
					std::ofstream fs(filename.string());
					fs << "The directory is writable";
					fs.flush();
					fs.close();
					if (std::filesystem::exists(filename)) {
						// The file has been created so we have permisions
						std::filesystem::remove(filename);
						path_ = path;
					} else {
						PERROR << "The path " << path.string() << " is not writable";
						return;
					}

					if (verify_data_structure(path) == false) {
						PINFO << "Creating data directory structure on " << path.string();
						create_data_structure(path);
					}
				} else {
					PERROR << "The path " << path.string() << " is not a directory";
				}
			} else {
				path_ = path; 
				PINFO << "Creating data directory structure on " << path.string();
				create_data_structure(path);
			}
		} catch (const std::exception &ex) {
			PERROR << ex.what();
		}

		update_available_space();
	}
}

void SessionWalletManager::setReadOnly(bool value) {

	PINFO << "Read only mode " << (value ? "on" : "off");
	read_only_ = value; 
}


WalletCreateCode SessionWalletManager::createWallet(const std::string &wallet, PetalusBlock &block) {

	if (read_only_)
		return WalletCreateCode::WALLET_READ_ONLY;

	return create_wallet(wallet, block);
}

void SessionWalletManager::setEncryptionPassword(const std::string &password) {

	// If there are errors due to a rotation key we can not update the password
	if (total_reencrypted_errors_ > 0) {
		PWARN << "Encryption password can not be set due to a wrong rotation key process";
	} else {
		if (password.length() == 0) {
			key_.reset();
			password_ = "";
			PINFO << "Encryption password unset";
		} else {
			if (set_encryption_password_key(password, key_)) {
				password_.assign(password);
				PINFO << "Encryption password set";
			} else {
				PWARN << "Encryption password can not be set, check length";
			}
		}
	}
}

bool SessionWalletManager::set_encryption_password_key(const std::string &password, EncryptionKey &bkey) {

	return bkey.password(password);
}

void SessionWalletManager::setBlockHashType(const std::string &name) {

	// Find if the method given by the user if is supported
	for (auto &item: hash_handlers)
		if (name.compare(item.name) == 0) {
			hash_func_type_ = item.id;
			PINFO << "Hashing algorithm set to:" << name;
			break;
		}
}

const char *SessionWalletManager::getBlockHashType() const {

	return hash_handlers[static_cast<uint8_t>(hash_func_type_)].name;
}

void SessionWalletManager::update_master_blockfile(const std::string &wallet, uint8_t seed[]) {

	if (master_file_) {
		nlohmann::json j;

		std::time_t now = std::time(nullptr);

		char mbstr[64];
		std::strftime(mbstr, 64, "%X %D", std::localtime(&now));

		j["name"] = wallet;
		j["creation_date"] = mbstr;

		PetalusBlock block(j);
		block.type(BlockType::SYS_JSON_MASTER_ENTRY);

		if (master_file_->write(block) > 0)
			master_file_->readBlockHash(master_file_->getTotalBlocks(), seed);
		
		master_file_->sync();
	}
}


// This method have the logic for create a user wallet
WalletCreateCode SessionWalletManager::create_wallet(const std::string &wallet, PetalusBlock &block) {

	WalletCreateCode code = WalletCreateCode::WALLET_NOT_AVAILABLE;

	if (block.type() != BlockType::USR_JSON_STRING) 
		return WalletCreateCode::WALLET_WRONG_FORMAT;

	// verify that the wallet and the password are hex values
	if (wallet.find_first_not_of("0123456789abcdefABCDEF") == std::string::npos) {

                std::filesystem::path path(path_);

		path /= wallet.substr(0, 1);
		path /= wallet.substr(1, 1);

		// Verify that the file dont exist
		std::filesystem::path filepath(path);

		filepath /= wallet + std::string(".mblk0000");
        	if (!std::filesystem::exists(filepath)) {
                	if (auto wa = wallet_cache_->acquire(); wa) {

				code = create_wallet(wa, path, block, wallet);
				if (code == WalletCreateCode::WALLET_CREATED_SUCCESS)
                       			++ total_created_wallets_;
				
				// Close the wallet
				wallet_cache_->release(wa);
			} else {
				code = WalletCreateCode::WALLET_NOT_AVAILABLE;
			}
                } else {
			code = WalletCreateCode::WALLET_EXISTS;
		}
	} else {
		code = WalletCreateCode::WALLET_WRONG_FORMAT;
	}
	return code;
}

int32_t SessionWalletManager::getLastBlockNumberWritten(const std::string &session) const {

	int32_t block_number = 0;
	if (auto wa = get_wallet_from_session(session); wa)
		block_number = wa->getLastBlockNumberWritten();
	
	return block_number;
}

WalletCreateCode SessionWalletManager::create_wallet(const SharedPointer<Wallet> &wa, const std::filesystem::path path, PetalusBlock &block, const std::string &name) {

	uint8_t seed[HASH_BLOCK_SIZE] = { 0x00 };

       	wa->setName(name);
	wa->setBlockHashType(hash_func_type_);
	wa->setMaxBlockFileSize(max_blockfile_size_);
	wa->setEncryptionKey(key_);

	std::string password = "";
	std::string base64pkey = "";
	std::string public_key_type = "";

	// find if the json contains a public key
	nlohmann::json j = nlohmann::json::parse(block.data());
	if (j.find("password") != j.end()) 
		if (j["password"].is_string()) {
			password = j["password"];
        		if (password.find_first_not_of("0123456789abcdefABCDEF") != std::string::npos)
				return WalletCreateCode::WALLET_WRONG_FORMAT;

		}

	if (password.length() == 0)
		return WalletCreateCode::WALLET_WRONG_FORMAT;

	j.erase("password"); // We dont store the original password recieived

	if (j.find("ec_public_key") != j.end()) {
		if (j["ec_public_key"].is_string()) {
                        public_key_type = "ec_public_key";
			base64pkey.assign(j["ec_public_key"]);	
			j.erase("ec_public_key");
			std::string decoded;
			CryptoPP::StringSource(base64pkey, true, new CryptoPP::Base64Decoder(new CryptoPP::StringSink(decoded)));

			CryptoPP::StringSource ss(decoded, true);
			PublicKey public_key;

			// Is unlikely that the system responsible of creates the wallet dont follow
			// the requirements of been EC keys, but sometimes.....
			try {
				public_key.Load(ss);

			} catch (const std::exception &ex) {
				wallet_cache_->release(wa);
				return WalletCreateCode::WALLET_WRONG_PUBLIC_KEY;
			}
			int signature_length = public_key.GetGroupParameters().GetCurve().GetField().MaxElementByteLength() * 2;
			if (signature_length > 254) {
				wallet_cache_->release(wa);
				return WalletCreateCode::WALLET_WRONG_PUBLIC_KEY;
			}
		}
	} else if (j.find("ed25519_public_key") != j.end()) {
		if (j["ed25519_public_key"].is_string()) {
                        public_key_type = "ed25519_public_key";
			CryptoPP::AutoSeededRandomPool prng;
			base64pkey.assign(j["ed25519_public_key"]);	
			j.erase("ed25519_public_key");
			std::string decoded;
			CryptoPP::StringSource(base64pkey, true, new CryptoPP::Base64Decoder(new CryptoPP::StringSink(decoded)));

			CryptoPP::StringSource ss(decoded, true);
			ED25519PublicKey public_key;

			try {
				public_key.Load(ss);

			} catch (const std::exception &ex) {
				wallet_cache_->release(wa);
				return WalletCreateCode::WALLET_WRONG_PUBLIC_KEY;
			}
                        EDVerifier verifier(public_key.GetPublicElement());

			bool valid = verifier.GetPublicKey().Validate(prng, 3);
			if (valid == false) {
                                wallet_cache_->release(wa);
                                return WalletCreateCode::WALLET_WRONG_PUBLIC_KEY;
			}
		}
	}

	update_master_blockfile(name, seed);

	// Open the wallet for creation, the block file will be created at this point
	wa->open(path, true, seed);

	// Write the password access to the wallet with a public key if exists
	if (password.length() > 0) 
		wa->writePassword(password, base64pkey, public_key_type);

	if ((j.find("folders") != j.end())and(j["folders"].is_array())) {
		for (nlohmann::json::iterator it = j["folders"].begin(); it != j["folders"].end(); ++it)
			wa->createFolder(it.value());
			
		j.erase("folders");
	}

	if (!j.empty()) { // Write the initial data
		// Create a new block because the original could be modified
		PetalusBlock pblock(j);
		// Encrypt the initial block
               	if (!key_.key().empty())
                	pblock.encrypt(key_);
                			
		wa->write(pblock);
	}
	return WalletCreateCode::WALLET_CREATED_SUCCESS;
}

void SessionWalletManager::setMaxBlockFileSize(int32_t max_size) {

	// Just change if the value is multiple of the page size
	if ((max_size % boost::iostreams::mapped_file::alignment()) == 0)
		max_blockfile_size_ = max_size;
}

int32_t SessionWalletManager::getTotalBlocks(const std::string &session) const {

	int32_t blocks = 0;
	if (auto wa = get_wallet_from_session(session); wa) 
		blocks = wa->getTotalBlocks();
	
	return blocks;
}

SharedPointer<Wallet> SessionWalletManager::get_wallet(const SessionContext &ctx) {

	if (auto it = sessions_.get<session_table_tag_unique>().find(ctx.getSession()); it != sessions_.end()) {
		if (ctx.getWallet().compare((*it)->getName()) == 0)
			return *it;
		++ total_session_mismatchs_;
	}	
	return nullptr;
}

SharedPointer<Wallet> SessionWalletManager::get_wallet_from_session(const std::string &session) const {

	if (auto it = sessions_.get<session_table_tag_unique>().find(session); it != sessions_.end())
		return *it;

	return nullptr;
}
	
int32_t SessionWalletManager::read(const SessionContext &ctx, PetalusBlock &block, const std::string &folder, int32_t number) {

	if (auto wa = get_wallet(ctx); wa) {
		int32_t bytes = 0;

		if (folder.length() > 0)
			bytes = wa->readFromFolder(folder, number, block);
		else
			bytes = wa->read(number, block);	

#if defined(PYTHON_BINDING)
                if (wa->callback_read.haveCallback()) {
                        wa->setCurrentBlock(&block);
                        // Execute the callback
                        try {
                                PythonGilContext gil_lock();

                                boost::python::call<void>(wa->callback_read.getCallback(), boost::python::ptr(wa.get()));
                        } catch (const std::exception &ex) {
				PERROR << ex.what();
				return 0;
                        }
			if (!block.accept_block()) { // The block has been refused by the callback
				PINFO << "Can not read from wallet " << wa->getName() << " block:" << number << " not accepted";	
				return 0;
			}
                }
#endif
		// Update the time of the wallet
		std::time_t current_time = std::time(nullptr);
		update_session_time(ctx.getSession(), current_time);

		++ total_reads_;
		bytes = block.length();
		total_bytes_reads_ += bytes;
		return bytes;
	}
	return 0;
}

int32_t SessionWalletManager::write(const SharedPointer<Wallet> &wa, const SessionContext &ctx, PetalusBlock &block) {

	return write(wa, ctx, block, std::string(""));
}

int32_t SessionWalletManager::write(const SessionContext &ctx, blockchain::PetalusBlock &block, const std::string &folder) {

	if (read_only_)
		return 0;

	if (auto wa = get_wallet(ctx); wa) 
		return write(wa, ctx, block, folder);

	return 0;	
}

int32_t SessionWalletManager::write(const SharedPointer<Wallet> &wa, const SessionContext &ctx, PetalusBlock &block, const std::string &folder) {

	int32_t bytes = 0;

#if defined(PYTHON_BINDING)
	if (wa->callback_write.haveCallback()) {
		wa->setCurrentBlock(&block);	
		// Execute the callback
		try {
			PythonGilContext gil_lock();

			boost::python::call<void>(wa->callback_write.getCallback(), boost::python::ptr(wa.get()));
		} catch (const std::exception &ex) {
			PERROR << ex.what();
			return 0;
		}
		if (!block.accept_block()) {
			PINFO << "Can not write on wallet " << wa->getName() << " block not accepted";	
			return 0;
		}
	}
#endif
	if (folder.length() > 0)
		bytes = wa->writeOnFolder(folder, block);
	else
		bytes = wa->write(block);


	// Update the time of the wallet
	std::time_t current_time = std::time(nullptr);
	update_session_time(ctx.getSession(), current_time);

	if (bytes > 0) {
		total_bytes_writes_ += bytes;
		++ total_writes_;
	} else {
		PWARN << "Can not write on wallet " << wa->getName() << " code:" << (int)wa->getWriteErrorCode();	
		++ total_error_writes_;
	}

	// Check if we are close to have no space on the data directory
	total_bytes_available_ -= bytes + sizeof(petalus_block_file_v1);
	if (total_bytes_available_ < read_only_safe_guard_) {
               	std::ostringstream msg;
                msg << "No space available for writing, changing to readonly mode";

                PWARN << msg.str();
                petalus::warning_message(msg.str());
		read_only_ = true;
	}
	return bytes;
}

bool SessionWalletManager::close(const std::string &session) {

	if (auto wa = get_wallet_from_session(session); wa) {
		++ total_close_wallets_;

		SessionByID::iterator it = sessions_.get<session_table_tag_unique>().find(session);

#if defined(PYTHON_BINDING)
		pending_read_callbacks_.erase(wa->getName());
		pending_write_callbacks_.erase(wa->getName());
#endif
		wallets_.erase(wa->getName());
		wallet_cache_->release(wa); // The wallet is closed and released
		sessions_.erase(it);
		return true;
	}
	return false;
}

void SessionWalletManager::close() {

	// Close all the wallets that are open
        auto &finx = sessions_.get<session_table_tag_duration>();

        for (auto it = sessions_.get<session_table_tag_duration>().begin() ; it != sessions_.get<session_table_tag_duration>().end(); ) {
                SharedPointer<Wallet> wa = (*it);

#if defined(PYTHON_BINDING)
		pending_read_callbacks_.erase(wa->getName());
		pending_write_callbacks_.erase(wa->getName());
#endif
		wallets_.erase(wa->getName());
		wallet_cache_->release(wa); // The wallet is closed and released

                // Remove the wallet from the multiindex
                it = finx.erase(it);
        }
	sessions_.clear();
	wallets_.clear();

	if (master_file_)
		master_file_->close();
}

// The wallet can be open as readonly just external systems
std::string SessionWalletManager::open_readonly_session(const std::string &wallet) {

	std::string session;

	wallet_open_code_ = WalletOpenCode::WALLET_NO_RESOURCES; 

	// Verify if the wallet is all ready open by the owner
	if (auto it = wallets_.find(wallet); it != wallets_.end()) {
		SharedPointer<Wallet> wa = (*it).second;

		session = wa->getSessionId(); // The external entity got the same id

		++ total_reuse_wallets_;
		wallet_open_code_ = WalletOpenCode::WALLET_OPEN_SUCCESS;
	} else {
                // verify that the string is hex
                if (wallet.find_first_not_of("0123456789abcdefABCDEF") == std::string::npos) {
                        std::filesystem::path path(path_);

                        path /= wallet.substr(0, 1);
                        path /= wallet.substr(1, 1);

                        if (auto wa = wallet_cache_->acquire(); wa) {
                                wa->setName(wallet);
                                wa->setBlockHashType(hash_func_type_);
                                wa->setEncryptionKey(key_);
                                wa->setMaxBlockFileSize(max_blockfile_size_);
                                wa->open(path);
                                wa->setLastTimeOperation(std::time(nullptr));

                                if (wa->is_open()) {
                                        if (!wa->isLock()) {
                                        	generateRandomBytes(session, 16);
                                                if (session.length() > 0) {
                                                	wa->setSessionId(session);

                                                        wallets_.insert(std::pair<std::string, SharedPointer<Wallet>>(wallet, wa));
                                                        sessions_.insert(wa);
                                                        ++ total_open_wallets_;
                                                        wallet_open_code_ = WalletOpenCode::WALLET_OPEN_SUCCESS;
                                                } else {
                                                	wallet_open_code_ = WalletOpenCode::WALLET_NO_RESOURCES;
                                                        ++ total_fail_open_wallets_;
                                                        wallet_cache_->release(wa);
                                                }
                                        } else {
                                                wallet_open_code_ = WalletOpenCode::WALLET_LOCK;
                                                ++ total_lock_wallets_;
                                                wallet_cache_->release(wa);
                                        }
                                } else {
                                        wallet_open_code_ = WalletOpenCode::WALLET_NO_EXISTS;
                                        ++ total_fail_open_wallets_;
                                        wallet_cache_->release(wa);
                                }
                        } else {
                                wallet_open_code_ = WalletOpenCode::WALLET_NO_RESOURCES;
                                ++ total_fail_open_wallets_;
                        }
                } else {
                        wallet_open_code_ = WalletOpenCode::WALLET_WRONG_FORMAT;
                        ++ total_fail_open_wallets_;
                }
        }
	return session;
}


// verify that the wallet can be open with that password so a new session id will be generated
std::string SessionWalletManager::open_user_session(const std::string &wallet, const std::string &password) {

	std::string session;

	wallet_open_code_ = WalletOpenCode::WALLET_NO_RESOURCES; 

	update_available_space();

	// verify if the wallet is all ready open
	if (auto it = wallets_.find(wallet); it != wallets_.end()) {
		SharedPointer<Wallet> wa = (*it).second;

		if (wa->validPassword(password)) {
			wa->setBlockHashType(hash_func_type_); // May be the user has change while running
			wa->setEncryptionKey(key_); // May be the user has change while running
			session = wa->getSessionId();
			++ total_reuse_wallets_;
			wallet_open_code_ = WalletOpenCode::WALLET_OPEN_SUCCESS;
		} else {
			++ total_wrong_credentials_;
			wallet_open_code_ = WalletOpenCode::WALLET_WRONG_CREDENTIALS;

			// the wallet should be removed;

			SessionByID::iterator sit = sessions_.get<session_table_tag_unique>().find(wa->getSessionId());                	
			sessions_.erase(sit);
			
			wallets_.erase(it);
			wallet_cache_->release(wa);
			++ total_close_wallets_;
		}
	} else { 
        	// verify that the string is hex
        	if ((wallet.find_first_not_of("0123456789abcdefABCDEF") == std::string::npos) and
			(password.find_first_not_of("0123456789abcdefABCDEF") == std::string::npos)) {

                	std::filesystem::path path(path_);

			path /= wallet.substr(0, 1);
			path /= wallet.substr(1, 1);

			if (auto wa = wallet_cache_->acquire(); wa) {
				wa->setName(wallet);
				wa->setBlockHashType(hash_func_type_);
				wa->setEncryptionKey(key_);
				wa->setMaxBlockFileSize(max_blockfile_size_);
				wa->open(path);
				wa->setLastTimeOperation(std::time(nullptr));

				if (wa->is_open()) {
					if (!wa->isLock()) {
						if (wa->validPassword(password) == true) {
							generateRandomBytes(session, 16);
							if (session.length() > 0) {
								wa->setSessionId(session);
#if defined(PYTHON_BINDING)
								// Check if there is pending callbacks for the wallet
								auto itr = pending_read_callbacks_.find(wallet);
								if (itr != pending_read_callbacks_.end()) {
									wa->callback_read.setCallback((*itr).second);
								}
								auto itw = pending_write_callbacks_.find(wallet);
								if (itw != pending_write_callbacks_.end()) {
									wa->callback_write.setCallback((*itw).second);
								}
#endif
								wallets_.insert(std::pair<std::string, SharedPointer<Wallet>>(wallet, wa));
                        					sessions_.insert(wa);
                        					++ total_open_wallets_;
								wallet_open_code_ = WalletOpenCode::WALLET_OPEN_SUCCESS; 
							} else {
								wallet_open_code_ = WalletOpenCode::WALLET_NO_RESOURCES;
								++ total_fail_open_wallets_;
								wallet_cache_->release(wa);
							}
						} else {
							wallet_open_code_ = WalletOpenCode::WALLET_WRONG_CREDENTIALS;
							++ total_wrong_credentials_;
							wallet_cache_->release(wa);
						}
					} else {
						wallet_open_code_ = WalletOpenCode::WALLET_LOCK;
						++ total_lock_wallets_;
						wallet_cache_->release(wa);
					}
				} else {
					wallet_open_code_ = WalletOpenCode::WALLET_NO_EXISTS;
					++ total_fail_open_wallets_;
					wallet_cache_->release(wa);
				}
			} else {
				wallet_open_code_ = WalletOpenCode::WALLET_NO_RESOURCES;
				++ total_fail_open_wallets_;
			}
                } else {
			wallet_open_code_ = WalletOpenCode::WALLET_WRONG_FORMAT;
			++ total_fail_open_wallets_;
		}
        }
	return session;
}

void SessionWalletManager::update_session_time(const std::string &session, std::time_t time) {

        auto &finxd = sessions_.get<session_table_tag_duration>();
        auto &finxu = sessions_.get<session_table_tag_unique>();

        SessionByID::const_iterator it = finxu.find(session);

        SessionByDuration::const_iterator itd = sessions_.project<session_table_tag_duration>(it);
        finxd.modify(itd, Wallet::updateTime(time));
}

void SessionWalletManager::updateTimers(std::time_t current_time) {

        auto &finx = sessions_.get<session_table_tag_duration>();

#ifdef MYDEBUG
        char mbstr[64];
        std::strftime(mbstr, 64, "%D %X", std::localtime(&current_time));

        std::cout << __FILE__ << ":" << __func__ << ":Checking Timers at " << mbstr << " total sessions:" << sessions_.size() << std::endl;
#endif
        // We check the iterator backwards because the old sessions will be at the end
        for (auto it = sessions_.get<session_table_tag_duration>().begin() ; it != sessions_.get<session_table_tag_duration>().end(); ) {
                SharedPointer<Wallet> wa = (*it);

                if (wa->getLastTimeOperation() + session_timeout_ <= current_time ) {
			++ total_timeout_wallets_;
			++ total_close_wallets_;
		        it = finx.erase(it);
#ifdef MYDEBUG
                        std::cout << __FILE__ << ":" << __func__ << ":Session Expires: " << *wa <<  " releasing from the multi_index" <<std::endl;
#endif

			wallets_.erase(wa->getName());
			wallet_cache_->release(wa);
		} else 
			break;
	}
}

void SessionWalletManager::showSessions(std::basic_ostream<char> &out) {

	std::time_t now = std::time(nullptr);

	if (now - last_user_show_sessions_ > session_timeout_) { 
		updateTimers(now);
		last_user_show_sessions_ = now;
	}

	PINFO << "User show sessions";

        const char *header = "%-32s %-64s %-12s %-8s %-8s";
        const char *format = "%-32s %-64d %-12d %-8d %-8s";

        out << "Sessions statistics summary" << std::endl;
        out << "\t" << boost::format(header) % "Session" % "Wallet" % "Bytes" % "Blocks" % "Access";
        out << std::endl;

        SessionByDuration::reverse_iterator begin = sessions_.get<session_table_tag_duration>().rbegin();
        SessionByDuration::reverse_iterator end = sessions_.get<session_table_tag_duration>().rend();

        // The sessions are sorted by most recent activity, so the actives will be first
        // and the ones with low activity will be the last to show
        for (auto it = begin ; it != end; ++it ) {
                SharedPointer<Wallet> wa = (*it);
		const char *name = wa->getName();
		std::string sid = wa->getSessionId();
		int32_t blocks = wa->getTotalBlocks();
		int32_t bytes = wa->getTotalBytes();
		std::time_t wa_time = wa->getLastTimeOperation();
        
		char mbstr[64];
        	std::strftime(mbstr, 64, "%X %D", std::localtime(&wa_time));

	        out << "\t" << boost::format(format) % sid % name % bytes % blocks % mbstr;
                out << "\n";
	}
	out << std::endl;
}

void SessionWalletManager::showSessions(nlohmann::json &j) {

        std::time_t now = std::time(nullptr);

        if (now - last_user_show_sessions_ > session_timeout_) {
                updateTimers(now);
                last_user_show_sessions_ = now;
        }

        SessionByDuration::reverse_iterator begin = sessions_.get<session_table_tag_duration>().rbegin();
        SessionByDuration::reverse_iterator end = sessions_.get<session_table_tag_duration>().rend();

        // The sessions are sorted by most recent activity, so the actives will be first
        // and the ones with low activity will be the last to show
        for (auto it = begin ; it != end; ++it ) {
                SharedPointer<Wallet> wa = (*it);
		nlohmann::json obj;

		obj["sid"] = wa->getSessionId();
		obj["wallet"] = wa->getName();
		obj["bytes"] = wa->getTotalBytes();
		obj["blocks"] = wa->getTotalBlocks();
		obj["time"] = wa->getLastTimeOperation();
	
		j.push_back(obj);	
        }
}

std::ostream& operator<< (std::ostream &out, const SessionWalletManager &bm) {

	std::string unit("Bytes");
	std::string sunit("Bytes");
	std::string available_unit("Bytes");
	std::string pass(bm.password_.substr(0, 25));
	std::string mbpass(bm.master_blockfile_password_.substr(0, 18));
	std::string mbfilename("not set");
	int64_t bytes = bm.compute_data_size();
	int64_t available = bm.total_bytes_available_;
	int64_t sbytes = bm.read_only_safe_guard_;
        Color::Modifier yellow(Color::FG_YELLOW);
        Color::Modifier def(Color::FG_DEFAULT);

	// Put the colors in yewllow for warnings

	unitConverter(bytes, unit);
	unitConverter(sbytes, sunit);
	unitConverter(available, available_unit);

	out << "SessionWalletManager\n";
        out << "\t" << "Data directory:" << bm.path_.string() << "\n";
	out << "\t" << "Available:              " << std::setw(9 - available_unit.length()) << available << " " << available_unit << "\n";
	out << "\t" << "Data size:              " << std::setw(9 - unit.length()) << bytes << " " << unit << "\n";
	out << "\t" << "Safe guard size:        " << std::setw(9 - sunit.length()) << sbytes << " " << sunit << "\n";
        out << "\t" << "Password:" << pass << "\n";
        out << "\t" << "Mblock password:" << mbpass << "\n";

	if (mbpass.length() > 0) {
		if (bm.master_file_) {
			mbfilename = bm.master_file_->getPath().filename().string();	
        		out << "\t" << "Mblock file:" << mbfilename << "\n";
		} else {
        		out << "\t" << "Mblock file:               " << yellow << "not set" << def << "\n";
		}
      	} 
        out << "\t" << "HashType:" << std::right << std::setfill(' ') << std::setw(25) << bm.getBlockHashType() << "\n";
        out << "\t" << "MaxBlockFileSize:       " << std::setw(10) << bm.max_blockfile_size_ << "\n";
        out << "\t" << "Timeout:                " << std::setw(10) << bm.session_timeout_ << "\n";
	if (bm.read_only_)
        	out << "\t" << "Read only mode:         " << std::setw(10) << yellow << "on" << def << "\n";
	else
        	out << "\t" << "Read only mode:         " << std::setw(10) << "off" << "\n";
        out << "\t" << "Total memory wallets:   " << std::setw(10) << bm.sessions_.size() << "\n";
        out << "\t" << "Total created wallets:  " << std::setw(10) << bm.total_created_wallets_ << "\n";
        out << "\t" << "Total open wallets:     " << std::setw(10) << bm.total_open_wallets_ << "\n";
        out << "\t" << "Total lock wallets:     " << std::setw(10) << bm.total_lock_wallets_ << "\n";
        out << "\t" << "Total reuse wallets:    " << std::setw(10) << bm.total_reuse_wallets_ << "\n";
        out << "\t" << "Total fail open wallets:" << std::setw(10) << bm.total_fail_open_wallets_ << "\n";
        out << "\t" << "Total close wallets:    " << std::setw(10) << bm.total_close_wallets_ << "\n";
        out << "\t" << "Total timeout wallets:  " << std::setw(10) << bm.total_timeout_wallets_ << "\n";
        out << "\t" << "Total read bytes:       " << std::setw(10) << bm.total_bytes_reads_ << "\n";
        out << "\t" << "Total write bytes:      " << std::setw(10) << bm.total_bytes_writes_ << "\n";
        out << "\t" << "Total writes:           " << std::setw(10) << bm.total_writes_ << "\n";
        out << "\t" << "Total reads:            " << std::setw(10) << bm.total_reads_ << "\n";
        out << "\t" << "Total error writes:     " << std::setw(10) << bm.total_error_writes_ << "\n";
        out << "\t" << "Total session mismatchs:" << std::setw(10) << bm.total_session_mismatchs_ << "\n";
	out << "\t" << "Total encrypted files:  " << std::setw(10) << bm.total_reencrypted_files_  << "\n";
	out << "\t" << "Total encrypted errors: " << std::setw(10) << bm.total_reencrypted_errors_  << "\n";
	out << "\t" << "Total replicated blocks:" << std::setw(10) << bm.total_replicated_blocks_  << "\n";
	out << "\t" << "Total replicated bytes: " << std::setw(10) << bm.total_replicated_bytes_  << "\n";
	bm.wallet_cache_->statistics(out);

	return out;
}

void SessionWalletManager::statistics(std::basic_ostream<char> &out) const {

        out << *this;
}

void SessionWalletManager::showSession(std::basic_ostream<char> &out, const std::string &session) const {

        if (auto wa = get_wallet_from_session(session); wa) {
                PINFO << "User show session " << session;

                out << *wa;
                wa->showLastBlock(out);
        }
}

void SessionWalletManager::showSession(std::basic_ostream<char> &out, const std::string &session, int32_t number) const {

        if (auto wa = get_wallet_from_session(session); wa) {
                PINFO << "User show session " << session << " block " << number;

                out << *wa;
                wa->showBlock(out, number);
	}
}

void SessionWalletManager::showSession(std::basic_ostream<char> &out, const std::string &session, const std::string &folder, int32_t number) const {

        if (auto wa = get_wallet_from_session(session); wa) {
                PINFO << "User show session " << session << " block " << number;

                out << *wa;
               	wa->showBlock(out, number, folder);
	}
}

void SessionWalletManager::showSession(nlohmann::json &out, const std::string &session, const std::string &folder, int32_t number) const {

        if (auto wa = get_wallet_from_session(session); wa) {
                PINFO << "User show session " << session << " block " << number;

                out << *wa;
               	wa->showBlock(out, number, folder);
	}
}

#if defined(PYTHON_BINDING)

void SessionWalletManager::showSessions() {

	showSessions(petalus::OutputManager::getInstance()->out());
}

void SessionWalletManager::showSession(const std::string &session) const {

	showSession(petalus::OutputManager::getInstance()->out(), session);
}

void SessionWalletManager::showSession(const std::string &session, int32_t number) const {

	showSession(petalus::OutputManager::getInstance()->out(), session, number);
}

// This value can be negative needs to check if possible to increase or decrease
void SessionWalletManager::setMaxCacheSessions(int value) {

	if (value > 0)
		wallet_cache_->create(value);
	else
		wallet_cache_->destroy(std::abs(value));
}

void SessionWalletManager::addReadTrigger(const std::string &wallet, PyObject *callback) {

	// Add the callbacks to a pending container for late adding

	// verify if the wallet is all ready open and update the callback directly
        if (auto it = wallets_.find(wallet); it != wallets_.end()) {
                SharedPointer<Wallet> wa = (*it).second;

		wa->callback_read.setCallback(callback);
        } 
	pending_read_callbacks_.insert(std::pair<std::string, PyObject*>(wallet, callback));
}

void SessionWalletManager::addWriteTrigger(const std::string &wallet, PyObject *callback) {

        if (auto it = wallets_.find(wallet); it != wallets_.end()) {
                SharedPointer<Wallet> wa = (*it).second;

		wa->callback_write.setCallback(callback);
        } 
	pending_write_callbacks_.insert(std::pair<std::string, PyObject*>(wallet, callback));
}

void SessionWalletManager::showTriggers() const {

	PINFO << "User show triggers";

	showTriggers(petalus::OutputManager::getInstance()->out());
}

void SessionWalletManager::showTriggers(std::basic_ostream<char> &out) const {

        const char *header = "%-64s %-64s %-8s";
        const char *format = "%-64d %-64s %-8s";

        out << "SessionWalletManager triggers" << std::endl;
        out << "\t" << boost::format(header) % "Wallet" % "Function" % "Type";
        out << std::endl;

        SessionByDuration::reverse_iterator begin = sessions_.get<session_table_tag_duration>().rbegin();
        SessionByDuration::reverse_iterator end = sessions_.get<session_table_tag_duration>().rend();

        // The sessions are sorted by most recent activity, so the actives will be first
        // and the ones with low activity will be the last to show
        for (auto it = begin ; it != end; ++it ) {
                SharedPointer<Wallet> wa = (*it);
		if (wa->callback_read.haveCallback()) {
			const char *cbname = wa->callback_read.getName();
                	out << "\t" << boost::format(format) % wa->getName() % cbname % "read";
                	out << "\n";
		} 
		if (wa->callback_write.haveCallback()) {
			const char *cbname = wa->callback_write.getName();
                	out << "\t" << boost::format(format) % wa->getName() % cbname % "write";
                	out << "\n";
		} 
        }
        out << std::endl;
}

void SessionWalletManager::closeSessions() {

	PINFO << "User close sessions";

	close();
}

#endif

// Computes the dize of the data directory
int64_t SessionWalletManager::compute_data_size() const {

	int64_t bytes = 0;

	if (path_.string().length() > 0) { // Is set to something
		for (std::filesystem::recursive_directory_iterator it(path_); 
			it != std::filesystem::recursive_directory_iterator(); ++it) {

			if(!std::filesystem::is_directory(*it)) {
				std::filesystem::path f(*it);
				std::string extension(f.extension().c_str()); 

				if (extension.compare(2, 3, "blk") == 0) // mblk and fblk
					bytes += std::filesystem::file_size(f); 
				}
		}  
	}
	return bytes;
}

// This method reencrypts all the data with a new key
void SessionWalletManager::rotateEncryptionKey(const std::string &password) {

	EncryptionKey newkey {};
	if ((password_.compare(password) != 0)and(set_encryption_password_key(password, newkey) == true)) {

		const char *new_extension = ".new";
		const char *old_extension = ".old";
		std::ostringstream errormsg;

		// close all the wallets
		close();

		int64_t total_bytes = compute_data_size();

		// Check if there is space on the disk
		update_available_space();

		// There is space on the disk?
		if (total_bytes < total_bytes_available_) {
			std::ostringstream msg;
			msg << "Re-encrypting " << total_bytes << " bytes";

			PINFO << msg.str();
        		petalus::information_message(msg.str());
		
			// Take all the main block files of the data directory
			std::list<std::filesystem::path> main_files;
			std::list<std::filesystem::path> all_files;
			if (path_.string().length() > 0) { // Is set to something
				for (std::filesystem::recursive_directory_iterator it(path_); 
					it != std::filesystem::recursive_directory_iterator(); ++it) {

					if(!std::filesystem::is_directory(*it)) {
						std::filesystem::path f(*it);
						std::string extension(f.extension().c_str()); 

						if (extension.compare(1, 4, "mblk") == 0) {
							main_files.push_back(*it);
							all_files.push_back(*it);
						} else if (extension.compare(1, 4, "fblk") == 0)
							all_files.push_back(*it);
					}
				}  
			}

			total_reencrypted_files_ = 0;
			total_reencrypted_errors_ = 0;

			// First make a copy of the existing main files with their correspondig folder files
			for (auto &f: main_files) {
                        	std::uintmax_t filesize = 0;
                       		std::error_code ec;

                        	// Take the size of the original file
                        	filesize = std::filesystem::file_size(f, ec);
                        	if (!ec) {
					auto bsrc = BlockFile(filesize);
					auto bdst = BlockFile(filesize + (1024 * 1024));

					// The source files could be not encrypted and now we want to encrypt all
					if (password_.length() > 0)
						bsrc.setEncryptionKey(key_);

					bsrc.open(f, boost::iostreams::mapped_file_base::mapmode::readonly);
					if (bsrc.is_open()) {
						std::filesystem::path path(f);
						std::string extension(path.extension().c_str());

						// The new files will have the extension .tmp
						extension.append(new_extension);

                                		path.replace_extension(extension);

       						bdst.setEncryptionKey(newkey);
						bdst.setBlockHashType(bsrc.getBlockHashType());
       						bdst.open(path);

                                        	// If the parent have public key we need to propagate to the other files
                                        	if (bsrc.havePublicKey()) {
							if (bsrc.isECPublicKey())
                                                		bdst.setPublicKey(bsrc.getECPublicKey());
							else
                                                		bdst.setPublicKey(bsrc.getED25519PublicKey());
                                        	}
						if (bdst.is_open()) {
       							bdst.clone(bsrc);
							++ total_reencrypted_files_;
						} else {
							++ total_reencrypted_errors_;
							errormsg << "Can not open:" << path.string();				 
						}
						// Check if there is folders
						auto folders = bsrc.getFolders();
						for (auto &f: folders) {
                					int32_t block_number = f.second.first;
							std::filesystem::path fpath(bdst.getPath());

							// Take the size of the original folder file
                                			filesize = std::filesystem::file_size(fpath, ec);
							if (!ec) {
								fpath.remove_filename();
								fpath /= f.second.second;

								auto bfsrc = BlockFile(filesize); // Folder source file

								// The folder could be encrypted also
								if (password_.length() > 0)
									bfsrc.setEncryptionKey(key_);

								bfsrc.open(fpath, boost::iostreams::mapped_file_base::mapmode::readonly);
								if (bfsrc.is_open()) {
									std::filesystem::path fnpath(fpath);
									std::string extension(fnpath.extension().c_str());

                							// The first block should be verified with the parent block
                							// Read the last block hash of the main file
                							uint8_t digest[HASH_BLOCK_SIZE];

                							bsrc.readBlockHash(block_number, digest);

									auto bfdst = BlockFile(filesize + (1024 * 1024)); // Folder destination file

									bfdst.setEncryptionKey(newkey);
									bfdst.setBlockHashType(bfsrc.getBlockHashType());
									bfdst.setSeed(digest);	
                                        				if (bfsrc.havePublicKey()) {
										if (bfsrc.isECPublicKey()) 
                                                					bfdst.setPublicKey(bfsrc.getECPublicKey());
										else
                                                					bfdst.setPublicKey(bfsrc.getED25519PublicKey());
									}
                                                			// The new files will have the extension .tmp
                                                			extension.append(new_extension);

                                                			fnpath.replace_extension(extension);
	
									bfdst.open(fnpath);
									if (bfdst.is_open()) {
										bfdst.clone(bfsrc);
										++ total_reencrypted_files_;
									} else {
										++ total_reencrypted_errors_; 
										errormsg << "Can not open:" << fnpath.string();				 
									}
									bfdst.close();
								} else {
									++ total_reencrypted_errors_; 
									errormsg << "Can not open:" << fpath.string();				 
								}
								bfsrc.close();
							} else {
								++ total_reencrypted_errors_; 
								errormsg << "Can not get file size:" << fpath.string();				 
							}
						}	
					} else {
						++ total_reencrypted_errors_; 
						errormsg << "Can not open:" << f.string();				 
					}
					bsrc.close();
					bdst.close();
				} else {
					++ total_reencrypted_errors_; 
					errormsg << "Can not get file size:" << f.string();				 
				}
			}

			// Check if all the files has been cloned correctly and with no errors
			if ((total_reencrypted_files_ == (int32_t)all_files.size())and(total_reencrypted_errors_ == 0)) {

				// If the clonning is correct just rename the original files to .old
				for (auto &f: all_files) {
					std::error_code ec;
					std::filesystem::path newfile(f);
					std::string extension(newfile.extension().c_str());

					extension.append(old_extension);
					newfile.replace_extension(extension);

					std::filesystem::rename(f, newfile, ec);
					if (ec) {
						++ total_reencrypted_errors_; 
						errormsg << "Can not rename file:" << f.string();				 
					}
				}

				// Remove the extension .new to the files
				for (auto & f: all_files) {
					std::error_code ec;
					std::filesystem::path newfile(f);
					std::string extension(newfile.extension().c_str());

					extension.append(new_extension);
					newfile.replace_extension(extension);

					std::filesystem::rename(newfile, f, ec);
					if (ec) {
						++ total_reencrypted_errors_; 
						errormsg << "Can not rename file:" << newfile.string();				 
					}
				}
				if (total_reencrypted_errors_ == 0) {
					std::ostringstream msg;
					msg << "Rotation encryption key successfull, total files re-encrypted " << total_reencrypted_files_;

					PINFO << msg.str();
        				petalus::information_message(msg.str());
				}
			} else {
				std::ostringstream msg;
				msg << "Found errors on the rotation key process, " << errormsg.str();

				PWARN << msg.str();
				petalus::warning_message(msg.str());
			}
		} else {
			std::ostringstream msg;
        		msg << "No available space on the disk";

        		PWARN << msg.str();
			petalus::warning_message(msg.str());
		}
	} else {
        	PWARN << "Encryption password can not be set, check length";
	}
}

} // namespace petalus::blockchain
