/*
 * Petalus a crypto micro service wallet database.
 *
 * Copyright (C) 2018-2022  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */

#include "HTTPAdministrationSession.h"
#include "System.h"
#include "PetalusUriBlock.h"

namespace petalus::http {

std::time_t process_start_time = std::time(nullptr);

// The format of the URI could be:
//    /v1/session/3128EF3B93EF9C1FF25B94ABD4F818A3
//    /v1/session/3128EF3B93EF9C1FF25B94ABD4F818A3/payments
//    /v1/session/3128EF3B93EF9C1FF25B94ABD4F818A3/13
//    /v1/session/3128EF3B93EF9C1FF25B94ABD4F818A3/payments/2

void HTTPAdministrationSession::handle_show_session(bhttp::response<bhttp::dynamic_body> &response) {

        std::ostringstream out;
	std::string uri(request.target());
	std::vector<std::string> uri_items;

	boost::split(uri_items, uri, boost::is_any_of("/"));
	response.set(bhttp::field::content_type, "text/plain");

	if (uri_items.size() > 3) {
		std::string session(uri_items[3]);
		std::string folder = "";
		int32_t block_number = 0;

		if (uri_items.size() > 4) {
			int32_t value = std::atoi(uri_items[4].c_str());
			if (value > 0) {
				block_number = value;
			} else {
				folder = uri_items[4];
				if (uri_items.size() > 5) {
					value = std::atoi(uri_items[5].c_str());
					if (value > 0)
						block_number = value;
				}
			}
		}

		if (boost::beast::string_view accept = request[bhttp::field::accept]; accept.length() > 0) {
                	if (std::size_t found = accept.find("application/json"); found != std::string::npos) {
				nlohmann::json j;
				swm->showSession(j, session, folder, block_number);
				response.set(bhttp::field::content_type, "application/json");
				out << j;
			} else
        			swm->showSession(out, session, folder, block_number);
		} else
        		swm->showSession(out, session, folder, block_number);

	}

	if (out.str().length() > 0) { 
        	boost::beast::ostream(response.body()).write(out.str().c_str(), out.str().length());
        	response.content_length(out.str().length());
        	response.result(bhttp::status::ok);
	} else {
		response.set(bhttp::field::content_type, "text/plain");
        	response.result(bhttp::status::not_found);
	}
}

void HTTPAdministrationSession::handle_show_sessions(bhttp::response<bhttp::dynamic_body> &response) {

        std::ostringstream out;

	response.set(bhttp::field::content_type, "text/plain");

	if (boost::beast::string_view accept = request[bhttp::field::accept]; accept.length() > 0) {
		if (std::size_t found = accept.find("application/json"); found != std::string::npos) {
			nlohmann::json  j;
			swm->showSessions(j);
			out << j;
			response.set(bhttp::field::content_type, "application/json");
		} else 
			swm->showSessions(out); 
	} else
		swm->showSessions(out);

        boost::beast::ostream(response.body()).write(out.str().c_str(), out.str().length());
	response.content_length(out.str().length()); 
	response.result(bhttp::status::ok);
}

// Shows the HTTP statisitics and the ones of the SessionWalletManager in JSON format
void HTTPAdministrationSession::handle_show_statistics(bhttp::response<bhttp::dynamic_body> &response) {

        bool make_plain_text = true;

        std::ostringstream out, name, uptime;

        if (boost::beast::string_view accept = request[bhttp::field::accept]; accept.length() > 0) 
                if (std::size_t found = accept.find("application/json"); found != std::string::npos)
                        make_plain_text = false;

	if (make_plain_text) {
		out << "Statistics summary\n";
		out << *gstats;

		response.set(bhttp::field::content_type, "text/plain");
	} else {
		nlohmann::json  j;

		j["http"]["errors"] = gstats->total_http_errors;
		j["http"]["gets"] = gstats->total_http_method_gets;
		j["http"]["posts"] = gstats->total_http_method_posts;
		j["http"]["unknowns"] = gstats->total_http_method_unknown;
		j["http"]["body_limit_exceeds"] = gstats->total_http_body_limit_exceeds;
		j["http"]["auths"] = gstats->total_http_success_auths;
		j["http"]["fail_auths"] = gstats->total_http_fail_auths;
		
		j["blocks"]["reads"] = gstats->total_blocks_reads;
		j["blocks"]["writes"] = gstats->total_blocks_writes;
		j["blocks"]["not_found"] = gstats->total_blocks_not_found;
		j["blocks"]["write_fails"] = gstats->total_blocks_write_fails;

		j["ssl"]["handshakes"] = gstats->total_ssl_handshakes;
		j["ssl"]["handkshake_errors"] = gstats->total_ssl_handshake_errors;

		out << j;
                response.set(bhttp::field::content_type, "application/json");
	}

        boost::beast::ostream(response.body()).write(out.str().c_str(), out.str().length());
	response.content_length(out.str().length()); 
	response.result(bhttp::status::ok);
}

void HTTPAdministrationSession::handle_show_status(bhttp::response<bhttp::dynamic_body> &response) {

	// compute the elapsed time
	std::time_t elapsed = std::time(nullptr) - process_start_time;
	std::tm *tmb = std::localtime(&elapsed);
	bool make_plain_text = true;
	int days = elapsed/(60 * 60 * 24);

	std::ostringstream out, name, uptime;
        System ss;

        if (boost::beast::string_view accept = request[bhttp::field::accept]; accept.length() > 0) 
                if (std::size_t found = accept.find("application/json"); found != std::string::npos) 
			make_plain_text = false;

	tmb->tm_hour = tmb->tm_hour - 1;
       	name << "PetalusEngine " << VERSION;
       	name << " " << ss.getOSName() << " kernel " << ss.getReleaseName() << " " << ss.getMachineName(); 
	uptime << days << " days " << std::put_time(tmb, "%H:%M:%S"); 

	if (make_plain_text) {
		out << name.str();
		out << " uptime [" << uptime.str() << "]"; 
		out << " reads " << gstats->total_blocks_reads;
		out << " writes " << gstats->total_blocks_writes;
		out << " pid " << getpid() << "\n";
		response.set(bhttp::field::content_type, "text/plain");
	} else {
		nlohmann::json  j;

		j["engine"] = name.str();
		j["uptime"] = uptime.str();
		j["reads"] = gstats->total_blocks_reads;
		j["writes"] = gstats->total_blocks_writes;
		j["pid"] = getpid(); 
			
		out << j;
                response.set(bhttp::field::content_type, "application/json");
	}

        boost::beast::ostream(response.body()).write(out.str().c_str(), out.str().length());
	response.content_length(out.str().length()); 
	response.result(bhttp::status::ok);
}

void HTTPAdministrationSession::handle_get_message(bhttp::response<bhttp::dynamic_body> &response) {

	std::string uri(request.target());

	if (auto it = request.find(boost::beast::string_view(http_session_header_value)); it != request.end()) {
		std::string session(it->value());

		// The session should be the same as the one given on the open operation
		if (sessionid.compare(session) == 0) {
			if (PetalusUriBlock puri(uri); puri.wallet.length() > 0) {
				PetalusBlock block;

				SessionContext ctx(puri.wallet, session);

				if (int32_t bytes = swm->read(ctx, block, puri.folder, puri.block_number); bytes > 0) {

					if (block.type() == BlockType::USR_JSON_STRING)
						response.set(bhttp::field::content_type, "application/json");
					else if (block.type() == BlockType::USR_BINARY_FILE)
						response.set(bhttp::field::content_type, "application/octet-stream");
					else if (block.type() == BlockType::USR_SOURCE_CRYPT) 
						response.set(bhttp::field::content_type, "application/pgp-encrypted");
					else
						response.set(bhttp::field::content_type, "text/plain");

            				boost::beast::ostream(response.body()).write(block.data(), block.length());
					response.content_length(block.length()); 
					response.result(bhttp::status::ok);
					++ gstats->total_blocks_reads;
					++ stats.total_blocks_reads;
				} else {
					std::ostringstream out;
					++ gstats->total_blocks_not_found;
					++ stats.total_blocks_not_found;

					response.result(bhttp::status::not_found);
            				response.set(bhttp::field::content_type, "text/plain");
					out << "Block " << puri.block_number << " not found";
            				boost::beast::ostream(response.body()) << out.str();
					response.content_length(out.str().length());
				}
			} else {
				const constexpr char *message = "Wallet not valid";
				constexpr std::size_t len = std::strlen(message);
				++ gstats->total_wrong_wallet_uri;
				++ stats.total_wrong_wallet_uri;

				response.result(bhttp::status::bad_request);
            			response.set(bhttp::field::content_type, "text/plain");
            			boost::beast::ostream(response.body()) << message;
				response.content_length(len);
			}
        	} else {
			const constexpr char *message = "Session ID not valid for read";
			constexpr std::size_t len = std::strlen(message);
			++ gstats->total_sessions_not_found;
			++ stats.total_sessions_not_found;

			response.result(bhttp::status::not_found);
            		response.set(bhttp::field::content_type, "text/plain");
            		boost::beast::ostream(response.body()) << message;
			response.content_length(len);
		}
	} else {
		// Check if the uri is status or wallets
		if (uri.compare(0, PetalusUris::status.length(), PetalusUris::status.data()) == 0) {
			handle_show_status(response);
		} else if (uri.compare(0, PetalusUris::sessions.length(), PetalusUris::sessions.data()) == 0) {
			handle_show_sessions(response);
		} else if (uri.compare(0, PetalusUris::session.length(), PetalusUris::session.data()) == 0) {
			handle_show_session(response);
		} else if (uri.compare(0, PetalusUris::statistics.length(), PetalusUris::statistics.data()) == 0) {
			handle_show_statistics(response);
		} else {
			++ gstats->total_sessions_not_found;
			++ stats.total_sessions_not_found;

			response.result(bhttp::status::not_found);
            		response.set(bhttp::field::content_type, "text/plain");
            		boost::beast::ostream(response.body()) << "Session ID not valid";
			response.content_length(response.payload_size());
		}
	}

	PINFO << "[" << ipsrc << ":" << portsrc << "] " 
		<< "GET:" << uri
		<< " " << response[bhttp::field::content_type] << " " << response.result_int();	
}


void HTTPAdministrationSession::handle_create_wallet(bhttp::response<bhttp::dynamic_body> &response) {

        if (swm->isReadOnly()) {
                const constexpr char *message = "System in read only mode";
                constexpr std::size_t len = std::strlen(message);

                response.set(bhttp::field::content_type, "text/html");
                response.result(bhttp::status::service_unavailable);
                boost::beast::ostream(response.body()) << message;
                response.content_length(len);
        } else {

		boost::beast::string_view content_type = request[bhttp::field::content_type];

		if (content_type.compare("application/json") == 0) {
			std::ostringstream bodydata;
			bodydata << boost::beast::buffers_to_string(request.body().data());

			nlohmann::json j = nlohmann::json::parse(bodydata.str(), nullptr, false);
			if (j.is_discarded()) {
				const constexpr char *message = "Not valid json content";
				constexpr std::size_t len = std::strlen(message);

				response.set(bhttp::field::content_type, "text/html");
				response.result(bhttp::status::bad_request);
				boost::beast::ostream(response.body()) << message;
				response.content_length(len);

				return;
			}

			// The verification of the json is inside the createWallet of the sesionwallet
			// TODO	
			if ((j.find("wallet") != j.end())and(j["wallet"].is_string())) {
				std::string wallet = j["wallet"];
				std::ostringstream data;
				j.erase("wallet");

				data << j;

				PetalusBlock jblock(data.str(), BlockType::USR_JSON_STRING);

				if (!j.empty()) {
					// Verify that some fields exists also if the json
					// contains folders for create the wallet and so on

					if ((j.find("folders") != j.end())and(j["folders"].is_array())) { // There is a folder definition
						std::vector<std::string> folders = j["folders"];
						
						if ((int)folders.size() > max_folders_per_wallet_) {
							// The current license dont allow to create more than max_folders_per_wallet_

							std::ostringstream out;

                                                        out << "Can not create wallet [Number of folders exceed license]";

                                                        PWARN << "[" << ipsrc << ":" << portsrc << "] "
                                                                        << out.str();

                                                        response.result(bhttp::status::forbidden);
                                                        response.set(bhttp::field::content_type, "text/plain");
                                                        boost::beast::ostream(response.body()) << out.str();
                                                        response.content_length(out.str().length());
							return;
						}
					}

					if (j.find("password") != j.end()) {
						if (j["password"].is_string()) {
							std::string password = j["password"];

							WalletCreateCode code = swm->createWallet(wallet, jblock);

							if (code == WalletCreateCode::WALLET_CREATED_SUCCESS) {
								response.result(bhttp::status::created);
							} else {
								std::ostringstream out;
								
								out << "Can not create wallet [error code:" << static_cast<int>(code) << "]";
							
								PWARN << "[" << ipsrc << ":" << portsrc << "] "
									<< out.str();
									
								response.result(bhttp::status::forbidden);
								response.set(bhttp::field::content_type, "text/plain");
								boost::beast::ostream(response.body()) << out.str(); 
								response.content_length(out.str().length());
							}
						} else {
							const constexpr char *message = "Wrong password type";
							constexpr std::size_t len = std::strlen(message);
							
							PWARN << "[" << ipsrc << ":" << portsrc << "] "
								<< message;

							response.result(bhttp::status::bad_request);
							response.set(bhttp::field::content_type, "text/plain");
							boost::beast::ostream(response.body()) << message;
							response.content_length(len);
						}
					} else {
						const constexpr char *message = "No password provided on the creation of the wallet";
						constexpr std::size_t len = std::strlen(message);

						PWARN << "[" << ipsrc << ":" << portsrc << "] "
							<< message;

						response.result(bhttp::status::bad_request);
						response.set(bhttp::field::content_type, "text/plain");
						boost::beast::ostream(response.body()) << message;
						response.content_length(len);
					}
				} else {
					const constexpr char *message = "No wallet definition";
					constexpr std::size_t len = std::strlen(message);

					PWARN << "[" << ipsrc << ":" << portsrc << "] "
						<< message;

					response.result(bhttp::status::bad_request);
					response.set(bhttp::field::content_type, "text/plain");
					boost::beast::ostream(response.body()) << message;
					response.content_length(len);
				}
			} else {
				const constexpr char *message = "No wallet string on wallet definition";
				constexpr std::size_t len = std::strlen(message);

				PWARN << "[" << ipsrc << ":" << portsrc << "] "
					<< message;

				response.result(bhttp::status::bad_request);
				boost::beast::ostream(response.body()) << message;
				response.content_length(len);
			}
		}
	}
}

/* The wallet can be open by the user or by a external system */

void HTTPAdministrationSession::handle_user_open_wallet(bhttp::response<bhttp::dynamic_body> &response) {
	
	boost::beast::string_view content_type = request[bhttp::field::content_type];

	if (content_type.compare("application/json") == 0) {
		std::ostringstream bodydata;
		bodydata << boost::beast::buffers_to_string(request.body().data());

                nlohmann::json j = nlohmann::json::parse(bodydata.str(), nullptr, false);
                if (j.is_discarded()) {
                        const constexpr char *message = "Not valid json content";
                        constexpr std::size_t len = std::strlen(message);

                        response.set(bhttp::field::content_type, "text/html");
                        response.result(bhttp::status::bad_request);
                        boost::beast::ostream(response.body()) << message;
                        response.content_length(len);

                        return;
                }

        	if (j.find("wallet") != j.end()) {
        		// Both parameters should be strings
                	if (j["wallet"].is_string()) {
                		std::string wallet = j["wallet"];

                        	if (sessionid = swm->openReadOnlySession(wallet); sessionid.length() > 0) {
                        		// send the session id
                               		response.set(http_session_header_value, sessionid);
                               		response.result(bhttp::status::ok);
                        	} else {
					WalletOpenCode code = swm->getOpenStatusCode();
					std::ostringstream out;
                               		response.set(bhttp::field::content_type, "text/html");
					if (code == WalletOpenCode::WALLET_WRONG_CREDENTIALS) {
						out << "Wrong credentials";

                               			response.result(bhttp::status::unauthorized);
					} else if (code == WalletOpenCode::WALLET_WRONG_FORMAT) {
						out << "Wrong wallet format";

                               			response.result(bhttp::status::bad_request);
                               		} else if (code == WalletOpenCode::WALLET_NO_EXISTS) {
                                                out << "Wallet no exists";

                                                response.result(bhttp::status::not_found);
					} else {
						out << "Can not open more sessions";

                                		response.result(bhttp::status::internal_server_error);
                        		}
					out << " [error code:" << (int)code << "]";
                               		boost::beast::ostream(response.body()) << out.str();
					response.content_length(out.str().length());
					PWARN << "[" << ipsrc << ":" << portsrc << "] "
						<< out.str();
				}
			} else {
				const constexpr char *message = "Incorrect parameter types";
				constexpr std::size_t len = std::strlen(message);

				PWARN << "[" << ipsrc << ":" << portsrc << "] "
					<< message;

                		response.set(bhttp::field::content_type, "text/html");
                        	response.result(bhttp::status::bad_request);
                        	boost::beast::ostream(response.body()) << message;
				response.content_length(len);
                	}
		} else {
			const constexpr char *message = "Parameters not found";
			constexpr std::size_t len = std::strlen(message);

			PWARN << "[" << ipsrc << ":" << portsrc << "] "
				<< message;

        		response.set(bhttp::field::content_type, "text/html");
                	response.result(bhttp::status::bad_request);
                	boost::beast::ostream(response.body()) << message;
			response.content_length(len);
        	}
	} else {
		const constexpr char *message = "Incorrect content type for opening the wallet";
		constexpr std::size_t len = std::strlen(message);

		PWARN << "[" << ipsrc << ":" << portsrc << "] "
			<< message;

        	response.set(bhttp::field::content_type, "text/html");
               	response.result(bhttp::status::unsupported_media_type);
               	boost::beast::ostream(response.body()) << message;
		response.content_length(len);
	}
}

void HTTPAdministrationSession::handle_post_message(bhttp::response<bhttp::dynamic_body> &response) {

	std::string uri(request.target());

	// Check if the user or a readonly system wants to open the wallet
	if (uri.compare(0, PetalusUris::open_wallet.length(), PetalusUris::open_wallet.data()) == 0) {
		handle_user_open_wallet(response);
	} else {	
		if (uri.compare(0, PetalusUris::create_wallet.length(), PetalusUris::create_wallet.data()) == 0) {
			handle_create_wallet(response);
		} else {
                	response.result(bhttp::status::not_found);
                       	const constexpr char *message = "Not found";
                       	constexpr std::size_t len = std::strlen(message);
		
                       	response.set(bhttp::field::content_type, "text/plain");
                       	boost::beast::ostream(response.body()) << message;
                       	response.content_length(len);
		}
        }

	PINFO << "[" << ipsrc << ":" << portsrc << "] "
		<< "POST:" << uri
		<< " " << request[bhttp::field::content_type] << " " << response.result_int();	
}

void HTTPAdministrationSession::process_request(bhttp::request<bhttp::dynamic_body>&& req) {

	bhttp::response<bhttp::dynamic_body> response;
	response.version(req.version());
        response.keep_alive(req.keep_alive());
        response.set(bhttp::field::server, "Petalus " VERSION);
	response.content_length(0);

	const char *reason = "No Authorization token found";
	bool is_auth = false; // Mark as false as default ;

	if (boost::beast::string_view auth = request[bhttp::field::authorization]; auth.length() > 0) {
		std::vector<std::string> items;
               	boost::split(items, auth, boost::is_any_of(" "));

		// The header should be on the form of 
		// Authorization: Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ==
               	if (items.size() == 2) {
			if (items[0].compare("Basic") == 0) {
				// decode the token to a string
				std::string decoded;
                                CryptoPP::StringSource(items[1], true, new CryptoPP::Base64Decoder(new CryptoPP::StringSink(decoded)));

				if (std::size_t found = decoded.find(":"); found != std::string::npos) 
					decoded = decoded.substr(found + 1);

				CryptoPP::SecByteBlock key(reinterpret_cast<const uint8_t*>(&decoded[0]), decoded.size());

				if (key == authorization_token_key_) {
					// The system is authorized
					is_auth = true;
					++ gstats->total_http_success_auths;
					++ stats.total_http_success_auths;
				} else {
					reason = "Authorization token failed";
				}
			} else {
				reason = "Only Authorization Basic supported";
			}
               	} else {
			reason = "Wrong Authorization parameters";
		}
	}

	if (!is_auth) {
		++ gstats->total_http_fail_auths;
		++ stats.total_http_fail_auths;
		// The session can not be marked as readonly

		PWARN << "[" << ipsrc << ":" << portsrc << "] "
			<< req.method() << ":" << req.target() << " " 
			<< req[bhttp::field::content_type] << " " << response.result_int()
			<< " " << reason;
       		response.keep_alive(false);
                response.result(bhttp::status::unauthorized);
                response.set(bhttp::field::content_type, "text/plain");
		response.set(bhttp::field::www_authenticate, "Basic");
                boost::beast::ostream(response.body()) << reason;
              	response.content_length(std::strlen(reason));

		return send_response(std::move(response));
	}

        switch(req.method()) {
        	case bhttp::verb::get:
			if (swm) {
	    			handle_get_message(response);
				++ gstats->total_http_method_gets;
				++ stats.total_http_method_gets;
			} else {
                		const constexpr char *message = "No Session Manager available";
                		constexpr std::size_t len = std::strlen(message);

				PWARN << "[" << ipsrc << ":" << portsrc << "] "
					<< message;

        			response.keep_alive(false);
                		response.set(bhttp::field::content_type, "text/html");
                		response.result(bhttp::status::service_unavailable);
                		boost::beast::ostream(response.body()) << message;
                		response.content_length(len);
			}
            		break;
        	case bhttp::verb::post:
			if (swm) {
	    			handle_post_message(response);
				++ gstats->total_http_method_posts;
				++ stats.total_http_method_posts;
			} else {
                		const constexpr char *message = "No Session Manager available";
                		constexpr std::size_t len = std::strlen(message);

				PWARN << "[" << ipsrc << ":" << portsrc << "] "
					<< message;

        			response.keep_alive(false);
                		response.set(bhttp::field::content_type, "text/html");
                		response.result(bhttp::status::service_unavailable);
                		boost::beast::ostream(response.body()) << message;
                		response.content_length(len);
			}
            		break;
        	default:
			++ gstats->total_http_method_unknown;
			++ stats.total_http_method_unknown;
            		response.result(bhttp::status::bad_request);
            		response.set(bhttp::field::content_type, "text/plain");
            		boost::beast::ostream(response.body())
                		<< "Invalid request-method '"
                		<< request.method_string().to_string()
                		<< "'";
            		break;
        }

	return send_response(std::move(response));
}

} // namespace petalus
