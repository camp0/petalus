/*
 * Petalus a crypto micro service wallet database.
 *
 * Copyright (C) 2018-2022  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */

#include "HTTPSession.h"
#include "System.h"

namespace petalus::http {

void HTTPSession::close_session() {

	do_close();
}

void HTTPSession::setSessionWalletManager(const SharedPointer<SessionWalletManager> &sw) { 

	swm = sw;
}

void HTTPSession::setProcessHandler(const std::function <void (bhttp::request<bhttp::dynamic_body>&&)> func) {

	process_session_request = func;
}

const char *HTTPSession::getSocketIPAddress() const {

	if (tls_support_)
		return boost::beast::get_lowest_layer(*ssl_stream_).socket().remote_endpoint().address().to_string().c_str();
	else
		return boost::beast::get_lowest_layer(*tcp_stream_).socket().remote_endpoint().address().to_string().c_str();
}

void HTTPSession::start() { 

	if (tls_support_) {
        	boost::beast::get_lowest_layer(*ssl_stream_).expires_after(std::chrono::seconds(timeout_));

		portsrc = boost::beast::get_lowest_layer(*ssl_stream_).socket().remote_endpoint().port();
		ipsrc = boost::beast::get_lowest_layer(*ssl_stream_).socket().remote_endpoint().address().to_string();

		ssl_stream_->async_handshake(
			boost::asio::ssl::stream_base::server,
            		boost::beast::bind_front_handler(
                		&HTTPSession::handle_handshake,
                		shared_from_this()));
	} else{
		portsrc = boost::beast::get_lowest_layer(*tcp_stream_).socket().remote_endpoint().port();
		ipsrc = boost::beast::get_lowest_layer(*tcp_stream_).socket().remote_endpoint().address().to_string();

		do_read();
	}
}

void HTTPSession::do_read() {

        // Make the request empty before reading,
        // otherwise the operation behavior is undefined.
        request = {};

	if (tls_support_) {
		boost::beast::get_lowest_layer(*ssl_stream_).expires_after(std::chrono::seconds(timeout_));

		boost::beast::http::async_read(*ssl_stream_, buffer_, request,
			boost::beast::bind_front_handler(
				&HTTPSession::handle_read,
				shared_from_this()));
	} else {
		boost::beast::get_lowest_layer(*tcp_stream_).expires_after(std::chrono::seconds(timeout_));

		boost::beast::http::async_read(*tcp_stream_, buffer_, request,
			boost::beast::bind_front_handler(
				&HTTPSession::handle_read,
				shared_from_this()));
	}
}

void HTTPSession::handle_read(boost::beast::error_code ec, std::size_t bytes_transferred) {

        boost::ignore_unused(bytes_transferred);

	total_bytes_ += bytes_transferred;

        // This means the client closed the connection
        if (ec == bhttp::error::end_of_stream ) {
            	return do_close();
	}

	// The client do a reset disconnect
	if ((boost::asio::error::eof == ec) || (boost::asio::error::connection_reset == ec) ||
		(ec == boost::asio::ssl::error::stream_truncated)) {
		return;
	} 

	if (ec == bhttp::error::body_limit) {
		++ gstats->total_http_body_limit_exceeds;
		++ stats.total_http_body_limit_exceeds;

		PWARN << "[" << ipsrc << ":" << portsrc << "] "
			<< ec.message()  << " bytes:" << request.body().size() << " " << buffer_.size();
		return;
	}

        if (ec) {
        	++ gstats->total_http_errors;
        	++ stats.total_http_errors;
	} else {
		++ total_requests_;
		last_time_ = std::time(nullptr); 
		process_session_request(std::move(request));
	}
}


void HTTPSession::handle_write(bool close, boost::beast::error_code ec, std::size_t bytes_transferred) {

        boost::ignore_unused(bytes_transferred);

	total_bytes_ += bytes_transferred;
	++ total_responses_;

        if (ec) {
		// std::cout << "Error on write:" << ec.message() << "\n";
		return;
	}

        if (close) {
            	// This means we should close the connection, usually because
            	// the response indicated the "Connection: close" semantic.
            	return do_close();
        }

        // We're done with the response so delete it
        res_ = nullptr;

        // Read another request
        do_read();
}

void HTTPSession::do_close() {

	if (tls_support_) {
		boost::beast::get_lowest_layer(*ssl_stream_).expires_after(std::chrono::seconds(timeout_));

		ssl_stream_->async_shutdown(
			boost::beast::bind_front_handler(
				&HTTPSession::handle_shutdown,
				shared_from_this()));
	} else {
		boost::beast::error_code ec;
		tcp_stream_->socket().shutdown(tcp::socket::shutdown_send, ec);
	}

}

void HTTPSession::handle_shutdown(boost::beast::error_code ec) {

	if (ec == boost::asio::ssl::error::stream_truncated) {
		// Ignore this error the client dont do a ssl shutdown properly
		// std::cout << "Error on shutdown:" << ec.message() << " socketstats:" << socket_.is_open() << std::endl;
	} else if (ec) {
		// std::cout << "Error on shutdown:" << ec.message() << " socketstats:" << socket_.is_open() << std::endl;
		return;
	}
        // At this point the connection is closed gracefully
	//std::cout << "connection and socket closed:" << socket_.is_open() << std::endl;
}

void HTTPSession::handle_handshake(boost::beast::error_code ec) {

        if (ec) {
		PWARN << "[" << ipsrc << ":" << portsrc << "] "
			<< "handshake:" << ec.message();

		++ gstats->total_ssl_handshake_errors;
		++ stats.total_ssl_handshake_errors;
		return;
	}

	++ gstats->total_ssl_handshakes;
	++ stats.total_ssl_handshakes;

	do_read();
}

} // namespace petalus::http
