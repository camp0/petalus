/*
 * Petalus a crypto micro service wallet database.
 *
 * Copyright (C) 2018-2022  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */

#ifndef SRC_INTERPRETER_H_
#define SRC_INTERPRETER_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>
#include <boost/asio.hpp>
#include <boost/bind/bind.hpp>
#include <chrono>
#include <fstream>

#if defined(PYTHON_BINDING)
#include <boost/python.hpp>
#include "PythonGilContext.h"
#endif
#include "OutputManager.h"
#include "Message.h"

namespace petalus {

#if defined(PYTHON_BINDING)

class Interpreter {
public:
        explicit Interpreter(boost::asio::io_context &io):Interpreter(io, STDIN_FILENO) {}
        explicit Interpreter(boost::asio::io_context &io, int fd);
        virtual ~Interpreter() { user_input_.close(); }

        static const int MaxInputBuffer = 160;
        static constexpr const char* Prompt = ">>> ";
        static constexpr const char* CodePrompt = "... ";

        void start();
        void stop();
        void readUserInput();

        void setLogUserCommands(bool enable);
        bool getLogUserCommands() const { return log_file_.is_open(); }

        void setShell(bool enable);
        bool getShell() const { return shell_enable_; }

        void executeRemoteCommand(const std::string &cmd) { execute_command(cmd); }

private:
        void execute_user_command(const std::string& cmd);
        void execute_command(const std::string& cmd);
        void handle_read_user_input(boost::system::error_code error);

        int fd_ = 0;
        boost::asio::posix::stream_descriptor user_input_;
        boost::asio::streambuf user_input_buffer_;
        bool shell_enable_ = false;
        bool want_exit_ = false;
        bool in_code_block_ = false;
        char *current_prompt_ = const_cast<char*>(Prompt);
        std::string cmd_ = "";
        std::ofstream log_file_;
};

#endif // PYTHON_BINDING

} // namespace petalus 

#endif // SRC_INTERPRETER_H_ 
