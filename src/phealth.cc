/*
 * Petalus a crypto micro service wallet database.
 *
 * Copyright (C) 2018-2022  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <execinfo.h>
#include <unistd.h>
#include <fstream>
#include <iostream>
#include <functional>
#include <cctype>
#include <csignal>
#include <boost/program_options.hpp>
#include <fstream>
#include <boost/date_time/posix_time/posix_time_io.hpp>
#include "BlockTypes.h"
#include "PetalusBlock.h"
#include "BlockFile.h"
#include "Message.h"

#define BINARY_NAME "phealth"

using namespace petalus;
using namespace petalus::blockchain;

std::string data_directory = "";
std::string wallet_name = "";
std::string password = "";
int verbose_level = 0;

// Stats values
int32_t total_locks = 0;
int32_t total_blocks = 0;
int64_t total_bytes = 0;
int32_t total_verified_blocks = 0;
int32_t total_valid_signature_blocks = 0;
int32_t total_invalid_signature_blocks = 0;

int32_t file_total_verified_blocks = 0;
int64_t file_total_bytes = 0;


void analyzeBlockFile(const BlockFile &bfile, int from, int32_t &locks, int32_t &blocks, int32_t &blocks_ok,
	int32_t &sig_blocks_valid, int32_t &sig_blocks_invalid) {

	if (bfile.is_lock()) {
		++ locks;
	} else {

		for (int i = from; i <= bfile.getTotalBlocks(); ++i) {
			++ blocks;
			if (bool value = bfile.verifyBlock(i); value)
				++ blocks_ok;
			if (const petalus_block_file_v1 *rblock = bfile.findBlock(i); rblock) {
				if (rblock->signature_length > 0) {
					if (bfile.verifySignatureBlock(i))
						++ sig_blocks_valid;
					else
						++ sig_blocks_invalid;
				}
			}
		}
                file_total_bytes += bfile.getTotalBytes();
	}
}

int main(int argc, char* argv[]) {

	namespace po = boost::program_options;
	po::variables_map var_map;

	po::options_description mandatory_ops("Mandatory arguments");
	mandatory_ops.add_options()
                ("data,d",    po::value<std::string>(&data_directory)->default_value("./petalus_data/"),
                        "Sets the data directory.")
        	;

        po::options_description optional_ops("Optional arguments");
        optional_ops.add_options()
                ("wallet,w",    po::value<std::string>(&wallet_name),
                        "Sets the wallet name for analyze.")
                ("password,p",    po::value<std::string>(&password),
                        "Sets the encryption password for validated signed blocks.")
                ("verbose,v",        po::value<int>(&verbose_level)->default_value(0),
                                        "Verbose level.")
                ;

	mandatory_ops.add(optional_ops);

	try {
	
        	po::store(po::parse_command_line(argc, argv, mandatory_ops), var_map);

        	if (var_map.count("help")) {
            		std::cout << PACKAGE " " BINARY_NAME " " VERSION << std::endl;
            		std::cout << mandatory_ops << std::endl;
			std::cout << std::endl;
			//show_gpl_banner();
            		exit(0);
        	}
        	if (var_map.count("version")) {
            		std::cout << PACKAGE " " BINARY_NAME " " VERSION << std::endl;
			std::cout << std::endl;
			// show_gpl_banner();
			std::cout << std::endl;
			//show_package_banner();
            		exit(0);
        	}

        	po::notify(var_map);
    	
	} catch(boost::program_options::required_option& e) {
            	std::cout << PACKAGE " " BINARY_NAME " " VERSION << std::endl;
        	std::cerr << "Error: " << e.what() << std::endl;
		std::cout << mandatory_ops << std::endl;
        	exit(-1);
	} catch(std::exception& e) {
            	std::cout << PACKAGE " " BINARY_NAME " " VERSION << std::endl;
        	std::cerr << "Unsupported option." << e.what() << std::endl;
		std::cout << mandatory_ops << std::endl;
        	exit(-2);
    	}


	std::filesystem::path path(data_directory);

	std::set<std::string> wallets;

	if (wallet_name.length() > 0) { // the user has choose to analyze a wallet
		wallets.insert(wallet_name);
	} else {
		// Trasverse the data directory and get the block files
		for (std::filesystem::recursive_directory_iterator it(path);
			it != std::filesystem::recursive_directory_iterator(); ++it) {

			if (std::filesystem::is_regular_file(*it)and((it->path().extension() == ".mblk0000"))) {
				std::string name(it->path().filename().string());
				std::size_t found = name.find(".");

				std::string wallet_name = name.substr(0, found);
				wallets.insert(wallet_name);
			}
		}
        }

	EncryptionKey ekey;
 
	if (password.length() > 0) { // The user set some password
		if (ekey.password(password) == false) {
                       	std::cout << "Encryption password can not be set, check length" << std::endl;
			exit(-1);
		}
	}

	std::cout << "Analyzing " << wallets.size() << " wallets\n";

	for(auto &w: wallets) {
        	if (w.find_first_not_of("0123456789abcdefABCDEF") == std::string::npos) {
                	std::filesystem::path fpath(path);

                	fpath /= w.substr(0, 1);
                	fpath /= w.substr(1, 1);

                	fpath /= w + std::string(".mblk0000");;

			BlockFile bfile(0);

			bfile.open(fpath, boost::iostreams::mapped_file_base::mapmode::readonly);

			if (verbose_level > 0)
				std::cout << "Analyzing " << fpath << std::endl;

			if (!ekey.key().empty())
				bfile.setEncryptionKey(ekey);

			int32_t blocks = 0;
			file_total_bytes = 0;
			int32_t valid_signature_blocks = 0;
			int32_t invalid_signature_blocks = 0;
			int32_t verified_blocks = 0;

			analyzeBlockFile(bfile, 1, total_locks, blocks, verified_blocks, valid_signature_blocks,
				invalid_signature_blocks);

			for (auto &it: bfile.getFolders()) {
                		std::filesystem::path folder_path = bfile.getPath();

                		int32_t block_number = it.second.first;
                		folder_path.remove_filename();
                		folder_path /= it.second.second;

                		auto bfilef = BlockFile(0);

				if (bfile.havePublicKey())
					if (bfile.isECPublicKey())
                                		bfilef.setPublicKey(bfile.getECPublicKey());

                		bfilef.open(folder_path, boost::iostreams::mapped_file_base::mapmode::readonly);
					
				if (verbose_level > 0)
					std::cout << "Analyzing folder " << folder_path << std::endl;

				if (!ekey.key().empty())
					bfilef.setEncryptionKey(ekey);

				// TODO move the public key and the singature length also to the folder files

                		// The first block should be verified with the parent block
                		uint8_t digest[HASH_BLOCK_SIZE];

                		bfile.readBlockHash(block_number, digest);// The definition of the folder block is on the first block

				++ blocks;
				bool value = bfilef.verifyBlock(1, digest);
				if (value)
					++ file_total_verified_blocks;

				analyzeBlockFile(bfilef, 2, total_locks, blocks, verified_blocks, valid_signature_blocks,
					invalid_signature_blocks);
                		bfilef.close();
			}

			if (verbose_level > 1) {
        			std::cout << "\t" << "Blocks:                 " << std::setw(10) << blocks << "\n";
        			std::cout << "\t" << "Bytes:                  " << std::setw(10) << file_total_bytes << "\n";
        			std::cout << "\t" << "Valid blocks:           " << std::setw(10) << verified_blocks << "\n";
        			std::cout << "\t" << "Invalid blocks:         " << std::setw(10) << blocks - verified_blocks << "\n";
       				std::cout << "\t" << "Signed blocks:          " << std::setw(10) << valid_signature_blocks + invalid_signature_blocks << "\n";
        			std::cout << "\t" << "Valid signed blocks:    " << std::setw(10) << valid_signature_blocks << "\n";
        			std::cout << "\t" << "Invalid signed blocks:  " << std::setw(10) << invalid_signature_blocks << "\n";
			}

			total_verified_blocks += verified_blocks;
			total_blocks += blocks;
			total_bytes += file_total_bytes;
			total_valid_signature_blocks += valid_signature_blocks;
			total_invalid_signature_blocks += invalid_signature_blocks ;

			bfile.close();
		}
	}

	std::cout << "Summary\n";
        std::cout << "\t" << "Locks:                  " << std::setw(10) << total_locks << "\n";
        std::cout << "\t" << "Blocks:                 " << std::setw(10) << total_blocks << "\n";
        std::cout << "\t" << "Bytes:                  " << std::setw(10) << total_bytes << "\n";
        std::cout << "\t" << "Valid blocks:           " << std::setw(10) << total_verified_blocks << "\n";
        std::cout << "\t" << "Invalid blocks:         " << std::setw(10) << total_blocks - total_verified_blocks << "\n";
        std::cout << "\t" << "Signed blocks:          " << std::setw(10) << total_valid_signature_blocks + total_invalid_signature_blocks << "\n";
        std::cout << "\t" << "Valid signed blocks:    " << std::setw(10) << total_valid_signature_blocks << "\n";
        std::cout << "\t" << "Invalid signed blocks:  " << std::setw(10) << total_invalid_signature_blocks << "\n";

	return 0;
}

