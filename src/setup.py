import os
import sys
import distutils.sysconfig
from setuptools import setup, Extension, Command

src_files =  ["PetalusBlock.cc", "BlockFile.cc" ]
src_files += ["Wallet.cc", "SessionWalletManager.cc", "EncryptionKey.cc", "Utils.cc"]
src_files += ["Callback.cc", "Logger.cc", "TimerManager.cc", "Interpreter.cc", "System.cc"]
src_files += ["HTTPSession.cc", "HTTPAdministrationSession.cc", "HTTPUserSession.cc" ]
src_files += ["Server.cc", "python_wrapper.cc" ]

def isUbuntuBaseDistro():
    try:
        os.stat("/etc/debian_version")
        return True
    except:
        return False

""" Copy from https://stackoverflow.com/questions/377017/test-if-executable-exists-in-python """
def which(program):
    import os
    def is_exe(fpath):
        return os.path.isfile(fpath) and os.access(fpath, os.X_OK)

    fpath, fname = os.path.split(program)
    if fpath:
        if is_exe(program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return exe_file

    return None

def setup_compiler ():
    distutils.sysconfig.get_config_vars()
    config_vars = distutils.sysconfig._config_vars

    includes = list()
    macros = list()

    macros.append(('BINDING','1'))
    macros.append(('PYTHON_BINDING','1'))
    macros.append(('HAVE_CONFIG_H','1'))
    includes.append(".")
    includes.append("..")

    print(sys.platform)
    if (sys.platform == 'sunos5'):
        config_vars['LDSHARED'] = "gcc -G"
        config_vars['CCSHARED'] = ""
    elif ('freebsd1' in sys.platform):
        os.environ["CC"] = "c++"
        includes.append("/usr/local/include")
        macros.append(('__FREEBSD__','1'))
    elif (sys.platform == 'openbsd5'):
        macros.append(('__OPENBSD__','1'))
        os.environ["CC"] = "eg++"
    elif (sys.platform == 'darwin'):
        macros.append(('__DARWIN__','1'))
        os.environ["CC"] = "g++"
    else:
        os.environ["CC"] = "g++"
        os.environ["CXX"] = "g++"

    ccache = which("ccache")
    if (ccache):
        os.environ["CC"] = ccache + " " + os.environ["CC"]

    return includes,macros

petalus_module = Extension("petalus",
    sources = src_files,
    libraries = ["boost_system", "boost_iostreams", "stdc++fs", "cryptopp", "ssl", "boost_log"],
    define_macros = [],
    extra_compile_args = ["-O3","-Wreorder","-std=c++17","-lpthread","-lstdc++"],
    )

if __name__ == "__main__":

    py_major = sys.version_info.major
    py_minor = sys.version_info.minor

    boost_python_lib = ""

    if (py_major > 2):
        """ Ubuntu loves to change names of the libs """
        if (isUbuntuBaseDistro()):
            boost_python_lib = "boost_python-py" + str(py_major) + str(py_minor)
        else:
            boost_python_lib = "boost_python39"
    else:
        boost_python_lib = "boost_python"

    # long_desc = long_description

    if (len(sys.argv) > 1):
        if (sys.argv[1].startswith("build_ext")):
            includes, macros = setup_compiler()

            print("Compiling petalus extension for %s" % sys.platform)
            print("\tOS name %s" % (os.name))
            print("\tArchitecture %s" % os.uname()[4])
            print("\tBoost python lib %s" % boost_python_lib)

            petalus_module.include_dirs = includes
            petalus_module.define_macros = macros
            petalus_module.libraries.append(boost_python_lib)

    """ use this for production library """
    # petalus_module.extra_compile_args.append("-fvisibility=hidden")

    setup(name="petalus",
        version = "1.0.0",
        author = "Luis Campo Giralte",
        author_email = "luis.camp0.2009@gmail.com",
        url = "https://bitbucket.org/camp0/petalus",
        license = "GPLv2",
        package_dir = {'': '.'},
        description = "Petalus a Blockchain micro service wallet database",
        long_description = "bubu" ,
        ext_modules = [petalus_module],
        py_modules = ["petalus"],
        keywords = ["security", "blockchain", "databases"],
        classifiers=[
            "Development Status :: 5 - Production/Stable",
            "Environment :: Console",
            "Intended Audience :: Information Technology",
            "Intended Audience :: Science/Research",
            "Intended Audience :: System Administrators",
            "Intended Audience :: Telecommunications Industry",
            "Intended Audience :: Developers",
            "License :: OSI Approved :: GNU General Public License v2 (GPLv2)",
            "Operating System :: POSIX :: BSD :: FreeBSD",
            "Operating System :: POSIX :: Linux",
            "Programming Language :: C++",
            "Programming Language :: Python :: 2.7",
            "Programming Language :: Python :: 3.3",
            "Programming Language :: Python :: 3.4",
            "Programming Language :: Python :: 3.5",
            "Programming Language :: Python :: 3.6",
            "Topic :: Internet",
            "Topic :: Scientific/Engineering :: Information Analysis",
            "Topic :: Security",
            "Topic :: System :: Networking",
            "Topic :: System :: Networking :: Monitoring",
          ],
    )

