/*
 * Petalus a crypto micro service wallet database.
 *
 * Copyright (C) 2018-2022  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */

#include "Callback.h"

namespace petalus {

#if defined(PYTHON_BINDING)

const char *Callback::getName() const {
        static char buffer[256] = { "Can not retrieve callback name" };

        PyObject *obj = PyObject_Repr(callback_);

        const char *name = reinterpret_cast<const char*>(&buffer[0]);

#if PY_MAJOR_VERSION < 3
        strncpy(buffer, PyString_AsString(obj), 255);
#else
        PyObject * temp_bytes = PyUnicode_AsEncodedString(obj, "ASCII", "strict");
        if (temp_bytes != NULL) {
                strncpy(buffer, PyBytes_AS_STRING(temp_bytes), 255);

                Py_XDECREF(temp_bytes);
        }
#endif
        Py_XDECREF(obj);
        return name;
}

void Callback::setCallbackWithNoArgs(PyObject *callback) {

        // the user unset the callback to none
        if ((callback == Py_None) or (callback == nullptr)) {
                if (callback_) {
                        Py_XDECREF(callback_);
                        callback_ = nullptr;
                        callback_set_ = false;
                }
                return;
        }

        if (!PyCallable_Check(callback)) {
                throw std::runtime_error("Object is not callable.\n");
        } else {
                if ( callback_ ) Py_XDECREF(callback_);
                callback_ = callback;
                Py_XINCREF(callback_);
                callback_set_ = true;
        }
}
void Callback::setCallback(PyObject *callback) {

        // the user unset the callback to none
        if (callback == Py_None) {
                if (callback_) {
                        Py_XDECREF(callback_);
                        callback_ = nullptr;
                        callback_set_ = false;
                }
                return;
        }

        if (!PyCallable_Check(callback)) {
                throw std::runtime_error("Object is not callable.\n");
        } else {
                int args = 0;
#if PY_MAJOR_VERSION < 3
                PyObject *fc = PyObject_GetAttrString(callback, "func_code");
#else
                PyObject *fc = PyObject_GetAttrString(callback, "__code__");
#endif
                if (fc) {
                        PyObject* ac = PyObject_GetAttrString(fc, "co_argcount");
                        if (ac) {
#if PY_MAJOR_VERSION < 3
                                args = PyInt_AsLong(ac);
#else
                                args = PyLong_AsLong(ac);
#endif
                        }
                        Py_XDECREF(ac);
                }
                Py_XDECREF(fc);

                if (args != 1) {
                        throw std::runtime_error("Object should have one parameter.\n");
                } else {
                        if ( callback_ ) Py_XDECREF(callback_);
                        callback_ = callback;
                        Py_XINCREF(callback_);
                        callback_set_ = true;
                }
        }
}

Callback::~Callback() {

        if ((callback_set_)and(callback_ != Py_None)) {
                Py_XDECREF(callback_);
                callback_ = nullptr;
        }
}

/*
void Callback::executeCallback(blockchain::Wallet *wallet) {

        try {
                PythonGilContext gil_lock();

                boost::python::call<void>(callback_, boost::python::ptr(wallet));
        } catch (std::exception &e) {
                std::cout << __FILE__ << ":" << __func__ << ":ERROR:" << e.what() << std::endl;
        }
}
*/

void Callback::executeCallback() {

        try {
                PythonGilContext gil_lock();

                boost::python::call<void>(callback_);
        } catch (std::exception &e) {
                std::cout << __FILE__ << ":" << __func__ << ":ERROR:" << e.what() << std::endl;
        }
}

#endif

} // namespace petalus
