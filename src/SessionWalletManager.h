/*
 * Petalus a crypto micro service wallet database.
 *
 * Copyright (C) 2018-2022  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */

#ifndef SRC_SESSIONWALLETMANAGER_H_
#define SRC_SESSIONWALLETMANAGER_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>
#include <unordered_map>
#include <utility>      // std::pair
#include <iomanip>
#include <filesystem>
#include <boost/multi_index_container.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/mem_fun.hpp>
#if defined(PYTHON_BINDING)
#include <boost/python.hpp>
#include "OutputManager.h"
#endif
#include "Color.h"
#include "Pointer.h"
#include "Wallet.h"
#include "Cache.h"
#include "PetalusBlock.h"
#include "Logger.h"
#include "SessionContext.h"
#include "Utils.h"

namespace petalus::blockchain {

struct session_table_tag_unique;
struct session_table_tag_duration;

typedef boost::multi_index::multi_index_container<
        SharedPointer<Wallet>,
        boost::multi_index::indexed_by<
                boost::multi_index::hashed_unique<
                        boost::multi_index::tag<session_table_tag_unique>,
                        boost::multi_index::const_mem_fun<Wallet, std::string, &Wallet::getSessionId>
                >,
                boost::multi_index::ordered_non_unique<
                        boost::multi_index::tag<session_table_tag_duration>,
                        boost::multi_index::const_mem_fun<Wallet, int, &Wallet::getLastTimeOperation>,
                        std::less<int> // The multiset is order by the most recent activity on the wallet !!! 
                >
        >
>SessionTable;

typedef SessionTable::nth_index<0>::type SessionByID;
typedef SessionTable::nth_index<1>::type SessionByDuration;

// Wallet creation return codes
enum class WalletCreateCode : int {
	WALLET_CREATED_SUCCESS =	0,
	WALLET_WRONG_FORMAT =		1,
	WALLET_EXISTS =     		2,
	WALLET_NOT_AVAILABLE = 		3,
	WALLET_READ_ONLY =		4,
	WALLET_WRONG_PUBLIC_KEY = 	5
};

// Wallet open session codes
enum class WalletOpenCode : int {
	WALLET_OPEN_SUCCESS = 		0,
	WALLET_NO_RESOURCES =		1,
	WALLET_WRONG_FORMAT =		2,
	WALLET_NO_EXISTS =		3,
	WALLET_WRONG_CREDENTIALS =	4,
	WALLET_LOCK =			5
};

// Write block replication codes
enum class ReplicationBlockWriteCode : int {
	BLOCK_WRITTEN = 		0,
	BLOCK_HASH_INVALID =		1,
	WALLET_LOCK = 			2,
	WALLET_NO_RESOURCES = 		3
};


// This class controls the access to the information for read and write
// manage the wallets basically
class SessionWalletManager {
public:

	explicit SessionWalletManager() {}
	explicit SessionWalletManager(const std::filesystem::path &path):path_(path) {}
	virtual ~SessionWalletManager() { close(); }

	void statistics(std::basic_ostream<char> &out) const;

	void setDataDirectory(const std::filesystem::path &path);
	const char *getDataDirectory() const { return path_.c_str(); }

	void setMasterBlockFilePassword(const std::string &password);
	const char *getMasterBlockFilePassword() const { return master_blockfile_password_.c_str(); }

	void setMasterBlockFile(const std::filesystem::path &master_block_file);
	const char *getMasterBlockFile() const { return master_block_file_.string().c_str(); }

	void setReadOnly(bool value);
	bool isReadOnly() const { return read_only_; }

	int32_t write(const SessionContext &ctx, PetalusBlock &block) { return write(ctx, block, ""); }
	int32_t write(const SessionContext &ctx, PetalusBlock &block, const std::string &folder);

	int32_t read(const SessionContext &ctx, PetalusBlock &block, int32_t number) { return read(ctx, block, "", number); }
	int32_t read(const SessionContext &ctx, PetalusBlock &block, const std::string &folder, int32_t number);

	void showSessions(nlohmann::json &j);
	void showSessions(std::basic_ostream<char> &out);
	void showSession(std::basic_ostream<char> &out, const std::string &session) const;
	void showSession(std::basic_ostream<char> &out, const std::string &session, int32_t number) const;
	void showSession(std::basic_ostream<char> &out, const std::string &session, const std::string &folder, int32_t number) const;
	void showSession(nlohmann::json &out, const std::string &session, const std::string &folder, int32_t number) const;

	int32_t getTotalBlocks(const std::string &session) const;
	int32_t getLastBlockNumberWritten(const std::string &session) const ;

	bool close(const std::string &session);
	void close();

	// Memory methods related to the alloc/deloc of the wallets
	void increaseAllocatedMemory(int value);
        void decreaseAllocatedMemory(int value);

        int64_t getCurrentUseMemory() const;
        int64_t getAllocatedMemory() const;
        int64_t getTotalAllocatedMemory() const;

        void setDynamicAllocatedMemory(bool value);
        bool isDynamicAllocatedMemory() const;

        int32_t getTotalCacheMisses() const;

	WalletCreateCode createWallet(const std::string &wallet, PetalusBlock &block);

	std::string openSession(const std::string &wallet, const std::string &password) { return open_user_session(wallet, password); }
	std::string openReadOnlySession(const std::string &wallet) { return open_readonly_session(wallet); }
	const WalletOpenCode getOpenStatusCode() const { return wallet_open_code_; } 

	void setMaxBlockFileSize(int32_t max_size);
	int32_t getMaxBlockFileSize() const { return max_blockfile_size_; }

	// sets/gets of number of bytes that will be used as warning for the manager
	// enters on readonly mode. This will prevents issues with disks that are getting full
	// and we dont have space for write the blocks.
	void setReadOnlySafeGuardBytes(int32_t value) { read_only_safe_guard_ = value; }
	int32_t getReadOnlySafeGuardBytes() const { return read_only_safe_guard_; }

#if defined(PYTHON_BINDING)
	void setDataDirectory(const std::string &path) { setDataDirectory(std::filesystem::path(path)); }
	void setMasterBlockFile(const std::string &path) { setMasterBlockFile(std::filesystem::path(path)); }

	void show() const { statistics(std::cout); }
	void showSessions(); 
	void showSession(const std::string &session) const;
	void showSession(const std::string &session, int32_t) const;
	void showTriggers() const;
	void showTriggers(std::basic_ostream<char> &out) const;

	void addReadTrigger(const std::string &wallet, PyObject *callback);
	void addWriteTrigger(const std::string &wallet, PyObject *callback);

	int getMaxCacheSessions() const { return wallet_cache_->getTotal(); }
	void setMaxCacheSessions(int value);

	void closeSessions();

	void PyRotateEncryptionKey(const std::string &password);

#endif

	// Sets or gets the encryption password for all the wallets and blockfiles
	void setEncryptionPassword(const std::string &password);
	const char *getEncryptionPassword() const { return password_.c_str(); }

        // Sets and gets the values of the blockfiles
        void setBlockHashType(const std::string &name);
        const char *getBlockHashType() const;

#if defined(STAND_ALONE_TEST) || defined(TESTING)

	void updateSessionTime(const std::string &session, std::time_t time) { update_session_time(session, time); } 
	void createDirectoryStructure(const std::string &path) { create_data_structure(path); }

	int32_t getTotalCreatedWallets() const { return total_created_wallets_; }
	int32_t getTotalOpenWallets() const { return total_open_wallets_; }
	int32_t getTotalFailOpenWallets() const { return total_fail_open_wallets_; }
	int32_t getTotalWrongCredentials() const { return total_wrong_credentials_; }
	int32_t getTotalCloseWallets() const { return total_close_wallets_; }
	int32_t getTotalTimeoutWallets() const { return total_timeout_wallets_; }
	int32_t getTotalWallets() const { return sessions_.size(); }
	int32_t getTotalBytesReads() const { return total_bytes_reads_; }
	int32_t getTotalBytesWrites() const { return total_bytes_writes_; }
	int64_t getTotalWrites() const { return total_writes_; }
	int32_t getTotalReads() const { return total_reads_; }
	int64_t getTotalErrorWrites() const { return total_error_writes_; }
	int32_t getTotalReuseWallets() const { return total_reuse_wallets_; }
	int32_t getTotalSessionMismatchs() const { return total_session_mismatchs_; }
	int64_t getTotalBytesAvailable() const { return total_bytes_available_; }
	
	SharedPointer<Wallet> getWallet(const std::string &session) const { return get_wallet_from_session(session); }
	Cache<Wallet>::CachePtr getWalletCache() const { return wallet_cache_; }

	EncryptionKey &getEncryptionKey() { return key_; }

	int32_t getTotalReencryptedFiles() const { return total_reencrypted_files_; }
	int32_t getTotalReencryptedErrors() const { return total_reencrypted_errors_; }
	int64_t computeDataSize() const { return compute_data_size(); }

	bool verifyDirectoryStructure(const std::string &path) const { return verify_data_structure(path); }

	SharedPointer<BlockFile> getMasterBlock() const { return master_file_; }

#endif
	friend std::ostream& operator<< (std::ostream &out, const SessionWalletManager &bf);

	void setSessionTimeout(int timeout) { session_timeout_ = timeout; }
	int getSessionTimeout() const { return session_timeout_; }

	void updateTimers(std::time_t current_time);

	void rotateEncryptionKey(const std::string &password);

	// For the replication part
	ReplicationBlockWriteCode writeBlockOnDisk(const std::string &wallet, int32_t number, PetalusBlock &block, uint8_t prev_digest[], const std::string &folder);
	void readBlockDataHash(const SessionContext &ctx , int32_t number, uint8_t prev_digest[], const std::string &folder);

	int computeTotalNumberOfWalletsOnDisk() const;


private:
	SharedPointer<Wallet> get_wallet(const SessionContext &ctx);
	SharedPointer<Wallet> get_wallet_from_session(const std::string &session) const;

	int32_t write(const SharedPointer<Wallet> &wa, const SessionContext &ctx, PetalusBlock &block);
	int32_t write(const SharedPointer<Wallet> &wa, const SessionContext &ctx, PetalusBlock &block, const std::string &folder);

	void create_data_structure(const std::filesystem::path &path);
	bool verify_data_structure(const std::filesystem::path &path) const;
	WalletCreateCode create_wallet(const std::string &wallet, PetalusBlock &block);
	WalletCreateCode create_wallet(const SharedPointer<Wallet> &wa, const std::filesystem::path path, PetalusBlock &block, const std::string &name);
	std::string open_user_session(const std::string &wallet, const std::string &password);
	std::string open_readonly_session(const std::string &wallet);
	void update_session_time(const std::string &session, std::time_t time);
	void update_available_space();
	int64_t compute_data_size() const;
	bool set_encryption_password_key(const std::string &password, EncryptionKey &key);
	void update_master_blockfile(const std::string &wallet, uint8_t seed[]);

	std::filesystem::path path_ {};
	std::filesystem::path master_block_file_ {};
	int64_t total_bytes_available_ = 0;
	int64_t total_writes_ = 0;
	int64_t total_error_writes_ = 0;
	int64_t total_reads_ = 0;
	int64_t total_bytes_reads_ = 0;
	int64_t total_bytes_writes_ = 0;
	int32_t total_created_wallets_ = 0;
	int32_t total_open_wallets_ = 0;
	int32_t total_reuse_wallets_ = 0;
	int32_t total_close_wallets_ = 0;
	int32_t total_fail_open_wallets_ = 0;
	int32_t total_wrong_credentials_ = 0;
	int32_t total_lock_wallets_ = 0;
	int32_t total_timeout_wallets_ = 0;
	int32_t total_session_mismatchs_ = 0;
	int32_t total_reencrypted_files_ = 0;
	int32_t total_reencrypted_errors_ = 0;
	int32_t total_replicated_blocks_ = 0;
	int64_t total_replicated_bytes_ = 0;
	int32_t max_blockfile_size_ = 1024 * 16 * boost::iostreams::mapped_file::alignment(); /* 64 MB File size */ 
	int32_t read_only_safe_guard_ = BlockFile::BufferBlockSize * boost::iostreams::mapped_file::alignment(); 
	int session_timeout_ = 180; // 180 seconds
	bool read_only_ = false; // Allows to write or not on the wallets
	std::string password_ = "";
	std::string master_blockfile_password_ = "";
	std::unordered_map<std::string, SharedPointer<Wallet>> wallets_;
	SessionTable sessions_;
	Cache<Wallet>::CachePtr wallet_cache_ = Cache<Wallet>::CachePtr(new Cache<Wallet>("Wallet Cache"));
	EncryptionKey key_ {}; 
	BlockHashTypes hash_func_type_ = BlockHashTypes::SHA256; // For all the wallets
	std::time_t last_user_show_sessions_ = std::time(nullptr);
#if defined(PYTHON_BINDING)
	std::unordered_map<std::string, PyObject*> pending_read_callbacks_ {};
	std::unordered_map<std::string, PyObject*> pending_write_callbacks_ {};
#endif
	WalletOpenCode wallet_open_code_ = WalletOpenCode::WALLET_OPEN_SUCCESS;
	SharedPointer<BlockFile> master_file_ = nullptr;;
};

} // namespace petalus::blockchain

#endif  // SRC_SESSIONWALLETMANAGER_H_
