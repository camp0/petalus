/*
 * Petalus a crypto micro service wallet database.
 *
 * Copyright (C) 2018-2022  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */

#ifndef SRC_CRYPTOOPERATIONS_H_
#define SRC_CRYPTOOPERATIONS_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iomanip>
#include <cassert>
#include <cryptopp/cryptlib.h>
#define CRYPTOPP_ENABLE_NAMESPACE_WEAK 1 // For enable weak hashes
#include <cryptopp/md5.h>
#include <cryptopp/sha.h>
#include <cryptopp/sha3.h>
#include <cryptopp/blake2.h>
#include <cryptopp/hex.h>
#include <cryptopp/modes.h>
#include <cryptopp/filters.h>
#include <cryptopp/aes.h>
#include <cryptopp/blowfish.h>
#include <cryptopp/osrng.h>
#include <cryptopp/rsa.h>
#include <cryptopp/eccrypto.h>
#include <cryptopp/asn.h>
#include <cryptopp/oids.h>
#include <cryptopp/dsa.h>
#include <cryptopp/base64.h>
#include <cryptopp/xed25519.h>
#include "BlockHashTypes.h"

namespace petalus::blockchain { 

static_assert(static_cast<int>(CryptoPP::SHA256::DIGESTSIZE) == static_cast<int>(CryptoPP::BLAKE2s::DIGESTSIZE), "Wrong hash block size definition");
static_assert(static_cast<int>(CryptoPP::SHA256::DIGESTSIZE) == static_cast<int>(CryptoPP::SHA3_256::DIGESTSIZE), "Wrong hash block size definition");

static constexpr int HASH_BLOCK_SIZE = CryptoPP::BLAKE2s::DIGESTSIZE;

using Verifier = CryptoPP::ECDSA<CryptoPP::ECP, CryptoPP::SHA256>::Verifier;
using Signer = CryptoPP::ECDSA<CryptoPP::ECP, CryptoPP::SHA256>::Signer; 
using PublicKey = CryptoPP::ECDSA<CryptoPP::ECP, CryptoPP::SHA256>::PublicKey; 
using PrivateKey = CryptoPP::ECDSA<CryptoPP::ECP, CryptoPP::SHA256>::PrivateKey; 
using ED25519PublicKey = CryptoPP::ed25519PublicKey;
using ED25519PrivateKey = CryptoPP::ed25519PrivateKey;
using EDSigner = CryptoPP::ed25519::Signer;
using EDVerifier = CryptoPP::ed25519::Verifier;

static void generateRandomBytes(std::string &s, int bytes) {
        CryptoPP::RandomPool prng;
        CryptoPP::SecByteBlock seed(bytes);

	// The code can generate an exception because tries to open /dev/urandom
	// and the number of open descriptors can be limited
	try {
        	CryptoPP::OS_GenerateRandomBlock(false, seed, seed.size());
        	prng.IncorporateEntropy(seed, seed.size());

        	CryptoPP::HexEncoder hex(new CryptoPP::StringSink(s));

        	hex.Put(seed, seed.size());
        	hex.MessageEnd();
	} catch (std::exception &e) {
		s = "";
	} 
}

static void updateKey(CryptoPP::SecByteBlock &key) {

       	uint8_t quillo[32] = {
        	0xBE, 0x01, 0xA0, 0xbb, 0x07, 0xAA, 0xAD, 0x03,
                0xCE, 0xF1, 0x10, 0xb2, 0xFE, 0xDA, 0x02, 0xB3,
                0x0E, 0xF0, 0xA8, 0x06, 0x1F, 0xA7, 0xAD, 0xE2,
                0xC7, 0xAA, 0xE5, 0x95, 0x05, 0x3A, 0x04, 0xBB };

	int i = -1;

        quillo[++i] = 0xBE;                     		/* 0 */
        quillo[++i] = 0x29;                     		/* 1 */
        quillo[++i] = quillo[0x01] + 0xFF;     			/* 2 */
        quillo[++i] = quillo[0x04] - 0xE7;      		/* 3 */
        quillo[++i] = quillo[0x08];             		/* 4 */
        quillo[++i] = quillo[0x09] - 0x01;             		/* 5 */
        quillo[++i] = quillo[0x0D] & 0xFF;      		/* 6 */
        quillo[++i] = quillo[0x10] + 0xBE;             		/* 7 */
        quillo[++i] = quillo[0x11] + 0x0F;             		/* 8 */
        quillo[++i] = quillo[0x00] + 0x05;      		/* 9 */
        quillo[++i] = quillo[0x07] * quillo[0x02] - 1; 		/* 10 */
        quillo[++i] = quillo[0x14] >> 1;         		/* 11 */
        quillo[++i] = quillo[0x13] - 0x08;       		/* 12 */
        quillo[++i] = quillo[0x12] << 0x02;       		/* 13 */
        quillo[++i] = (quillo[0x11] ^ quillo[0x11]) + 0x03;	/* 14 */
        quillo[++i] = 0x89;                     		/* 15 */
        quillo[++i] = quillo[0x0F] | 0xCA;      		/* 16 */
        quillo[++i] = quillo[0x0E] + 0x01;             		/* 17 */
        quillo[++i] = quillo[0x0D] >> 0x01;             	/* 18 */
        quillo[++i] = 0x71;             			/* 19 */
        quillo[++i] = quillo[0x0B] - 0x08;             		/* 20 */
        quillo[++i] = 0xE9;             			/* 21 */
        quillo[++i] = 0x6B;             			/* 22 */
        quillo[++i] = quillo[0x06] * quillo[0x0A];             	/* 23 */
        quillo[++i] = quillo[0x07] + quillo[0x1A];             	/* 24 */
        quillo[++i] = quillo[0x06] - quillo[0x0F];             	/* 25 */
        quillo[++i] = quillo[0x05] ^ quillo[0x05];             	/* 26 */
        quillo[++i] = quillo[0x04] + 0xFA;             		/* 27 */
        quillo[++i] = quillo[0x03] * quillo[0x0A];   		/* 28 */
        quillo[++i] = quillo[0x02] + 0x1D;         		/* 29 */
        quillo[++i] = quillo[0x01] * 4;         		/* 30 */
        quillo[++i] = quillo[0x00] - 0x08;      		/* 31 */

	key.CleanNew(32);
	key.Assign(quillo, 32);

#ifdef STAND_ALONE_TEST
	assert(quillo[0x00] == 0xBE); assert(quillo[0x01] == 0x29);
	assert(quillo[0x02] == 0x28); assert(quillo[0x03] == 0x20);
	assert(quillo[0x04] == 0xCE); assert(quillo[0x05] == 0xF0);
	assert(quillo[0x06] == 0xDA); assert(quillo[0x07] == 0xCC);
	assert(quillo[0x08] == 0xFF); assert(quillo[0x09] == 0xC3);
	assert(quillo[0x0A] == 0xDF); assert(quillo[0x0B] == 0x0F);
	assert(quillo[0x0C] == 0xFE); assert(quillo[0x0D] == 0xA0);
	assert(quillo[0x0E] == 0x03); assert(quillo[0x0F] == 0x89);

	assert(quillo[0x10] == 0xCB); assert(quillo[0x11] == 0x04);
	assert(quillo[0x12] == 0x50); assert(quillo[0x13] == 0x71);
	assert(quillo[0x14] == 0x07); assert(quillo[0x15] == 0xE9);
	assert(quillo[0x16] == 0x6B); assert(quillo[0x17] == 0xE6);
	assert(quillo[0x18] == 0xB1); assert(quillo[0x19] == 0x51);
	assert(quillo[0x1A] == 0x00); assert(quillo[0x1B] == 0xC8);
	assert(quillo[0x1C] == 0xE0); assert(quillo[0x1D] == 0x45);
	assert(quillo[0x1E] == 0xA4); assert(quillo[0x1F] == 0xB6);
#endif
}

static void computeSHA256 (uint8_t digest[], const uint8_t *data, int datalen) {
        CryptoPP::SHA256 hash;

        hash.CalculateDigest(digest, data, datalen);
}

static void computeBLAKE2s (uint8_t digest[], const uint8_t *data, int datalen) {
        CryptoPP::BLAKE2s hash;

        hash.CalculateDigest(digest, data, datalen);
}

static void computeSHA3_256 (uint8_t digest[], const uint8_t *data, int datalen) {
        CryptoPP::SHA3_256 hash;

        hash.CalculateDigest(digest, data, datalen);
}

struct hash_function_handler {
	BlockHashTypes id;
	const char *name;
	std::function <void (uint8_t digest[], const uint8_t *data, int datalen)> func;
};


static hash_function_handler hash_handlers [static_cast<uint8_t>(BlockHashTypes::MAX_BLOCK_HASH_TYPES)] {
	{ BlockHashTypes::SHA256, "sha256", computeSHA256 },
	{ BlockHashTypes::BLAKE2S, "blake2s", computeBLAKE2s },
	{ BlockHashTypes::SHA3_256, "sha3-256", computeSHA3_256 }
};

} // namespace petalus::blockchain

#endif  // SRC_CRYPTOOPERATIONS_H_
