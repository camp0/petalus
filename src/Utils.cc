/*
 * Petalus a crypto micro service wallet database.
 *
 * Copyright (C) 2018-2022  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */

#include "Utils.h"

namespace petalus {

auto print_payload_line = [](std::basic_ostream<char> &out, const uint8_t *payload, int from, int to) noexcept {

        out << "         ";
        for (int i = from ; i <= to; ++i) {
                if (payload[i] >= 32 && payload[i] <= 128)
                        out << (unsigned char)payload[i];
                else
                        out << ".";
        }
        out << std::endl;
};

void showData (std::basic_ostream<char> &out, const uint8_t *payload, int length) {

        std::ios_base::fmtflags f(out.flags());

        for (int i = 0; i < length; ++i) {
                if ((i != 0)and(i % 16 == 0)) {
                        print_payload_line(out, payload, i - 16, i);
                }

                if (i % 16 == 0) out << "\t";
                out << std::hex << std::setfill('0') << std::setw(2) << (unsigned int)payload[i] << " ";

                if (i == length -1) {
                        for (int j = 0; j < 15 - i % 16; ++j) out << "   ";

                        print_payload_line(out, payload, i - i % 16, i);
                }
        }
        // out << std::dec; // restore the decimal
        out.flags(f);
};

} // namespace petalus
