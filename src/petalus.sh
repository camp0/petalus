#!/bin/bash
#
# chkconfig: 35 90 12
# description: Petalus multiple instances server
#

# Get function from functions library
. /etc/init.d/functions

# Start the service 
start() {
    # initlog -c "echo -n Starting Petalus server: "

    /usr/bin/screen -d -m -S instance0 python instance.py instance0 5555
    /usr/bin/screen -d -m -S instance1 python instance.py instance1 5556
    /usr/bin/screen -d -m -S instance2 python instance.py instance2 5557
    /usr/bin/screen -d -m -S instance3 python instance.py instance3 5558

    ### Create the lock file ###
    touch /var/lock/subsys/petalus
    success $"Petalus server startup"
    echo
}

# Restart the service Petalus
stop() {
    # initlog -c "echo -n Stopping Petalus server: "

    /usr/bin/screen -X -S instance0 stuff "^C"
    /usr/bin/screen -X -S instance1 stuff "^C"
    /usr/bin/screen -X -S instance2 stuff "^C"
    /usr/bin/screen -X -S instance3 stuff "^C"
    ### Now, delete the lock file ###
    rm -f /var/lock/subsys/petalus
    success $"Petalus server stop"
    echo
}

status() {

    /usr/bin/screen -ls | grep instance
}

### main logic ###
case "$1" in
    start)
        start
        ;;
    stop)
        stop
        ;;
    status)
        status 
        ;;
    restart|reload|condrestart)
        stop
        start
        ;;
    *)
        echo $"Usage: $0 {start|stop|restart|reload|status}"
        exit 1
esac

exit 0
