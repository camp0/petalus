/*
 * Petalus a crypto micro service wallet database.
 *
 * Copyright (C) 2018-2022  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */

#ifndef SRC_BLOCKCHAIN_BLOCKFILE_H_
#define SRC_BLOCKCHAIN_BLOCKFILE_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <fcntl.h>
#include <iostream>
#include <fstream>
#include <sys/mman.h>
#include <filesystem>
#include <boost/iostreams/device/mapped_file.hpp>
#include <boost/utility/string_ref.hpp>
#include <cryptopp/eccrypto.h>
#if defined(PYTHON_BINDING)
#include <boost/python.hpp>
#endif
#include <sys/file.h> // flock
#include "json.hpp"
#include "Pointer.h"
#include "Message.h"
#include "PetalusBlockFile.h"
#include "PetalusBlock.h"

namespace petalus::blockchain {

// Write errors
enum class BlockFileWriteCode : uint8_t {
	BLOCKFILE_WRITE_SUCCESS =	0x00,
	BLOCKFILE_WRITE_FILE_NOT_OPEN =	0x01,
	BLOCKFILE_WRITE_SIGN_FAIL =     0x02,
	BLOCKFILE_WRITE_NO_SPACE =      0x03
};

typedef std::map<std::string, std::pair<int32_t, std::string>> FolderEntry;

class BlockFile {
public:
	explicit BlockFile(int32_t size): max_size_per_file_(size) {}

	// By default the system creates a mmap of 128 MBs
	// Depending on the use of this functionality may be a small
	// size of a bigger size is required.

#if defined(STAND_ALONE_TEST) || defined(TESTING)
	explicit BlockFile():BlockFile(32 * boost::iostreams::mapped_file::alignment()) {}
#endif
  	BlockFile(const BlockFile &bf); 
	 
	virtual ~BlockFile() { this->close(); }

	static constexpr int32_t BufferBlockSize = 16 * 256; // this value is multiply by the page alignment of the machine

	void statistics(std::basic_ostream<char> &out) const;

	void open(const std::filesystem::path &filename) {
		open(filename, boost::iostreams::mapped_file_base::mapmode::readwrite); 
	}

	void open(const std::filesystem::path &filename, const boost::iostreams::mapped_file_base::mapmode flags);
	void close();
	void sync();

	const std::filesystem::path &getPath() const { return path_; }
	int32_t getTotalBlocks() const { return hdr_.blocks; }
	int32_t getTotalBytes() const { return total_bytes_; }
	int32_t getLastWrittenBlockSize() const { return hdr_.last_block_size; }

	/* Returns the bytes of all the blocks */
	int32_t getTotalDataBytes() const { return total_data_bytes_; } 
	/* Returns the bytes of all the blocks that belongs to user */
	int32_t getTotalUserDataBytes() const { return total_user_data_bytes_; }
	int32_t getMaxFileSize() const { return max_size_per_file_; }

	bool haveSpaceOnFile(int32_t bytes);
	bool verifyBlock(int32_t number) const;
	bool verifySignatureBlock(int32_t number) const;
	bool verifyBlock(int32_t number, uint8_t digest[]) const;

	int32_t read(int32_t number, PetalusBlock &pblock) const;
	void readBlockHash(int32_t number, uint8_t digest[]) const;
	void readBlockDataHash(int32_t number, uint8_t digest[]) const;

	bool is_open() const { return mfile_.is_open(); }
	bool is_lock() const; 
	bool verify(const BlockFile &bf) const;

	void setSeed(uint8_t digest[]);
	void getSeed(uint8_t digest[]) const;
	
	void showBlocks(std::basic_ostream<char> &out) const;
	void showBlock(std::basic_ostream<char> &out, int32_t number) const;
	void showBlock(nlohmann::json &out, int32_t number) const;

	int32_t write(PetalusBlock &pblock);
	BlockFileWriteCode getWriteErrorCode() const { return write_error_code_; }

	void setShowMissingBlocks(int bytes) { show_missing_bytes_ = bytes; }
	void setDebug(bool value) { debug_ = value; }
	
#if defined(PYTHON_BINDING)
	std::string pyread(int32_t number);
	void open(const std::string &filename) { open(std::filesystem::path(filename)); }
	int32_t pywrite(const std::string &data);
#endif


#if defined(STAND_ALONE_TEST) || defined(TESTING)

	void writeOffset(int32_t offset, const uint8_t *data, int length);

	int32_t getTotalHeaderCorruptions() const { return total_header_corruptions_; }
	int32_t getTotalBlockCorruptions() const { return total_block_corruptions_; }
	int32_t getTotalLengthCorruptions() const { return total_length_corruptions_; }
	int32_t getTotalMissingBlocks() const { return total_missing_blocks_; }
	int32_t getLastBlockLoaded() const { return block_index_.size(); }	

	EncryptionKey &getEncryptionKey() { return ekey_; }

	// This writes bypass the public/private key mechanism, only use on testing pourposes
	int32_t writeRawBlock(PetalusBlock &block, uint8_t flags) { return write_block_on_file(block); }

	int getSignatureLength() const {

		if (isECPublicKey())
			return ec_public_key_->GetGroupParameters().GetCurve().GetField().MaxElementByteLength() * 2;
		else
			return 64;
	}
#endif
	friend std::ostream& operator<< (std::ostream &out, const BlockFile &bf);

	SharedPointer<BlockFile> getFolder(const std::string &name) const;
	const petalus_block_file_v1 *findBlock(int32_t number) const { return find_block(number); }

	// Security functions
	const char *getAccessPassword() const { return access_password_.c_str(); }
	const char *getSalt() const { return salt_.c_str(); }

	// Sets the encryption key for all the user blocks
	void setEncryptionKey(const EncryptionKey &ekey) { ekey_ = ekey; }

	void setBlockHashType(BlockHashTypes type);
	const char *getBlockHashTypeName() const { return hash_handlers[static_cast<uint8_t>(hash_func_type_)].name; }
	BlockHashTypes getBlockHashType() const { return hash_func_type_; }

	void setPublicKey(const PublicKey &pkey);
	void setPublicKey(const ED25519PublicKey &pkey);
	PublicKey getECPublicKey() const { return *ec_public_key_.get(); }
	ED25519PublicKey getED25519PublicKey() const { return *ed25519_public_key_.get(); }
	bool havePublicKey() const { return (ec_public_key_ or ed25519_public_key_); }
	bool isECPublicKey() const { return (ec_public_key_ != nullptr); }
	bool isED25519PublicKey() const { return (ed25519_public_key_ != nullptr); }

        const FolderEntry getFolders() const { return folders_; }

	void clone(const BlockFile &bfile);

private:
	void create_header_file();
	void update_header_file();
	bool populate_blocks_index();
	const petalus_block_file_v1 *find_block(int32_t number) const;
	void show_block(std::basic_ostream<char> &out, const petalus_block_file_v1 *block, int32_t number) const;
	void show_block(nlohmann::json &out, const petalus_block_file_v1 *block, int32_t number) const;
	void compute_block_hash(const petalus_block_file_v1 &block, uint8_t digest[]) const;
	bool verify_block(const petalus_block_file_v1 &block) const;
	bool verify_signature_block(const petalus_block_file_v1 &block) const;
	int32_t write_block_on_file(PetalusBlock &pblock);
	bool lock(const char *filename);
	void unlock();

	boost::iostreams::mapped_file mfile_ {};
	petalus_header_file hdr_ {};
	std::filesystem::path path_ {};
	mutable int32_t total_header_corruptions_ = 0;
	mutable int32_t total_block_corruptions_ = 0;
	mutable int32_t total_missing_blocks_ = 0;
	mutable int32_t total_length_corruptions_ = 0;
	int32_t max_size_per_file_ = 0;
	int32_t total_bytes_ = 0; // total bytes with headers
	int32_t total_data_bytes_ = 0; // total data bytes without headers
	int32_t total_user_data_bytes_ = 0;
	int32_t show_missing_bytes_ = 0;
	std::map<int32_t, const petalus_block_file_v1*> block_index_ {};
	FolderEntry folders_ {};
	uint8_t initial_hash_[HASH_BLOCK_SIZE] = { 0x00 };
	BlockFileWriteCode write_error_code_ = BlockFileWriteCode::BLOCKFILE_WRITE_SUCCESS;
       	EncryptionKey ekey_; 
	std::string password_ = "";
	std::string access_password_ = "";
	std::string salt_ = "";
	bool file_lock_ = false;
	bool debug_ = false;
	BlockHashTypes hash_func_type_ = BlockHashTypes::SHA256;
	SharedPointer<PublicKey> ec_public_key_ = nullptr;
	SharedPointer<ED25519PublicKey> ed25519_public_key_ = nullptr;
#if defined(__LINUX__)
	int fd_ = 0;
#endif
};

} // namespace petalus::blockchain

#endif  // SRC_BLOCKCHAIN_BLOCKFILE_H_
