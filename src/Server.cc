/*
 * Petalus a crypto micro service wallet database.
 *
 * Copyright (C) 2018-2022  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */

#include "Server.h"

namespace petalus {

void Server::setSessionWalletManager(const SharedPointer<SessionWalletManager> &sw) { 

	sw_ = sw; 
	for (auto &it: *user_connections_)
		if (auto c = it.second; c->conn.lock())
			c->conn.lock()->setSessionWalletManager(sw_);
	
	for (auto &it: *admin_connections_)
		if (auto c = it.second; c->conn.lock())
			c->conn.lock()->setSessionWalletManager(sw_);
}

void Server::setAuthorizationToken(const std::string &token) {

        if (token.compare(0, 5, "{AES}") == 0) {
        	// Decrypt the token
        	std::string source = token.substr(5, token.length() - 5);

        	std::string destination;
        	CryptoPP::StringSource ss(source, true, new CryptoPP::HexDecoder(new CryptoPP::StringSink(destination)));

        	PetalusBlock block(destination);
        	block.type(BlockType::USR_CRYPT_PLAIN);

        	CryptoPP::SecByteBlock key { 32 };
        	updateKey(key);

        	block.decrypt(key);

        	// AES passwords can only be 16 byte, 24 byte or 32 length
                if ((block.length() == 16)or(block.length() == 24)or(block.length() == 32)) {
			authorization_token_ = token;
                        authorization_token_key_.CleanNew(block.length());
                        authorization_token_key_.Assign((const uint8_t*)block.data(), block.length());
                        PINFO << "Authorization token set";
                } else {
                        PWARN << "Authorization token  can not be set, length:" << block.length();
                }
	} else 
        	PWARN << "Authorization token not valid, please encrypt the token";
}

std::string Server::private_key_password_callback() {

	// Decrypt the password
        std::string source = private_key_password_.substr(5, private_key_password_.length() - 5);

        std::string destination;
        CryptoPP::StringSource ss(source, true, new CryptoPP::HexDecoder(new CryptoPP::StringSink(destination)));

        PetalusBlock block(destination);
        block.type(BlockType::USR_CRYPT_PLAIN);

        CryptoPP::SecByteBlock key { 32 };
        updateKey(key);

        block.decrypt(key);

        // AES passwords can only be 16 byte, 24 byte or 32 length
        if ((block.length() == 16)or(block.length() == 24)or(block.length() == 32))
		return block.data();
	else 
		return "";
} 

std::string Server::retrieve_secure_data(const std::string &data) {

	std::error_code ec;
	std::string value(data);

	if (std::filesystem::exists(value, ec)) {
		std::ifstream pfile(value);
		if ((!ec)and(pfile.is_open())) {
			std::ostringstream out;
			out << pfile.rdbuf();
			value = out.str();
		}
	}
	return value;
}

void Server::setPrivateKeyPassword(const std::string &password) {

	std::string data = retrieve_secure_data(password);

        if (data.compare(0, 5, "{AES}") == 0) {
		private_key_password_ = data;
		ssl_ctx_.set_password_callback(std::bind(&Server::private_key_password_callback, this));
	}
}

void Server::setPrivateKey(const std::string &key) {

	std::string data = retrieve_secure_data(key);

	short_private_key_.assign(data, 0, 22);
	try {
		ssl_ctx_.use_private_key(boost::asio::buffer(data.data(), data.size()), boost::asio::ssl::context::pem);
	} catch (const std::exception &e) {
        	std::ostringstream msg;
        	msg << "Invalid private key password:" << e.what();

        	petalus::error_message(msg.str());
		PERROR << msg.str();
		ready_ = false; 
	}
}

void Server::setCertificate(const std::string &cert) {

	std::string data = retrieve_secure_data(cert);
	try {
    		ssl_ctx_.use_certificate_chain(boost::asio::buffer(data.data(), data.size()));
		tls_support_ = true;
		short_certificate_.assign(data, 0, 22);
		PINFO << "TLS support enabled";
	} catch (const std::exception &e) {
		PERROR << e.what();
		ready_ = false; 
	}
}

void Server::setCertificateAuthority(const std::string &cert) {

	std::string data = retrieve_secure_data(cert);
        try {
                ssl_ctx_.add_certificate_authority(boost::asio::buffer(data.data(), data.size()));
                PINFO << "TLS ca support enabled";
        } catch (const std::exception &e) {
                PERROR << e.what();
                ready_ = false;
        }
}


void Server::setProxySupport(bool value) {

	PINFO << "Proxy mode " << (value ? "enable" : "disabled" );
	proxy_support_ = value;
}

void Server::start() {

	std::string mode = "TLS";

	if (mtls_) mode = "mTLS";

	std::string tls_message = mode + " enable";

	if ((!tls_support_)or(short_certificate_.length() == 0)) {
		PINFO << "No TLS support configured";
		tls_message = "TLS disable";
	}

	if (!sw_)
		PWARN << "No SessionWalletManager plugged to the server";
	else {
		std::string data(sw_->getDataDirectory());

		if (data.length() == 0)
			PWARN << "No data_directory set on the SessionWalletManager";
	}

	if (internet_port_ > 0) {
		std::ostringstream msg;
		boost::asio::ip::tcp::endpoint endpoint(internet_address_, internet_port_);

        	user_acceptor_.open(endpoint.protocol());
        	user_acceptor_.set_option(boost::asio::ip::tcp::acceptor::reuse_address(true));
		user_acceptor_.bind(endpoint);
		user_acceptor_.listen();
		msg << "Listening user requests on ";
		msg << internet_address_ << ":" << internet_port_ << " " << tls_message;
		
		PINFO << msg.str(); 
		accept_user_connections();
	}

	if (administration_port_ > 0) {
		std::ostringstream msg;
		boost::asio::ip::tcp::endpoint admin_endpoint(administration_address_, administration_port_);

        	admin_acceptor_.open(admin_endpoint.protocol());
        	admin_acceptor_.set_option(boost::asio::ip::tcp::acceptor::reuse_address(true));
		admin_acceptor_.bind(admin_endpoint);
		admin_acceptor_.listen();
		msg << "Listening administration requests on ";
		msg << administration_address_ << ":" << administration_port_ << " " << tls_message;
		
		PINFO << msg.str(); 
		accept_administration_connections();
	}

	std::ostringstream out;

	out << "Petalus engine started";

	PINFO << out.str();
	petalus::information_message(out.str());
}

void Server::accept_user_connections() {

	// Acceptor for Connections from users from the Internet
	user_acceptor_.async_accept(user_socket_, [&] (boost::beast::error_code ec) {
        	if (!ec) {
			std::time_t now = std::time(nullptr);
			boost::posix_time::ptime pnow(boost::posix_time::from_time_t(now));

			std::string ipaddress(user_socket_.remote_endpoint().address().to_string());
			std::stringstream id;

			id << ipaddress << ":" << user_socket_.remote_endpoint().port();

			SharedPointer<HTTPUserSession> conn;
			// Allocate space for the new connection
			if (tls_support_)
				conn = SharedPointer<HTTPUserSession>(new HTTPUserSession(std::move(user_socket_), ssl_ctx_));
			else
				conn = SharedPointer<HTTPUserSession>(new HTTPUserSession(std::move(user_socket_)));
#if defined(PYTHON_BINDING)
			if ((security_callback_)and(security_callback_->haveCallback()))
				conn->addSecurityCallback(security_callback_);
#endif
			conn->setSessionWalletManager(sw_);
			conn->setHTTPStatistics(&stats);
			conn->enableProxySupport(proxy_support_);	
		
			// Update the timers of the sessions so if one was comatose
			// on memory them it will be removed before accesing to it
			if ((sw_)and(now - last_timeout_time_ > sw_->getSessionTimeout())) {
				// Update the timer of the sessions and close if needed
				sw_->updateTimers(now);
				last_timeout_time_ = now;
			}

			// Start processing the HTTP
			conn->start();

			if (auto it = user_connections_->find(id.str()); it != user_connections_->end()) {
				auto c = (it->second);
			} else {
				if (auto c = SharedPointer<Connection<HTTPUserSession>>(new Connection<HTTPUserSession>()); c) {
					c->ip = id.str();
					c->conn = conn;
					user_connections_->insert(std::make_pair(id.str(), c));
				}
			}
        	}
                accept_user_connections();
        });
}

void Server::accept_administration_connections() {

	// Acceptor for Connections from administration or read systems 
        admin_acceptor_.async_accept(admin_socket_, [&] (boost::beast::error_code ec) {
                if (!ec) {
                        std::string ipaddress(admin_socket_.remote_endpoint().address().to_string());

			// Check if the ip address is from administration IP
			if (auto it = administration_ips_.find(ipaddress); it != administration_ips_.end()) {

				if (!authorization_token_key_.empty()) {
                        		std::time_t now = std::time(nullptr);
                        		std::stringstream id;

                        		id << ipaddress << ":" << admin_socket_.remote_endpoint().port();

					// Allocate space for the new administration connection
					SharedPointer<HTTPAdministrationSession> conn;

					if (tls_support_)
						conn = SharedPointer<HTTPAdministrationSession>(new HTTPAdministrationSession(std::move(admin_socket_), ssl_ctx_));
					else
						conn = SharedPointer<HTTPAdministrationSession>(new HTTPAdministrationSession(std::move(admin_socket_)));

					conn->setAuthorizationTokenKey(authorization_token_key_);
					conn->setLicenseMaxFoldersPerWallet(max_folders_per_wallet_);
					conn->setSessionWalletManager(sw_);
					conn->setHTTPStatistics(&stats);

					// Update the timers of the sessions so if one was comatose
					// on memory them it will be removed before accesing to it
					if ((sw_)and(now - last_timeout_time_ > sw_->getSessionTimeout())) {
						// Update the timer of the sessions and close if needed
						sw_->updateTimers(now);
						last_timeout_time_ = now;
					}

					// Start processing the HTTP
					conn->start();

					if (auto it = admin_connections_->find(id.str()); it != admin_connections_->end()) {
						auto c = (it->second);
					} else {
						if (auto c = SharedPointer<Connection<HTTPAdministrationSession>>(new Connection<HTTPAdministrationSession>()); c) {
							c->ip = id.str();
                                        		c->conn = conn;
                                        		admin_connections_->insert(std::make_pair(id.str(), c));
                                		}
					}
                        	} else {
					PWARN << "No authorization token configured for administrative IPs";	
					admin_socket_.close();
				}
                        } else {
				PWARN << "The IP addresss " << ipaddress << " is not authorized for administration";	
				admin_socket_.close();
			}
		}
		accept_administration_connections();
	});
}

void Server::run() { 

	try {

		if (!ready_) {
			std::ostringstream msg;

			msg << "The server is not able to start";

			petalus::warning_message(msg.str());
		}

#if defined(PYTHON_BINDING)
                start_read_user_input();
#endif
        	io_.run();

	} catch (const std::ios_base::failure& e) {
		// This exception is trigger if there is a limit of open descriptors
		// and the process reach that limit. But the output by e.code().value()
		// e.code().message() and e.code().category().name() is not good.
		std::ostringstream msg;

		msg << "ERROR:" << e.code().message() << std::endl;
		msg << "This error happens in general when there is no enough file descriptors\n";
		msg << "Please consider or increase the limit with 'ulimit' or decrease the \n";
		msg << "number of Wallets on the cache.\n";

		PFATAL << msg.str();		
		error_message(msg.str()); 

	} catch (const std::exception &e) {
        	std::ostringstream msg;
                msg << "ERROR:" << e.what() << std::endl;
                msg << boost::diagnostic_information(e);
		// std::cout << "Errno:" << errno << " enfile:" << ENFILE  << std::endl;
		PFATAL << msg.str();		
                error_message(msg.str());
	        void *array[10];
                size_t size;

                size = backtrace(array, 10);

                backtrace_symbols_fd(array, size, STDERR_FILENO);
	}
}

void Server::stop() { 

	std::ostringstream msg;
	
	user_acceptor_.close();
	admin_acceptor_.close();
	 
	msg << "Petalus Engine stopped"; 

	PINFO << msg.str();

        petalus::information_message(msg.str());
}

std::ostream& operator<< (std::ostream &out, const Server &bs) {

	out << "Server statistics\n";
	std::string_view socket_status = (bs.user_acceptor_.is_open() ? "Listening" : "Close");
	std::string_view admin_socket_status = (bs.admin_acceptor_.is_open() ? "Listening" : "Close");

	std::string token(bs.authorization_token_.substr(0, 14));
	std::string pass(bs.private_key_password_.substr(0, 13));

	out << "\t" << "Address:" << bs.internet_address_ << ":" << bs.internet_port_ << " " << socket_status << "\n";
	out << "\t" << "Admin address:" << bs.administration_address_ << ":" << bs.administration_port_ << " " << admin_socket_status << "\n";
	out << "\t" << "Private key:" << bs.short_private_key_ << "\n";
	out << "\t" << "Private key password:" << pass << "\n";
	out << "\t" << "Certificate:" << bs.short_certificate_ << "\n";
	out << "\t" << "Proxy support:               " << std::setw(5) << (bs.proxy_support_ ? "yes" : "no") << "\n";
	out << "\t" << "Authorization token:" << token << "\n";
#if defined(PYTHON_BINDING)
        if ((bs.security_callback_)and(bs.security_callback_->haveCallback()))
		out << "\t" << "Security callback:" << bs.security_callback_->getName() << "\n";
#endif
	out << bs.stats;

	bs.sys_->statistics(out);
#if defined(PYTHON_BINDING)
	bs.tm_->statistics(out);
#endif
	return out;
}

void Server::statistics(std::basic_ostream<char> &out) const {

	out << *this;
}

void Server::show_user_connections(std::basic_ostream<char> &out) {

        std::vector<SharedPointer<Connection<HTTPUserSession>>> usr_conn;
        std::time_t now = std::time(nullptr);

        out << "User connections\n";

        out << "\t" << boost::format(show_connection_header) % "IP:Port" % "Session" % "Bytes" % "Requests" % "TLS" % "Access" << std::endl;

        // Remove the old user connections
        for (auto it = user_connections_->begin(); it != user_connections_->end(); ) {
                SharedPointer<Connection<HTTPUserSession>> con = (*it).second;

		if (auto http = con->conn.lock(); http) { 
			// The conection still exists
                        if ((now - http->getLastTime() > sw_->getSessionTimeout())) {
                                it = user_connections_->erase(it);
                        } else {
                                usr_conn.push_back(con);
                                ++it;
                        }
                } else {
                        it = user_connections_->erase(it);
                }
        }

        std::sort(usr_conn.begin(), usr_conn.end(),
                [] (SharedPointer<Connection<HTTPUserSession>> const& a, SharedPointer<Connection<HTTPUserSession>> const& b) {
                        return a->conn.lock()->getLastTime() > b->conn.lock()->getLastTime();
                }
        );

        for (auto &it: usr_conn) {
                char mbstr[64];
                std::time_t ctime = it->conn.lock()->getLastTime();
                int32_t requests = it->conn.lock()->getTotalRequests();
                int64_t bytes = it->conn.lock()->getTotalBytes();
                std::strftime(mbstr, 64, "%X %D", std::localtime(&ctime));
                const char *strtls = it->conn.lock()->haveTLSSupport() ? "yes":"no";

                out << "\t" << boost::format(show_connection_format) % it->ip % it->conn.lock()->getSessionId() % bytes % requests % strtls % mbstr;
                out << "\n";
        }

        out.flush();
}

void Server::show_administration_connections(std::basic_ostream<char> &out) {

        std::time_t now = std::time(nullptr);
        std::vector<SharedPointer<Connection<HTTPAdministrationSession>>> adm_conn;

        out << "Administrative connections\n";
        out << "\t" << boost::format(show_connection_header) % "IP:Port" % "Session" % "Bytes" % "Requests" % "TLS" % "Access" << std::endl;

        // Remove the old admin connections
        for (auto it = admin_connections_->begin(); it != admin_connections_->end(); ) {
                SharedPointer<Connection<HTTPAdministrationSession>> con = (*it).second;

		if (auto http = con->conn.lock(); http) {
                	// The conection still exists
                        if ((now - http->getLastTime() > sw_->getSessionTimeout())) {
                                it = admin_connections_->erase(it);
                        } else {
                                adm_conn.push_back(con);
                                ++it;
                        }
                } else {
                        it = admin_connections_->erase(it);
                }
        }

        std::sort(adm_conn.begin(), adm_conn.end(),
                [] (SharedPointer<Connection<HTTPAdministrationSession>> const& a, SharedPointer<Connection<HTTPAdministrationSession>> const& b) {
                        return a->conn.lock()->getLastTime() > b->conn.lock()->getLastTime();
                }
        );

        for (auto &it: adm_conn) {
                char mbstr[64];
                std::time_t ctime = it->conn.lock()->getLastTime();
                int32_t requests = it->conn.lock()->getTotalRequests();
                int64_t bytes = it->conn.lock()->getTotalBytes();
                std::strftime(mbstr, 64, "%X %D", std::localtime(&ctime));
                const char *strtls = it->conn.lock()->haveTLSSupport() ? "yes":"no";

                out << "\t" << boost::format(show_connection_format) % it->ip % it->conn.lock()->getSessionId() % bytes % requests % strtls % mbstr;
                out << "\n";
        }

        out.flush();
}

void Server::show_connections(std::basic_ostream<char> &out) {

	show_user_connections(out);
	show_administration_connections(out);
}

void Server::addAdministrationIPAddress(const std::string &ip) {

	struct sockaddr_in sa;

        if (inet_pton(AF_INET, ip.c_str(), &(sa.sin_addr))) {
        	PINFO << "Adding administration IP " << ip;

                administration_ips_.insert(ip);
	}
}

#if defined(PYTHON_BINDING)

void Server::setMTLS(bool value) {

	if (value)
		// context set_verify_mode
		SSL_CTX_set_verify(ssl_ctx_.native_handle(), SSL_VERIFY_PEER|SSL_VERIFY_FAIL_IF_NO_PEER_CERT, NULL);
	else
		SSL_CTX_set_verify(ssl_ctx_.native_handle(), SSL_VERIFY_NONE, NULL);
	mtls_ = value; 
}

bool Server::getMTLS() const {

        return mtls_;
}

void Server::setShell(bool enable) {

        user_shell_->setShell(enable);
}

bool Server::getShell() const {

        return user_shell_->getShell();
}

void Server::addTimer(PyObject *callback, int seconds) {

        tm_->addTimer(callback, seconds);
}

void Server::start_read_user_input(void) {

        user_shell_->readUserInput();
}

void Server::addAdministrationIPAddress(const boost::python::list &items) {

        for (int i = 0; i < len(items); ++i ) {
                // Check if is a std::string 
                boost::python::extract<std::string> extractor(items[i]);
                if (extractor.check()) {
                        auto ip = extractor();

			addAdministrationIPAddress(ip);
                }
        }
}

void Server::show_connection(std::basic_ostream<char> &out, const std::string &id) {

        if (auto it = user_connections_->find(id); it != user_connections_->end()) {
		SharedPointer<Connection<HTTPUserSession>> con = (*it).second;

		if (auto http = con->conn.lock(); http) {
			PINFO << "User showing connection " << id;

			std::string_view strtls = http->haveTLSSupport() ? "yes":"no";

			out << "Connection: " << id << "\n";
			out << "\tTLS support:            " << std::setw(10) << strtls << "\n";
			out << "\tUser IP:" << std::right << std::setfill(' ') << std::setw(26) << http->getIP() << "\n";
			out << http->getHTTPStatistics();
			out.flush();
		}
	}
}

void Server::closeConnection(const std::string &id) {

        if (auto it = user_connections_->find(id); it != user_connections_->end()) {
		SharedPointer<Connection<HTTPUserSession>> con = (*it).second;

		if (auto http = con->conn.lock(); http) {
			PINFO << "User close connection " << id;

			http->close_connection();
		}
	}
}

void Server::addSecurityCallback(PyObject *callback) {

	PINFO << "Security callback set";

	security_callback_->setCallback(callback);
}

boost::python::list Server::getAdministrationIPAddresses() const {
	
	boost::python::list items;

	for (auto &it: administration_ips_)
		items.append(it);	

	return items;
}

#endif

void Server::show_administration_ips(std::basic_ostream<char> &out) {

        out << "Administration IPs\n";
	for (auto &it: administration_ips_)
		out << "\t" << it << "\n";

       	out.flush(); 
}

} // namespace petalus
