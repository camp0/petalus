/*
 * Petalus a crypto micro service wallet database.
 *
 * Copyright (C) 2018-2022  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <boost/python.hpp>
#include <boost/python/module_init.hpp>
#include <boost/python/docstring_options.hpp>
#include <boost/asio.hpp>
#include <Python.h> // compatibility
#include "Server.h"
#include "python_help.h"

using namespace boost::python;
using namespace petalus;
using namespace petalus::blockchain;

BOOST_PYTHON_MODULE(petalus)
{
        using namespace std;
        using namespace boost::asio;
        using self_ns::str;

#if defined(HAVE_PYTHON_GIL)
        if (! PyEval_ThreadsInitialized()) {
                PyEval_InitThreads();
                PyEval_ReleaseLock();
        }
#endif

        // Enable de documentation, for help(petalus)
        boost::python::docstring_options doc_options(true, true, false);

	void (SessionWalletManager::*setDataDirectoryString)(const std::string&) =      &SessionWalletManager::setDataDirectory;
	void (SessionWalletManager::*setMasterBlockFileString)(const std::string&) =    &SessionWalletManager::setMasterBlockFile;
	void (SessionWalletManager::*showAllSessions)() =        			&SessionWalletManager::showSessions;
	void (SessionWalletManager::*showAllTriggers)() const =        			&SessionWalletManager::showTriggers;
	bool (SessionWalletManager::*closeSession)(const std::string&) =		&SessionWalletManager::close;
	void (SessionWalletManager::*showUserSession1)(const std::string&) const =	&SessionWalletManager::showSession;
	void (SessionWalletManager::*showUserSession2)(const std::string&, int32_t ) const = &SessionWalletManager::showSession;

        boost::python::class_<SessionWalletManager, SharedPointer<SessionWalletManager>, boost::noncopyable >("SessionWalletManager",
                "Class that manages the wallets.")
                .def(init<>())
                .add_property("data_directory", &SessionWalletManager::getDataDirectory, setDataDirectoryString,
                        help_swm_data_directory)
                .add_property("encryption_password", &SessionWalletManager::getEncryptionPassword, &SessionWalletManager::setEncryptionPassword,
                        help_swm_password)
                .add_property("master_blockfile", &SessionWalletManager::getMasterBlockFile, setMasterBlockFileString,
                        help_swm_master_blockfile)
                .add_property("master_blockfile_password", &SessionWalletManager::getMasterBlockFilePassword, &SessionWalletManager::setMasterBlockFilePassword,
                        "TODO")
                .add_property("hash_type", &SessionWalletManager::getBlockHashType, &SessionWalletManager::setBlockHashType,
                        help_swm_hash_type)
                .add_property("timeout", &SessionWalletManager::getSessionTimeout, &SessionWalletManager::setSessionTimeout,
                        help_swm_timeout)
                .add_property("max_blockfile_size", &SessionWalletManager::getMaxBlockFileSize, &SessionWalletManager::setMaxBlockFileSize,
                        help_swm_max_blockfile_size)
                .add_property("read_only", &SessionWalletManager::isReadOnly, &SessionWalletManager::setReadOnly,
                        help_swm_read_only)
		.def("show", &SessionWalletManager::show,
			help_swm_show)
		.def("show_sessions", showAllSessions,
			help_swm_show_sessions)
		.def("show_session", showUserSession1,
			help_swm_show_session)
		.def("show_session", showUserSession2,
			help_swm_show_session)
		.def("show_triggers", showAllTriggers,
			help_swm_show_triggers)
		.def("close_session", closeSession,
			help_swm_close_session)
		.def("close_sessions", &SessionWalletManager::closeSessions,
			help_swm_close_sessions)
		.def("increase_allocated_memory", &SessionWalletManager::increaseAllocatedMemory,
			help_swm_inc_memory)
		.def("decrease_allocated_memory", &SessionWalletManager::decreaseAllocatedMemory,
			help_swm_dec_memory)
		.def("add_read_trigger", &SessionWalletManager::addReadTrigger,
			help_swm_add_read_trigger)
		.def("add_write_trigger", &SessionWalletManager::addWriteTrigger,
			help_swm_add_write_trigger)
		.def("rotate_encryption_key", &SessionWalletManager::rotateEncryptionKey,
			help_swm_rotation_key)
                .def(self_ns::str(self_ns::self))
        ;

        boost::python::class_<Wallet, SharedPointer<Wallet>,
                boost::noncopyable >("Wallet", "Class that manages the wallets.")
		.def_readonly("name", &Wallet::getName,
			help_wallet_name)
		.def_readonly("filename", &Wallet::getFilename,
			help_wallet_filename)
		.def_readonly("total_bytes", &Wallet::getTotalBytes,
			help_wallet_total_bytes)
		.def_readonly("total_blocks", &Wallet::getTotalBlocks,
			help_wallet_total_blocks)
		.add_property("block", make_function(&Wallet::getCurrentBlock, return_internal_reference<>()),
			help_wallet_block)
                .def(self_ns::str(self_ns::self))
        ;

	void (Server::*addAdministrationIPs)(const boost::python::list&) =		&Server::addAdministrationIPAddress;

        boost::python::class_<Server, SharedPointer<Server>, boost::noncopyable >("Server",
                "Class that manages the wallets.")
                .def(init<>())
                .add_property("address", &Server::getIPAddress, &Server::setIPAddress,
                        help_server_address)
                .add_property("port", &Server::getPort, &Server::setPort,
                        help_server_port)
                .add_property("administration_address", &Server::getAdministrationIPAddress, &Server::setAdministrationIPAddress,
                        help_server_adm_address)
                .add_property("administration_port", &Server::getAdministrationPort, &Server::setAdministrationPort,
                        help_server_adm_port)
                .add_property("session_wallet_manager", &Server::getSessionWalletManager, &Server::setSessionWalletManager,
                        help_server_swm)
                .add_property("enable_shell", &Server::getShell, &Server::setShell,
                       	help_server_shell) 
                .add_property("mtls", &Server::getMTLS, &Server::setMTLS,
                       	help_server_mtls) 
                .add_property("private_key", &Server::getPrivateKey, &Server::setPrivateKey,
                       	help_server_priv_key) 
                .add_property("certificate", &Server::getCertificate, &Server::setCertificate,
                       	help_server_cert) 
                .add_property("certificate_authority", &Server::getCertificate, &Server::setCertificateAuthority,
                       	help_server_ca) 
		.add_property("administration_ip_addresses", &Server::getAdministrationIPAddresses, addAdministrationIPs,
		        help_server_adm_ip_addresses)	
		.add_property("security_callback", &Server::getSecurityCallback, &Server::addSecurityCallback,
			help_server_sec_callback)
                .add_property("private_key_password", &Server::getPrivateKeyPassword, &Server::setPrivateKeyPassword,
                        help_server_private_key_pass)
                .add_property("proxy_support", &Server::getProxySupport, &Server::setProxySupport,
                        help_server_proxy_support)
                .add_property("authorization_token", &Server::getAuthorizationToken, &Server::setAuthorizationToken,
                        "TODO")
                .def("start", &Server::start,
                        help_server_start)
                .def("run", &Server::run,
                        help_server_run)
                .def("stop", &Server::stop,
                        help_server_stop)
                .def("show", &Server::show,
                        help_server_show)
                .def("show_connections", &Server::showConnections,
                        help_server_show_conns)
                .def("add_timer", &Server::addTimer,
                        help_server_add_timer)
		.def("show_administration_ips", &Server::showAdministrationIPAddresses,
			"todo")
		.def("show_connection", &Server::showConnection,
			help_server_show_conn)
		.def("close_connection", &Server::closeConnection,
			help_server_close_conn)
                .def(self_ns::str(self_ns::self))
	;

        boost::python::class_<PetalusBlock, SharedPointer<PetalusBlock>, boost::noncopyable>("PetalusBlock")
                .def(init<>())
                .def_readonly("length", &PetalusBlock::length,
                        help_wallet_length)
                .def_readonly("signed", &PetalusBlock::have_signature,
                        help_wallet_signed)
                .add_property("data", &PetalusBlock::getData, &PetalusBlock::setData,
                        help_wallet_data)
                .add_property("accept", &PetalusBlock::getAccept, &PetalusBlock::setAccept,
                        help_wallet_data)
                .def(self_ns::str(self_ns::self))
        ;

	boost::python::class_<HTTPUserSession, SharedPointer<HTTPUserSession>, boost::noncopyable>("HTTPUserSession", no_init)
		.def_readonly("ip", &HTTPUserSession::getIPAddress,
			help_httpcon_ip_address)
		.def_readonly("http_errors", &HTTPUserSession::getHTTPErrors,
			help_httpcon_http_errors)
		.def_readonly("http_gets", &HTTPUserSession::getHTTPMethodGets,
			help_httpcon_http_gets)
		.def_readonly("http_posts", &HTTPUserSession::getHTTPMethodPosts,
			help_httpcon_http_posts)
		.def_readonly("http_unknowns", &HTTPUserSession::getHTTPMethodUnknowns,
			help_httpcon_http_unknowns)
		.def_readonly("http_no_sessions", &HTTPUserSession::getHTTPSessionsNotFound,
			help_httpcon_no_sessions)
		.def_readonly("http_wrong_wallet_uris", &HTTPUserSession::getHTTPWrongWalletUris,
			help_httpcon_wrong_uri_wallet)
		.def_readonly("http_body_exceeds", &HTTPUserSession::getHTTPBodyLimitExceeds,
			help_httpcon_body_exceeds)
		.def("close", &HTTPUserSession::close_connection,
			help_httpcon_close)
	;
}
