#!/usr/bin/python
# File that generates a rpm spec file with the options that has been compiled
#
import sys
import os.path

def write_description_section(fd):

    lines = [
        "%description\n",
        "%{summary}\n"
        "\n"
    ]
    fd.writelines(lines)

def write_clean_section(fd):

    lines = [
        "%clean\n",
        "rm -r -f \"$RPM_BUILD_ROOT\"\n",
        "\n"
    ]
    fd.writelines(lines)

def write_header_section(fd, version):
    
    lines = [ 
        "Summary: Petalus build system\n",
        "Name: petalus\n",
        "Version: %s\n" % version,
        "Release: 1\n",
    #Group: Foo/Bar
        "License: GPL\n",
        "Source: %{expand:%%(pwd)}\n",
        "BuildRoot: %{_topdir}/BUILD/%{name}-%{version}-%{release}\n",
        "\n"
    ]

    fd.writelines(lines)

def write_prep_section(fd):

    lines = [ 
        "%prep\n",
        "rm -rf $RPM_BUILD_ROOT\n"
        "mkdir -p $RPM_BUILD_ROOT/usr/bin\n",
        "mkdir -p $RPM_BUILD_ROOT/usr/lib64\n",
        "mkdir -p $RPM_BUILD_ROOT/etc\n"
        "cd $RPM_BUILD_ROOT\n",
        "cp %{SOURCEURL0}/../src/pserver ./usr/bin/pserver\n",
        "cp %{SOURCEURL0}/../src/pgen ./usr/bin/pgen\n",
        "cp %{SOURCEURL0}/../src/breader ./usr/bin/breader\n",
        "cp %{SOURCEURL0}/../src/pserver.conf ./etc/pserver.conf\n",
        "\n"
    ]

    """ Verify if the python wrappers exists """
    libname = "petalus.so"
    if (os.path.isfile("../src/" + libname)):
        lines.append("cp %{SOURCEURL0}/../src/" + libname + " ./usr/lib64/" + libname + "\n")

    libname = "petalus.cpython-36m-x86_64-linux-gnu.so"
    if (os.path.isfile("../src/" + libname)):
        lines.append("cp %{SOURCEURL0}/../src/" + libname + " ./usr/lib64/" + libname + "\n")

    lines.append("\n")
    fd.writelines(lines)


def write_file_section(fd):

    lines = [ "%files\n",
        "%defattr(644,root,root)\n",
        "%defattr(755,root,root)\n",
        "%{_bindir}/pserver\n",
        "%{_bindir}/breader\n",
        "%{_bindir}/pgen\n",
    ]

    """ Verify if the python wrappers exists """
    libname = "petalus.so"
    if (os.path.isfile("../src/" + libname)):
        lines.append("%{_libdir}/" + libname + "\n")

    libname = "petalus.cpython-36m-x86_64-linux-gnu.so"
    if (os.path.isfile("../src/" + libname)):
        lines.append("%{_libdir}/" + libname + "\n")

    lines.append("\n")
    fd.writelines(lines)


if __name__ == '__main__':



    #if (len(options.hostname) == 0):
    #    print("The parameter 'hostname' is mandatory")
    #    option_parser().print_help()
    #    sys.exit(-1)

    version = ""
    with open("../config.h") as fd:
        for line in fd.readlines():
            if (line.startswith("#define VERSION")):
                version = line.replace("\"", "").split(" ")[2].strip()


    if (len(version) == 0):
        print("Incorrect version tag")
        sys.exit(-1)

    filename = "petalus." + version + ".spec"

    with open(filename, "w") as fd:
        write_header_section(fd, version)
        write_description_section(fd)
        write_prep_section(fd)
        write_clean_section(fd)
        write_file_section(fd)
