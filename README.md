Petalus 
=======

Petalus is a crypto wallet microservice that allows users to store any type of information on a virtual wallet.

The main functionalities of Petalus are:

- Blockchain support on the storage data.
- Multiple hash for the blockchain (sha256, blake2s and sha3-256).
- Multiple process execution (Multiple process with the same data running).
- Support for read/write triggers on the wallets.
- Autorization of write blocks with public/private key.

Using Petalus
-------------

For a complete description of the class methods in Python 

    import petalus
    help(petalus)

Check the directory examples in order to have usefull use cases, and check /docs for documentation

Compile Petalus
---------------

The next libraries are need for compile and verification.

- libboost-all
- cryptopp (openssl)
- python-cryptography
- python-ecdsa

the boost libraries and cryptopp are mandatory libraries.

    $ git clone https://bitbucket.com/camp0/petalus
    $ ./autogen.sh
    $ ./configure
    $ make
    $ make python
