#!/usr/bin/python
__author__ = "Luis Campo Giralte"
__copyright__ = "Copyright 2018"
__version__ = "1.0"
__maintainer__ = "Luis Campo Giralte"
__email__ = "Luis@pepe.com"
__status__ = "Beta"

import optparse
import hashlib
import sys, time, copy
from random import randint
import wallet

if __name__ == '__main__':

    w = wallet.Wallet("Bert", "LaMar", 44, "france123456789")

    print("Use case 02")
    w.hostname = "127.0.0.1:5555"

    raw_input("Press return to create a wallet")
    w.create()
    raw_input("Press return to open the wallet")
    w.open()
    text = raw_input("Write some text to be written on the wallet:")
    w.write(text)
    a = raw_input("Enter the block number for read from the wallet:")
    cad = w.read(int(a))
    print(cad)
    raw_input("Press return to close the wallet")
    w.close()

    sys.exit(0)

