#!/usr/bin/python
__author__ = "Luis Campo Giralte"
__copyright__ = "Copyright 2018"
__version__ = "1.0"
__maintainer__ = "Luis Campo Giralte"
__email__ = "Luis@pepe.com"
__status__ = "Beta"

import os
import optparse
import chomsky
import hashlib
import sys, time, copy
from random import randint
#from multiprocessing.dummy import Pool as ThreadPool 
#import threading
import wallet
from multiprocessing import Process

layer = "http"
hostname = "127.0.0.1:5555"
admin_hostname = "127.0.0.1:5556"

names = [ "Romain", "Craig", "John", "Stan", "Robert", "Martin" ,"Tony", "Alice" , "Luke", "Donal"]
surnames = [ "Smith", "Stanibrook", "Garcia", "Redwood", "O'sullivan", "Miller", "McCarty", "Rodriguez", "Skywalker", "Thrum" ]
passwords = [ "XXXX", "aaaaaa", "Garcia", "Redwood", "O'sullivan", "Miller" , "a", "a", "a", "a"]
user_folders = [ "claims", "notes", "ids" ]
wallets = dict()

def create_wallets(number, random_wallets):

    w = dict()

    for i in xrange(0, number):
        if (random_wallets == True):
            nidx = randint(0, len(names) - 1)    
            sidx = randint(0, len(names) - 1)    
            pidx = randint(0, len(names) - 1)    
    
            name = names[nidx] + str(os.getpid())
            surname = surnames[sidx]
            password = passwords[pidx]
            age = randint(18, 50)
        else:
            name = names[i]
            surname = surnames[i]
            password = passwords[i]
            age = 34

        w[i] = wallet.Wallet(name, surname, age, password, folders = user_folders, layer = layer, debug = 0, is_proxy = options.is_proxy ) 
        w[i].hostname = hostname
        w[i].admin_hostname = admin_hostname

    return w


def option_parser():

    def get_comma_separated_args(option, opt, value, parser):
        setattr(parser.values, option.dest, value.split(','))

    p = optparse.OptionParser()

    p.add_option("-i","--hostname", dest="hostname", default=hostname, type="string",
        help="Specify the hostname (default %default).")

    p.add_option("-w", "--wallets", dest="wallets", default=1, type="int",
        help="Number of wallets (default %default).")

    p.add_option("-b", "--blocks", dest="blocks", default=1, type="int",
        help="Number of blocks written by wallet (default %default).")

    p.add_option("-s", "--sleep", dest="sleep", default=0.0, type="float",
        help="Sleep randomly between post/get operations (default %default).")

    p.add_option("-I", "--index", dest="wallet_index", default=-1, type="int",
        help="Index of wallet for use the same user for the post/get operations.")

    p.add_option("-r", "--doreads", dest="doreads", default=False, action="store_true",
        help="Enable random read operations (default %default).")

    p.add_option("-d", "--showreads", dest="showreads", default=False, action="store_true",
        help="Shows the read operations (default %default).")

    p.add_option("-f", "--fork", dest="forks", default=1, type="int",
        help="Launch process (%default).")

    p.add_option("-S", "--sign", dest="sign", default=False, action="store_true",
        help="Signs the message with ECDSA (default %default).")

    p.add_option("-R", "--random", dest="random", default=True, action="store_false",
        help="Generates random wallets for the execution (default %default).")

    p.add_option("-B", "--bytes", dest="bytes", default=4096, type = "int",
        help="Number of Chomsky lines that will be generated for writing. (default %default).")

    p.add_option("-p", "--proxy", dest="is_proxy", default=False, action="store_true",
        help="Generates a Forwarded HTTP header with a random IP address (default %default).")

    return p


def print_response(res):
    print('HTTP/1.1 {status_code}\n{headers}\n\n{body}'.format(
        status_code=res.status_code,
        headers='\n'.join('{}: {}'.format(k, v) for k, v in res.headers.items()),
        body=res.content,
    ))


def do_work(options):
    global wallets

    if (options.wallet_index > -1):
        name = names[options.wallet_index]
        surname = surnames[options.wallet_index]
        password = passwords[options.wallet_index]
        age = 45

        wallets[0] = wallet.Wallet(name, surname, age, password, folders = user_folders )
        wallets[0].hostname = hostname
   
    else:
        wallets = create_wallets(options.wallets, options.random)

    total_post = 0
    sessions = {}
    for k, v in wallets.iteritems():
        v.create()

    for k, v in wallets.iteritems():
        v.open()

    for i in xrange(0, options.blocks * options.wallets):
        wi = randint(0, len(wallets) -1)
        wf = randint(0, len(user_folders) + 5)
        wa = wallets[wi]
        if (wa.authenticated == True):
            folder = ""
            if (wf < len(user_folders)):
                folder = user_folders[wf]

            if (options.sign == True):
                r = wa.write_and_sign(chomsky.chomsky(options.bytes), folder)
            else:
                r = wa.write(chomsky.chomsky(options.bytes), folder)

            if (r.status_code == 503)or(r.status_code == 500):
                """ The server is unavailable right now """
                print("Not available the server")
                time.sleep(5)
                wa.open()

            time.sleep(randint(0, options.sleep))

            total_post += 1

            if (options.doreads == True)and(randint(0, 3) == 0): # do a read of the wallet
                wf = randint(0, len(user_folders) + 5)
                folder = ""
                if (wf < len(user_folders)):
                    folder = user_folders[wf]

                bnumber = randint(1, wa.total_write_blocks)
                code, data = wa.read(bnumber, folder)
                if (options.showreads)and(code == 200):
                    print("READ block %d (%s):%s" % (bnumber, folder, data))
                time.sleep(options.sleep)

    total_write_blocks = 0
    total_time_writes = 0
    total_read_blocks = 0
    total_time_reads = 0
    print("Closing %d wallets" % len(wallets))
    for k, w in wallets.iteritems():
        w.close()
        total_write_blocks += w.total_write_blocks
        total_time_writes += w.total_time_writes
        total_read_blocks += w.total_read_blocks
        total_time_reads += w.total_time_reads
        time.sleep(options.sleep)

    print("Total write operations:%d" % total_write_blocks)
    if (total_write_blocks > 0):
        print("Average write time:%s" % (total_time_writes / total_write_blocks))
    print("Total read operations:%d" % total_read_blocks)
    if (total_read_blocks > 0):
        print("Average read time:%s" % (total_time_reads / total_read_blocks))
   
    sys.exit(0)

if __name__ == '__main__':


    (options, args) = option_parser().parse_args()

    if (len(options.hostname) == 0):
        print("The parameter 'hostname' is mandatory")
        option_parser().print_help()
        sys.exit(-1)

    procs = []
    try:

        for i in xrange(0, options.forks):
            p = Process(target = do_work, args = (options,))
            p.daemon = True
            p.start()
            procs.append(p)

        print("Parent waiting")
        for p in procs:
            p.join()
        print("Parent exiting")

    except KeyboardInterrupt as e:
        print("Stoping process")
        for p in procs:
            p.terminate()

    sys.exit(0)

