#!/usr/bin/python
__author__ = "Luis Campo Giralte"
__copyright__ = "Copyright 2019"
__version__ = "1.0"
__maintainer__ = "Luis Campo Giralte"
__email__ = "Luis@pepe.com"
__status__ = "Beta"

import optparse
import hashlib
import sys, time, copy
from random import randint
import wallet

hostname = "127.0.0.1:5555"

if __name__ == '__main__':

    w = wallet.Wallet("Craig", "Kelly", 47, "vermont1234", debug = 1, layer = "http")

    print("Use case 01")
    w.hostname = "127.0.0.1:5555"
    w.admin_hostname = "127.0.0.1:5556"
    raw_input("Press return to create a wallet")
    w.create()
    raw_input("Press return to open the wallet")
    w.open()
    text = raw_input("Write some text to be written on the wallet:")
    w.write(text)
    raw_input("Press return to close the wallet")
    w.close()

    sys.exit(0)

