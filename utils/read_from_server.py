#!/usr/bin/python
__author__ = "Luis Campo Giralte"
__copyright__ = "Copyright 2018"
__version__ = "1.0"
__maintainer__ = "Luis Campo Giralte"
__email__ = "Luis@pepe.com"
__status__ = "Beta"

import os
import optparse
import chomsky
import hashlib
import sys, time, copy
from random import randint
import pycurl
from io import BytesIO
from StringIO import StringIO
import json

hostname = "127.0.0.1:5555"

def option_parser():

    def get_comma_separated_args(option, opt, value, parser):
        setattr(parser.values, option.dest, value.split(','))

    p = optparse.OptionParser()

    p.add_option("-i","--hostname", dest="hostname", default=hostname, type="string",
        help="Specify the hostname (default %default).")

    p.add_option("-w", "--wallet", dest="wallet", default="", type="string",
        help="Wallet that will be read.")

    p.add_option("-b", "--block", dest="block", default=1, type="int",
        help="Block that will be read (default %default).")

    p.add_option("-t", "--token", dest="token", default="YWFhYWFhYWFhYWFhYWFhYQ==", type="string",
        help="Authorized token (default %default).")


    return p

def curl_request(url, headers, data, iface):
    c = pycurl.Curl()
    buffer = BytesIO()
    rheaders = StringIO()
    c.setopt(pycurl.URL, url)
    if (data != None):
        c.setopt(pycurl.POST, True)
        c.setopt(pycurl.POSTFIELDS, data)

    h = list()
    headers["Content-Type"] = "application/json"
    for k, v in headers.items():
        h.append("%s: %s" % (k, v))

    c.setopt(pycurl.HEADERFUNCTION, rheaders.write)
    c.setopt(pycurl.HTTPHEADER, h)
    c.setopt(pycurl.TIMEOUT, 10)
    c.setopt(pycurl.WRITEFUNCTION, buffer.write)
    c.setopt(pycurl.INTERFACE, iface)
    # c.setopt(pycurl.VERBOSE, 1)
    c.perform()

    id = ""
    h = rheaders.getvalue()
    if "Petalus-SessionId" in rheaders.getvalue():
        a = h.split("Petalus-SessionId:")
        id = a[1].lstrip().rstrip()

    # Json response
    resp = buffer.getvalue().decode('UTF-8')
    code = c.getinfo(pycurl.HTTP_CODE)

    #  Check response is a JSON if not there was an error
    try:
        resp = json.loads(resp)
    except ValueError:
        pass

    buffer.close()
    c.close()
    return code, resp, id



if __name__ == '__main__':


    (options, args) = option_parser().parse_args()

    if (len(options.wallet) == 0):
        print("The parameter 'wallet' is mandatory")
        option_parser().print_help()
        sys.exit(-1)

    uri = "http://" + hostname + "/petalus/wallet/v1/open"
    open_data = {
        "wallet": options.wallet
    }

    headers = { "Authorization" : "Basic %s" % options.token }

    code, text, id = curl_request(uri, headers, json.dumps(open_data), "virbr0")
    if (code != 200):
        print(text)
        print("Can not open the wallet %s" % options.wallet)
        sys.exit(-2)

    sid = id
    while True:
        a = raw_input("Enter the block number you want to read (q for quit):")
        if a == "q":
            break

        uri = "http://" + hostname + "/petalus/block/v1/" + options.wallet + "/" + a
        headers["Petalus-SessionId"] = sid

        code, text, id = curl_request(uri, headers, None, "virbr0")
        print(text)

    sys.exit(0)

