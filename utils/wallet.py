#!/usr/bin/python
__author__ = "Luis Campo Giralte"
__copyright__ = "Copyright 2018"
__version__ = "1.0"
__maintainer__ = "Luis Campo Giralte"
__email__ = "luis@lehc.es"
__status__ = "Beta"

import socket
import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
import json
import hashlib
import sys, os
import datetime
import urllib3
import ecdsa
import base64
import logging
import time
from random import randint

try:
    import http.client as http_client
except ImportError:
    # Python 2
    import httplib as http_client
http_client.HTTPConnection.debuglevel = 0

if (http_client.HTTPConnection.debuglevel > 0):
    # You must initialize logging, otherwise you'll not see debug output.
    logging.basicConfig()
    logging.getLogger().setLevel(logging.DEBUG)
    requests_log = logging.getLogger("requests.packages.urllib3")
    requests_log.setLevel(logging.DEBUG)
    requests_log.propagate = True

urllib3.disable_warnings()

verify_ssl = False
petalus_session_identifier = "Petalus-SessionId"
petalus_user_signature = "Petalus-User-Signature"
http_uri_create_wallet = "/v1/create"
http_uri_open_wallet = "/v1/open"
http_uri_close_wallet = "/v1/close"
http_uri_block_wallet = "/v1/block/"

class EcdsaKey (object):
    def __init__(self):
        # SECP256k1 is the Bitcoin elliptic curve
        self.__sk = ecdsa.SigningKey.generate(curve=ecdsa.SECP256k1)
        self.__vk = self.__sk.get_verifying_key()
        self.__pubkey = self.__vk.to_der()

    def sign(self, data):
        return self.__sk.sign(data, hashfunc=hashlib.sha256)

    def public_key(self):
        return base64.b64encode(self.__pubkey)

    def write_private_key(self):
        with open("private.key", "w") as fd:
            fd.write(self.__sk.to_pem())

    def load_private_key(self, filename):
        k = open(filename, "r").read()
        self.__sk = ecdsa.SigningKey.from_pem(k)
        self.__vk = self.__sk.get_verifying_key()
        self.__pubkey = self.__vk.to_der()

class Wallet (object):
    def __init__(self, name, surname, age, password, folders = [], debug = 1, key = EcdsaKey(), layer = "https", is_proxy = False):
        self.__name = name
        self.__surname = surname
        self.__age = age
        self.__password = password
        self.__session_id = ""
        self.__folders = folders
        self.__debug = debug
        self.__authenticated = False
        self.__k = key
        self.__layer = layer
        self.__is_proxy = is_proxy
        if (is_proxy):
            self.__forwarded_value = "%d.%d.%d.%d" % (randint(1, 254), randint(1, 254), randint(1, 254), randint(1, 254)) 

        self.total_write_blocks = 0
        self.total_time_writes = 0
        self.total_read_blocks = 0
        self.total_time_reads = 0

    def __write(self, uri, data, headers):
        if (type(data) is str):
            r = self.__session.post(uri, headers= headers, data=data, verify = verify_ssl)
        elif (type(data) is dict):
            r = self.__session.post(uri, headers= headers, json=data, verify = verify_ssl)
        self.total_write_blocks += 1
        return r

    def create(self, extra_data = {}):
        uri = self.__layer + "://" + self.__admin_hostname + http_uri_create_wallet
        data = { "name": self.__name, 
            "surname": self.__surname, 
            "age": self.__age, 
            # "date": str(datetime.datetime.now()), 
            "password": hashlib.sha256(self.__password).hexdigest().upper() }
        
        data.update(extra_data)

        cad = json.dumps(data)
        self.__wallet_id = hashlib.sha256(cad).hexdigest().upper()

        data["wallet"] = self.__wallet_id
        # data["public_key"] = self.__k.public_key() 
        if (len(self.__folders) > 0):
            data["folders"] = self.__folders 

        if (self.__debug > 0):
            now = datetime.datetime.now().strftime("%H:%M:%S")
            print("[%s] %s" % (now, uri))

        headers = {
            "Authorization" : "Basic YWFhYWFhYWFhYWFhYWFhYQ==",
            "Connection" : "close"
        }
        r = requests.post(uri, json=data, verify = verify_ssl, headers = headers)
        if (int(r.status_code) == 200):
            print("Wallet %s has been created" % self.__wallet_id, str(r.text))

    def open(self):
        uri = self.__layer + "://" + self.__hostname + http_uri_open_wallet
        self.__session = requests.Session()
        data = { 
            "wallet": self.__wallet_id, 
            "password": hashlib.sha256(self.__password).hexdigest().upper() }

        if (self.__debug > 0):
            now = datetime.datetime.now().strftime("%H:%M:%S")
            print("[%s] %s" % (now, uri))

        headers = {}
        if (self.__is_proxy):
            headers["Forwarded"] =  self.__forwarded_value 

        r = self.__session.post(uri, json=data, verify = verify_ssl, headers = headers)
        if (petalus_session_identifier in r.headers):
            self.__session_id = r.headers[petalus_session_identifier]
            print("Open wallet %s with Session:%s" % (self.__wallet_id, self.__session_id))
            self.__authenticated = True
            return True
        else: 
            print("Wallet %s can not be open" % self.__wallet_id)
            return False

    def close(self):
        uri = self.__layer + "://" + self.__hostname + http_uri_close_wallet

        if (self.__debug > 0):
            now = datetime.datetime.now().strftime("%H:%M:%S")
            print("[%s] %s" % (now, uri))

        headers = { petalus_session_identifier : self.__session_id, 'Connection':'close'}
        r = self.__session.post(uri, headers=headers)

    def write_and_sign(self, data, folder = ""):
        start = time.time()
        uri = self.__layer + "://" + self.__hostname + http_uri_block_wallet + self.__wallet_id

        if (len(folder) > 0):
            uri = uri + "/" + folder

        if (self.__debug > 0):
            now = datetime.datetime.now().strftime("%H:%M:%S")
            print("[%s] POST %s" % (now, uri))

        """ sha256 the data received """
        hdata = hashlib.sha256(data).digest();

        """ sign the data """
        s = self.__k.sign(hdata)

        """ base64 the generated signature """
        sb = base64.b64encode(s)

        headers = { 
            petalus_session_identifier : self.__session_id, 
            petalus_user_signature: sb,
            "Content-Type" : "text/plain"
         }

        r = self.__write(uri, data, headers)
        self.total_time_writes += (time.time() - start)
        return r

    def write(self, data, folder = ""):
        start = time.time()
        uri = self.__layer + "://" + self.__hostname + http_uri_block_wallet + self.__wallet_id

        if (len(folder) > 0):
            uri = uri + "/" + folder

        if (self.__debug > 0):
            now = datetime.datetime.now().strftime("%H:%M:%S")
            print("[%s] POST %s" % (now, uri))

        headers = { 
            petalus_session_identifier : self.__session_id,
            "Content-Type" : "text/plain"
        }
        r = self.__write(uri, data, headers)
        self.total_time_writes += (time.time() - start)
        return r

    def write_file(self, filename):
        uri = self.__layer + "://" + self.__hostname + http_uri_block_wallet + self.__wallet_id
        headers = { 
            petalus_session_identifier : self.__session_id, 
            'Content-type': 'application/octet-stream'
        }
        data = open(filename, 'rb').read()

        if (self.__debug > 0):
            now = datetime.datetime.now().strftime("%H:%M:%S")
            print("[%s] %s" % (now, uri))

        r = self.__write(uri, data, headers)
        return r

    def write_and_sign_file(self, filename):
        data = open(filename, 'rb').read()

        uri = self.__layer + "://" + self.__hostname + http_uri_block_wallet + self.__wallet_id

        if (self.__debug > 0):
            now = datetime.datetime.now().strftime("%H:%M:%S")
            print("[%s] POST %s" % (now, uri))

        """ sha256 the data received """
        hdata = hashlib.sha256(data).digest();

        """ sign the data """
        s = self.__k.sign(hdata)

        """ base64 the generated signature """
        sb = base64.b64encode(s)

        headers = { petalus_session_identifier : self.__session_id, petalus_user_signature: sb,
            'Content-type': 'application/octet-stream' }
        r = self.__write(uri, data, headers)
        return r

    def read(self, block, folder = ""):
        start = time.time()
        self.total_read_blocks += 1
        uri = self.__layer + "://" + self.__hostname + http_uri_block_wallet + self.__wallet_id + "/"
     
        if (len(folder) > 0):
            uri = uri + folder + "/"

        uri = uri + str(block)

        if (self.__debug > 0):
            now = datetime.datetime.now().strftime("%H:%M:%S")
            print("[%s] GET %s" % (now, uri))

        headers = { petalus_session_identifier : self.__session_id }
        r = self.__session.get(uri, headers=headers, verify = verify_ssl)
        ct = r.headers.get('content-type')
        data = ""
        if (ct == "application/json"):
            data = r.json()
        elif (ct == "application/octet-stream"):
            filename = "block_%d_%d.dat" % (block, os.getpid())
            f = open(filename, "w")
            f.write(r.content)
            f.close()
            data = "File generated %s" % filename
        else:
            data = r.text
        self.total_time_reads += (time.time() - start)
        return r.status_code, data

    @property
    def hostname(self):
        return self.__hostname

    @hostname.setter
    def hostname(self,value):
        self.__hostname = value

    @property
    def admin_hostname(self):
        return self.__admin_hostname

    @admin_hostname.setter
    def admin_hostname(self,value):
        self.__admin_hostname = value

    @property
    def authenticated(self):
        return self.__authenticated

