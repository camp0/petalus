#!/usr/bin/python
__author__ = "Luis Campo Giralte"
__copyright__ = "Copyright 2018"
__version__ = "1.0"
__maintainer__ = "Luis Campo Giralte"
__email__ = "Luis@pepe.com"
__status__ = "Beta"

import optparse
import hashlib
import sys, time, copy
from random import randint
import wallet

if __name__ == '__main__':

    w = wallet.Wallet("James Marshall", "Hendrix", 64, "accesspassword", debug = 1)

    w.hostname = "127.0.0.1:5555"
    raw_input('Press return to create a wallet')
    w.create(extra_data = { "Nationality" : "USA" })
    raw_input('Press return to open the wallet')
    w.open()
    text = raw_input('Write on the wallet signed')
    w.write_and_sign(text)
    a = raw_input('Get the block number for retrieve')
    cad = w.read(int(a))
    print(cad)
    raw_input('Press return to close the wallet')
    w.close()

    sys.exit(0)

