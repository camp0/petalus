#!/usr/bin/python
__author__ = "Luis Campo Giralte"
__copyright__ = "Copyright 2018"
__version__ = "1.0"
__maintainer__ = "Luis Campo Giralte"
__email__ = "Luis@pepe.com"
__status__ = "Beta"
""" Check the status of a petalus instance and show the sessions """

import requests
import os
import optparse
import sys, time, copy

verify_ssl = False
token = "YWFhYWFhYWFhYWFhYWFhYQ=="
layer = "http"
hostname = "127.0.0.1:5556"
headers = {
    "Authorization" : "Basic %s" % token,
    "Accept" : "application/json"
}

def option_parser():

    def get_comma_separated_args(option, opt, value, parser):
        setattr(parser.values, option.dest, value.split(','))

    p = optparse.OptionParser()

    p.add_option("-i","--hostname", dest="hostname", default=hostname, type="string",
        help="Specify the hostname (default %default).")

    p.add_option("-s", "--session", dest="session_id", default=None,
        help="Session id")

    p.add_option("-f", "--folder", dest="folder", default="",
        help="Folder")

    p.add_option("-b", "--block", dest="block", default=0,
        help="Block")

    p.add_option("-S", "--statistics", dest="statistics", default=None,
        help="Session id")

    return p


def show_status(con):

    uri = layer + "://" + hostname + "/petalus/status"
    r = con.get(uri, verify = verify_ssl, headers = headers)
    j = r.json()
    print("{0:<16s} {1}".format("System",j["engine"]))
    print("{0:<16s} {1}".format("Pid",j["pid"]))
    print("{0:<16s} {1}".format("Reads",j["reads"]))
    print("{0:<16s} {1}".format("Writes",j["writes"]))
    print("{0:<16s} {1}".format("Uptime",j["uptime"]))


def show_sessions(con):

    uri = layer + "://" + hostname + "/petalus/sessions"
    r = con.get(uri, verify = verify_ssl, headers = headers)
    j = r.json()
    if j:
        print("Current open sesssions")
        template = "{0:38}{1:<68}{2:<16}" # column widths: 8, 10, 15, 7, 10
        print(template.format("Session", "Wallet", "Blocks"))

        for s in j:
            items = (s["sid"], s["wallet"], s["blocks"])
            print(template.format(*items))


def show_session(con, sid, folder, block):

    uri = layer + "://" + hostname + "/petalus/session/" + sid

    if (len(folder) > 0):
        uri += "/" + folder

    if (block > 0):
        uri += "/" + str(block)

    r = con.get(uri, verify = verify_ssl, headers = headers)
    j = r.json()

    if (j):
        print("{0:<16s} {1}".format("Wallet",j["name"]))
        print("{0:<16s} {1}".format("Bytes",j["bytes"]))
        print("{0:<16s} {1}".format("Blocks",j["blocks"]))
        print("{0:<16s} {1}".format("Path",j["path"]))

        data = ""
        if ("data" in j):
            for i in j["data"]:
                data += str(unichr(i))

            print(data)

if __name__ == '__main__':


    (options, args) = option_parser().parse_args()

    if (len(options.hostname) == 0):
        print("The parameter 'hostname' is mandatory")
        option_parser().print_help()
        sys.exit(-1)

    con = requests.Session()

    show_status(con)
    if (options.session_id):
        show_session(con, options.session_id, options.folder, options.block)
    else:
        show_sessions(con)

    import time
    time.sleep(60)
    con.close()
    sys.exit(0)

