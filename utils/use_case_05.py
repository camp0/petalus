#!/usr/bin/python
__author__ = "Luis Campo Giralte"
__copyright__ = "Copyright 2018"
__version__ = "1.0"
__maintainer__ = "Luis Campo Giralte"
__email__ = "Luis@pepe.com"
__status__ = "Beta"

import optparse
import hashlib
import sys, time, copy
from random import randint
import wallet

data = { "value1": 1, 
    "value2": [ 1, 2, 3, 4, 5 ],
    "value3": "Some string" }

if __name__ == '__main__':

    w = wallet.Wallet("Nicolas", "Muller", 34, "verBiEr1234", folders = ["notes", "bills"] )

    w.hostname = "127.0.0.1:5555"

    raw_input("Press return to create a wallet")
    w.create()
    raw_input("Press return to open the wallet")
    w.open()
    f = raw_input("Type the name of the folder for writing [notes, bills]:")
    a = raw_input("Type Some text for write on the folder '%s':" % f)
    w.write(a, folder = f)
    a = raw_input("Enter the block number for read from the folder '%s'" % f)
    cad = w.read(int(a), folder = f)
    print(cad)
    raw_input('Press return to close the wallet')
    w.close()

    sys.exit(0)

