In this section we are going to benchmark one instance and see how many write operations is able to achive 
on the blockchain.

The information of the blockchain is encrypted by default.


As basic value Virtual CPU Intel Core Processor 2.7 MHZ single instance supports 1000 write transactions per second
consuming 12GB of memory for support 200 wallets with TLS suppport and encryption.


Test I
------

We will use the utility python send_to_server.py -w 20 -b 10000 -f 10

where 20 wallets, each wallet will make 10000 writes with an average of 4000 bytes each transaction.


- The first setup the instance will be pure HTTP 
- the second setup will be with HTTPS
- Third setup HTTPS + Signed blocks

+----------+--------------+--------+
| setup    | Transactions |  CPU   |
+==========+==============+========+
| 1        |    923 sec   |  11%   |
+----------+--------------+--------+
| 2        |    807 sec   |  14%   |
+----------+--------------+--------+
| 3        |    54 sec    |  13%   |
+----------+--------------+--------+
